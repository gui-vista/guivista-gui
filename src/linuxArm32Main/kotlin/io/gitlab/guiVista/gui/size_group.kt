package io.gitlab.guiVista.gui

import glib2.FALSE
import glib2.TRUE
import glib2.g_object_unref
import gtk3.*
import kotlinx.cinterop.CPointer
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.dataType.SinglyLinkedList
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual class SizeGroup private constructor(
    ptr: CPointer<GtkSizeGroup>? = null,
    mode: GtkSizeGroupMode = GtkSizeGroupMode.GTK_SIZE_GROUP_NONE
) : ObjectBase {
    public val gtkSizeGroupPtr: CPointer<GtkSizeGroup>? = ptr ?: gtk_size_group_new(mode)

    /**
     * The directions in which the size group affects the requested sizes of its component widgets. Default value is
     * *GtkSizeGroupMode.GTK_SIZE_GROUP_HORIZONTAL*
     *
     * Data binding property name: **mode**
     */
    public var mode: GtkSizeGroupMode
        get() = gtk_size_group_get_mode(gtkSizeGroupPtr)
        set(value) = gtk_size_group_set_mode(gtkSizeGroupPtr, value)
    public actual var ignoreHidden: Boolean
        get() = gtk_size_group_get_ignore_hidden(gtkSizeGroupPtr) == TRUE
        set(value) = gtk_size_group_set_ignore_hidden(gtkSizeGroupPtr, if (value) TRUE else FALSE)
    public actual val widgets: SinglyLinkedList
        get() = SinglyLinkedList(gtk_size_group_get_widgets(gtkSizeGroupPtr))

    public companion object {
        public fun create(mode: GtkSizeGroupMode = GtkSizeGroupMode.GTK_SIZE_GROUP_NONE): SizeGroup =
            SizeGroup(mode = mode)

        public fun fromPointer(ptr: CPointer<GtkSizeGroup>?): SizeGroup = SizeGroup(ptr = ptr)
    }

    public actual operator fun plusAssign(widget: WidgetBase) {
        addWidget(widget)
    }

    public actual infix fun addWidget(widget: WidgetBase) {
        gtk_size_group_add_widget(gtkSizeGroupPtr, widget.gtkWidgetPtr)
    }

    public actual operator fun minusAssign(widget: WidgetBase) {
        removeWidget(widget)
    }

    public actual infix fun removeWidget(widget: WidgetBase) {
        gtk_size_group_remove_widget(gtkSizeGroupPtr, widget.gtkWidgetPtr)
    }

    override fun close() {
        g_object_unref(gtkSizeGroupPtr)
    }
}

public fun createSizeGroup(
    ptr: CPointer<GtkSizeGroup>? = null,
    mode: GtkSizeGroupMode = GtkSizeGroupMode.GTK_SIZE_GROUP_NONE,
    init: SizeGroup.() -> Unit = {}
): SizeGroup {
    val sizeGroup = if (ptr != null) SizeGroup.fromPointer(ptr) else SizeGroup.create(mode)
    sizeGroup.init()
    return sizeGroup
}

public fun CPointer<GtkSizeGroup>?.toSizeGroup(): SizeGroup = SizeGroup.fromPointer(this)
