package io.gitlab.guiVista.gui.event

import gtk3.GdkEvent
import gtk3.GdkEventButton
import gtk3.gdk_event_free
import gtk3.gdk_event_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.value

public actual class ButtonEvent private constructor(ptr: CPointer<GdkEventButton>?) : EventBase {
    public val gdkEventButtonPtr: CPointer<GdkEventButton>? = ptr
    override val gdkEventPtr: CPointer<GdkEvent>?
        get() = gdkEventButtonPtr?.reinterpret()

    public actual val sendEvent: Boolean
        get() = gdkEventButtonPtr?.pointed?.send_event == 1.toByte()

    public actual val time: UInt
        get() = gdkEventButtonPtr?.pointed?.time ?: 0u

    public actual val xPos: Double
        get() = gdkEventButtonPtr?.pointed?.x ?: 0.0

    public actual val yPos: Double
        get() = gdkEventButtonPtr?.pointed?.y ?: 0.0

    public actual val axes: Double?
        get() = gdkEventButtonPtr?.pointed?.axes?.pointed?.value

    public actual val state: UInt
        get() = gdkEventButtonPtr?.pointed?.state ?: 0u

    public actual val button: UInt
        get() = gdkEventButtonPtr?.pointed?.button ?: 0u

    public actual val xPosRoot: Double
        get() = gdkEventButtonPtr?.pointed?.x_root ?: 0.0

    public actual val yPosRoot: Double
        get() = gdkEventButtonPtr?.pointed?.y_root ?: 0.0

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GdkEventButton>?): ButtonEvent = ButtonEvent(ptr)

        public actual fun create(type: Int): ButtonEvent = ButtonEvent(gdk_event_new(type)?.reinterpret())
    }
}

public fun CPointer<GdkEventButton>?.toButtonEvent(): ButtonEvent = ButtonEvent.fromPointer(this)
