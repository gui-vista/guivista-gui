package io.gitlab.guiVista.gui.event

import gtk3.GdkEvent
import gtk3.gdk_event_free
import gtk3.gdk_event_new
import kotlinx.cinterop.CPointer

public actual class Event private constructor(ptr: CPointer<GdkEvent>?) : EventBase {
    override val gdkEventPtr: CPointer<GdkEvent>? = ptr

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GdkEvent>?): Event = Event(ptr)

        public actual fun create(type: Int): Event = Event(gdk_event_new(type))
    }
}
