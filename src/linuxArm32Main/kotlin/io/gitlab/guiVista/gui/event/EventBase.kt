package io.gitlab.guiVista.gui.event

import gtk3.GdkEvent
import gtk3.gdk_event_free
import io.gitlab.guiVista.core.Closable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed

public actual interface EventBase : Closable {
    public val gdkEventPtr: CPointer<GdkEvent>?
    public actual val type: Int
        get() = gdkEventPtr?.pointed?.type ?: -1

    override fun close() {
        if (gdkEventPtr != null) gdk_event_free(gdkEventPtr)
    }
}
