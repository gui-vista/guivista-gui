package io.gitlab.guiVista.gui.event

import gtk3.*
import kotlinx.cinterop.*

public actual class ScrollEvent private constructor(ptr: CPointer<GdkEventScroll>?) : EventBase {
    public val gdkEventScrollPtr: CPointer<GdkEventScroll>? = ptr

    override val gdkEventPtr: CPointer<GdkEvent>?
        get() = gdkEventScrollPtr?.reinterpret()

    public actual val scrollDeltas: Pair<Double, Double>
        get() = memScoped {
            val deltaX = alloc<DoubleVar>()
            val deltaY = alloc<DoubleVar>()
            gdk_event_get_scroll_deltas(event = gdkEventPtr, delta_x = deltaX.ptr, delta_y = deltaY.ptr)
            deltaX.value to deltaY.value
        }

    public actual val sendEvent: Boolean
        get() = gdkEventScrollPtr?.pointed?.send_event == 1.toByte()

    public actual val time: UInt
        get() = gdkEventScrollPtr?.pointed?.time ?: 0u

    public actual val xPos: Double
        get() = gdkEventScrollPtr?.pointed?.x ?: 0.0

    public actual val yPos: Double
        get() = gdkEventScrollPtr?.pointed?.y ?: 0.0

    public actual val state: UInt
        get() = gdkEventScrollPtr?.pointed?.state ?: 0u

    public actual val xPosRoot: Double
        get() = gdkEventScrollPtr?.pointed?.x_root ?: 0.0

    public actual val yPosRoot: Double
        get() = gdkEventScrollPtr?.pointed?.y_root ?: 0.0

    public actual val deltaXPos: Double
        get() = gdkEventScrollPtr?.pointed?.delta_x ?: 0.0

    public actual val deltaYPos: Double
        get() = gdkEventScrollPtr?.pointed?.delta_y ?: 0.0

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GdkEventScroll>?): ScrollEvent = ScrollEvent(ptr)

        public actual fun create(type: Int): ScrollEvent = ScrollEvent(gdk_event_new(type)?.reinterpret())
    }
}

public fun CPointer<GdkEventScroll>?.toScrollEvent(): ScrollEvent = ScrollEvent.fromPointer(this)
