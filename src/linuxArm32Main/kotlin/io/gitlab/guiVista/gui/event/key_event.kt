package io.gitlab.guiVista.gui.event

import gtk3.GdkEvent
import gtk3.GdkEventKey
import gtk3.gdk_event_free
import gtk3.gdk_event_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.reinterpret

public actual class KeyEvent private constructor(ptr: CPointer<GdkEventKey>?) : EventBase {
    public val gdkEventKeyPtr: CPointer<GdkEventKey>? = ptr

    override val gdkEventPtr: CPointer<GdkEvent>?
        get() = gdkEventKeyPtr?.reinterpret()

    public actual val sendEvent: Boolean
        get() = gdkEventKeyPtr?.pointed?.send_event == 1.toByte()

    public actual val time: UInt
        get() = gdkEventKeyPtr?.pointed?.time ?: 0u

    public actual val state: UInt
        get() = gdkEventKeyPtr?.pointed?.state ?: 0u

    public actual val keyValue: UInt
        get() = gdkEventKeyPtr?.pointed?.keyval ?: 0u

    public actual val hardwareKeyCode: UShort
        get() = gdkEventKeyPtr?.pointed?.hardware_keycode ?: 0u.toUShort()

    public actual val group: UByte
        get() = gdkEventKeyPtr?.pointed?.group ?: 0u.toUByte()

    public actual val isModifier: Boolean
        get() = gdkEventKeyPtr?.pointed?.is_modifier == 1u

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GdkEventKey>?): KeyEvent = KeyEvent(ptr)

        public actual fun create(type: Int): KeyEvent = KeyEvent(gdk_event_new(type)?.reinterpret())
    }
}

public fun CPointer<GdkEventKey>?.toKeyEvent(): KeyEvent = KeyEvent.fromPointer(this)
