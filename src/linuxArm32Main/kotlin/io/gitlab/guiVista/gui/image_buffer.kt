package io.gitlab.guiVista.gui

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.dataType.SinglyLinkedList
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.*

public actual class ImageBuffer private constructor(ptr: CPointer<GdkPixbuf>?) : ObjectBase {
    public val gdkPixbufPtr: CPointer<GdkPixbuf>? = ptr
    public actual val totalChannels: Int
        get() = gdk_pixbuf_get_n_channels(gdkPixbufPtr)
    public actual val hasAlpha: Boolean
        get() = gdk_pixbuf_get_has_alpha(gdkPixbufPtr) == TRUE
    public actual val bitsPerSample: Int
        get() = gdk_pixbuf_get_bits_per_sample(gdkPixbufPtr)
    public actual val width: Int
        get() = gdk_pixbuf_get_width(gdkPixbufPtr)
    public actual val height: Int
        get() = gdk_pixbuf_get_height(gdkPixbufPtr)
    public actual val rowStride: Int
        get() = gdk_pixbuf_get_rowstride(gdkPixbufPtr)
    public actual val byteLength: ULong
        get() = gdk_pixbuf_get_byte_length(gdkPixbufPtr).toULong()

    public actual fun fetchPixels(): Pair<UInt, UByte> = memScoped {
        val length = alloc<UIntVar>()
        val pixels = gdk_pixbuf_get_pixels_with_length(gdkPixbufPtr, length.ptr)
        length.value to (pixels?.pointed?.value ?: 0.toUByte())
    }

    public actual fun fetchOption(key: String): String = gdk_pixbuf_get_option(gdkPixbufPtr, key)?.toKString() ?: ""

    public actual fun changeOption(key: String, value: String): Boolean =
        gdk_pixbuf_set_option(pixbuf = gdkPixbufPtr, key = key, value = value) == TRUE

    public actual fun readPixels(): UByte = gdk_pixbuf_read_pixels(gdkPixbufPtr)?.pointed?.value ?: 0.toUByte()

    override fun close() {
        gdk_pixbuf_unref(gdkPixbufPtr)
    }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GdkPixbuf>?): ImageBuffer = ImageBuffer(ptr)

        public actual fun fromFile(fileName: String, error: Error?): ImageBuffer? {
            val ptr = gdk_pixbuf_new_from_file(fileName, error = cValuesOf(error?.gErrorPtr))
            return if (ptr != null) fromPointer(ptr) else null
        }

        public actual fun fromFileAtSize(fileName: String, width: Int, height: Int, error: Error?): ImageBuffer? {
            val ptr = gdk_pixbuf_new_from_file_at_size(
                filename = fileName,
                width = width,
                height = height,
                error = cValuesOf(error?.gErrorPtr)
            )
            return if (ptr != null) fromPointer(ptr) else null
        }

        public actual fun fromFileAtScale(
            fileName: String,
            width: Int,
            height: Int,
            preserveAspectRatio: Boolean,
            error: Error?
        ): ImageBuffer? {
            val ptr = gdk_pixbuf_new_from_file_at_scale(
                filename = fileName,
                width = width,
                height = height,
                preserve_aspect_ratio = if (preserveAspectRatio) TRUE else FALSE,
                error = cValuesOf(error?.gErrorPtr)
            )
            return if (ptr != null) fromPointer(ptr) else null
        }

        public actual fun fromResource(resPath: String, error: Error?): ImageBuffer? {
            val ptr = gdk_pixbuf_new_from_resource(resource_path = resPath, error = cValuesOf(error?.gErrorPtr))
            return if (ptr != null) fromPointer(ptr) else null
        }

        public actual fun fromResourceAtScale(
            resPath: String,
            width: Int,
            height: Int,
            preserveAspectRatio: Boolean,
            error: Error?
        ): ImageBuffer? {
            val ptr = gdk_pixbuf_new_from_resource_at_scale(
                resource_path = resPath,
                width = width,
                height = height,
                preserve_aspect_ratio = if (preserveAspectRatio) TRUE else FALSE,
                error = cValuesOf(error?.gErrorPtr)
            )
            return if (ptr != null) fromPointer(ptr) else null
        }

        /**
         * Creates a new [ImageBuffer] and allocates a buffer for it. The buffer has an optimal rowStride. Note that
         * the buffer is not cleared; you will have to fill it completely yourself.
         * @param colorSpace Color space for the image.
         * @param hasAlpha Whether the image should have transparency information.
         * @param bitsPerSample Number of bits per color sample.
         * @param width Width of image in pixels. Must be > *0*
         * @param height Height of image in pixels. Must be > *0*
         * @return A newly created [ImageBuffer] with a reference count of *1*, or *null* if not enough memory could
         * be allocated for the image buffer.
         */
        public fun create(
            colorSpace: GdkColorspace,
            hasAlpha: Boolean,
            bitsPerSample: Int,
            width: Int,
            height: Int
        ): ImageBuffer? {
            val ptr = gdk_pixbuf_new(
                colorspace = colorSpace,
                has_alpha = if (hasAlpha) TRUE else FALSE,
                width = width,
                height = height,
                bits_per_sample = bitsPerSample
            )
            return if (ptr != null) fromPointer(ptr) else null
        }

        /**
         * Creates a new [ImageBuffer] out of in-memory image data. Currently only RGB images with 8 bits per sample
         * are supported. Since you are providing a pre-allocated pixel buffer, you **MUST** also specify a way to
         * free that data. This is done with a function of type `GdkPixbufDestroyNotify`. When a [ImageBuffer] created
         * with is finalized, your destroy notification function will be called, and it is its responsibility to free
         * the pixel array.
         * @param data Image data in 8-bit/sample packed format.
         * @param colorSpace Color space for the image data.
         * @param hasAlpha Whether the data has an opacity channel.
         * @param bitsPerSample Number of bits per sample,
         * @param width Width of the image in pixels, must be > *0*.
         * @param height Height of the image in pixels, must be > *0*.
         * @param rowStride Distance in bytes between row starts.
         * @param destroyFunc The function used to free the data when the image buffers reference count drops to zero,
         * or *null* if the data should not be freed.
         * @param userData The user data to pass to [destroyFunc].
         * @return A new [ImageBuffer] with a reference count of *1*.
         */
        public fun fromData(
            data: UByteArray,
            colorSpace: GdkColorspace,
            hasAlpha: Boolean,
            bitsPerSample: Int,
            width: Int,
            height: Int,
            rowStride: Int,
            destroyFunc: GdkPixbufDestroyNotify,
            userData: gpointer = fetchEmptyDataPointer()
        ): ImageBuffer {
            val ptr = gdk_pixbuf_new_from_data(
                width = width,
                height = height,
                bits_per_sample = bitsPerSample,
                rowstride = rowStride,
                has_alpha = if (hasAlpha) TRUE else FALSE,
                destroy_fn = destroyFunc,
                destroy_fn_data = userData,
                colorspace = colorSpace,
                data = data.toCValues()
            )
            return fromPointer(ptr)
        }

        public actual fun fromXpmData(data: ByteArray): ImageBuffer = memScoped {
            val array = allocArray<CPointerVar<ByteVar>>(data.size)
            @Suppress("ReplaceRangeToWithUntil")
            (0..(data.size - 1)).forEach { pos -> array[pos] = data.refTo(pos).getPointer(this) }
            fromPointer(gdk_pixbuf_new_from_xpm_data(array))
        }
    }

    public actual fun saveToFile(
        fileName: String,
        type: String,
        options: Map<String, String>,
        error: Error?
    ): Boolean {
        val (keys, values) = options.createByteArrays()
        val keysPinned = keys.pin()
        val valuesPinned = values.pin()
        val result = gdk_pixbuf_savev(
            pixbuf = gdkPixbufPtr,
            filename = fileName,
            type = type,
            error = cValuesOf(error?.gErrorPtr),
            option_keys = cValuesOf(keysPinned.addressOf(0)),
            option_values = cValuesOf(valuesPinned.addressOf(0))
        ) == TRUE
        keysPinned.unpin()
        valuesPinned.unpin()
        return result
    }

    /**
     * Saves [ImageBuffer] in format [type] (currently **jpeg**, **png**, **tiff**, **ico**, or **bmp**) by feeding the
     * produced data to a [callback][saveFunc]. Can be used when you want to store the image to something other than a
     * file, such as an in-memory buffer or a socket. If [error] is set then *false* will be returned. Possible errors
     * include those in the `GDK_PIXBUF_ERROR` domain, and whatever the [save function][saveFunc] generates.
     * @param options The options to set.
     * @param type Name of file format.
     * @param saveFunc A function that is called to save each block of data that the save function generates.
     * @param userData The user data to pass to [saveFunc].
     * @param error The error to use or *null*.
     */
    public fun saveToCallback(
        options: Map<String, String>,
        type: String,
        saveFunc: GdkPixbufSaveFunc,
        userData: gpointer = fetchEmptyDataPointer(),
        error: Error? = null
    ): Boolean {
        val (keys, values) = options.createByteArrays()
        val keysPinned = keys.pin()
        val valuesPinned = values.pin()
        val result = gdk_pixbuf_save_to_callbackv(
            pixbuf = gdkPixbufPtr,
            type = type,
            error = cValuesOf(error?.gErrorPtr),
            option_keys = cValuesOf(keysPinned.addressOf(0)),
            option_values = cValuesOf(valuesPinned.addressOf(0)),
            save_func = saveFunc,
            user_data = userData
        ) == TRUE
        keysPinned.unpin()
        valuesPinned.unpin()
        return result
    }

    public actual fun saveToBuffer(
        type: String,
        options: Map<String, String>,
        bufferSize: Int,
        error: Error?
    ): Pair<ByteArray, Boolean> {
        val buffer = ByteArray(bufferSize)
        val (keys, values) = options.createByteArrays()
        var errorSet = false
        val keysPinned = keys.pin()
        val valuesPinned = values.pin()
        buffer.usePinned { bufferPinned ->
            errorSet = gdk_pixbuf_save_to_bufferv(
                pixbuf = gdkPixbufPtr,
                type = type,
                error = cValuesOf(error?.gErrorPtr),
                option_keys = cValuesOf(keysPinned.addressOf(0)),
                option_values = cValuesOf(valuesPinned.addressOf(0)),
                buffer_size = null,
                buffer = cValuesOf(bufferPinned.addressOf(0))
            ) == TRUE
        }
        keysPinned.unpin()
        valuesPinned.unpin()
        return buffer to errorSet
    }

    public actual fun fetchFileInfo(fileName: String): Triple<Int, Int, ImageBufferFormat?> = memScoped {
        val width = alloc<IntVar>()
        val height = alloc<IntVar>()
        val ptr = gdk_pixbuf_get_file_info(filename = fileName, width = width.ptr, height = height.ptr)
        Triple(width.value, height.value, ImageBufferFormat.fromPointer(ptr))
    }
}

public actual val imageBufferFormats: SinglyLinkedList =
    SinglyLinkedList.fromPointer(gdk_pixbuf_get_formats())

public fun CPointer<GdkPixbuf>?.toImageBuffer(): ImageBuffer = ImageBuffer.fromPointer(this)
