package io.gitlab.guiVista.gui

import gtk3.GdkRGBA
import io.gitlab.guiVista.core.Closable
import kotlinx.cinterop.*

public actual class RgbaColor private constructor(
    ptr: CPointer<GdkRGBA>? = null,
    red: Double = 0.0,
    green: Double = 0.0,
    blue: Double = 0.0,
    alpha: Double = 0.0
) : Closable {
    private val arena = Arena()
    public val gdkRgbaPtr: CPointer<GdkRGBA> = ptr
        ?: createPointer(red = red, green = green, blue = blue, alpha = alpha)
    public actual var red: Double
        get() = gdkRgbaPtr.pointed.red
        set(value) {
            gdkRgbaPtr.pointed.red = value
        }
    public actual var green: Double
        get() = gdkRgbaPtr.pointed.green
        set(value) {
            gdkRgbaPtr.pointed.green = value
        }
    public actual var blue: Double
        get() = gdkRgbaPtr.pointed.blue
        set(value) {
            gdkRgbaPtr.pointed.blue = value
        }
    public actual var alpha: Double
        get() = gdkRgbaPtr.pointed.alpha
        set(value) {
            gdkRgbaPtr.pointed.alpha = value
        }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GdkRGBA>?): RgbaColor = RgbaColor(ptr = ptr)

        public actual fun create(red: Double, green: Double, blue: Double, alpha: Double): RgbaColor =
            RgbaColor(red = red, green = green, blue = blue, alpha = alpha)
    }

    private fun createPointer(red: Double, green: Double, blue: Double, alpha: Double): CPointer<GdkRGBA> {
        val tmp = arena.alloc<GdkRGBA>()
        tmp.red = red
        tmp.green = green
        tmp.blue = blue
        tmp.alpha = alpha
        return tmp.ptr
    }

    override fun close() {
        arena.clear()
    }
}

public fun CPointer<GdkRGBA>?.toRgbaColor(): RgbaColor = RgbaColor.fromPointer(this)
