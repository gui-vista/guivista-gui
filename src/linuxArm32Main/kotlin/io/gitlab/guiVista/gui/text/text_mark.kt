package io.gitlab.guiVista.gui.text

import glib2.FALSE
import glib2.TRUE
import glib2.g_object_unref
import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import io.gitlab.guiVista.core.ObjectBase

public actual class TextMark private constructor(
    ptr: CPointer<GtkTextMark>? = null,
    name: String = "",
    leftGravity: Boolean = false
) : ObjectBase {
    public val gtkTextMarkPtr: CPointer<GtkTextMark>? =
        ptr ?: gtk_text_mark_new(name.ifEmpty { null }, if (leftGravity) TRUE else FALSE)

    public actual var visible: Boolean
        get() = gtk_text_mark_get_visible(gtkTextMarkPtr) == TRUE
        set(value) = gtk_text_mark_set_visible(gtkTextMarkPtr, if (value) TRUE else FALSE)

    public actual val deleted: Boolean
        get() = gtk_text_mark_get_deleted(gtkTextMarkPtr) == TRUE

    public actual val name: String
        get() = gtk_text_mark_get_name(gtkTextMarkPtr)?.toKString() ?: ""

    public actual val buffer: TextBuffer?
        get() {
            val ptr = gtk_text_mark_get_buffer(gtkTextMarkPtr)
            return if (ptr != null) TextBuffer(ptr) else null
        }

    public actual val leftGravity: Boolean
        get() = gtk_text_mark_get_left_gravity(gtkTextMarkPtr) == TRUE

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkTextMark>?): TextMark = TextMark(ptr = ptr)

        public actual fun create(name: String, leftGravity: Boolean): TextMark =
            TextMark(name = name, leftGravity = leftGravity)
    }

    override fun close() {
        g_object_unref(gtkTextMarkPtr)
    }
}

public fun CPointer<GtkTextMark>?.toTextMark(): TextMark = TextMark.fromPointer(this)
