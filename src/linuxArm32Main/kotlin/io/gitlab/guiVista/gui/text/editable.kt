package io.gitlab.guiVista.gui.text

import glib2.FALSE
import glib2.TRUE
import glib2.g_free
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.*

public actual interface Editable : ObjectBase {
    public val gtkEditablePtr: CPointer<GtkEditable>?
    public actual var pos: Int
        get() = gtk_editable_get_position(gtkEditablePtr)
        set(value) = gtk_editable_set_position(gtkEditablePtr, value)
    public actual var isEditable: Boolean
        get() = gtk_editable_get_editable(gtkEditablePtr) == TRUE
        set(value) = gtk_editable_set_editable(gtkEditablePtr, if (value) TRUE else FALSE)

    public actual fun selectRegion(startPos: Int, endPos: Int) {
        gtk_editable_select_region(editable = gtkEditablePtr, start_pos = startPos, end_pos = endPos)
    }

    public actual fun fetchSelectionBounds(): Triple<Int?, Int?, Boolean> = memScoped {
        val startPos = alloc<IntVar>()
        val endPos = alloc<IntVar>()
        val tmp = gtk_editable_get_selection_bounds(editable = gtkEditablePtr, start_pos = startPos.ptr,
            end_pos = endPos.ptr)
        Triple(startPos.value, endPos.value, tmp == TRUE)
    }

    public actual fun insertText(newText: String, length: Int, pos: Int): Unit = memScoped {
        val tmpPos = alloc<IntVar>()
        tmpPos.value = pos
        gtk_editable_insert_text(editable = gtkEditablePtr, new_text = newText, new_text_length = length,
            position = tmpPos.ptr)
    }

    public actual fun deleteText(startPos: Int, endPos: Int) {
        gtk_editable_delete_text(editable = gtkEditablePtr, start_pos = startPos, end_pos = endPos)
    }

    public actual fun fetchCharacters(startPos: Int, endPos: Int): String {
        val tmp = gtk_editable_get_chars(editable = gtkEditablePtr, start_pos = startPos, end_pos = endPos)
        val result = tmp?.toKString() ?: ""
        g_free(tmp)
        return result
    }

    public actual fun cutClipboard() {
        gtk_editable_cut_clipboard(gtkEditablePtr)
    }

    public actual fun copyClipboard() {
        gtk_editable_copy_clipboard(gtkEditablePtr)
    }

    public actual fun pasteClipboard() {
        gtk_editable_paste_clipboard(gtkEditablePtr)
    }

    public actual fun deleteSelection() {
        gtk_editable_delete_selection(gtkEditablePtr)
    }

    /**
     * Connects the *changed* event to a [handler] on an [Editable]. This event is used when at the end of a
     * single user-visible operation on the contents of the [Editable]. For example a paste operation that replaces the
     * contents of the selection will cause only one event to be fired (even though it is implemented by first deleting
     * the selection, then inserting the new content, and may cause multiple `::notify::text` events to be fired).
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectChangedEvent(handler: CPointer<EditableChangedHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkEditablePtr, signal = EditableEvent.changed, slot = handler, data = userData)

    /**
     * Connects the *delete-text* event to a [handler] on an [Editable]. This event is used when text is deleted from
     * the widget by the user. The default handler for this event will normally be responsible for deleting the text,
     * so by connecting to this event and then stopping the signal with `g_signal_stop_emission()`, it is possible to
     * modify the range of deleted text, or prevent it from being deleted entirely. The `startPos` and `endPos`
     * parameters are interpreted as for [deleteText].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectDeleteTextEvent(handler: CPointer<EditableDeleteTextHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkEditablePtr, signal = EditableEvent.deleteText, slot = handler, data = userData)

    /**
     * Connects the *insert-text* event to a [handler] on an [Editable]. This event is used when text is inserted into
     * the widget by the user. The default handler for this event will normally be responsible for inserting the text,
     * so by connecting to this event and then stopping the event with `g_signal_stop_emission()`, it is possible to
     * modify the inserted text, or prevent it from being inserted entirely.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectInsertTextEvent(handler: CPointer<EditableInsertTextHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkEditablePtr, signal = EditableEvent.insertText, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkEditablePtr, handlerId)
    }
}

/**
 * The event handler for the *changed* event. Arguments:
 * 1. editable: CPointer<GtkEditable>
 * 2. userData: gpointer
 */
public typealias EditableChangedHandler = CFunction<(editable: CPointer<GtkEditable>, userData: gpointer) -> Unit>

/**
 * The event handler for the *delete-text* event. Arguments:
 * 1. editable: CPointer<GtkEditable>
 * 2. startPos: Int
 * 3. endPos: Int
 * 4. userData: gpointer
 */
public typealias EditableDeleteTextHandler = CFunction<(
    editable: CPointer<GtkEditable>,
    startPos: Int,
    endPos: Int,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *insert-text* event. Arguments:
 * 1. editable: CPointer<GtkEditable>
 * 2. newText: CPointer<ByteVar> (represents a String)
 * 3. length: Int
 * 4. pos: CPointer<IntVar>
 * 5. userData: gpointer
 */
public typealias EditableInsertTextHandler = CFunction<(
    editable: CPointer<GtkEditable>,
    newText: CPointer<ByteVar>,
    length: Int,
    pos: CPointer<IntVar>,
    userData: gpointer
) -> Unit>
