package io.gitlab.guiVista.gui.tree

import glib2.TRUE
import glib2.g_object_unref
import gtk3.*
import io.gitlab.guiVista.core.ValueBase
import kotlinx.cinterop.*

public actual class TreeStore private constructor(
    ptr: CPointer<GtkTreeStore>? = null,
    types: ULongArray = ULongArray(0)
) : TreeModelBase {
    public val gtkTreeStorePtr: CPointer<GtkTreeStore>? = ptr ?: createGtkTreeStorePtr(types)
    override val gtkTreeModelPtr: CPointer<GtkTreeModel>?
        get() = gtkTreeStorePtr?.reinterpret()

    private fun createGtkTreeStorePtr(types: ULongArray): CPointer<GtkTreeStore>? {
        val tmp = UIntArray(types.size)
        types.map { it.toUInt() }.forEachIndexed { pos, item -> tmp[pos] = item }
        return gtk_tree_store_newv(types.size, tmp.toCValues())
    }

    override fun close() {
        g_object_unref(gtkTreeStorePtr)
    }

    public actual companion object {
        public actual fun create(vararg types: ULong): TreeStore = TreeStore(types = types)

        public fun fromPointer(ptr: CPointer<GtkTreeStore>?): TreeStore = TreeStore(ptr = ptr)
    }

    public actual fun changeStringValue(iter: TreeModelIterator, column: Int, value: String) {
        gtk_tree_store_set(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeUIntValue(iter: TreeModelIterator, column: Int, value: UInt) {
        gtk_tree_store_set(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeIntValue(iter: TreeModelIterator, column: Int, value: Int) {
        gtk_tree_store_set(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeULongValue(iter: TreeModelIterator, column: Int, value: ULong) {
        gtk_tree_store_set(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeLongValue(iter: TreeModelIterator, column: Int, value: Long) {
        gtk_tree_store_set(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeFloatValue(iter: TreeModelIterator, column: Int, value: Float) {
        gtk_tree_store_set(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeDoubleValue(iter: TreeModelIterator, column: Int, value: Double) {
        gtk_tree_store_set(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeUShortValue(iter: TreeModelIterator, column: Int, value: UShort) {
        gtk_tree_store_set(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeShortValue(iter: TreeModelIterator, column: Int, value: Short) {
        gtk_tree_store_set(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeValue(iter: TreeModelIterator, column: Int, value: ValueBase) {
        gtk_tree_store_set_value(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr, column = column,
            value = value.gValuePtr)
    }

    public actual fun changeMultipleValues(iter: TreeModelIterator, columns: IntArray, values: Array<ValueBase>) {
        @Suppress("ReplaceRangeToWithUntil")
        (0..(columns.size - 1)).forEach { changeValue(iter = iter, column = columns[it], value = values[it]) }
    }

    public actual fun remove(iter: TreeModelIterator): Boolean =
        gtk_tree_store_remove(gtkTreeStorePtr, iter.gtkTreeIterPtr) == TRUE

    public actual fun insert(iter: TreeModelIterator, parent: TreeModelIterator?, pos: Int) {
        gtk_tree_store_insert(
            tree_store = gtkTreeStorePtr,
            iter = iter.gtkTreeIterPtr,
            parent = parent?.gtkTreeIterPtr,
            position = pos
        )
    }

    public actual fun insertBefore(iter: TreeModelIterator, parent: TreeModelIterator?, sibling: TreeModelIterator?) {
        gtk_tree_store_insert_before(
            tree_store = gtkTreeStorePtr,
            iter = iter.gtkTreeIterPtr,
            parent = parent?.gtkTreeIterPtr,
            sibling = sibling?.gtkTreeIterPtr
        )
    }

    public actual fun insertAfter(iter: TreeModelIterator, parent: TreeModelIterator?, sibling: TreeModelIterator?) {
        gtk_tree_store_insert_after(
            tree_store = gtkTreeStorePtr,
            parent = parent?.gtkTreeIterPtr,
            sibling = sibling?.gtkTreeIterPtr,
            iter = iter.gtkTreeIterPtr
        )
    }

    public actual fun insertWithValues(
        iter: TreeModelIterator,
        parent: TreeModelIterator?,
        pos: Int,
        columns: IntArray,
        values: Array<ValueBase>
    ) {
        insert(iter = iter, pos = pos, parent = parent)
        changeMultipleValues(iter = iter, columns = columns, values = values)
    }

    public actual fun prepend(iter: TreeModelIterator, parent: TreeModelIterator?) {
        gtk_tree_store_prepend(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr,
            parent = parent?.gtkTreeIterPtr)
    }

    public actual fun append(iter: TreeModelIterator, parent: TreeModelIterator?) {
        gtk_tree_store_append(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr,
            parent = parent?.gtkTreeIterPtr)
    }

    public actual fun clear() {
        gtk_tree_store_clear(gtkTreeStorePtr)
    }

    public actual fun isAncestor(iter: TreeModelIterator, descendant: TreeModelIterator): Boolean =
        gtk_tree_store_is_ancestor(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr,
            descendant = descendant.gtkTreeIterPtr) == TRUE

    public actual fun iteratorDepth(iter: TreeModelIterator): Int =
        gtk_tree_store_iter_depth(gtkTreeStorePtr, iter.gtkTreeIterPtr)

    public actual fun reorder(parent: TreeModelIterator?, newOrder: IntArray) {
        newOrder.usePinned { pin ->
            gtk_tree_store_reorder(tree_store = gtkTreeStorePtr, parent = parent?.gtkTreeIterPtr,
                new_order = pin.addressOf(0))
        }
    }

    public actual fun swap(iterA: TreeModelIterator, iterB: TreeModelIterator) {
        gtk_tree_store_swap(tree_store = gtkTreeStorePtr, a = iterA.gtkTreeIterPtr, b = iterB.gtkTreeIterPtr)
    }

    public actual fun moveBefore(iter: TreeModelIterator, pos: TreeModelIterator?) {
        gtk_tree_store_move_before(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr,
            position = pos?.gtkTreeIterPtr)
    }

    public actual fun moveAfter(iter: TreeModelIterator, pos: TreeModelIterator?) {
        gtk_tree_store_move_after(tree_store = gtkTreeStorePtr, iter = iter.gtkTreeIterPtr,
            position = pos?.gtkTreeIterPtr)
    }
}

public fun CPointer<GtkTreeStore>?.toTreeStore(): TreeStore = TreeStore.fromPointer(this)
