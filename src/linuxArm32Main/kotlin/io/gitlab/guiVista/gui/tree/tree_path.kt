package io.gitlab.guiVista.gui.tree

import glib2.TRUE
import gtk3.*
import io.gitlab.guiVista.core.Closable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.get

public actual class TreePath private constructor(ptr: CPointer<GtkTreePath>? = null, path: String = "") : Closable {
    public val gtkTreePathPtr: CPointer<GtkTreePath>? = when {
        ptr != null -> ptr
        path.isNotEmpty() -> gtk_tree_path_new_from_string(path)
        else -> gtk_tree_path_new()
    }
    public actual val depth: Int
        get() = gtk_tree_path_get_depth(gtkTreePathPtr)

    public actual val indices: Array<Int>
        get() = fetchIndices()

    override fun close() {
        gtk_tree_path_free(gtkTreePathPtr)
    }

    private fun fetchIndices(): Array<Int> {
        val tmpList = mutableListOf<Int>()
        val tmpIndices = gtk_tree_path_get_indices(gtkTreePathPtr)
        @Suppress("ReplaceRangeToWithUntil")
        (0..(depth - 1)).forEach { pos ->
            val item = tmpIndices?.get(pos)
            if (item != null) tmpList += item
        }
        return tmpList.toTypedArray()
    }

    public actual companion object {
        public actual fun create(): TreePath = TreePath()

        public actual fun fromString(path: String): TreePath? {
            val tmp = TreePath(path = path)
            return if (tmp.gtkTreePathPtr == null) null else tmp
        }

        public fun fromPointer(ptr: CPointer<GtkTreePath>?): TreePath = TreePath(ptr = ptr)
    }

    public actual infix fun appendIndex(index: Int) {
        gtk_tree_path_append_index(gtkTreePathPtr, index)
    }

    public actual infix fun prependIndex(index: Int) {
        gtk_tree_path_prepend_index(gtkTreePathPtr, index)
    }

    public actual fun copy(): TreePath = TreePath(ptr = gtk_tree_path_copy(gtkTreePathPtr))

    public actual fun compareTo(otherPath: TreePath): Int =
        gtk_tree_path_compare(gtkTreePathPtr, otherPath.gtkTreePathPtr)

    public actual fun next() {
        gtk_tree_path_next(gtkTreePathPtr)
    }

    public actual fun previous(): Boolean = gtk_tree_path_prev(gtkTreePathPtr) == TRUE

    public actual fun up(): Boolean = gtk_tree_path_up(gtkTreePathPtr) == TRUE

    public actual fun down() {
        gtk_tree_path_down(gtkTreePathPtr)
    }

    public actual fun isAncestor(descendant: TreePath): Boolean =
        gtk_tree_path_is_ancestor(gtkTreePathPtr, descendant.gtkTreePathPtr) == TRUE

    public actual fun isDescendant(ancestor: TreePath): Boolean =
        gtk_tree_path_is_descendant(gtkTreePathPtr, ancestor.gtkTreePathPtr) == TRUE
}

public fun CPointer<GtkTreePath>?.toTreePath(): TreePath = TreePath.fromPointer(this)
