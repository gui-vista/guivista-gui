package io.gitlab.guiVista.gui.tree

import glib2.TRUE
import glib2.g_free
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.*
import kotlinx.cinterop.*

public actual interface TreeModelBase : ObjectBase {
    public val gtkTreeModelPtr: CPointer<GtkTreeModel>?
    public val flags: GtkTreeModelFlags
        get() = gtk_tree_model_get_flags(gtkTreeModelPtr)
    public actual val totalColumns: Int
        get() = gtk_tree_model_get_n_columns(gtkTreeModelPtr)

    public actual fun fetchColumnType(index: Int): ULong =
        gtk_tree_model_get_column_type(gtkTreeModelPtr, index).toULong()

    public actual fun fetchIterator(path: TreePath): Pair<Boolean, TreeModelIterator> {
        val iter = TreeModelIterator.create()
        val isSet = gtk_tree_model_get_iter(
            tree_model = gtkTreeModelPtr,
            iter = iter.gtkTreeIterPtr,
            path = path.gtkTreePathPtr
        ) == TRUE
        return isSet to iter
    }

    public actual fun fetchIteratorFromString(pathStr: String): Pair<Boolean, TreeModelIterator> {
        val iter = TreeModelIterator.create()
        val isSet = gtk_tree_model_get_iter_from_string(
            tree_model = gtkTreeModelPtr,
            iter = iter.gtkTreeIterPtr,
            path_string = pathStr
        ) == TRUE
        return isSet to iter
    }

    public actual fun fetchFirstIterator(): Pair<Boolean, TreeModelIterator> {
        val iter = TreeModelIterator.create()
        val isSet = gtk_tree_model_get_iter_first(gtkTreeModelPtr, iter.gtkTreeIterPtr) == TRUE
        return isSet to iter
    }

    public actual fun fetchPath(iter: TreeModelIterator): TreePath =
        TreePath.fromPointer(gtk_tree_model_get_path(gtkTreeModelPtr, iter.gtkTreeIterPtr))

    public actual fun fetchValue(iter: TreeModelIterator, col: Int, valueType: ULong): ValueBase = memScoped {
        val result = Value.create(valueType)
        gtk_tree_model_get_value(
            tree_model = gtkTreeModelPtr,
            iter = iter.gtkTreeIterPtr,
            column = col,
            value = result.gValuePtr
        )
        return result
    }

    public actual fun fetchStringValue(iter: TreeModelIterator, col: Int): String = memScoped {
        val str = alloc<ByteVar>()
        gtk_tree_model_get(tree_model = gtkTreeModelPtr, iter = iter.gtkTreeIterPtr, col, str.ptr, -1)
        str.ptr.toKString()
    }

    public actual fun fetchUIntValue(iter: TreeModelIterator, col: Int): UInt = memScoped {
        val value = alloc<UIntVar>()
        gtk_tree_model_get(tree_model = gtkTreeModelPtr, iter = iter.gtkTreeIterPtr, col, value.ptr, -1)
        value.value
    }

    public actual fun fetchIntValue(iter: TreeModelIterator, col: Int): Int = memScoped {
        val value = alloc<IntVar>()
        gtk_tree_model_get(tree_model = gtkTreeModelPtr, iter = iter.gtkTreeIterPtr, col, value.ptr, -1)
        value.value
    }

    public actual fun fetchULongValue(iter: TreeModelIterator, col: Int): ULong = memScoped {
        val value = alloc<ULongVar>()
        gtk_tree_model_get(tree_model = gtkTreeModelPtr, iter = iter.gtkTreeIterPtr, col, value.ptr, -1)
        value.value
    }

    public actual fun fetchLongValue(iter: TreeModelIterator, col: Int): Long = memScoped {
        val value = alloc<LongVar>()
        gtk_tree_model_get(tree_model = gtkTreeModelPtr, iter = iter.gtkTreeIterPtr, col, value.ptr, -1)
        value.value
    }

    public actual fun fetchFloatValue(iter: TreeModelIterator, col: Int): Float = memScoped {
        val value = alloc<FloatVar>()
        gtk_tree_model_get(tree_model = gtkTreeModelPtr, iter = iter.gtkTreeIterPtr, col, value.ptr, -1)
        value.value
    }

    public actual fun fetchDoubleValue(iter: TreeModelIterator, col: Int): Double = memScoped {
        val value = alloc<DoubleVar>()
        gtk_tree_model_get(tree_model = gtkTreeModelPtr, iter = iter.gtkTreeIterPtr, col, value.ptr, -1)
        value.value
    }

    public actual fun fetchUShortValue(iter: TreeModelIterator, col: Int): UShort = memScoped {
        val value = alloc<UShortVar>()
        gtk_tree_model_get(tree_model = gtkTreeModelPtr, iter = iter.gtkTreeIterPtr, col, value.ptr, -1)
        value.value
    }

    public actual fun fetchShortValue(iter: TreeModelIterator, col: Int): Short = memScoped {
        val value = alloc<ShortVar>()
        gtk_tree_model_get(tree_model = gtkTreeModelPtr, iter = iter.gtkTreeIterPtr, col, value.ptr, -1)
        value.value
    }

    public actual fun fetchStringFromIterator(iter: TreeModelIterator): String {
        val tmp = gtk_tree_model_get_string_from_iter(gtkTreeModelPtr, iter.gtkTreeIterPtr)
        g_free(tmp)
        return tmp?.toKString() ?: ""
    }

    public actual infix fun referenceNode(iter: TreeModelIterator) {
        gtk_tree_model_ref_node(gtkTreeModelPtr, iter.gtkTreeIterPtr)
    }

    public actual infix fun unreferenceNode(iter: TreeModelIterator) {
        gtk_tree_model_unref_node(gtkTreeModelPtr, iter.gtkTreeIterPtr)
    }

    /**
     * Connects the *row-changed* event to a [handler] on a tree model. This event is used when a row in the model
     * has changed.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRowChangedEvent(handler: CPointer<RowChangedHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeModelPtr, signal = TreeModelBaseEvent.rowChanged, slot = handler, data = userData)

    /**
     * Connects the *row-deleted* event to a [handler] on a tree model. This event is used when a row has been deleted.
     * Note that no iterator is passed to the event handler since the row is already deleted. This should be called by
     * models after a row has been removed. The location pointed to by path should be the location that the row
     * previously was at. It may not be a valid location anymore.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRowDeletedEvent(handler: CPointer<RowDeletedHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeModelPtr, signal = TreeModelBaseEvent.rowDeleted, slot = handler, data = userData)

    /**
     * Connects the *row-has-child-toggled* event to a [handler] on a tree model. This event is used when a row has
     * the first child row, or lost its last child row.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRowHasChildToggledEvent(
        handler: CPointer<RowHasChildToggledHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkTreeModelPtr, signal = TreeModelBaseEvent.rowHasChildToggled, slot = handler,
            data = userData)

    /**
     * Connects the *row-inserted* event to a [handler] on a tree model. This event is used when a new row has been
     * inserted in the model. Note that the row may still be empty at this point since it is a common pattern to first
     * insert an empty row, and then fill it with the desired values.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRowInsertedEvent(handler: CPointer<RowInsertedHandler>,
                                       userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeModelPtr, signal = TreeModelBaseEvent.rowInserted, slot = handler, data = userData)

    /**
     * Connects the *ros-reordered* event to a [handler] on a tree model. This event is used when the children of a
     * node in the tree model have been reordered. Note that this event is not fired when rows are reordered by
     * drag and drop since this is implemented by removing, and then reinserting the row.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRowsReorderedEvent(
        handler: CPointer<RowsReorderedHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkTreeModelPtr, signal = TreeModelBaseEvent.rowsReordered, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkTreeModelPtr, handlerId)
    }
}

/**
 * The event handler for the *row-changed* event. Arguments:
 * 1. treeModel: CPointer<GtkTreeModel>
 * 2. path: CPointer<GtkTreePath>
 * 3. iter: CPointer<GtkTreeIter>
 * 4. userData: gpointer
 */
public typealias RowChangedHandler = CFunction<(
    treeModel: CPointer<GtkTreeModel>,
    path: CPointer<GtkTreePath>,
    iter: CPointer<GtkTreeIter>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *row-deleted* event. Arguments:
 * 1. treeModel: CPointer<GtkTreeModel>
 * 2. path: CPointer<GtkTreePath>
 * 3. userData: gpointer
 */
public typealias RowDeletedHandler = CFunction<(
    treeModel: CPointer<GtkTreeModel>,
    path: CPointer<GtkTreePath>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *row-has-child-toggled* event. Arguments:
 * 1. treeModel: CPointer<GtkTreeModel>
 * 2. path: CPointer<GtkTreePath>
 * 3. iter: CPointer<GtkTreeIter>
 * 4. userData: gpointer
 */
public typealias RowHasChildToggledHandler = CFunction<(
    treeModel: CPointer<GtkTreeModel>,
    path: CPointer<GtkTreePath>,
    iter: CPointer<GtkTreeIter>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *row-inserted* event. Arguments:
 * 1. treeModel: CPointer<GtkTreeModel>
 * 2. path: CPointer<GtkTreePath>
 * 3. iter: CPointer<GtkTreeIter>
 * 4. userData: gpointer
 */
public typealias RowInsertedHandler = CFunction<(
    treeModel: CPointer<GtkTreeModel>,
    path: CPointer<GtkTreePath>,
    iter: CPointer<GtkTreeIter>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *rows-reordered* event. Arguments:
 * 1. treeModel: CPointer<GtkTreeModel>
 * 2. path: CPointer<GtkTreePath>
 * 3. iter: CPointer<GtkTreeIter>
 * 4. newOrder: gpointer
 * 5. userData: gpointer
 */
public typealias RowsReorderedHandler = CFunction<(
    treeModel: CPointer<GtkTreeModel>,
    path: CPointer<GtkTreePath>,
    iter: CPointer<GtkTreeIter>,
    newOrder: gpointer,
    userData: gpointer
) -> Unit>
