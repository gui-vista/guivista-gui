package io.gitlab.guiVista.gui.cell.renderer

import gtk3.GtkCellRenderer
import gtk3.GtkCellRendererText
import gtk3.gtk_cell_renderer_text_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class TextCellRenderer private constructor(ptr: CPointer<GtkCellRendererText>? = null) :
    TextCellRendererBase {
    override val gtkCellRendererPtr: CPointer<GtkCellRenderer>? = ptr?.reinterpret() ?: gtk_cell_renderer_text_new()
    override val gtkCellRendererTextPtr: CPointer<GtkCellRendererText>?
        get() = gtkCellRendererPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkCellRendererText>?): TextCellRenderer = TextCellRenderer(ptr)

        public actual fun create(): TextCellRenderer = TextCellRenderer()
    }
}

public fun textCellRenderer(ptr: CPointer<GtkCellRendererText>? = null, init: TextCellRenderer.() -> Unit = {}):
    TextCellRenderer {
    val result = if (ptr != null) TextCellRenderer.fromPointer(ptr) else TextCellRenderer.create()
    result.init()
    return result
}

public fun CPointer<GtkCellRendererText>?.toTextCellRenderer(): TextCellRenderer = TextCellRenderer.fromPointer(this)
