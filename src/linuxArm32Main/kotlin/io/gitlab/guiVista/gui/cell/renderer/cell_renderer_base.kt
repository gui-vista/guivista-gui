package io.gitlab.guiVista.gui.cell.renderer

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.*
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.*

public actual interface CellRendererBase : InitiallyUnowned {
    public val gtkCellRendererPtr: CPointer<GtkCellRenderer>?
    public actual var visible: Boolean
        get() = gtk_cell_renderer_get_visible(gtkCellRendererPtr) == TRUE
        set(value) = gtk_cell_renderer_set_visible(gtkCellRendererPtr, if (value) TRUE else FALSE)
    public actual var sensitive: Boolean
        get() = gtk_cell_renderer_get_sensitive(gtkCellRendererPtr) == TRUE
        set(value) = gtk_cell_renderer_set_sensitive(gtkCellRendererPtr, if (value) TRUE else FALSE)
    public actual val isActivatable: Boolean
        get() = gtk_cell_renderer_is_activatable(gtkCellRendererPtr) == TRUE

    public actual val requestMode: UInt
        get() = gtk_cell_renderer_get_request_mode(gtkCellRendererPtr)

    public actual fun fetchFixedSize(): Pair<Int, Int> = memScoped {
        val width = alloc<IntVar>().apply { value = 0 }
        val height = alloc<IntVar>().apply { value = 0 }
        gtk_cell_renderer_get_fixed_size(cell = gtkCellRendererPtr, width = width.ptr, height = height.ptr)
        width.value to height.value
    }

    public actual fun changeFixedSize(width: Int, height: Int) {
        gtk_cell_renderer_set_fixed_size(cell = gtkCellRendererPtr, width = width, height = height)
    }

    public actual fun fetchAlignment(): Pair<Float, Float> = memScoped {
        val xAlign = alloc<FloatVar>().apply { value = 0F }
        val yAlign = alloc<FloatVar>().apply { value = 0F }
        gtk_cell_renderer_get_alignment(cell = gtkCellRendererPtr, xalign = xAlign.ptr, yalign = yAlign.ptr)
        xAlign.value to yAlign.value
    }

    public actual fun changeAlignment(xAlign: Float, yAlign: Float) {
        gtk_cell_renderer_set_alignment(cell = gtkCellRendererPtr, xalign = xAlign, yalign = yAlign)
    }

    public actual fun fetchPadding(): Pair<Int, Int> = memScoped {
        val xPad = alloc<IntVar>().apply { value = 0 }
        val yPad = alloc<IntVar>().apply { value = 0 }
        gtk_cell_renderer_get_padding(cell = gtkCellRendererPtr, xpad = xPad.ptr, ypad = yPad.ptr)
        xPad.value to yPad.value
    }

    public actual fun changePadding(xPad: Int, yPad: Int) {
        gtk_cell_renderer_set_padding(cell = gtkCellRendererPtr, xpad = xPad, ypad = yPad)
    }

    public actual fun fetchPreferredHeight(widget: WidgetBase): Pair<Int, Int> = memScoped {
        val minSize = alloc<IntVar>().apply { value = 0 }
        val naturalSize = alloc<IntVar>().apply { value = 0 }
        gtk_cell_renderer_get_preferred_height(
            cell = gtkCellRendererPtr,
            widget = widget.gtkWidgetPtr,
            minimum_size = minSize.ptr,
            natural_size = naturalSize.ptr
        )
        minSize.value to naturalSize.value
    }

    public actual fun fetchPreferredHeightForWidth(widget: WidgetBase, width: Int): Pair<Int, Int> = memScoped {
        val minHeight = alloc<IntVar>().apply { value = 0 }
        val naturalHeight = alloc<IntVar>().apply { value = 0 }
        gtk_cell_renderer_get_preferred_height_for_width(
            cell = gtkCellRendererPtr,
            widget = widget.gtkWidgetPtr,
            width = width,
            minimum_height = minHeight.ptr,
            natural_height = naturalHeight.ptr
        )
        minHeight.value to naturalHeight.value
    }

    public actual fun fetchPreferredWidth(widget: WidgetBase): Pair<Int, Int> = memScoped {
        val minSize = alloc<IntVar>().apply { value = 0 }
        val naturalSize = alloc<IntVar>().apply { value = 0 }
        gtk_cell_renderer_get_preferred_width(
            cell = gtkCellRendererPtr,
            widget = widget.gtkWidgetPtr,
            minimum_size = minSize.ptr,
            natural_size = naturalSize.ptr
        )
        minSize.value to naturalSize.value
    }

    public actual fun fetchPreferredWidthForHeight(widget: WidgetBase, height: Int): Pair<Int, Int> = memScoped {
        val minSize = alloc<IntVar>().apply { value = 0 }
        val naturalWidth = alloc<IntVar>().apply { value = 0 }
        gtk_cell_renderer_get_preferred_width_for_height(
            cell = gtkCellRendererPtr,
            widget = widget.gtkWidgetPtr,
            height = height,
            minimum_width = minSize.ptr,
            natural_width = naturalWidth.ptr
        )
        minSize.value to naturalWidth.value
    }

    public actual fun fetchState(widget: WidgetBase?, cellState: UInt): UInt =
        gtk_cell_renderer_get_state(cell = gtkCellRendererPtr, widget = widget?.gtkWidgetPtr, cell_state = cellState)

    /**
     * Connects the *editing-cancelled* event to a [handler] on a cell renderer. This event is used when the user
     * cancels the process of editing a cell. For example an editable cell renderer could be written to cancel editing
     * when the user presses **Escape**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectEditingCancelledEvent(
        handler: CPointer<EditingCancelledHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkCellRendererPtr, signal = CellRendererBaseEvent.editingCancelled, slot = handler,
            data = userData)

    /**
     * Connects the *editing-started* event to a [handler] on a cell renderer. This event is used when a cell starts
     * to be edited. The intended use of this event is to do special setup on editable, e.g. adding a
     * `GtkEntryCompletion` or setting up additional columns in a combo box.
     *
     * Note that GTK+ doesn't guarantee that cell renderers will continue to use the same kind of widget for editing in
     * future releases, therefore you should check the type of editable before doing any specific setup.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectEditingStartedEvent(
        handler: CPointer<EditingStartedHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkCellRendererPtr, signal = CellRendererBaseEvent.editingStarted, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkCellRendererPtr, handlerId)
    }
}

/**
 * The event handler for the *editing-cancelled* event. Arguments:
 * 1. renderer: CPointer<GtkCellRenderer>
 * 2. userData: gpointer
 */
public typealias EditingCancelledHandler = CFunction<(renderer: CPointer<GtkCellRenderer>, userData: gpointer) -> Unit>

/**
 * The event handler for the *editing-started* event. Arguments:
 * 1. renderer: CPointer<GtkCellRenderer>
 * 2. editable: CPointer<GtkCellEditable>
 * 3. path: CPointer<ByteVar> (represents a String)
 * 4. userData: gpointer
 */
public typealias EditingStartedHandler = CFunction<(
    renderer: CPointer<GtkCellRenderer>,
    editable: CPointer<GtkCellEditable>,
    path: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>
