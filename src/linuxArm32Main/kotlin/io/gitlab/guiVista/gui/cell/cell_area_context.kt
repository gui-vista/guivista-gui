package io.gitlab.guiVista.gui.cell

import glib2.g_object_unref
import gtk3.*
import io.gitlab.guiVista.core.ObjectBase
import kotlinx.cinterop.*

public actual class CellAreaContext private constructor(ptr: CPointer<GtkCellAreaContext>?) : ObjectBase {
    public val gtkCellAreaContextPtr: CPointer<GtkCellAreaContext>? = ptr
    public actual val area: CellAreaBase
        get() = CellArea.fromPointer(gtk_cell_area_context_get_area(gtkCellAreaContextPtr))

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkCellAreaContext>?): CellAreaContext = CellAreaContext(ptr)
    }

    override fun close() {
        g_object_unref(gtkCellAreaContextPtr)
    }

    public actual fun allocate(width: Int, height: Int) {
        gtk_cell_area_context_allocate(context = gtkCellAreaContextPtr, width = width, height = height)
    }

    public actual fun reset() {
        gtk_cell_area_context_reset(gtkCellAreaContextPtr)
    }

    public actual fun fetchPreferredWidth(): Pair<Int, Int> = memScoped {
        val minWidth = alloc<IntVar>().apply { value = 0 }
        val naturalWidth = alloc<IntVar>().apply { value = 0 }
        gtk_cell_area_context_get_preferred_width(context = gtkCellAreaContextPtr, minimum_width = minWidth.ptr,
            natural_width = naturalWidth.ptr)
        minWidth.value to naturalWidth.value
    }

    public actual fun fetchPreferredHeight(): Pair<Int, Int> = memScoped {
        val minHeight = alloc<IntVar>().apply { value = 0 }
        val naturalHeight = alloc<IntVar>().apply { value = 0 }
        gtk_cell_area_context_get_preferred_height(context = gtkCellAreaContextPtr, minimum_height = minHeight.ptr,
            natural_height = naturalHeight.ptr)
        minHeight.value to naturalHeight.value
    }

    public actual fun fetchPreferredHeightForWidth(width: Int): Pair<Int, Int> = memScoped {
        val minHeight = alloc<IntVar>().apply { value = 0 }
        val naturalHeight = alloc<IntVar>().apply { value = 0 }
        gtk_cell_area_context_get_preferred_height_for_width(
            context = gtkCellAreaContextPtr,
            minimum_height = minHeight.ptr,
            natural_height = naturalHeight.ptr,
            width = width
        )
        minHeight.value to naturalHeight.value
    }

    public actual fun fetchPreferredWidthForHeight(height: Int): Pair<Int, Int> = memScoped {
        val minWidth = alloc<IntVar>().apply { value = 0 }
        val naturalWidth = alloc<IntVar>().apply { value = 0 }
        gtk_cell_area_context_get_preferred_width_for_height(
            context = gtkCellAreaContextPtr,
            minimum_width = minWidth.ptr,
            natural_width = naturalWidth.ptr,
            height = height
        )
        minWidth.value to naturalWidth.value
    }

    public actual fun fetchAllocation(): Pair<Int, Int> = memScoped {
        val width = alloc<IntVar>().apply { value = 0 }
        val height = alloc<IntVar>().apply { value = 0 }
        gtk_cell_area_context_get_allocation(context = gtkCellAreaContextPtr, width = width.ptr, height = height.ptr)
        width.value to height.value
    }

    public actual fun pushPreferredWidth(minWidth: Int, naturalWidth: Int) {
        gtk_cell_area_context_push_preferred_width(context = gtkCellAreaContextPtr, minimum_width = minWidth,
            natural_width = naturalWidth)
    }

    public actual fun pushPreferredHeight(minHeight: Int, naturalHeight: Int) {
        gtk_cell_area_context_push_preferred_height(context = gtkCellAreaContextPtr, minimum_height = minHeight,
            natural_height = naturalHeight)
    }
}

public fun CPointer<GtkCellAreaContext>?.toCellAreaContext(): CellAreaContext = CellAreaContext.fromPointer(this)
