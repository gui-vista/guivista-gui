package io.gitlab.guiVista.gui.cell.renderer

import glib2.gpointer
import gtk3.GtkCellRendererText
import gtk3.gtk_cell_renderer_text_set_fixed_height_from_font
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer

public actual interface TextCellRendererBase : CellRendererBase {
    public val gtkCellRendererTextPtr: CPointer<GtkCellRendererText>?

    public actual fun changeFixedHeightFromFont(totalRows: Int) {
        gtk_cell_renderer_text_set_fixed_height_from_font(gtkCellRendererTextPtr, totalRows)
    }

    /**
     * Connects the *edited* event to a [handler] on a text cell renderer. This event is used after the renderer has
     * been edited. It is the responsibility of the application to update the model, and store new text at the position
     * indicated by the path.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectEditedEvent(
        handler: CPointer<EditedHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong = connectGSignal(obj = gtkCellRendererTextPtr, signal = TextCellRendererBaseEvent.edited, slot = handler,
        data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkCellRendererTextPtr, handlerId)
    }
}

/**
 * The event handler for the *edited* event. Arguments:
 * 1. renderer: CPointer<GtkCellRendererText>
 * 2. path: CPointer<ByteVar> (represents a String)
 * 3. newText: CPointer<ByteVar> (represents a String)
 * 4. userData: gpointer
 */
public typealias EditedHandler = CFunction<(
    renderer: CPointer<GtkCellRendererText>,
    path: CPointer<ByteVar>,
    newText: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>
