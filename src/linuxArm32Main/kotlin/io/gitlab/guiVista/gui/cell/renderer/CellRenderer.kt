package io.gitlab.guiVista.gui.cell.renderer

import gtk3.GtkCellRenderer
import kotlinx.cinterop.CPointer

public actual class CellRenderer private constructor(ptr: CPointer<GtkCellRenderer>?) : CellRendererBase {
    override val gtkCellRendererPtr: CPointer<GtkCellRenderer>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkCellRenderer>?): CellRenderer = CellRenderer(ptr)
    }
}
