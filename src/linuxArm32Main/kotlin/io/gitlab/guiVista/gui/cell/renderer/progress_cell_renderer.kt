package io.gitlab.guiVista.gui.cell.renderer

import gtk3.GtkCellRenderer
import gtk3.GtkCellRendererProgress
import gtk3.GtkOrientable
import gtk3.gtk_cell_renderer_progress_new
import io.gitlab.guiVista.gui.layout.Orientable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class ProgressCellRenderer private constructor(ptr: CPointer<GtkCellRendererProgress>? = null) :
    CellRendererBase, Orientable {
    override val gtkCellRendererPtr: CPointer<GtkCellRenderer>? = ptr?.reinterpret() ?: gtk_cell_renderer_progress_new()
    public val gtkCellRendererProgressPtr: CPointer<GtkCellRendererProgress>?
        get() = gtkCellRendererPtr?.reinterpret()
    override val gtkOrientable: CPointer<GtkOrientable>?
        get() = gtkCellRendererProgressPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkCellRendererProgress>?): ProgressCellRenderer =
            ProgressCellRenderer(ptr)

        public actual fun create(): ProgressCellRenderer = ProgressCellRenderer()
    }
}

public fun progressCellRenderer(
    ptr: CPointer<GtkCellRendererProgress>? = null,
    init: ProgressCellRenderer.() -> Unit = {}
): ProgressCellRenderer {
    val result = if (ptr != null) ProgressCellRenderer.fromPointer(ptr) else ProgressCellRenderer.create()
    result.init()
    return result
}

public fun CPointer<GtkCellRendererProgress>?.toProgressCellRenderer(): ProgressCellRenderer =
    ProgressCellRenderer.fromPointer(this)
