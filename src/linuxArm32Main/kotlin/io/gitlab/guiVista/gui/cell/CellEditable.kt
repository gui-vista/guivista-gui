package io.gitlab.guiVista.gui.cell

import gtk3.GtkCellEditable
import kotlinx.cinterop.CPointer

public actual class CellEditable private constructor(ptr: CPointer<GtkCellEditable>?) : CellEditableBase {
    override val gtkCellEditablePtr: CPointer<GtkCellEditable>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkCellEditable>?): CellEditable = CellEditable(ptr)
    }

    override fun close() {
        // Do nothing.
    }
}
