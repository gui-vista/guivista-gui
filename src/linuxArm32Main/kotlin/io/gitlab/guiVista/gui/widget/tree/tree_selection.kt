package io.gitlab.guiVista.gui.widget.tree

import glib2.GDestroyNotify
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.tree.TreeModel
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreeModelIterator
import io.gitlab.guiVista.gui.tree.TreePath
import kotlinx.cinterop.*

public actual class TreeSelection private constructor(ptr: CPointer<GtkTreeSelection>?) : ObjectBase {
    public val gtkTreeSelectionPtr: CPointer<GtkTreeSelection>? = ptr

    /**
     * The selection mode of the selection. If the previous type was `GTK_SELECTION_MULTIPLE` then the anchor is kept
     * selected, if it was previously selected.
     *
     * Data binding property name: **mode**
     */
    public var mode: GtkSelectionMode
        get() = gtk_tree_selection_get_mode(gtkTreeSelectionPtr)
        set(value) = gtk_tree_selection_set_mode(gtkTreeSelectionPtr, value)
    public actual val treeView: TreeView
        get() = TreeView.fromPointer(gtk_tree_selection_get_tree_view(gtkTreeSelectionPtr))
    public actual val totalSelectedRows: Int
        get() = gtk_tree_selection_count_selected_rows(gtkTreeSelectionPtr)

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkTreeSelection>?): TreeSelection = TreeSelection(ptr)
    }

    override fun close() {
        // Do nothing.
    }

    /**
     * Connects the *changed* event to a [handler] on a [TreeSelection]. This event occurs whenever the selection has
     * (possibly) changed. Please note that this event is mostly a hint. It may only be fired once when a range of
     * rows are selected, and it may occasionally be fired when nothing has happened.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectChangedEvent(handler: CPointer<ChangedHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeSelectionPtr, signal = TreeSelectionEvent.changed, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkTreeSelectionPtr, handlerId)
    }

    public actual fun fetchSelected(): Triple<TreeModelBase?, TreeModelIterator?, Boolean> = memScoped {
        val iter = TreeModelIterator.create()
        val model = alloc<CPointerVar<GtkTreeModel>>()
        val selectedNode = gtk_tree_selection_get_selected(
            selection = gtkTreeSelectionPtr,
            model = model.ptr,
            iter = iter.gtkTreeIterPtr
        ) == TRUE
        Triple(TreeModel.fromPointer(model.value), iter, selectedNode)
    }

    public actual fun fetchSelectedRows(model: TreeModelBase?): DoublyLinkedList =
        DoublyLinkedList.fromPointer(gtk_tree_selection_get_selected_rows(gtkTreeSelectionPtr,
            cValuesOf(model?.gtkTreeModelPtr)))

    public actual fun selectPath(path: TreePath) {
        gtk_tree_selection_select_path(gtkTreeSelectionPtr, path.gtkTreePathPtr)
    }

    public actual fun unselectPath(path: TreePath) {
        gtk_tree_selection_unselect_path(gtkTreeSelectionPtr, path.gtkTreePathPtr)
    }

    public actual fun pathIsSelected(path: TreePath): Boolean =
        gtk_tree_selection_path_is_selected(gtkTreeSelectionPtr, path.gtkTreePathPtr) == TRUE

    public actual fun selectIterator(iter: TreeModelIterator) {
        gtk_tree_selection_select_iter(gtkTreeSelectionPtr, iter.gtkTreeIterPtr)
    }

    public actual fun unselectIterator(iter: TreeModelIterator) {
        gtk_tree_selection_unselect_iter(gtkTreeSelectionPtr, iter.gtkTreeIterPtr)
    }

    public actual fun iteratorIsSelected(iter: TreeModelIterator): Boolean =
        gtk_tree_selection_iter_is_selected(gtkTreeSelectionPtr, iter.gtkTreeIterPtr) == TRUE

    public actual fun selectAll() {
        gtk_tree_selection_select_all(gtkTreeSelectionPtr)
    }

    public actual fun unselectAll() {
        gtk_tree_selection_unselect_all(gtkTreeSelectionPtr)
    }

    public actual fun selectRange(startPath: TreePath, endPath: TreePath) {
        gtk_tree_selection_select_range(selection = gtkTreeSelectionPtr, start_path = startPath.gtkTreePathPtr,
            end_path = endPath.gtkTreePathPtr)
    }

    public actual fun unselectRange(startPath: TreePath, endPath: TreePath) {
        gtk_tree_selection_unselect_range(selection = gtkTreeSelectionPtr, start_path = startPath.gtkTreePathPtr,
            end_path = endPath.gtkTreePathPtr)
    }

    /**
     * Sets the selection function. If set then this function is called before any node is selected or unselected,
     * giving some control over which nodes are selected. The select function should return *true* if the state of the
     * node may be toggled, and *false* if the state of the node should be left unchanged.
     * @param selectFunc The selection function, may be *null*.
     * @param userData The user data for the [selectFunc].
     * @param destroyFunc The destroy function for [userData], may be *null*.
     */
    public fun changeSelectFunction(
        selectFunc: GtkTreeSelectionFunc? = null,
        userData: gpointer = fetchEmptyDataPointer(),
        destroyFunc: GDestroyNotify? = null
    ) {
        gtk_tree_selection_set_select_function(
            selection = gtkTreeSelectionPtr,
            data = userData,
            func = selectFunc,
            destroy = destroyFunc
        )
    }

    /**
     * Returns the current selection function.
     * @return The selection function or *null*.
     */
    public fun fetchSelectFunction(): GtkTreeSelectionFunc? =
        gtk_tree_selection_get_select_function(gtkTreeSelectionPtr)

    /**
     * Returns the user data for the selection function.
     * @return The user data or *null*.
     */
    public fun fetchUserData(): gpointer? = gtk_tree_selection_get_user_data(gtkTreeSelectionPtr)
}

/**
 * The event handler for the *changed* event. Arguments:
 * 1. treeSelection: CPointer<GtkTreeSelection>
 * 2. userData: gpointer
 */
public typealias ChangedHandler = CFunction<(treeSelection: CPointer<GtkTreeSelection>, userData: gpointer) -> Unit>

public fun CPointer<GtkTreeSelection>?.toTreeSelection(): TreeSelection = TreeSelection.fromPointer(this)
