package io.gitlab.guiVista.gui.widget.button

import gtk3.GtkIconSize
import gtk3.GtkScaleButton
import gtk3.GtkWidget
import gtk3.gtk_scale_button_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toCStringArray

public actual class ScaleButton private constructor(
    ptr: CPointer<GtkScaleButton>? = null,
    size: GtkIconSize = GtkIconSize.GTK_ICON_SIZE_BUTTON,
    min: Double = 0.0,
    max: Double = 0.0,
    step: Double = 0.0,
    icons: Array<String> = emptyArray()
) : ScaleButtonBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: createWidgetPointer(
        size = size,
        min = min,
        max = max,
        step = step,
        icons = icons
    )

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkScaleButton>?): ScaleButton = ScaleButton(ptr = ptr)

        public fun create(
            size: GtkIconSize,
            min: Double,
            max: Double,
            step: Double,
            icons: Array<String>
        ): ScaleButton = ScaleButton(size = size, min = min, max = max, step = step, icons = icons)
    }

    private fun createWidgetPointer(
        size: GtkIconSize,
        min: Double,
        max: Double,
        step: Double,
        icons: Array<String>
    ) = memScoped {
        gtk_scale_button_new(size = size, min = min, max = max, step = step, icons = icons.toCStringArray(this))
    }
}

public fun scaleButtonWidget(
    scaleButtonPtr: CPointer<GtkScaleButton>? = null,
    size: GtkIconSize,
    min: Double,
    max: Double,
    step: Double,
    icons: Array<String>,
    init: ScaleButton.() -> Unit = {}
): ScaleButton {
    val scaleButton = if (scaleButtonPtr != null) {
        ScaleButton.fromPointer(scaleButtonPtr)
    } else {
        ScaleButton.create(
            size = size,
            min = min,
            max = max,
            step = step,
            icons = icons
        )
    }
    scaleButton.init()
    return scaleButton
}

public fun CPointer<GtkScaleButton>?.toScaleButton(): ScaleButton = ScaleButton.fromPointer(this)
