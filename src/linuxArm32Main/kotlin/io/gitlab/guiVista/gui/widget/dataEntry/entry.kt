package io.gitlab.guiVista.gui.widget.dataEntry

import gtk3.GtkCellEditable
import gtk3.GtkEntry
import gtk3.GtkWidget
import gtk3.gtk_entry_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Entry private constructor(ptr: CPointer<GtkEntry>? = null) : EntryBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_entry_new()
    override val gtkCellEditablePtr: CPointer<GtkCellEditable>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public actual fun create(): Entry = Entry()

        public fun fromPointer(ptr: CPointer<GtkEntry>?): Entry = Entry(ptr)
    }
}

public fun entryWidget(ptr: CPointer<GtkEntry>? = null, init: Entry.() -> Unit = {}): Entry {
    val entry = if (ptr != null) Entry.fromPointer(ptr) else Entry.create()
    entry.init()
    return entry
}

public fun CPointer<GtkEntry>?.toEntry(): Entry = Entry.fromPointer(this)
