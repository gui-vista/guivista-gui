package io.gitlab.guiVista.gui.widget.tool.item

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class SeparatorToolItem private constructor(ptr: CPointer<GtkSeparatorToolItem>? = null) : ToolItemBase {
    override val gtkToolItemPtr: CPointer<GtkToolItem>? =
        ptr?.reinterpret() ?: gtk_separator_tool_item_new()
    public val gtkSeparatorToolItemPtr: CPointer<GtkSeparatorToolItem>?
        get() = gtkToolItemPtr?.reinterpret()

    public actual var draw: Boolean
        get() = gtk_separator_tool_item_get_draw(gtkSeparatorToolItemPtr) == TRUE
        set(value) = gtk_separator_tool_item_set_draw(gtkSeparatorToolItemPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkSeparatorToolItem>?): SeparatorToolItem = SeparatorToolItem(ptr)

        public actual fun create(): SeparatorToolItem = SeparatorToolItem()
    }
}

public fun separatorToolItemWidget(
    ptr: CPointer<GtkSeparatorToolItem>? = null,
    init: SeparatorToolItem.() -> Unit = {}
): SeparatorToolItem {
    val toolItem = if (ptr != null) SeparatorToolItem.fromPointer(ptr) else SeparatorToolItem.create()
    toolItem.init()
    return toolItem
}

public fun CPointer<GtkSeparatorToolItem>?.toSeparatorToolItem(): SeparatorToolItem =
    SeparatorToolItem.fromPointer(this)
