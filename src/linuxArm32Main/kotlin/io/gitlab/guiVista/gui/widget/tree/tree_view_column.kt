package io.gitlab.guiVista.gui.widget.tree

import glib2.FALSE
import glib2.GDestroyNotify
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.cell.CellAreaBase
import io.gitlab.guiVista.gui.cell.CellLayout
import io.gitlab.guiVista.gui.cell.renderer.*
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.*

public actual class TreeViewColumn private constructor(
    ptr: CPointer<GtkTreeViewColumn>? = null,
    area: CellAreaBase? = null
) : ObjectBase, CellLayout {
    public val gtkTreeViewColumnPtr: CPointer<GtkTreeViewColumn>? = when {
        ptr != null -> ptr
        area != null -> gtk_tree_view_column_new_with_area(area.gtkCellAreaPtr)
        else -> gtk_tree_view_column_new()
    }
    override val gtkCellLayoutPtr: CPointer<GtkCellLayout>?
        get() = gtkTreeViewColumnPtr?.reinterpret()

    /**
     * Resize mode of the column. Default value is `GTK_TREE_VIEW_COLUMN_GROW_ONLY`.
     *
     * Data binding property name: **sizing**
     */
    public var sizing: GtkTreeViewColumnSizing
        get() = gtk_tree_view_column_get_sizing(gtkTreeViewColumnPtr)
        set(value) = gtk_tree_view_column_set_sizing(gtkTreeViewColumnPtr, value)

    /**
     * Sort direction the sort indicator should indicate. Default value is *GTK_SORT_ASCENDING*.
     *
     * Data binding property name: **sort-order**
     */
    public var sortOrder: GtkSortType
        get() = gtk_tree_view_column_get_sort_order(gtkTreeViewColumnPtr)
        set(value) = gtk_tree_view_column_set_sort_order(gtkTreeViewColumnPtr, value)
    public actual var title: String
        get() = gtk_tree_view_column_get_title(gtkTreeViewColumnPtr)?.toKString() ?: ""
        set(value) = gtk_tree_view_column_set_title(gtkTreeViewColumnPtr, value)
    public actual var spacing: Int
        get() = gtk_tree_view_column_get_spacing(gtkTreeViewColumnPtr)
        set(value) = gtk_tree_view_column_set_spacing(gtkTreeViewColumnPtr, value)
    public actual var visible: Boolean
        get() = gtk_tree_view_column_get_visible(gtkTreeViewColumnPtr) == TRUE
        set(value) = gtk_tree_view_column_set_visible(gtkTreeViewColumnPtr, if (value) TRUE else FALSE)
    public actual var resizable: Boolean
        get() = gtk_tree_view_column_get_resizable(gtkTreeViewColumnPtr) == TRUE
        set(value) = gtk_tree_view_column_set_resizable(gtkTreeViewColumnPtr, if (value) TRUE else FALSE)
    public actual val width: Int
        get() = gtk_tree_view_column_get_width(gtkTreeViewColumnPtr)
    public actual var fixedWidth: Int
        get() = gtk_tree_view_column_get_fixed_width(gtkTreeViewColumnPtr)
        set(value) = gtk_tree_view_column_set_fixed_width(gtkTreeViewColumnPtr, value)
    public actual var maxWidth: Int
        get() = gtk_tree_view_column_get_max_width(gtkTreeViewColumnPtr)
        set(value) = gtk_tree_view_column_set_max_width(gtkTreeViewColumnPtr, value)
    public actual var minWidth: Int
        get() = gtk_tree_view_column_get_min_width(gtkTreeViewColumnPtr)
        set(value) = gtk_tree_view_column_set_min_width(gtkTreeViewColumnPtr, value)
    public actual var expand: Boolean
        get() = gtk_tree_view_column_get_expand(gtkTreeViewColumnPtr) == TRUE
        set(value) = gtk_tree_view_column_set_expand(gtkTreeViewColumnPtr, if (value) TRUE else FALSE)
    public actual var clickable: Boolean
        get() = gtk_tree_view_column_get_clickable(gtkTreeViewColumnPtr) == TRUE
        set(value) = gtk_tree_view_column_set_clickable(gtkTreeViewColumnPtr, if (value) TRUE else FALSE)
    public actual var widget: WidgetBase?
        get() {
            val ptr = gtk_tree_view_column_get_widget(gtkTreeViewColumnPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) = gtk_tree_view_column_set_widget(gtkTreeViewColumnPtr, value?.gtkWidgetPtr)
    public actual var alignment: Float
        get() = gtk_tree_view_column_get_alignment(gtkTreeViewColumnPtr)
        set(value) = gtk_tree_view_column_set_alignment(gtkTreeViewColumnPtr, value)
    public actual var reorderable: Boolean
        get() = gtk_tree_view_column_get_reorderable(gtkTreeViewColumnPtr) == TRUE
        set(value) = gtk_tree_view_column_set_reorderable(gtkTreeViewColumnPtr, if (value) TRUE else FALSE)
    public actual var sortColumnId: Int
        get() = gtk_tree_view_column_get_sort_column_id(gtkTreeViewColumnPtr)
        set(value) = gtk_tree_view_column_set_sort_column_id(gtkTreeViewColumnPtr, value)
    public actual var sortIndicator: Boolean
        get() = gtk_tree_view_column_get_sort_indicator(gtkTreeViewColumnPtr) == TRUE
        set(value) = gtk_tree_view_column_set_sort_indicator(gtkTreeViewColumnPtr, if (value) TRUE else FALSE)
    public actual val treeView: TreeView?
        get() {
            val ptr = gtk_tree_view_column_get_tree_view(gtkTreeViewColumnPtr)
            return if (ptr != null) TreeView.fromPointer(ptr.reinterpret()) else null
        }
    public actual val xOffset: Int
        get() = gtk_tree_view_column_get_x_offset(gtkTreeViewColumnPtr)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkTreeViewColumn>?): TreeViewColumn = TreeViewColumn(ptr = ptr)

        public actual fun create(): TreeViewColumn = TreeViewColumn()

        public actual fun createWithArea(area: CellAreaBase): TreeViewColumn = TreeViewColumn(area = area)
    }

    actual override fun addAttribute(cell: CellRendererBase, attr: String, column: Int) {
        gtk_tree_view_column_add_attribute(
            tree_column = gtkTreeViewColumnPtr,
            cell_renderer = cell.gtkCellRendererPtr,
            attribute = attr,
            column = column
        )
    }

    actual override fun addMultipleAttributes(cell: CellRendererBase, vararg attributes: Pair<String, Int>) {
        attributes.forEach { (attrName, column) -> addAttribute(cell = cell, attr = attrName, column = column) }
    }

    public actual override fun clearAttributes(cell: CellRendererBase) {
        gtk_tree_view_column_clear_attributes(gtkTreeViewColumnPtr, cell.gtkCellRendererPtr)
    }

    public actual fun fetchCellPosition(cellRenderer: CellRendererBase): Triple<Int, Int, Boolean> = memScoped {
        val xOffset = alloc<IntVar>().apply { value = 0 }
        val width = alloc<IntVar>().apply { value = 0 }
        val inTreeColumn = gtk_tree_view_column_cell_get_position(
            tree_column = gtkTreeViewColumnPtr,
            cell_renderer = cellRenderer.gtkCellRendererPtr,
            x_offset = xOffset.ptr,
            width = width.ptr
        ) == TRUE
        Triple(xOffset.value, width.value, inTreeColumn)
    }

    public actual fun focusCell(cell: CellRendererBase) {
        gtk_tree_view_column_focus_cell(gtkTreeViewColumnPtr, cell.gtkCellRendererPtr)
    }

    public actual fun resizeQueue() {
        gtk_tree_view_column_queue_resize(gtkTreeViewColumnPtr)
    }

    override fun close() {
        // Do nothing.
    }

    /**
     * Sets the [data function][dataFunc] to use for the column. This function is used instead of the standard
     * attributes mapping for setting the column value, and should set the value of tree column's cell renderer as
     * appropriate. Note that [dataFunc] may be *null* to remove an older one.
     * @param cellRenderer The cell renderer to use.
     * @param dataFunc The callback to use as the data function.
     * @param userData User data for [dataFunc].
     * @param destroyFunc The destroy notification for [userData].
     */
    public fun changeCellDataFunc(
        cellRenderer: CellRendererBase,
        dataFunc: GtkTreeCellDataFunc?,
        userData: gpointer = fetchEmptyDataPointer(),
        destroyFunc: GDestroyNotify
    ) {
        gtk_tree_view_column_set_cell_data_func(
            tree_column = gtkTreeViewColumnPtr,
            cell_renderer = cellRenderer.gtkCellRendererPtr,
            func = dataFunc,
            func_data = userData,
            destroy = destroyFunc
        )
    }

    /**
     * Connects the *clicked* event to a [handler] on a [TreeViewColumn]. This event is used when a click occurs.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectClickedEvent(
        handler: CPointer<ClickedHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkTreeViewColumnPtr, signal = TreeViewColumnEvent.clicked, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkTreeViewColumnPtr, handlerId)
    }
}

public fun treeViewTextColumn(
    columnId: Int,
    enableSorting: Boolean = false,
    ptr: CPointer<GtkTreeViewColumn>? = null,
    area: CellAreaBase? = null,
    init: TreeViewColumn.() -> Unit = {}
): TreeViewColumn {
    val result = when {
        ptr != null -> TreeViewColumn.fromPointer(ptr)
        area != null -> TreeViewColumn.createWithArea(area)
        else -> TreeViewColumn.create()
    }
    val renderer = TextCellRenderer.create()
    result.init()
    result.clear()
    result.prependCell(renderer)
    result.addAttribute(cell = renderer, attr = "text", column = columnId)
    if (enableSorting) result.sortColumnId = columnId
    return result
}

public fun treeViewToggleColumn(
    columnId: Int,
    enableSorting: Boolean = false,
    ptr: CPointer<GtkTreeViewColumn>? = null,
    area: CellAreaBase? = null,
    init: TreeViewColumn.() -> Unit = {}
): TreeViewColumn {
    val result = when {
        ptr != null -> TreeViewColumn.fromPointer(ptr)
        area != null -> TreeViewColumn.createWithArea(area)
        else -> TreeViewColumn.create()
    }
    val renderer = ToggleCellRenderer.create()
    result.init()
    result.clear()
    result.prependCell(renderer)
    result.addAttribute(cell = renderer, attr = "active", column = columnId)
    if (enableSorting) result.sortColumnId = columnId
    return result
}

public fun treeViewSpinnerColumn(
    columnId: Int,
    enableSorting: Boolean = false,
    ptr: CPointer<GtkTreeViewColumn>? = null,
    area: CellAreaBase? = null,
    init: TreeViewColumn.() -> Unit = {}
): TreeViewColumn {
    val result = when {
        ptr != null -> TreeViewColumn.fromPointer(ptr)
        area != null -> TreeViewColumn.createWithArea(area)
        else -> TreeViewColumn.create()
    }
    val renderer = SpinnerCellRenderer.create()
    result.init()
    result.clear()
    result.prependCell(renderer)
    result.addAttribute(cell = renderer, attr = "active", column = columnId)
    if (enableSorting) result.sortColumnId = columnId
    return result
}

public fun treeViewProgressColumn(
    columnId: Int,
    enableSorting: Boolean = false,
    ptr: CPointer<GtkTreeViewColumn>? = null,
    area: CellAreaBase? = null,
    init: TreeViewColumn.() -> Unit = {}
): TreeViewColumn {
    val result = when {
        ptr != null -> TreeViewColumn.fromPointer(ptr)
        area != null -> TreeViewColumn.createWithArea(area)
        else -> TreeViewColumn.create()
    }
    val renderer = ProgressCellRenderer.create()
    result.init()
    result.clear()
    result.prependCell(renderer)
    result.addAttribute(cell = renderer, attr = "pulse", column = columnId)
    result.addAttribute(cell = renderer, attr = "value", column = columnId)
    if (enableSorting) result.sortColumnId = columnId
    return result
}

public fun treeViewColumn(
    ptr: CPointer<GtkTreeViewColumn>? = null,
    area: CellAreaBase? = null,
    init: TreeViewColumn.() -> Unit = {}
): TreeViewColumn {
    val result = when {
        ptr != null -> TreeViewColumn.fromPointer(ptr)
        area != null -> TreeViewColumn.createWithArea(area)
        else -> TreeViewColumn.create()
    }
    result.init()
    return result
}

/**
 * The event handler for the *clicked* event. Arguments:
 * 1. treeViewColumn: CPointer<GtkTreeViewColumn>
 * 2. userData: gpointer
 */
public typealias ClickedHandler = CFunction<(treeViewColumn: CPointer<GtkTreeViewColumn>, userData: gpointer) -> Unit>

public fun CPointer<GtkTreeViewColumn>?.toTreeViewColumn(): TreeViewColumn = TreeViewColumn.fromPointer(this)
