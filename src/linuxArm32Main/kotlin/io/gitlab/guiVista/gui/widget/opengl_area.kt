package io.gitlab.guiVista.gui.widget

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.*
import io.gitlab.guiVista.gui.OpenGlContext
import kotlinx.cinterop.*

public actual class OpenGlArea private constructor(ptr: CPointer<GtkGLArea>? = null) : InitiallyUnowned {
    public val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_gl_area_new()
    public val gtkGlAreaPtr: CPointer<GtkGLArea>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual val context: OpenGlContext?
        get() {
            val ptr = gtk_gl_area_get_context(gtkGlAreaPtr)
            return if (ptr != null) OpenGlContext.fromPointer(ptr) else null
        }
    public actual var error: Error?
        get() {
            val ptr = gtk_gl_area_get_error(gtkGlAreaPtr)
            return if (ptr != null) Error.fromErrorPointer(ptr) else null
        }
        set(value) = gtk_gl_area_set_error(gtkGlAreaPtr, value?.gErrorPtr)
    public actual var hasAlpha: Boolean
        get() = gtk_gl_area_get_has_alpha(gtkGlAreaPtr) == TRUE
        set(value) = gtk_gl_area_set_has_alpha(gtkGlAreaPtr, if (value) TRUE else FALSE)
    public actual var hasDepthBuffer: Boolean
        get() = gtk_gl_area_get_has_depth_buffer(gtkGlAreaPtr) == TRUE
        set(value) = gtk_gl_area_set_has_depth_buffer(gtkGlAreaPtr, if (value) TRUE else FALSE)
    public actual var hasStencilBuffer: Boolean
        get() = gtk_gl_area_get_has_stencil_buffer(gtkGlAreaPtr) == TRUE
        set(value) = gtk_gl_area_set_has_stencil_buffer(gtkGlAreaPtr, if (value) TRUE else FALSE)
    public actual var autoRender: Boolean
        get() = gtk_gl_area_get_auto_render(gtkGlAreaPtr) == TRUE
        set(value) = gtk_gl_area_set_auto_render(gtkGlAreaPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkGLArea>?): OpenGlArea = OpenGlArea(ptr)

        public actual fun create(): OpenGlArea = OpenGlArea()
    }

    public actual fun makeCurrent() {
        gtk_gl_area_make_current(gtkGlAreaPtr)
    }

    public actual fun queueRender() {
        gtk_gl_area_queue_render(gtkGlAreaPtr)
    }

    public actual fun attachBuffers() {
        gtk_gl_area_attach_buffers(gtkGlAreaPtr)
    }

    public actual fun fetchRequiredVersion(): Pair<Int, Int> = memScoped {
        val major = alloc<IntVar>()
        val minor = alloc<IntVar>()
        gtk_gl_area_get_required_version(area = gtkGlAreaPtr, major = major.ptr, minor = minor.ptr)
        major.value to minor.value
    }

    public actual fun changeRequiredVersion(major: Int, minor: Int) {
        gtk_gl_area_set_required_version(area = gtkGlAreaPtr, major = major, minor = minor)
    }

    /**
     * Connects the *create-context* event to a [handler] on a [OpenGlArea]. This event occurs when the widget is being
     * realized, and allows you to override how the GL context is created. This is useful when you want to reuse an
     * existing GL context, or if you want to try creating different kinds of GL options. If context creation fails
     * then the event handler can use the [error] property to register a more detailed error of how the construction
     * failed.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCreateContextEvent(
        handler: CPointer<CreateContextHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkGlAreaPtr, signal = OpenGlAreaEvent.createContext, slot = handler, data = userData)

    /**
     * Connects the *render* event to a [handler] on a [OpenGlArea]. This event occurs every time the contents of the
     * [OpenGlArea] should be redrawn. The context is bound to the area prior to emitting this function, and the
     * buffers are painted to the window once the emission terminates.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRenderEvent(
        handler: CPointer<CreateContextHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkGlAreaPtr, signal = OpenGlAreaEvent.createContext, slot = handler, data = userData)

    /**
     * Connects the *resize* event to a [handler] on a [OpenGlArea]. This event occurs once when the widget is
     * realized, and then each time the widget is changed while realized. This is useful in order to keep GL state up
     * to date with the widget size, like for instance camera properties which may depend on the width/height ratio.
     * The GL context for the area is guaranteed to be current when this event is fired.
     *
     * The default handler sets up the GL viewport.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectResizeEvent(
        handler: CPointer<CreateContextHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkGlAreaPtr, signal = OpenGlAreaEvent.createContext, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkGlAreaPtr, handlerId)
    }
}

public fun openGlAreaWidget(ptr: CPointer<GtkGLArea>? = null, init: OpenGlArea.() -> Unit = {}): OpenGlArea {
    val result = if (ptr != null) OpenGlArea.fromPointer(ptr) else OpenGlArea.create()
    result.init()
    return result
}

/**
 * The event handler for the *create-context* event. Arguments:
 * 1. area: CPointer<GtkGLArea>
 * 2. userData: gpointer
 * Returns a CPointer<GdkGLContext>? object.
 */
public typealias CreateContextHandler = CFunction<(
    area: CPointer<GtkGLArea>,
    userData: gpointer
) -> CPointer<GdkGLContext>?>

/**
 * The event handler for the *render* event. Arguments:
 * 1. area: CPointer<GtkGLArea>
 * 2. ctx: CPointer<GdkGLContext>
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias RenderHandler = CFunction<(
    area: CPointer<GtkGLArea>,
    ctx: CPointer<GdkGLContext>,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *resize* event. Arguments:
 * 1. area: CPointer<GtkGLArea>
 * 2. width: Int
 * 3. height: Int
 * 4. userData: gpointer
 */
public typealias ResizeHandler = CFunction<(
    area: CPointer<GtkGLArea>,
    width: Int,
    height: Int,
    userData: gpointer
) -> Unit>
