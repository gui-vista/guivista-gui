package io.gitlab.guiVista.gui.widget.button

import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.Adjustment
import io.gitlab.guiVista.gui.layout.Orientable
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.*

public actual interface ScaleButtonBase : ButtonBase, Orientable {
    override val gtkOrientable: CPointer<GtkOrientable>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkScaleButtonPtr: CPointer<GtkScaleButton>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var adjustment: Adjustment
        get() = Adjustment.fromPointer(gtk_scale_button_get_adjustment(gtkScaleButtonPtr))
        set(value) = gtk_scale_button_set_adjustment(gtkScaleButtonPtr, value.gtkAdjustmentPtr)
    public actual var value: Double
        get() = gtk_scale_button_get_value(gtkScaleButtonPtr)
        set(value) = gtk_scale_button_set_value(gtkScaleButtonPtr, value)
    public actual val plusButton: WidgetBase
        get() = Widget.fromPointer(gtk_scale_button_get_plus_button(gtkScaleButtonPtr))
    public actual val minusButton: WidgetBase
        get() = Widget.fromPointer(gtk_scale_button_get_minus_button(gtkScaleButtonPtr))
    public actual val popup: WidgetBase
        get() = Widget.fromPointer(gtk_scale_button_get_popup(gtkScaleButtonPtr))

    public actual fun changeIcons(icons: Array<String>): Unit = memScoped {
        gtk_scale_button_set_icons(gtkScaleButtonPtr, icons.toCStringArray(this))
    }

    /**
     * Connects the *popdown* event to a [handler] on a scale button. This event is used to popdown the scale widget.
     * The default key binding for this event is **Escape**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPopdownEvent(handler: CPointer<PopdownHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkScaleButtonPtr, signal = ScaleButtonBaseEvent.popdown, slot = handler, data = userData)

    /**
     * Connects the *popup* event to a [handler] on a scale button. This event is used to popup the scale widget.
     * The default key binding for this event are **Space**, **Enter**, and **Return**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPopupEvent(handler: CPointer<PopupHandler>,
                                 userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkScaleButtonPtr, signal = ScaleButtonBaseEvent.popup, slot = handler, data = userData)

    /**
     * Connects the *popup* event to a [handler] on a scale button. This event occurs when the value field has changed.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectValueChangedEvent(
        handler: CPointer<ValueChangedHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong = connectGSignal(obj = gtkScaleButtonPtr, signal = ScaleButtonBaseEvent.valueChanged, slot = handler,
        data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkScaleButtonPtr, handlerId)
    }
}

/**
 * The event handler for the *popdown* event. Arguments:
 * 1. button: CPointer<GtkScaleButton>
 * 2. userData: gpointer
 */
public typealias PopdownHandler = CFunction<(button: CPointer<GtkScaleButton>, userData: gpointer) -> Unit>

/**
 * The event handler for the *popup* event. Arguments:
 * 1. button: CPointer<GtkScaleButton>
 * 2. userData: gpointer
 */
public typealias PopupHandler = CFunction<(button: CPointer<GtkScaleButton>, userData: gpointer) -> Unit>

/**
 * The event handler for the *value-changed* event. Arguments:
 * 1. button: CPointer<GtkScaleButton>
 * 2. value: Double
 * 3. userData: gpointer
 */
public typealias ValueChangedHandler = CFunction<(
    button: CPointer<GtkScaleButton>,
    value: Double,
    userData: gpointer
) -> Unit>
