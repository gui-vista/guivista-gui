package io.gitlab.guiVista.gui.widget.textEditor

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.text.TextBuffer
import io.gitlab.guiVista.gui.text.TextBufferIterator
import io.gitlab.guiVista.gui.text.TextMark
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual class TextView private constructor(
    textViewPtr: CPointer<GtkTextView>? = null,
    textBufferPtr: CPointer<GtkTextBuffer>? = null
) : ContainerBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? =
        if (textViewPtr != null && textBufferPtr == null) textViewPtr.reinterpret()
        else if (textBufferPtr != null && textViewPtr == null) gtk_text_view_new_with_buffer(textBufferPtr)
        else gtk_text_view_new()
    public val gtkTextViewPtr: CPointer<GtkTextView>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var buffer: TextBuffer
        get() = TextBuffer(gtk_text_view_get_buffer(gtkTextViewPtr))
        set(value) = gtk_text_view_set_buffer(gtkTextViewPtr, value.gtkTextBufferPtr)
    public actual var editable: Boolean
        get() = gtk_text_view_get_editable(gtkTextViewPtr) == TRUE
        set(value) = gtk_text_view_set_editable(gtkTextViewPtr, if (value) TRUE else FALSE)
    public actual var cursorVisible: Boolean
        get() = gtk_text_view_get_cursor_visible(gtkTextViewPtr) == TRUE
        set(value) = gtk_text_view_set_cursor_visible(gtkTextViewPtr, if (value) TRUE else FALSE)
    public actual var overwrite: Boolean
        get() = gtk_text_view_get_overwrite(gtkTextViewPtr) == TRUE
        set(value) = gtk_text_view_set_overwrite(gtkTextViewPtr, if (value) TRUE else FALSE)
    public actual var pixelsAboveLines: Int
        get() = gtk_text_view_get_pixels_above_lines(gtkTextViewPtr)
        set(value) {
            if (value < 0) throw IllegalArgumentException("The pixelsAboveLines value must be >= 0.")
            gtk_text_view_set_pixels_above_lines(gtkTextViewPtr, value)
        }
    public actual var pixelsBelowLines: Int
        get() = gtk_text_view_get_pixels_below_lines(gtkTextViewPtr)
        set(value) {
            if (value < 0) throw IllegalArgumentException("The pixelsBelowLines value must be >= 0.")
            gtk_text_view_set_pixels_below_lines(gtkTextViewPtr, value)
        }
    public actual var pixelsInsideWrap: Int
        get() = gtk_text_view_get_pixels_inside_wrap(gtkTextViewPtr)
        set(value) {
            if (value < 0) throw IllegalArgumentException("The pixelsInsideWrap value must be >= 0.")
            gtk_text_view_set_pixels_inside_wrap(gtkTextViewPtr, value)
        }
    public actual var leftMargin: Int
        get() = gtk_text_view_get_left_margin(gtkTextViewPtr)
        set(value) {
            if (value < 0) throw IllegalArgumentException("The leftMargin value must be >= 0.")
            gtk_text_view_set_left_margin(gtkTextViewPtr, value)
        }
    public actual var rightMargin: Int
        get() = gtk_text_view_get_right_margin(gtkTextViewPtr)
        set(value) {
            if (value < 0) throw IllegalArgumentException("The rightMargin value must be >= 0.")
            gtk_text_view_set_right_margin(gtkTextViewPtr, value)
        }
    public actual var topMargin: Int
        get() = gtk_text_view_get_top_margin(gtkTextViewPtr)
        set(value) {
            if (value < 0) throw IllegalArgumentException("The topMargin value must be >= 0.")
            gtk_text_view_set_top_margin(gtkTextViewPtr, value)
        }
    public actual var bottomMargin: Int
        get() = gtk_text_view_get_bottom_margin(gtkTextViewPtr)
        set(value) {
            if (value < 0) throw IllegalArgumentException("The bottomMargin value must be >= 0.")
            gtk_text_view_set_bottom_margin(gtkTextViewPtr, value)
        }
    public actual var indent: Int
        get() = gtk_text_view_get_indent(gtkTextViewPtr)
        set(value) = gtk_text_view_set_indent(gtkTextViewPtr, value)
    public actual var acceptsTab: Boolean
        get() = gtk_text_view_get_accepts_tab(gtkTextViewPtr) == TRUE
        set(value) = gtk_text_view_set_accepts_tab(gtkTextViewPtr, if (value) TRUE else FALSE)
    public actual var monospace: Boolean
        get() = gtk_text_view_get_monospace(gtkTextViewPtr) == TRUE
        set(value) = gtk_text_view_set_monospace(gtkTextViewPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public actual fun create(): TextView = TextView()

        public fun fromTextViewPointer(ptr: CPointer<GtkTextView>?): TextView = TextView(textViewPtr = ptr)

        public fun fromTextBufferPointer(ptr: CPointer<GtkTextBuffer>?): TextView = TextView(textBufferPtr = ptr)
    }

    public actual fun placeCursorOnscreen(): Boolean = gtk_text_view_place_cursor_onscreen(gtkTextViewPtr) == TRUE

    public actual fun moveChild(child: WidgetBase, xPos: Int, yPos: Int) {
        gtk_text_view_move_child(text_view = gtkTextViewPtr, child = child.gtkWidgetPtr, xpos = xPos, ypos = yPos)
    }

    public actual fun resetCursorBlink() {
        gtk_text_view_reset_cursor_blink(gtkTextViewPtr)
    }

    /**
     * Connects the *backspace* event to a [handler] on a [TextView]. This event occurs when the user asks for it. The
     * default bindings for this event are **Backspace** and **Shift-Backspace**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectBackspaceEvent(handler: CPointer<BackspaceHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTextViewPtr, signal = TextViewEvent.backspace, slot = handler,
            data = userData)

    /**
     * Connects the *copy-clipboard* event to a [handler] on a [TextView]. This event occurs when a selection is copied
     * to the clipboard. The default bindings for this event are **Ctrl-c**, and **Ctrl-Insert**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCopyClipboardEvent(handler: CPointer<CopyClipboardHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTextViewPtr, signal = TextViewEvent.copyClipboard, slot = handler,
            data = userData)

    /**
     * Connects the *cut-clipboard* event to a [handler] on a [TextView]. This event occurs when a selection is cut
     * from the clipboard. The default bindings for this event are **Ctrl-x**, and **Shift-Delete**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCutClipboardEvent(handler: CPointer<CutClipboardHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTextViewPtr, signal = TextViewEvent.cutClipboard, slot = handler,
            data = userData)

    /**
     * Connects the *insert-at-cursor* event to a [handler] on a [TextView]. This event occurs when the user initiates
     * the insertion of a fixed string at the cursor. This event has no default bindings.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectInsertAtCursorEvent(handler: CPointer<InsertAtCursorHandler>,
                                          userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTextViewPtr, signal = TextViewEvent.insertAtCursor, slot = handler,
            data = userData)

    /**
     * Connects the *paste-clipboard* event to a [handler] on a [TextView]. This event occurs when a selection is
     * pasted from the clipboard. The default bindings for this event are **Ctrl-v**, and **Shift-Insert**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPasteClipboardEvent(handler: CPointer<PasteClipboardHandler>,
                                          userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTextViewPtr, signal = TextViewEvent.pasteClipboard, slot = handler,
            data = userData)

    /**
     * Connects the *populate-popup* event to a [handler] on a [TextView]. This event occurs before showing the context
     * menu of the text view. If you need to add items to the context menu, connect to this event and append your
     * items to the popup, which will be a GtkMenu in this case. If **populate-all** is *true* then this event will
     * also be emitted to populate touch popups. In this case, popup will be a different container, e.g. a GtkToolbar.
     *
     * The event handler should **NOT** make assumptions about the type of widget, but check whether popup is a
     * GtkMenu, or GtkToolbar, or another kind of [container][ContainerBase].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPopulatePopupEvent(handler: CPointer<PopuplatePopupHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTextViewPtr, signal = TextViewEvent.populatePopup, slot = handler,
            data = userData)

    /**
     * Connects the *select-all* event to a [handler] on a [TextView]. This event occurs when selecting or unselecting
     * the complete contents of the text view. The default bindings for this event are **Ctrl-a** and
     * **Ctrl-forward_slash** for selecting, and **Shift-Ctrl-a** and **Ctrl-\** for unselecting.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSelectAllEvent(handler: CPointer<SelectAllHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTextViewPtr, signal = TextViewEvent.selectAll, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkTextViewPtr, handlerId)
    }

    public actual fun scrollToMark(
        mark: TextMark,
        withinMargin: Double,
        useAlign: Boolean,
        xAlign: Double,
        yAlign: Double
    ) {
        gtk_text_view_scroll_to_mark(
            text_view = gtkTextViewPtr,
            mark = mark.gtkTextMarkPtr,
            within_margin = withinMargin,
            use_align = if (useAlign) TRUE else FALSE,
            xalign = xAlign,
            yalign = yAlign
        )
    }

    public actual fun scrollToIterator(
        iter: TextBufferIterator,
        withinMargin: Double,
        useAlign: Boolean,
        xAlign: Double,
        yAlign: Double
    ): Boolean = gtk_text_view_scroll_to_iter(
        text_view = gtkTextViewPtr,
        iter = iter.gtkTextIterPtr,
        within_margin = withinMargin,
        use_align = if (useAlign) TRUE else FALSE,
        xalign = xAlign,
        yalign = yAlign
    ) == TRUE

    public actual fun scrollMarkOnScreen(mark: TextMark) {
        gtk_text_view_scroll_mark_onscreen(gtkTextViewPtr, mark.gtkTextMarkPtr)
    }

    public actual fun moveMarkOnScreen(mark: TextMark): Boolean =
        gtk_text_view_move_mark_onscreen(gtkTextViewPtr, mark.gtkTextMarkPtr) == TRUE

    public actual fun fetchLineAtYPos(yPos: Int): Pair<Int, TextBufferIterator> = memScoped {
        val lineTop = alloc<IntVar>()
        val iterator = TextBufferIterator()
        gtk_text_view_get_line_at_y(
            text_view = gtkTextViewPtr,
            target_iter = iterator.gtkTextIterPtr,
            y = yPos,
            line_top = lineTop.ptr
        )
        lineTop.value to iterator
    }

    public actual fun fetchLineAtYPosRange(): Triple<Int, Int, TextBufferIterator> = memScoped {
        val yPos = alloc<IntVar>()
        val height = alloc<IntVar>()
        val iterator = TextBufferIterator()
        gtk_text_view_get_line_yrange(
            text_view = gtkTextViewPtr,
            iter = iterator.gtkTextIterPtr,
            y = yPos.ptr,
            height = height.ptr
        )
        Triple(yPos.value, height.value, iterator)
    }

    public actual fun fetchIteratorAtLocation(xPos: Int, yPos: Int): Pair<Boolean, TextBufferIterator> {
        val iterator = TextBufferIterator()
        val overTxt = gtk_text_view_get_iter_at_location(
            text_view = gtkTextViewPtr,
            iter = iterator.gtkTextIterPtr,
            x = xPos,
            y = yPos
        ) == TRUE
        return overTxt to iterator
    }

    public actual fun fetchIteratorAtPosition(
        xPos: Int,
        yPos: Int
    ): Triple<Boolean, Int, TextBufferIterator> = memScoped {
        val iterator = TextBufferIterator()
        val trailing = alloc<IntVar>()
        val overTxt = gtk_text_view_get_iter_at_position(
            text_view = gtkTextViewPtr,
            iter = iterator.gtkTextIterPtr,
            trailing = trailing.ptr,
            x = xPos,
            y = yPos
        ) == TRUE
        Triple(overTxt, trailing.value, iterator)
    }

    /**
     * Converts coordinate ([bufferX] , [bufferY] ) to coordinates for the [window type][type], and returns the result.
     * Note that you can’t convert coordinates for a non existing window.
     * @return A Pair containing the following:
     * 1. winX: Int
     * 2. winY: Int
     * @see changeBorderWindowSize
     */
    public fun bufferToWindowCoords(bufferX: Int, bufferY: Int, type: GtkTextWindowType): Pair<Int, Int> = memScoped {
        val winX = alloc<IntVar>()
        val winY = alloc<IntVar>()
        gtk_text_view_buffer_to_window_coords(
            text_view = gtkTextViewPtr,
            win = type,
            buffer_x = bufferX,
            buffer_y = bufferY,
            window_x = winX.ptr,
            window_y = winY.ptr
        )
        winX.value to winY.value
    }

    /**
     * Converts coordinates on the window identified by [type] to buffer coordinates, and returns the result. Note that
     * you can’t convert coordinates for a non existing window.
     * @param type The window type.
     * @param winX Window X coordinate.
     * @param winY Window Y coordinate.
     * @return A Pair containg the following:
     * 1. bufferX: Int
     * 2. bufferY: Int
     * @see changeBorderWindowSize
     */
    public fun windowToBufferCoords(type: GtkTextWindowType, winX: Int, winY: Int): Pair<Int, Int> = memScoped {
        val bufferX = alloc<IntVar>()
        val bufferY = alloc<IntVar>()
        gtk_text_view_window_to_buffer_coords(
            text_view = gtkTextViewPtr,
            win = type,
            window_x = winX,
            window_y = winY,
            buffer_x = bufferX.ptr,
            buffer_y = bufferY.ptr
        )
        bufferX.value to bufferY.value
    }

    /**
     * Sets the width of `GTK_TEXT_WINDOW_LEFT` or `GTK_TEXT_WINDOW_RIGHT`, or the height of `GTK_TEXT_WINDOW_TOP` or
     * `GTK_TEXT_WINDOW_BOTTOM`. Automatically destroys the corresponding window if the size is set to *0*, and creates
     * the window if the size is set to non zero. This function can only be used for the “border windows,” it doesn’t
     * work with `GTK_TEXT_WINDOW_WIDGET`, `GTK_TEXT_WINDOW_TEXT`, or `GTK_TEXT_WINDOW_PRIVATE`.
     * @param type The type of window to affect.
     * @param size Width or height of the window.
     */
    public fun changeBorderWindowSize(type: GtkTextWindowType, size: Int) {
        gtk_text_view_set_border_window_size(text_view = gtkTextViewPtr, type = type, size = size)
    }

    /**
     * Gets the width of the specified border window.
     * @param type The type of window to return the size from.
     * @return Width of the window.
     * @see changeBorderWindowSize
     */
    public fun fetchBorderWindowSize(type: GtkTextWindowType): Int =
        gtk_text_view_get_border_window_size(gtkTextViewPtr, type)

    public actual fun forwardDisplayLine(iter: TextBufferIterator): Boolean =
        gtk_text_view_forward_display_line(gtkTextViewPtr, iter.gtkTextIterPtr) == TRUE

    public actual fun backwardDisplayLine(iter: TextBufferIterator): Boolean =
        gtk_text_view_backward_display_line(gtkTextViewPtr, iter.gtkTextIterPtr) == TRUE

    public actual fun forwardDisplayLineEnd(iter: TextBufferIterator): Boolean =
        gtk_text_view_forward_display_line_end(gtkTextViewPtr, iter.gtkTextIterPtr) == TRUE

    public actual fun backwardDisplayLineStart(iter: TextBufferIterator): Boolean =
        gtk_text_view_backward_display_line_start(gtkTextViewPtr, iter.gtkTextIterPtr) == TRUE

    public actual fun startsDisplayLine(iter: TextBufferIterator): Boolean =
        gtk_text_view_starts_display_line(gtkTextViewPtr, iter.gtkTextIterPtr) == TRUE

    public actual fun moveVisually(iter: TextBufferIterator, count: Int): Boolean =
        gtk_text_view_move_visually(text_view = gtkTextViewPtr, iter = iter.gtkTextIterPtr, count = count) == TRUE
}

public fun textViewWidget(
    textViewPtr: CPointer<GtkTextView>? = null,
    textBufferPtr: CPointer<GtkTextBuffer>? = null,
    init: TextView.() -> Unit = {}
): TextView {
    val textView =
        if (textViewPtr != null && textBufferPtr == null) TextView.fromTextViewPointer(textViewPtr)
        else if (textBufferPtr != null && textViewPtr == null) TextView.fromTextBufferPointer(textBufferPtr)
        else TextView.create()
    textView.init()
    return textView
}

/**
 * The event handler for the *backspace* event. Arguments:
 * 1. textView: CPointer<GtkTextView>
 * 2. userData: gpointer
 */
public typealias BackspaceHandler = CFunction<(textView: CPointer<GtkTextView>, userData: gpointer) -> Unit>

/**
 * The event handler for the *copy-clipboard* event. Arguments:
 * 1. textView: CPointer<GtkTextView>
 * 2. userData: gpointer
 */
public typealias CopyClipboardHandler = CFunction<(textView: CPointer<GtkTextView>, userData: gpointer) -> Unit>

/**
 * The event handler for the *cut-clipboard* event. Arguments:
 * 1. textView: CPointer<GtkTextView>
 * 2. userData: gpointer
 */
public typealias CutClipboardHandler = CFunction<(textView: CPointer<GtkTextView>, userData: gpointer) -> Unit>

/**
 * The event handler for the *insert-at-cursor* event. Arguments:
 * 1. textView: CPointer<GtkTextView>
 * 2. string: CPointer<ByteVar> (represents a String)
 * 3. userData: gpointer
 */
public typealias InsertAtCursorHandler = CFunction<(
    textView: CPointer<GtkTextView>,
    string: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *paste-clipboard* event. Arguments:
 * 1. textView: CPointer<GtkTextView>
 * 2. userData: gpointer
 */
public typealias PasteClipboardHandler = CFunction<(textView: CPointer<GtkTextView>, userData: gpointer) -> Unit>

/**
 * The event handler for the *populate-popup* event. Arguments:
 * 1. textView: CPointer<GtkTextView>
 * 2. popup: CPointer<GtkWidget>
 * 3. userData: gpointer
 */
public typealias PopuplatePopupHandler = CFunction<(
    textView: CPointer<GtkTextView>,
    popup: CPointer<GtkWidget>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *select-all* event. Arguments:
 * 1. textView: CPointer<GtkTextView>
 * 2. select: Int (represents a Boolean)
 * 3. userData: gpointer
 */
public typealias SelectAllHandler = CFunction<(
    textView: CPointer<GtkTextView>,
    select: Int,
    userData: gpointer
) -> Unit>

public fun CPointer<GtkTextView>?.toTextView(): TextView = TextView.fromTextViewPointer(this)
