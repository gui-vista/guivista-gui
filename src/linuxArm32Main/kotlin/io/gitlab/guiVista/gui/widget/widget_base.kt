package io.gitlab.guiVista.gui.widget

import glib2.*
import gtk3.*
import io.gitlab.guiVista.core.*
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.keyboard.AcceleratorGroup
import io.gitlab.guiVista.io.application.action.ActionGroup
import io.gitlab.guiVista.io.application.action.ActionGroupBase
import kotlinx.cinterop.*

public actual interface WidgetBase : InitiallyUnowned {
    public val gtkWidgetPtr: CPointer<GtkWidget>?
    public actual val styleContext: StyleContext
        get() = StyleContext.fromPointer(gtk_widget_get_style_context(gtkWidgetPtr))
    public actual var canFocus: Boolean
        set(value) = gtk_widget_set_can_focus(gtkWidgetPtr, if (value) TRUE else FALSE)
        get() = gtk_widget_get_can_focus(gtkWidgetPtr) == TRUE
    public actual var hasTooltip: Boolean
        set(value) = gtk_widget_set_has_tooltip(gtkWidgetPtr, if (value) TRUE else FALSE)
        get() = gtk_widget_get_has_tooltip(gtkWidgetPtr) == TRUE
    public actual var tooltipText: String
        set(value) = gtk_widget_set_tooltip_text(gtkWidgetPtr, value)
        get() {
            val tmp = gtk_widget_get_tooltip_text(gtkWidgetPtr)
            val result = tmp?.toKString() ?: ""
            g_free(tmp)
            return result
        }
    public actual var visible: Boolean
        set(value) = gtk_widget_set_visible(gtkWidgetPtr, if (value) TRUE else FALSE)
        get() = gtk_widget_get_visible(gtkWidgetPtr) == TRUE
    public actual var marginBottom: Int
        set(value) {
            if (value in marginRange) gtk_widget_set_margin_bottom(gtkWidgetPtr, value)
        }
        get() = gtk_widget_get_margin_bottom(gtkWidgetPtr)
    public actual var marginEnd: Int
        set(value) {
            if (value in marginRange) gtk_widget_set_margin_end(gtkWidgetPtr, value)
        }
        get() = gtk_widget_get_margin_end(gtkWidgetPtr)
    public actual var marginStart: Int
        set(value) {
            if (value in marginRange) gtk_widget_set_margin_start(gtkWidgetPtr, value)
        }
        get() = gtk_widget_get_margin_start(gtkWidgetPtr)
    public actual var marginTop: Int
        set(value) {
            if (value in marginRange) gtk_widget_set_margin_top(gtkWidgetPtr, value)
        }
        get() = gtk_widget_get_margin_top(gtkWidgetPtr)
    public actual var appPaintable: Boolean
        get() = gtk_widget_get_app_paintable(gtkWidgetPtr) == TRUE
        set(value) = gtk_widget_set_app_paintable(gtkWidgetPtr, if (value) TRUE else FALSE)
    public actual var canDefault: Boolean
        get() = gtk_widget_get_can_default(gtkWidgetPtr) == TRUE
        set(value) = gtk_widget_set_can_default(gtkWidgetPtr, if (value) TRUE else FALSE)
    public actual var focusOnClick: Boolean
        get() = gtk_widget_get_focus_on_click(gtkWidgetPtr) == TRUE
        set(value) = gtk_widget_set_focus_on_click(gtkWidgetPtr, if (value) TRUE else FALSE)

    /**
     * How to distribute horizontal space if widget gets extra space.
     *
     * Data binding property name: **halign**
     */
    public var hAlign: GtkAlign
        get() = gtk_widget_get_halign(gtkWidgetPtr)
        set(value) = gtk_widget_set_halign(gtkWidgetPtr, value)
    public actual val hasDefault: Boolean
        get() = gtk_widget_has_default(gtkWidgetPtr) == TRUE
    public actual val hasFocus: Boolean
        get() = gtk_widget_has_focus(gtkWidgetPtr) == TRUE
    public actual var hExpand: Boolean
        get() = gtk_widget_get_hexpand(gtkWidgetPtr) == TRUE
        set(value) = gtk_widget_set_hexpand(gtkWidgetPtr, if (value) TRUE else FALSE)
    public actual var hExpandSet: Boolean
        get() = gtk_widget_get_hexpand_set(gtkWidgetPtr) == TRUE
        set(value) = gtk_widget_set_hexpand_set(gtkWidgetPtr, if (value) TRUE else FALSE)
    public actual val isFocus: Boolean
        get() = gtk_widget_is_focus(gtkWidgetPtr) == TRUE
    public actual var name: String
        get() = gtk_widget_get_name(gtkWidgetPtr)?.toKString() ?: ""
        set(value) = gtk_widget_set_name(gtkWidgetPtr, value)
    public actual var noShowAll: Boolean
        get() = gtk_widget_get_no_show_all(gtkWidgetPtr) == TRUE
        set(value) = gtk_widget_set_no_show_all(gtkWidgetPtr, if (value) TRUE else FALSE)
    public actual var opacity: Double
        get() = gtk_widget_get_opacity(gtkWidgetPtr)
        set(value) {
            if (value in (0.0..1.0)) gtk_widget_set_opacity(gtkWidgetPtr, value)
        }
    public actual var receivesDefault: Boolean
        get() = gtk_widget_get_receives_default(gtkWidgetPtr) == TRUE
        set(value) = gtk_widget_set_receives_default(gtkWidgetPtr, if (value) TRUE else FALSE)
    public actual val scaleFactor: Int
        get() = gtk_widget_get_scale_factor(gtkWidgetPtr)
    public actual var sensitive: Boolean
        get() = gtk_widget_get_sensitive(gtkWidgetPtr) == TRUE
        set(value) = gtk_widget_set_sensitive(gtkWidgetPtr, if (value) TRUE else FALSE)
    public actual var tooltipMarkup: String
        get() {
            val tmp = gtk_widget_get_tooltip_markup(gtkWidgetPtr)
            val result = tmp?.toKString() ?: ""
            g_free(tmp)
            return result
        }
        set(value) = gtk_widget_set_tooltip_markup(gtkWidgetPtr, value)

    /**
     * How to distribute vertical space if widget gets extra space. Default value is *GtkAlign.GTK_ALIGN_FILL*.
     *
     * Data binding property name: **valign**
     */
    public var vAlign: GtkAlign
        get() = gtk_widget_get_valign(gtkWidgetPtr)
        set(value) = gtk_widget_set_valign(gtkWidgetPtr, value)
    public actual var vExpand: Boolean
        get() = gtk_widget_get_vexpand(gtkWidgetPtr) == TRUE
        set(value) = gtk_widget_set_vexpand(gtkWidgetPtr, if (value) TRUE else FALSE)
    public actual var vExpandSet: Boolean
        get() = gtk_widget_get_vexpand_set(gtkWidgetPtr) == TRUE
        set(value) = gtk_widget_set_vexpand_set(gtkWidgetPtr, if (value) TRUE else FALSE)

    private companion object {
        private val marginRange = 0..32767
    }

    public actual fun addAccelerator(
        accelEvent: String,
        accelGroup: AcceleratorGroup,
        accelKey: UInt,
        accelMods: UInt,
        accelFlags: UInt
    ) {
        gtk_widget_add_accelerator(
            widget = gtkWidgetPtr,
            accel_signal = accelEvent,
            accel_group = accelGroup.gtkAccelGroupPtr,
            accel_key = accelKey,
            accel_mods = accelMods,
            accel_flags = accelFlags
        )
    }

    public actual fun removeAccelerator(accelGroup: AcceleratorGroup, accelKey: UInt, accelMods: UInt): Boolean =
        gtk_widget_remove_accelerator(
            widget = gtkWidgetPtr,
            accel_group = accelGroup.gtkAccelGroupPtr,
            accel_key = accelKey,
            accel_mods = accelMods
        ) == TRUE

    public actual fun changeAccelPath(accelPath: String, accelGroup: AcceleratorGroup) {
        gtk_widget_set_accel_path(widget = gtkWidgetPtr, accel_path = accelPath,
            accel_group = accelGroup.gtkAccelGroupPtr)
    }

    public actual fun listAccelClosures(): DoublyLinkedList =
        DoublyLinkedList.fromPointer(gtk_widget_list_accel_closures(gtkWidgetPtr))

    public actual infix fun canActivateAccel(eventId: UInt): Boolean =
        gtk_widget_can_activate_accel(gtkWidgetPtr, eventId) == TRUE

    public actual fun changeMargins(bottom: Int, end: Int, start: Int, top: Int) {
        marginBottom = bottom
        marginEnd = end
        marginStart = start
        marginTop = top
    }

    public actual fun changeAllMargins(value: Int) {
        marginBottom = value
        marginEnd = value
        marginStart = value
        marginTop = value
    }

    public actual fun clearMargins() {
        marginBottom = 0
        marginEnd = 0
        marginStart = 0
        marginTop = 0
    }

    public actual fun grabFocus() {
        gtk_widget_grab_focus(gtkWidgetPtr)
    }

    /**
     * Connects the *grab-focus* event to a [handler] on a [WidgetBase]. This event is used when a widget is
     * "grabbing" focus.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectGrabFocusEvent(handler: CPointer<GrabFocusHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWidgetPtr, signal = WidgetBaseEvent.grabFocus, slot = handler,
            data = userData)

    /**
     * Connects the *show* event to a [handler] on a [WidgetBase]. This event is used when a widget is shown.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectShowEvent(handler: CPointer<ShowHandler>,
                                userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWidgetPtr, signal = WidgetBaseEvent.show, slot = handler, data = userData)

    /**
     * Connects the *hide* event to a [handler] on a [WidgetBase]. This event is used when a widget is hidden.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectHideEvent(handler: CPointer<HideHandler>,
                                userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWidgetPtr, signal = WidgetBaseEvent.hide, slot = handler, data = userData)

    /**
     * Connects the *configure-event* event to a [handler] on a [WidgetBase]. This event is used when the size, position
     * or stacking of the widget's window has changed. To receive this event, the GdkWindow associated to the widget
     * needs to enable the `GDK_STRUCTURE_MASK` mask. GDK will enable this mask automatically for all new windows.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectConfigureEvent(handler: CPointer<ConfigureEventHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWidgetPtr, signal = WidgetBaseEvent.configure, slot = handler,
            data = userData)

    /**
     * Connects the *key-press-event* event to a [handler] on a [WidgetBase]. This event is used when a key is pressed.
     * The event emission will reoccur at the key-repeat rate when the key is kept pressed. To receive this event the
     * GdkWindow associated to the widget needs to enable the GDK_KEY_PRESS_MASK mask.
     *
     * This event will be sent to the grab widget if there is one.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectKeyPressEvent(handler: CPointer<KeyPressEventHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWidgetPtr, signal = WidgetBaseEvent.keyPress, slot = handler, data = userData)

    /**
     * Connects the *key-release-event* event to a [handler] on a [WidgetBase]. This event is used when a key is
     * released. To receive this event the GdkWindow associated to the widget needs to enable the
     * `GDK_KEY_RELEASE_MASK` mask. This event will be sent to the grab widget if there is one.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectKeyReleaseEvent(handler: CPointer<KeyReleaseEventHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWidgetPtr, signal = WidgetBaseEvent.keyRelease, slot = handler,
            data = userData)

    /**
     * Connects the *activate* event to a [handler] on a [WidgetBase]. This event is used when an action occurs on a
     * widget.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActivateEvent(handler: CPointer<ActivateHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWidgetPtr, signal = WidgetBaseEvent.activate, slot = handler, data = userData)

    /**
     * Connects the *realize* event to a [handler] on a [WidgetBase]. This event is used when the widget is associated
     * with a window, which means that `realize` has been called or the widget has been mapped (is going to be drawn).
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRealizeEvent(handler: CPointer<RealizeHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWidgetPtr, signal = WidgetBaseEvent.realize, slot = handler, data = userData)

    /**
     * Connects the *draw* event to a [handler] on a [WidgetBase]. This event is used when a widget is supposed to
     * render itself. The widget‘s top left corner must be painted at the origin of the passed in cairoCtx, and be
     * sized to the values returned by `gtk_widget_get_allocated_width()` and `gtk_widget_get_allocated_height()`.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectDrawEvent(handler: CPointer<DrawHandler>,
                                userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWidgetPtr, signal = WidgetBaseEvent.draw, slot = handler, data = userData)

    /**
     * Connects the *size-allocate* event to a [handler] on a [WidgetBase]. This event occurs when the widget changes
     * size.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSizeAllocateEvent(handler: CPointer<SizeAllocateHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWidgetPtr, signal = WidgetBaseEvent.sizeAllocate, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkWidgetPtr, handlerId)
    }

    public actual fun showAll() {
        gtk_widget_show_all(gtkWidgetPtr)
    }

    override fun close() {
        gtk_widget_destroy(gtkWidgetPtr)
    }

    public actual fun insertActionGroup(name: String, group: ActionGroup?) {
        gtk_widget_insert_action_group(widget = gtkWidgetPtr, name = name, group = group?.gActionGroupPtr)
    }

    public actual fun listActionPrefixes(): Array<String> {
        val tmpList = mutableListOf<String>()
        val actionPrefixes = gtk_widget_list_action_prefixes(gtkWidgetPtr)
        var pos = 0
        var tmpItem: CPointer<ByteVar>?
        do {
            tmpItem = actionPrefixes?.get(pos)
            if (tmpItem != null) tmpList += tmpItem.toKString()
            pos++
        } while (tmpItem != null)
        return tmpList.toTypedArray()
    }

    public actual fun fetchActionGroup(prefix: String): ActionGroupBase? {
        val ptr = gtk_widget_get_action_group(gtkWidgetPtr, prefix)
        return if (ptr != null) ActionGroup.fromPointer(ptr) else null
    }
}

/**
 * The event handler for the *grab-focus* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. userData: gpointer
 */
public typealias GrabFocusHandler = CFunction<(widget: CPointer<GtkWidget>, userData: gpointer) -> Unit>

/**
 * The event handler for the *show* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. userData: gpointer
 */
public typealias ShowHandler = CFunction<(widget: CPointer<GtkWidget>, userData: gpointer) -> Unit>

/**
 * The event handler for the *hide* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. userData: gpointer
 */
public typealias HideHandler = CFunction<(widget: CPointer<GtkWidget>, userData: gpointer) -> Unit>

/**
 * The event handler for the *configure-event* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. event: CPointer<GdkEvent>
 * 3. userData: gpointer
 *
 * Return *true* (an Int the represents a Boolean) to stop other handlers from being invoked for the event, otherwise
 * *false* to propagate the event further.
 */
public typealias ConfigureEventHandler =
    CFunction<(widget: CPointer<GtkWidget>, event: CPointer<GdkEvent>, userData: gpointer) -> Int>

/**
 * The event handler for the *key-press-event* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. event: CPointer<GdkEvent>
 * 3. userData: gpointer
 *
 * Return *true* (an Int the represents a Boolean) to stop other handlers from being invoked for the event, otherwise
 * *false* to propagate the event further.
 */
public typealias KeyPressEventHandler =
    CFunction<(widget: CPointer<GtkWidget>, event: CPointer<GdkEvent>, userData: gpointer) -> Int>

/**
 * The event handler for the *key-release-event* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. event: CPointer<GdkEvent>
 * 3. userData: gpointer
 *
 * Return *true* (an Int the represents a Boolean) to stop other handlers from being invoked for the event, otherwise
 * *false* to propagate the event further.
 */
public typealias KeyReleaseEventHandler =
    CFunction<(widget: CPointer<GtkWidget>, event: CPointer<GdkEvent>, userData: gpointer) -> Int>

/**
 * The event handler for the *activate* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. userData: gpointer
 */
public typealias ActivateHandler = CFunction<(widget: CPointer<GtkWidget>, userData: gpointer) -> Unit>

/**
 * The event handler for the *realize* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. userData: gpointer
 */
public typealias RealizeHandler = CFunction<(widget: CPointer<GtkWidget>, userData: gpointer) -> Unit>

/**
 * The event handler for the *size-allocate* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. allocate: CPointer<GtkAllocation>
 * 3. userData: gpointer
 */
public typealias SizeAllocateHandler =
    CFunction<(widget: CPointer<GtkWidget>, allocate: CPointer<GtkAllocation>, userData: gpointer) -> Unit>

/**
 * The event handler for the *draw* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. cairoCtx: CPointer<cairo_t>
 * 3. userData: gpointer
 * Return an Int (represents a Boolean).
 */
public typealias DrawHandler =
    CFunction<(widget: CPointer<GtkWidget>, cairoCtx: CPointer<cairo_t>, userData: gpointer) -> Int>
