package io.gitlab.guiVista.gui.widget

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.layout.ContainerBase
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

public actual class Notebook private constructor(ptr: CPointer<GtkNotebook>? = null) : ContainerBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_notebook_new()
    public val gtkNotebookPtr: CPointer<GtkNotebook>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual var showTabs: Boolean
        get() = gtk_notebook_get_show_tabs(gtkNotebookPtr) == TRUE
        set(value) = gtk_notebook_set_show_tabs(gtkNotebookPtr, if (value) TRUE else FALSE)

    public actual var showBorder: Boolean
        get() = gtk_notebook_get_show_border(gtkNotebookPtr) == TRUE
        set(value) = gtk_notebook_set_show_border(gtkNotebookPtr, if (value) TRUE else FALSE)

    public actual var scrollable: Boolean
        get() = gtk_notebook_get_scrollable(gtkNotebookPtr) == TRUE
        set(value) = gtk_notebook_set_scrollable(gtkNotebookPtr, if (value) TRUE else FALSE)

    public actual var groupName: String
        get() = gtk_notebook_get_group_name(gtkNotebookPtr)?.toKString() ?: ""
        set(value) = gtk_notebook_set_group_name(gtkNotebookPtr, value)

    public actual val currentPage: Int
        get() = gtk_notebook_get_current_page(gtkNotebookPtr)

    public actual val totalPages: Int
        get() = gtk_notebook_get_n_pages(gtkNotebookPtr)

    public actual companion object {
        public actual fun create(): Notebook = Notebook()

        public fun fromPointer(ptr: CPointer<GtkNotebook>?): Notebook = Notebook(ptr)
    }

    public actual fun appendPage(child: WidgetBase, tabLabel: WidgetBase?): Int =
        gtk_notebook_append_page(notebook = gtkNotebookPtr, child = child.gtkWidgetPtr,
            tab_label = tabLabel?.gtkWidgetPtr)

    public actual fun appendPageMenu(child: WidgetBase, tabLabel: WidgetBase?, menuLabel: WidgetBase?): Int =
        gtk_notebook_append_page_menu(
            notebook = gtkNotebookPtr,
            child = child.gtkWidgetPtr,
            tab_label = tabLabel?.gtkWidgetPtr,
            menu_label = menuLabel?.gtkWidgetPtr
        )

    public actual fun prependPage(child: WidgetBase, tabLabel: WidgetBase?): Int =
        gtk_notebook_prepend_page(notebook = gtkNotebookPtr, child = child.gtkWidgetPtr,
            tab_label = tabLabel?.gtkWidgetPtr)

    public actual fun prependPageMenu(child: WidgetBase, tabLabel: WidgetBase?, menuLabel: WidgetBase?): Int =
        gtk_notebook_prepend_page_menu(
            notebook = gtkNotebookPtr,
            child = child.gtkWidgetPtr,
            tab_label = tabLabel?.gtkWidgetPtr,
            menu_label = menuLabel?.gtkWidgetPtr
        )

    public actual fun insertPage(child: WidgetBase, tabLabel: WidgetBase?, pos: Int): Int =
        gtk_notebook_insert_page(
            notebook = gtkNotebookPtr,
            child = child.gtkWidgetPtr,
            tab_label = tabLabel?.gtkWidgetPtr,
            position = pos
        )

    public actual fun insertPageMenu(child: WidgetBase, pos: Int, tabLabel: WidgetBase?, menuLabel: WidgetBase?): Int =
        gtk_notebook_insert_page_menu(
            notebook = gtkNotebookPtr,
            child = child.gtkWidgetPtr,
            tab_label = tabLabel?.gtkWidgetPtr,
            menu_label = menuLabel?.gtkWidgetPtr,
            position = pos
        )

    public actual fun removePage(pageNum: Int) {
        gtk_notebook_remove_page(gtkNotebookPtr, pageNum)
    }

    public actual fun detachTab(child: WidgetBase) {
        gtk_notebook_detach_tab(gtkNotebookPtr, child.gtkWidgetPtr)
    }

    public actual fun pageNumber(child: WidgetBase): Int = gtk_notebook_page_num(gtkNotebookPtr, child.gtkWidgetPtr)

    public actual fun nextPage() {
        gtk_notebook_next_page(gtkNotebookPtr)
    }

    public actual fun previousPage() {
        gtk_notebook_prev_page(gtkNotebookPtr)
    }

    public actual fun reorderChild(child: WidgetBase, pos: Int) {
        gtk_notebook_reorder_child(notebook = gtkNotebookPtr, child = child.gtkWidgetPtr, position = pos)
    }

    public actual fun enablePopup() {
        gtk_notebook_popup_enable(gtkNotebookPtr)
    }

    public actual fun disablePopup() {
        gtk_notebook_popup_disable(gtkNotebookPtr)
    }

    public actual fun fetchMenuLabel(child: WidgetBase): WidgetBase? {
        val ptr = gtk_notebook_get_menu_label(gtkNotebookPtr, child.gtkWidgetPtr)
        return if (ptr != null) Widget.fromPointer(ptr) else null
    }

    public actual fun fetchPageByNum(pageNum: Int): WidgetBase? {
        val ptr = gtk_notebook_get_nth_page(gtkNotebookPtr, pageNum)
        return if (ptr != null) Widget.fromPointer(ptr) else null
    }

    public actual fun fetchTabLabel(child: WidgetBase): WidgetBase? {
        val ptr = gtk_notebook_get_tab_label(gtkNotebookPtr, child.gtkWidgetPtr)
        return if (ptr != null) Widget.fromPointer(ptr) else null
    }

    public actual fun changeMenuLabel(child: WidgetBase, menuLabel: WidgetBase?) {
        gtk_notebook_set_menu_label(notebook = gtkNotebookPtr, child = child.gtkWidgetPtr,
            menu_label = menuLabel?.gtkWidgetPtr)
    }

    public actual fun changeMenuLabelText(child: WidgetBase, menuText: String) {
        gtk_notebook_set_menu_label_text(notebook = gtkNotebookPtr, child = child.gtkWidgetPtr, menu_text = menuText)
    }

    public actual fun changeTabLabel(child: WidgetBase, tabLabel: WidgetBase?) {
        gtk_notebook_set_tab_label(notebook = gtkNotebookPtr, child = child.gtkWidgetPtr,
            tab_label = tabLabel?.gtkWidgetPtr)
    }

    public actual fun changeTabLabelText(child: WidgetBase, tabText: String) {
        gtk_notebook_set_tab_label_text(notebook = gtkNotebookPtr, child = child.gtkWidgetPtr, tab_text = tabText)
    }

    public actual fun changeTabReorderable(child: WidgetBase, reorderable: Boolean) {
        gtk_notebook_set_tab_reorderable(notebook = gtkNotebookPtr, child = child.gtkWidgetPtr,
            reorderable = if (reorderable) TRUE else FALSE)
    }

    public actual fun changeTabDetachable(child: WidgetBase, detachable: Boolean) {
        gtk_notebook_set_tab_detachable(notebook = gtkNotebookPtr, child = child.gtkWidgetPtr,
            detachable = if (detachable) TRUE else FALSE)
    }

    public actual fun fetchMenuLabelText(child: WidgetBase): String =
        gtk_notebook_get_menu_label_text(gtkNotebookPtr, child.gtkWidgetPtr)?.toKString() ?: ""

    public actual fun fetchTabLabelText(child: WidgetBase): String =
        gtk_notebook_get_tab_label_text(gtkNotebookPtr, child.gtkWidgetPtr)?.toKString() ?: ""

    public actual fun fetchTabReorderable(child: WidgetBase): Boolean =
        gtk_notebook_get_tab_reorderable(gtkNotebookPtr, child.gtkWidgetPtr) == TRUE

    public actual fun fetchTabDetachable(child: WidgetBase): Boolean =
        gtk_notebook_get_tab_detachable(gtkNotebookPtr, child.gtkWidgetPtr) == TRUE

    /**
     * Sets widget as one of the action widgets. Depending on the [pack type][packType] the [widget] will be placed
     * before or after the tabs. You can use a [io.gitlab.guiVista.gui.layout.Box] if you need to pack more than one
     * widget on the same side.
     *
     * Note that action widgets are “internal” children of the notebook.
     * @param widget The widget to use.
     * @param packType Pack type of the action widget.
     */
    public fun changeActionWidget(widget: WidgetBase, packType: GtkPackType) {
        gtk_notebook_set_action_widget(notebook = gtkNotebookPtr, widget = widget.gtkWidgetPtr, pack_type = packType)
    }

    /**
     * Gets one of the action widgets.
     * @param packType The pack type of the action widget to receive.
     * @return The action widget with the given [packType] or *null* when this action widget has not been set.
     * @see changeActionWidget
     */
    public fun fetchActionWidget(packType: GtkPackType): WidgetBase? {
        val ptr = gtk_notebook_get_action_widget(gtkNotebookPtr, packType)
        return if (ptr != null) Widget.fromPointer(ptr) else null
    }

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkNotebookPtr, handlerId)
    }

    /**
     * Connects the *change-current-page* event to a [handler] on a [Notebook].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectChangeCurrentPageEvent(handler: CPointer<ChangeCurrentPageHandler>,
                                             userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkNotebookPtr, signal = NotebookEvent.changeCurrentPage, slot = handler, data = userData)

    /**
     * Connects the *create-window* event to a [handler] on a [Notebook]. This event occurs when a detachable tab is
     * dropped on the root window. A handler for this event can create a window containing a notebook where the tab
     * will be attached. It is also responsible for moving/resizing the window and adding the necessary properties to
     * the notebook (e.g. **group-name**).
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCreateWindowEvent(handler: CPointer<CreateWindowHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkNotebookPtr, signal = NotebookEvent.createWindow, slot = handler, data = userData)

    /**
     * Connects the *focus-tab* event to a [handler] on a [Notebook].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectFocusTabEvent(handler: CPointer<FocusTabHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkNotebookPtr, signal = NotebookEvent.focusTab, slot = handler, data = userData)

    /**
     * Connects the *move-focus-out* event to a [handler] on a [Notebook].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectMoveFocusOutEvent(handler: CPointer<MoveFocusOutHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkNotebookPtr, signal = NotebookEvent.moveFocusOut, slot = handler, data = userData)

    /**
     * Connects the *page-added* event to a [handler] on a [Notebook]. This event occurs in the notebook right after a
     * page is added to the notebook.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPageAddedEvent(handler: CPointer<PageAddedHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkNotebookPtr, signal = NotebookEvent.pageAdded, slot = handler, data = userData)

    /**
     * Connects the *page-remove* event to a [handler] on a [Notebook]. This event occurs in the notebook right after a
     * page is removed from the notebook.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPageRemovedEvent(handler: CPointer<PageRemovedHandler>,
                                       userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkNotebookPtr, signal = NotebookEvent.pageRemoved, slot = handler, data = userData)

    /**
     * Connects the *page-reordered* event to a [handler] on a [Notebook]. This event occurs in the notebook right
     * after a page has been reordered.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPageReorderedEvent(handler: CPointer<PageReorderedHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkNotebookPtr, signal = NotebookEvent.pageReordered, slot = handler, data = userData)

    /**
     * Connects the *reorder-tab* event to a [handler] on a [Notebook].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectReorderTabEvent(handler: CPointer<ReorderTabHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkNotebookPtr, signal = NotebookEvent.reorderTab, slot = handler, data = userData)

    /**
     * Connects the *select-page* event to a [handler] on a [Notebook].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSelectPageEvent(handler: CPointer<SelectPageHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkNotebookPtr, signal = NotebookEvent.selectPage, slot = handler, data = userData)

    /**
     * Connects the *switch-page* event to a [handler] on a [Notebook].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSwitchPageEvent(handler: CPointer<SwitchPageHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkNotebookPtr, signal = NotebookEvent.switchPage, slot = handler, data = userData)
}

/**
 * The event handler for the *change-current-page* event. Arguments:
 * 1. notebook: CPointer<GtkNotebook>
 * 2. arg1: Int
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias ChangeCurrentPageHandler = CFunction<(
    notebook: CPointer<GtkNotebook>,
    arg1: Int,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *create-window* event. Arguments:
 * 1. notebook: CPointer<GtkNotebook>
 * 2. page: CPointer<GtkWidget>
 * 3. xPos: Int
 * 4. yPos: Int
 * 5. userData: gpointer
 * Returns a *CPointer<GtkNotebook>?*.
 */
public typealias CreateWindowHandler = CFunction<(
    notebook: CPointer<GtkNotebook>,
    page: CPointer<GtkWidget>,
    xPos: Int,
    yPos: Int,
    userData: gpointer
) -> CPointer<GtkNotebook>?>

/**
 * The event handler for the *focus-tab* event. Arguments:
 * 1. notebook: CPointer<GtkNotebook>
 * 2. arg1: GtkNotebookTab
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias FocusTabHandler = CFunction<(
    notebook: CPointer<GtkNotebook>,
    arg1: GtkNotebookTab,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *move-focus-out* event. Arguments:
 * 1. notebook: CPointer<GtkNotebook>
 * 2. arg1: GtkDirectionType
 * 3. userData: gpointer
 */
public typealias MoveFocusOutHandler = CFunction<(
    notebook: CPointer<GtkNotebook>,
    arg1: GtkDirectionType,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *page-added* event. Arguments:
 * 1. notebook: CPointer<GtkNotebook>
 * 2. child: CPointer<GtkWidget>
 * 3. pageNum: UInt
 * 4. userData: gpointer
 */
public typealias PageAddedHandler = CFunction<(
    notebook: CPointer<GtkNotebook>,
    child: CPointer<GtkWidget>,
    pageNum: UInt,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *page-removed* event. Arguments:
 * 1. notebook: CPointer<GtkNotebook>
 * 2. child: CPointer<GtkWidget>
 * 3. pageNum: UInt
 * 4. userData: gpointer
 */
public typealias PageRemovedHandler = CFunction<(
    notebook: CPointer<GtkNotebook>,
    child: CPointer<GtkWidget>,
    pageNum: UInt,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *page-reordered* event. Arguments:
 * 1. notebook: CPointer<GtkNotebook>
 * 2. child: CPointer<GtkWidget>
 * 3. pageNum: UInt
 * 4. userData: gpointer
 */
public typealias PageReorderedHandler = CFunction<(
    notebook: CPointer<GtkNotebook>,
    child: CPointer<GtkWidget>,
    pageNum: UInt,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *reorder-tab* event. Arguments:
 * 1. notebook: CPointer<GtkNotebook>
 * 2. arg1: CPointer<GtkWidget>
 * 3. arg2: Int (represents a Boolean)
 * 4. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias ReorderTabHandler = CFunction<(
    notebook: CPointer<GtkNotebook>,
    arg1: GtkDirectionType,
    arg2: Int,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *select-page* event. Arguments:
 * 1. notebook: CPointer<GtkNotebook>
 * 2. arg1: Int (represents a Boolean)
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias SelectPageHandler = CFunction<(
    notebook: CPointer<GtkNotebook>,
    arg1: Int,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *switch-page* event. Arguments:
 * 1. notebook: CPointer<GtkNotebook>
 * 2. page: CPointer<GtkWidget>
 * 3. pageNum: UInt,
 * 4. userData: gpointer
 */
public typealias SwitchPageHandler = CFunction<(
    notebook: CPointer<GtkNotebook>,
    page: CPointer<GtkWidget>,
    pageNum: UInt,
    userData: gpointer
) -> Unit>

public fun CPointer<GtkNotebook>?.toNotebook(): Notebook = Notebook.fromPointer(this)
