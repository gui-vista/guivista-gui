package io.gitlab.guiVista.gui.widget.dataEntry

import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.event.EventBase

public actual class SearchEntry private constructor(ptr: CPointer<GtkSearchEntry>? = null) : EntryBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_search_entry_new()
    override val gtkCellEditablePtr: CPointer<GtkCellEditable>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkSearchEntryPtr: CPointer<GtkSearchEntry>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public actual fun create(): SearchEntry = SearchEntry()

        public fun fromPointer(ptr: CPointer<GtkSearchEntry>?): SearchEntry = SearchEntry(ptr)
    }

    public actual fun handleEvent(event: EventBase): Boolean =
        gtk_search_entry_handle_event(gtkSearchEntryPtr, event.gdkEventPtr) == TRUE

    /**
     * Connects the *next-match* event to a [handler] on a [SearchEntry]. This event is a key binding event that is used
     * when the user initiates a move to the next match for the current search String. Applications should connect to
     * it to implement moving between matches. The default bindings for this event is *Ctrl-g*.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectNextMatchEvent(handler: CPointer<NextMatchHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkSearchEntryPtr, signal = SearchEntryEvent.nextMatch, slot = handler,
            data = userData)

    /**
     * Connects the *previous-match* event to a [handler] on a [SearchEntry]. This event is a key binding event that is
     * used when the user initiates a move to the previous match for the current search String. Applications should
     * connect to it to implement moving between matches. The default bindings for this event is *Ctrl-Shift-g*.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPreviousMatchEvent(handler: CPointer<PreviousMatchHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkSearchEntryPtr, signal = SearchEntryEvent.previousMatch, slot = handler,
            data = userData)

    /**
     * Connects the *search-changed* event to a [handler] on a [SearchEntry]. This event is emitted with a short delay
     * of 150 milliseconds after the last change to the entry text.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSearchChangedEvent(handler: CPointer<SearchChangedHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkSearchEntryPtr, signal = SearchEntryEvent.searchChanged, slot = handler,
            data = userData)

    /**
     * Connects the *stop-search* event to a [handler] on a [SearchEntry]. This event is a key binding event that is
     * used when the user stops a search via keyboard input. Applications should connect to it to implement hiding the
     * [SearchEntry] in this case. The default bindings for this event is *Escape*.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectStopSearchEvent(handler: CPointer<StopSearchHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkSearchEntryPtr, signal = SearchEntryEvent.stopSearch, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkSearchEntryPtr, handlerId)
    }
}

public fun searchEntryWidget(
    ptr: CPointer<GtkSearchEntry>? = null,
    init: SearchEntry.() -> Unit = {}
): SearchEntry {
    val searchEntry = if (ptr != null) SearchEntry.fromPointer(ptr) else SearchEntry.create()
    searchEntry.init()
    return searchEntry
}

/**
 * The event handler for the *next-match* event. Arguments:
 * 1. entry: CPointer<GtkSearchEntry>
 * 2. userData: gpointer
 */
public typealias NextMatchHandler = CFunction<(entry: CPointer<GtkSearchEntry>, userData: gpointer) -> Unit>

/**
 * The event handler for the *previous-match* event. Arguments:
 * 1. entry: CPointer<GtkSearchEntry>
 * 2. userData: gpointer
 */
public typealias PreviousMatchHandler = CFunction<(entry: CPointer<GtkSearchEntry>, userData: gpointer) -> Unit>

/**
 * The event handler for the *search-changed* event. Arguments:
 * 1. entry: CPointer<GtkSearchEntry>
 * 2. userData: gpointer
 */
public typealias SearchChangedHandler = CFunction<(entry: CPointer<GtkSearchEntry>, userData: gpointer) -> Unit>

/**
 * The event handler for the *stop-search* event. Arguments:
 * 1. entry: CPointer<GtkSearchEntry>
 * 2. userData: gpointer
 */
public typealias StopSearchHandler = CFunction<(app: CPointer<GtkSearchEntry>, userData: gpointer) -> Unit>

public fun CPointer<GtkSearchEntry>?.toSearchEntry(): SearchEntry = SearchEntry.fromPointer(this)
