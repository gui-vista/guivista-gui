package io.gitlab.guiVista.gui.widget.menu.item

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.widget.Actionable
import io.gitlab.guiVista.gui.widget.menu.Menu

public actual interface MenuItemBase : BinBase, Actionable {
    public val gtkMenuItemPtr: CPointer<GtkMenuItem>?
        get() = gtkWidgetPtr?.reinterpret()
    override val gtkActionablePtr: CPointer<GtkActionable>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var reserveIndicator: Boolean
        get() = gtk_menu_item_get_reserve_indicator(gtkMenuItemPtr) == TRUE
        set(value) = gtk_menu_item_set_reserve_indicator(gtkMenuItemPtr, if (value) TRUE else FALSE)
    public actual var label: String
        get() = gtk_menu_item_get_label(gtkMenuItemPtr)?.toKString() ?: ""
        set(value) = gtk_menu_item_set_label(gtkMenuItemPtr, value)
    public actual var useUnderline: Boolean
        get() = gtk_menu_item_get_use_underline(gtkMenuItemPtr) == TRUE
        set(value) = gtk_menu_item_set_use_underline(gtkMenuItemPtr, if (value) TRUE else FALSE)
    public actual var accelPath: String
        get() = gtk_menu_item_get_accel_path(gtkMenuItemPtr)?.toKString() ?: ""
        set(value) = gtk_menu_item_set_accel_path(gtkMenuItemPtr, value)
    public actual var rightJustified: Boolean
        get() = gtk_menu_item_get_right_justified(gtkMenuItemPtr) == TRUE
        set(value) = gtk_menu_item_set_right_justified(gtkMenuItemPtr, if (value) TRUE else FALSE)
    public actual var subMenu: Menu?
        get() {
            val ptr = gtk_menu_item_get_submenu(gtkMenuItemPtr)
            return if (ptr != null) Menu.fromWidgetPointer(ptr) else null
        }
        set(value) = gtk_menu_item_set_submenu(gtkMenuItemPtr, value?.gtkWidgetPtr)

    /**
     * Connects the *activate* event to a [handler] on a menu item. This event is used when the item is activated.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActivateEvent(handler: CPointer<ActivateHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkMenuItemPtr, signal = MenuItemBaseEvent.activate, slot = handler, data = userData)

    /**
     * Connects the *activate-item* event to a [handler] on a menu item. This event is used when the item is activated,
     * and if the item has a submenu. For normal applications use the "activate" event.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActivateItemEvent(handler: CPointer<ActivateItemHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkMenuItemPtr, signal = MenuItemBaseEvent.activateItem, slot = handler, data = userData)

    /**
     * Connects the *deselect* event to a [handler] on a menu item. This event is used when a item is deselected.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectDeselectEvent(handler: CPointer<DeselectHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkMenuItemPtr, signal = MenuItemBaseEvent.deselect, slot = handler, data = userData)

    /**
     * Connects the *select* event to a [handler] on a menu item. This event is used when a item is selected.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSelectEvent(handler: CPointer<SelectHandler>,
                                  userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkMenuItemPtr, signal = MenuItemBaseEvent.select, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkMenuItemPtr, handlerId)
    }
}

/**
 * The event handler for the *activate* event. Arguments:
 * 1. menuItem: CPointer<GtkMenuItem>
 * 2. userData: gpointer
 */
public typealias ActivateHandler = CFunction<(menuItem: CPointer<GtkMenuItem>, userData: gpointer) -> Unit>

/**
 * The event handler for the *activate-item* event. Arguments:
 * 1. menuItem: CPointer<GtkMenuItem>
 * 2. userData: gpointer
 */
public typealias ActivateItemHandler = CFunction<(menuItem: CPointer<GtkMenuItem>, userData: gpointer) -> Unit>

/**
 * The event handler for the *deselect* event. Arguments:
 * 1. menuItem: CPointer<GtkMenuItem>
 * 2. userData: gpointer
 */
public typealias DeselectHandler = CFunction<(menuItem: CPointer<GtkMenuItem>, userData: gpointer) -> Unit>

/**
 * The event handler for the *select* event. Arguments:
 * 1. menuItem: CPointer<GtkMenuItem>
 * 2. userData: gpointer
 */
public typealias SelectHandler = CFunction<(menuItem: CPointer<GtkMenuItem>, userData: gpointer) -> Unit>
