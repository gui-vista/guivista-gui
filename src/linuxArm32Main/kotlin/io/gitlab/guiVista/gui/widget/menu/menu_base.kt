package io.gitlab.guiVista.gui.widget.menu

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.keyboard.AcceleratorGroup
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

public actual interface MenuBase : MenuShell {
    public val gtkMenuPtr: CPointer<GtkMenu>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var monitor: Int
        set(value) = gtk_menu_set_monitor(gtkMenuPtr, value)
        get() = gtk_menu_get_monitor(gtkMenuPtr)
    public actual var reserveToggleSize: Boolean
        get() = gtk_menu_get_reserve_toggle_size(gtkMenuPtr) == TRUE
        set(value) = gtk_menu_set_reserve_toggle_size(gtkMenuPtr, if (value) TRUE else FALSE)
    public actual var accelPath: String
        get() = gtk_menu_get_accel_path(gtkMenuPtr)?.toKString() ?: ""
        set(value) = gtk_menu_set_accel_path(gtkMenuPtr, value)
    public actual var accelGroup: AcceleratorGroup
        get() = AcceleratorGroup.fromPointer(gtk_menu_get_accel_group(gtkMenuPtr))
        set(value) = gtk_menu_set_accel_group(gtkMenuPtr, value.gtkAccelGroupPtr)

    public actual fun reorderChild(child: WidgetBase, pos: Int) {
        gtk_menu_reorder_child(menu = gtkMenuPtr, child = child.gtkWidgetPtr, position = pos)
    }

    public actual fun attach(child: WidgetBase, leftAttach: UInt, rightAttach: UInt, topAttach: UInt,
                             bottomAttach: UInt) {
        gtk_menu_attach(
            menu = gtkMenuPtr,
            child = child.gtkWidgetPtr,
            left_attach = leftAttach,
            right_attach = rightAttach,
            top_attach = topAttach,
            bottom_attach = bottomAttach
        )
    }

    public actual fun fetchActive(): WidgetBase? {
        val ptr = gtk_menu_get_active(gtkMenuPtr)
        return if (ptr != null) Widget.fromPointer(ptr) else null
    }

    public actual fun changeActive(index: UInt) {
        gtk_menu_set_active(gtkMenuPtr, index)
    }

    public actual fun fetchAttachWidget(): WidgetBase? {
        val ptr = gtk_menu_get_attach_widget(gtkMenuPtr)
        return if (ptr != null) Widget.fromPointer(ptr) else null
    }

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkMenuPtr, handlerId)
    }

    public actual fun popDown() {
        gtk_menu_popdown(gtkMenuPtr)
    }

    /**
     * Displays a menu and makes it available for selection. Applications can use this function to display context
     * sensitive menus, and will typically supply *null* for the [parentMenuShell], [parentMenuItem], [func], and
     * [userData] parameters. The default menu positioning function will position the menu at the current mouse cursor
     * position. The [button] parameter should be the mouse button pressed to initiate the menu popup. If the menu
     * popup was initiated by something other than a mouse button press, such as a mouse button release or a key
     * press then [button] should be *0*.
     *
     * The [activateTime] parameter is used to conflict-resolve initiation of concurrent requests for mouse/keyboard
     * grab requests. To function properly this needs to be the timestamp of the user event (such as a mouse click or
     * key press) that caused the initiation of the popup. If no such event is available then
     * `gtk_get_current_event_time()` can be used instead.
     *
     * Note that this function does not work very well on GDK backends that do not have global coordinates, such as
     * Wayland or Mir.
     * @param button The mouse button which was pressed to initiate the event, or *0* if the event isn't a mouse button
     * press.
     * @param activateTime The time at which the activation event occurred.
     * @param parentMenuShell The menu shell containing the triggering menu item, or *null*.
     * @param parentMenuItem The menu item whose activation triggered the popup, or *null*.
     * @param func A user supplied function used to position the menu, or *null*.
     * @param userData User supplied data to be passed to [func], or *null*.
     */
    public fun popup(
        button: UInt,
        activateTime: UInt = gtk_get_current_event_time(),
        parentMenuShell: WidgetBase? = null,
        parentMenuItem: WidgetBase? = null,
        func: GtkMenuPositionFunc? = null,
        userData: gpointer? = null
    ) {
        gtk_menu_popup(
            menu = gtkMenuPtr,
            parent_menu_shell = parentMenuShell?.gtkWidgetPtr,
            parent_menu_item = parentMenuItem?.gtkWidgetPtr,
            func = func,
            data = userData,
            button = button,
            activate_time = activateTime
        )
    }

    public actual fun reposition() {
        gtk_menu_reposition(gtkMenuPtr)
    }

    public actual fun detach() {
        gtk_menu_detach(gtkMenuPtr)
    }

    /**
     * Connects the *move-scroll* event to a [handler] on a menu. The event occurs when scrolling is done on a menu.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectMoveScrollEvent(handler: CPointer<MoveScrollHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkMenuPtr, signal = MenuBaseEvent.moveScroll, slot = handler, data = userData)
}

/**
 * The event handler for the *move-scroll* event. Arguments:
 * 1. menu: CPointer<GtkMenu>
 * 2. scrollType: GtkScrollType
 * 3. userData: gpointer
 */
public typealias MoveScrollHandler = CFunction<(
    menu: CPointer<GtkMenu>,
    scrollType: GtkScrollType,
    userData: gpointer
) -> Unit>
