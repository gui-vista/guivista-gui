package io.gitlab.guiVista.gui.widget

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import io.gitlab.guiVista.core.dataType.variant.Variant

public actual interface Actionable {
    public val gtkActionablePtr: CPointer<GtkActionable>?
    public actual var actionName: String
        get() = gtk_actionable_get_action_name(gtkActionablePtr)?.toKString() ?: ""
        set(value) = gtk_actionable_set_action_name(gtkActionablePtr, if (value.isEmpty()) null else value)
    public actual var actionTargetValue: Variant?
        get() = Variant.fromPointer(gtk_actionable_get_action_target_value(gtkActionablePtr))
        set(value) = gtk_actionable_set_action_target_value(gtkActionablePtr, value?.gVariantPtr)

    public actual infix fun changeActionTarget(formatStr: String) {
        gtk_actionable_set_action_target(gtkActionablePtr, formatStr)
    }

    public actual infix fun changeDetailedActionName(detailedActionName: String) {
        gtk_actionable_set_detailed_action_name(gtkActionablePtr, detailedActionName)
    }
}
