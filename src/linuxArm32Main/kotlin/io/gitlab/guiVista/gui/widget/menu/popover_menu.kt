package io.gitlab.guiVista.gui.widget.menu

import gtk3.GtkPopoverMenu
import gtk3.GtkWidget
import gtk3.gtk_popover_menu_new
import gtk3.gtk_popover_menu_open_submenu
import io.gitlab.guiVista.gui.widget.PopoverBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class PopoverMenu private constructor(ptr: CPointer<GtkPopoverMenu>? = null) : PopoverBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_popover_menu_new()
    public val gtkPopoverMenuPtr: CPointer<GtkPopoverMenu>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public actual fun create(): PopoverMenu = PopoverMenu()

        public fun fromPointer(ptr: CPointer<GtkPopoverMenu>?): PopoverMenu = PopoverMenu(ptr)
    }

    public actual fun openSubMenu(name: String) {
        gtk_popover_menu_open_submenu(gtkPopoverMenuPtr, name)
    }
}

public fun popoverMenuWidget(
    popoverMenuPtr: CPointer<GtkPopoverMenu>? = null,
    init: PopoverMenu.() -> Unit
): PopoverMenu {
    val popoverMenu = if (popoverMenuPtr != null) PopoverMenu.fromPointer(popoverMenuPtr) else PopoverMenu.create()
    popoverMenu.init()
    return popoverMenu
}

public fun CPointer<GtkPopoverMenu>?.toPopoverMenu(): PopoverMenu = PopoverMenu.fromPointer(this)
