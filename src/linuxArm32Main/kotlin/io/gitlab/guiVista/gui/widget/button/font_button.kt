package io.gitlab.guiVista.gui.widget.button

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.dialog.FontChooser
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

public actual class FontButton private constructor(
    ptr: CPointer<GtkFontButton>? = null,
    fontName: String = ""
) : ButtonBase, FontChooser {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = when {
        ptr != null -> ptr.reinterpret()
        fontName.isNotEmpty() -> gtk_font_button_new_with_font(fontName)
        else -> gtk_font_button_new()
    }
    public val gtkFontButtonPtr: CPointer<GtkFontButton>?
        get() = gtkWidgetPtr?.reinterpret()
    override val gtkFontChooserPtr: CPointer<GtkFontChooser>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var fontName: String
        get() = gtk_font_button_get_font_name(gtkFontButtonPtr)?.toKString() ?: ""
        set(value) {
            gtk_font_button_set_font_name(gtkFontButtonPtr, value)
        }
    public actual var showStyle: Boolean
        get() = gtk_font_button_get_show_style(gtkFontButtonPtr) == TRUE
        set(value) = gtk_font_button_set_show_style(gtkFontButtonPtr, if (value) TRUE else FALSE)
    public actual var showSize: Boolean
        get() = gtk_font_button_get_show_size(gtkFontButtonPtr) == TRUE
        set(value) = gtk_font_button_set_show_size(gtkFontButtonPtr, if (value) TRUE else FALSE)
    public actual var useFont: Boolean
        get() = gtk_font_button_get_use_font(gtkFontButtonPtr) == TRUE
        set(value) = gtk_font_button_set_use_font(gtkFontButtonPtr, if (value) TRUE else FALSE)
    public actual var useSize: Boolean
        get() = gtk_font_button_get_use_size(gtkFontButtonPtr) == TRUE
        set(value) = gtk_font_button_set_use_size(gtkFontButtonPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkFontButton>?): FontButton = FontButton(ptr = ptr)

        public actual fun create(): FontButton = FontButton()

        public actual fun createWithFont(fontName: String): FontButton = FontButton(fontName = fontName)
    }

    actual override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gtkFontButtonPtr, handlerId)
    }

    /**
     * Connects the *font-set* event to a [handler] on a [FontButton]. This event is used when the user selects a font.
     * When handling this event use [fontName] to find out which font was just selected. Note that this event is only
     * fired when the user changes the font. If you need to react to programmatic font changes as well then use the
     * `notify::font-name` event.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectFontSetEvent(
        handler: CPointer<FontSetHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkFontButtonPtr, signal = FontButtonEvent.fontSet, slot = handler, data = userData)
}

/**
 * The event handler for the *font-set* event. Arguments:
 * 1. fontBtn: CPointer<GtkFontButton>
 * 2. userData: gpointer
 */
public typealias FontSetHandler = CFunction<(fontBtn: CPointer<GtkFontButton>, userData: gpointer) -> Unit>

public fun fontButtonWidget(
    ptr: CPointer<GtkFontButton>? = null,
    fontName: String = "",
    init: FontButton.() -> Unit = {}
): FontButton {
    val result = when {
        ptr != null -> FontButton.fromPointer(ptr)
        fontName.isNotEmpty() -> FontButton.createWithFont(fontName)
        else -> FontButton.create()
    }
    result.init()
    return result
}

public fun CPointer<GtkFontButton>?.toFontButton(): FontButton = FontButton.fromPointer(this)
