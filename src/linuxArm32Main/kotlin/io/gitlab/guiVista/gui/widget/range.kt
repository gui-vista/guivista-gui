package io.gitlab.guiVista.gui.widget

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.Adjustment
import io.gitlab.guiVista.gui.layout.Orientable

public actual interface Range : WidgetBase, Orientable {
    override val gtkOrientable: CPointer<GtkOrientable>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkRangePtr: CPointer<GtkRange>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var adjustment: Adjustment?
        get() {
            val tmp = gtk_range_get_adjustment(gtkRangePtr)
            return if (tmp != null) Adjustment.fromPointer(tmp) else null
        }
        set(value) = gtk_range_set_adjustment(gtkRangePtr, value?.gtkAdjustmentPtr)
    public actual var fillLevel: Double
        get() = gtk_range_get_fill_level(gtkRangePtr)
        set(value) = gtk_range_set_fill_level(gtkRangePtr, value)
    public actual var inverted: Boolean
        get() = gtk_range_get_inverted(gtkRangePtr) == TRUE
        set(value) = gtk_range_set_inverted(gtkRangePtr, if (value) TRUE else FALSE)

    /**
     * The sensitivity policy for the stepper that points to the adjustment's lower side. Default value is
     * *GtkSensitivityType.GTK_SENSITIVITY_AUTO*.
     *
     * Data binding property name: **lower-stepper-sensitivity**
     */
    public var lowerStepperSensitivity: GtkSensitivityType
        get() = gtk_range_get_lower_stepper_sensitivity(gtkRangePtr)
        set(value) = gtk_range_set_lower_stepper_sensitivity(gtkRangePtr, value)
    public actual var restrictToFillLevel: Boolean
        get() = gtk_range_get_restrict_to_fill_level(gtkRangePtr) == TRUE
        set(value) = gtk_range_set_restrict_to_fill_level(gtkRangePtr, if (value) TRUE else FALSE)
    public actual var roundDigits: Int
        get() = gtk_range_get_round_digits(gtkRangePtr)
        set(value) {
            if (value >= -1) gtk_range_set_round_digits(gtkRangePtr, value)
        }
    public actual var showFillLevel: Boolean
        get() = gtk_range_get_show_fill_level(gtkRangePtr) == TRUE
        set(value) = gtk_range_set_show_fill_level(gtkRangePtr, if (value) TRUE else FALSE)

    /**
     * The sensitivity policy for the stepper that points to the adjustment's upper side. Default value is
     * *GtkSensitivityType.GTK_SENSITIVITY_AUTO*.
     *
     * Data binding property name: **upper-stepper-sensitivity**
     */
    public var upperStepperSensitivity: GtkSensitivityType
        get() = gtk_range_get_upper_stepper_sensitivity(gtkRangePtr)
        set(value) = gtk_range_set_upper_stepper_sensitivity(gtkRangePtr, value)

    /**
     * Connects the *adjust-bounds* event to a [handler] on a [Range]. This event is used to give the application a
     * chance to adjust the bounds.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectAdjustBoundsEvent(handler: CPointer<AdjustBoundsHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkRangePtr, signal = RangeEvent.adjustBounds, slot = handler, data = userData)

    /**
     * Connects the *change-value* event to a [handler] on a [Range]. This event is used when a scroll action is
     * performed on a [Range]. It allows an application to determine the type of scroll event that occurred
     * and the new value.
     *
     * The application can handle the event itself and return *true* to prevent further processing,
     * or by returning *false* it can pass the event to other handlers until the default GTK handler is reached. The
     * value parameter isn't rounded. An application that overrides the GtkRange::change-value event is responsible
     * for clamping the value to the desired number of decimal digits; the default GTK handler clamps the value based
     * on “round-digits”.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectChangeValueEvent(handler: CPointer<ChangeValueHandler>,
                                       userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkRangePtr, signal = RangeEvent.changeValue, slot = handler, data = userData)

    /**
     * Connects the *move-slider* event to a [handler] on a [Range]. This event is used for key bindings when a
     * slider moves.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectMoveSliderEvent(handler: CPointer<MoveSliderHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkRangePtr, signal = RangeEvent.moveSlider, slot = handler, data = userData)

    /**
     * Connects the *value-changed* event to a [handler] on a [Range]. This event is used when the ranged value
     * changes.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectValueChangedEvent(handler: CPointer<ValueChangedHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkRangePtr, signal = RangeEvent.valueChanged, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkRangePtr, handlerId)
    }
}

/**
 * The event handler for the *adjust-bounds* event. Arguments:
 * 1. range: CPointer<GtkRange>
 * 2. value: Double
 * 3. userData: gpointer
 */
public typealias AdjustBoundsHandler = CFunction<(range: CPointer<GtkRange>, value: Double, userData: gpointer) -> Unit>

/**
 * The event handler for the *change-value* event. Arguments:
 * 1. range: CPointer<GtkRange>
 * 2. scroll: GtkScrollType
 * 3. value: double
 * 4. userData: gpointer
 *
 * Return *true* (an Int representing a Boolean) to prevent other handlers from being invoked for the event, otherwise
 * *false* to propagate the event further.
 */
public typealias ChangeValueHandler = CFunction<(
    range: CPointer<GtkRange>,
    scroll: GtkScrollType,
    value: Double,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *move-slider* event. Arguments:
 * 1. range: CPointer<GtkRange>
 * 2. step: GtkScrollType
 * 3. userData: gpointer
 */
public typealias MoveSliderHandler =
    CFunction<(range: CPointer<GtkRange>, step: GtkScrollType, userData: gpointer) -> Unit>

/**
 * The event handler for the *value-changed* event. Arguments:
 * 1. range: CPointer<GtkRange>
 * 2. userData: gpointer
 */
public typealias ValueChangedHandler = CFunction<(range: CPointer<GtkRange>, userData: gpointer) -> Unit>
