package io.gitlab.guiVista.gui.widget

import gtk3.GtkFontChooser
import gtk3.GtkFontChooserWidget
import gtk3.GtkWidget
import gtk3.gtk_font_chooser_widget_new
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.gui.dialog.FontChooser
import io.gitlab.guiVista.gui.layout.BoxBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class FontChooserWidget private constructor(ptr: CPointer<GtkFontChooserWidget>? = null) :
    BoxBase, FontChooser {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_font_chooser_widget_new()
    public val gtkFontChooserWidgetPtr: CPointer<GtkFontChooserWidget>?
        get() = gtkWidgetPtr?.reinterpret()
    override val gtkFontChooserPtr: CPointer<GtkFontChooser>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkFontChooserWidget>?): FontChooserWidget = FontChooserWidget(ptr)

        public actual fun create(): FontChooserWidget = FontChooserWidget()
    }

    actual override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gtkFontChooserWidgetPtr, handlerId)
    }
}

public fun fontChooserWidget(
    ptr: CPointer<GtkFontChooserWidget>? = null,
    init: FontChooserWidget.() -> Unit = {}
): FontChooserWidget {
    val result = if (ptr != null) FontChooserWidget.fromPointer(ptr) else FontChooserWidget.create()
    result.init()
    return result
}

public fun CPointer<GtkFontChooserWidget>?.toFontChooserWidget(): FontChooserWidget =
    FontChooserWidget.fromPointer(this)
