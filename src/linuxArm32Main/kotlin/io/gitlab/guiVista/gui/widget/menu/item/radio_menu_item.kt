package io.gitlab.guiVista.gui.widget.menu.item

import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.dataType.SinglyLinkedList
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer

public actual class RadioMenuItem private constructor(ptr: CPointer<GtkRadioMenuItem>?) :
    CheckMenuItemBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret()
    public val gtkRadioMenuItemPtr: CPointer<GtkRadioMenuItem>?
        get() = gtkWidgetPtr?.reinterpret()

    /**
     * Connects the *group-changed* event to a [handler] on a [RadioMenuItem]. This event is used when the group is
     * changed.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectGroupChangedEvent(handler: CPointer<GroupChangedHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkRadioMenuItemPtr, signal = RadioMenuItemEvent.groupChanged, slot = handler,
            data = userData)

    public actual fun joinGroup(groupSource: RadioMenuItem) {
        gtk_radio_menu_item_join_group(gtkRadioMenuItemPtr, groupSource.gtkRadioMenuItemPtr)
    }

    public actual fun leaveGroup() {
        gtk_radio_menu_item_join_group(gtkRadioMenuItemPtr, null)
    }

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkRadioMenuItemPtr, handlerId)
    }

    public actual companion object {
        public fun fromPointer(radioMenuItemPtr: CPointer<GtkRadioMenuItem>?): RadioMenuItem =
            RadioMenuItem(radioMenuItemPtr)

        public actual fun create(group: SinglyLinkedList?): RadioMenuItem =
            RadioMenuItem(gtk_radio_menu_item_new(group?.gSListPtr)?.reinterpret())

        public actual fun fromMnemonic(group: SinglyLinkedList?, label: String): RadioMenuItem =
            RadioMenuItem(gtk_radio_menu_item_new_with_mnemonic(group?.gSListPtr, label)?.reinterpret())

        public actual fun fromLabel(group: SinglyLinkedList?, label: String): RadioMenuItem =
            RadioMenuItem(gtk_radio_menu_item_new_with_label(group?.gSListPtr, label)?.reinterpret())

        public actual fun fromGroup(group: RadioMenuItem): RadioMenuItem =
            RadioMenuItem(gtk_radio_menu_item_new_from_widget(group.gtkRadioMenuItemPtr)?.reinterpret())
    }
}

public fun radioMenuItem(
    radioMenuItemPtr: CPointer<GtkRadioMenuItem>? = null,
    groupList: SinglyLinkedList? = null,
    group: RadioMenuItem? = null,
    label: String = "",
    mnemonic: String = "",
    init: RadioMenuItem.() -> Unit = {}
): RadioMenuItem {
    val menuItem = when {
        radioMenuItemPtr != null -> RadioMenuItem.fromPointer(radioMenuItemPtr)
        group != null -> RadioMenuItem.fromGroup(group)
        label.isNotEmpty() -> RadioMenuItem.fromLabel(groupList, label)
        mnemonic.isNotEmpty() -> RadioMenuItem.fromMnemonic(groupList, mnemonic)
        else -> RadioMenuItem.create()
    }
    menuItem.init()
    return menuItem
}

/**
 * The event handler for the *group-changed* event. Arguments:
 * 1. radioMenuItem: CPointer<GtkRadioMenuItem>
 * 2. userData: gpointer
 */
public typealias GroupChangedHandler = CFunction<(
    radioMenuItem: CPointer<GtkRadioMenuItem>,
    userData: gpointer
) -> Unit>

public fun CPointer<GtkRadioMenuItem>?.toRadioMenuItem(): RadioMenuItem = RadioMenuItem.fromPointer(this)
