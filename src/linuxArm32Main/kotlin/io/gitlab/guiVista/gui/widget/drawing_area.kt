package io.gitlab.guiVista.gui.widget

import gtk3.GtkDrawingArea
import gtk3.GtkWidget
import gtk3.gtk_drawing_area_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class DrawingArea private constructor(ptr: CPointer<GtkDrawingArea>? = null) : WidgetBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_drawing_area_new()
    public val gtkDrawingAreaPtr: CPointer<GtkDrawingArea>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public actual fun create(): DrawingArea = DrawingArea()

        public fun fromPointer(ptr: CPointer<GtkDrawingArea>?): DrawingArea = DrawingArea(ptr)
    }
}

public fun drawingAreaWidget(ptr: CPointer<GtkDrawingArea>? = null, init: DrawingArea.() -> Unit = {}): DrawingArea {
    val result = if (ptr != null) DrawingArea.fromPointer(ptr) else DrawingArea.create()
    result.init()
    return result
}
