package io.gitlab.guiVista.gui.widget

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class AspectFrame private constructor(
    ptr: CPointer<GtkAspectFrame>? = null,
    label: String = "",
    xAlign: Float = 0F,
    yAlign: Float = 0F,
    ratio: Float = 0F,
    obeyChild: Boolean = false
) : FrameBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? =
        ptr?.reinterpret() ?: gtk_aspect_frame_new(
            label = label,
            xalign = xAlign,
            yalign = yAlign,
            ratio = ratio,
            obey_child = if (obeyChild) TRUE else FALSE
        )
    public val gtkAspectFramePtr: CPointer<GtkAspectFrame>?
        get() = gtkWidgetPtr?.reinterpret()
    override val gtkFramePtr: CPointer<GtkFrame>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkAspectFrame>?): AspectFrame = AspectFrame(ptr = ptr)

        public actual fun create(
            label: String,
            xAlign: Float,
            yAlign: Float,
            ratio: Float,
            obeyChild: Boolean
        ): AspectFrame =
            AspectFrame(
                label = label,
                xAlign = xAlign,
                yAlign = yAlign,
                ratio = ratio,
                obeyChild = obeyChild
            )
    }

    public actual fun changeParameters(xAlign: Float, yAlign: Float, ratio: Float, obeyChild: Boolean) {
        gtk_aspect_frame_set(
            aspect_frame = gtkAspectFramePtr,
            xalign = xAlign,
            yalign = yAlign,
            ratio = ratio,
            obey_child = if (obeyChild) TRUE else FALSE
        )
    }
}

public fun CPointer<GtkAspectFrame>?.toAspectFrame(): AspectFrame = AspectFrame.fromPointer(this)
