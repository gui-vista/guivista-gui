package io.gitlab.guiVista.gui.widget

import gtk3.*
import io.gitlab.guiVista.io.application.menu.MenuModel
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Popover private constructor(
    ptr: CPointer<GtkPopover>? = null,
    relativeTo: WidgetBase? = null,
    model: MenuModel? = null
) : PopoverBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = when {
        model == null && relativeTo != null -> gtk_popover_new(relativeTo.gtkWidgetPtr)
        relativeTo != null && model != null -> gtk_popover_new_from_model(relativeTo.gtkWidgetPtr, model.gMenuModelPtr)
        else -> ptr?.reinterpret()
    }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkPopover>?): Popover = Popover(ptr = ptr)

        public actual fun create(relativeTo: WidgetBase): Popover = Popover(relativeTo = relativeTo)

        public actual fun fromModel(relativeTo: WidgetBase, model: MenuModel): Popover =
            Popover(relativeTo = relativeTo, model = model)
    }
}

public fun popoverWidgetFromModel(relativeTo: WidgetBase, model: MenuModel, init: Popover.() -> Unit = {}): Popover {
    val result = Popover.fromModel(relativeTo, model)
    result.init()
    return result
}

public fun popoverWidget(
    ptr: CPointer<GtkPopover>? = null,
    relativeTo: WidgetBase? = null,
    init: Popover.() -> Unit = {}
): Popover {
    val result = if (relativeTo != null) Popover.create(relativeTo) else Popover.fromPointer(ptr)
    result.init()
    return result
}

public fun CPointer<GtkPopover>?.toPopover(): Popover = Popover.fromPointer(this)
