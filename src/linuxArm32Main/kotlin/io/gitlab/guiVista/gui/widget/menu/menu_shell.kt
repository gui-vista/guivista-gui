package io.gitlab.guiVista.gui.widget.menu

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual interface MenuShell : ContainerBase {
    public val gtkMenuShellPtr: CPointer<GtkMenuShell>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var takeFocus: Boolean
        get() = gtk_menu_shell_get_take_focus(gtkMenuShellPtr) == TRUE
        set(value) = gtk_menu_shell_set_take_focus(gtkMenuShellPtr, if (value) TRUE else FALSE)
    public actual val selectedItem: WidgetBase?
        get() {
            val tmp = gtk_menu_shell_get_selected_item(gtkMenuShellPtr)
            return if (tmp != null) Widget.fromPointer(tmp) else null
        }
    public actual val parentShell: WidgetBase?
        get() {
            val tmp = gtk_menu_shell_get_parent_shell(gtkMenuShellPtr)
            return if (tmp != null) Widget.fromPointer(tmp) else null
        }

    public actual infix fun append(child: WidgetBase) {
        gtk_menu_shell_append(gtkMenuShellPtr, child.gtkWidgetPtr)
    }

    /**
     * Adds a new menu item to the end of the menu shell's item list.
     * @param widget The menu item to append.
     */
    override operator fun plusAssign(widget: WidgetBase) {
        append(widget)
    }

    public actual infix fun prepend(child: WidgetBase) {
        gtk_menu_shell_prepend(gtkMenuShellPtr, child.gtkWidgetPtr)
    }

    public actual fun insert(child: WidgetBase, position: Int) {
        gtk_menu_shell_insert(menu_shell = gtkMenuShellPtr, child = child.gtkWidgetPtr, position = position)
    }

    public actual fun deactivate() {
        gtk_menu_shell_deactivate(gtkMenuShellPtr)
    }

    public actual infix fun selectItem(menuItem: WidgetBase) {
        gtk_menu_shell_select_item(gtkMenuShellPtr, menuItem.gtkWidgetPtr)
    }

    public actual fun selectFirst(searchSensitive: Boolean) {
        gtk_menu_shell_select_first(gtkMenuShellPtr, if (searchSensitive) TRUE else FALSE)
    }

    public actual fun deselect() {
        gtk_menu_shell_deselect(gtkMenuShellPtr)
    }

    public actual fun activateItem(menuItem: WidgetBase, forceDeactivate: Boolean) {
        gtk_menu_shell_activate_item(menu_shell = gtkMenuShellPtr, menu_item = menuItem.gtkWidgetPtr,
            force_deactivate = if (forceDeactivate) TRUE else FALSE)
    }

    public actual fun cancel() {
        gtk_menu_shell_cancel(gtkMenuShellPtr)
    }

    /**
     * Connects the *cancel* event to a [handler] on a [MenuShell]. This event is used when selection is
     * cancelled in a [MenuShell].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCancelEvent(handler: CPointer<CancelHandler>,
                                  userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkMenuShellPtr, signal = MenuShellEvent.cancel, slot = handler, data = userData)

    /**
     * Connects the *selection-done* event to a [handler] on a [MenuShell]. This event is used when selection is
     * completed within a [MenuShell].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSelectionDoneEvent(handler: CPointer<SelectionDoneHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkMenuShellPtr, signal = MenuShellEvent.selectionDone, slot = handler,
            data = userData)

    /**
     * Connects the *activate-current* event to a [handler] on a [MenuShell]. This event is used when the current item
     * is activated within the [MenuShell].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActivateCurrentEvent(handler: CPointer<ActivateCurrentHandler>,
                                           userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkMenuShellPtr, signal = MenuShellEvent.activateCurrent, slot = handler,
            data = userData)

    /**
     * Connects the *deactivate* event to a [handler] on a [MenuShell]. This event is used when the [MenuShell] is
     * deactivated.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectDeactivateEvent(handler: CPointer<DeactivateHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkMenuShellPtr, signal = MenuShellEvent.deactivate, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkMenuShellPtr, handlerId)
    }
}

/**
 * The event handler for the *cancel* event. Arguments:
 * 1. menuShell: CPointer<GtkMenuShell>
 * 2. userData: gpointer
 */
public typealias CancelHandler = CFunction<(menuShell: CPointer<GtkMenuShell>, userData: gpointer) -> Unit>

/**
 * The event handler for the *selection-done* event. Arguments:
 * 1. menuShell: CPointer<GtkMenuShell>
 * 2. userData: gpointer
 */
public typealias SelectionDoneHandler = CFunction<(menuShell: CPointer<GtkMenuShell>, userData: gpointer) -> Unit>

/**
 * The event handler for the *activate-current* event. Arguments:
 * 1. menuShell: CPointer<GtkMenuShell>
 * 2. forceHide: Int (represents a Boolean)
 * 3. userData: gpointer
 */
public typealias ActivateCurrentHandler =
    CFunction<(menuShell: CPointer<GtkMenuShell>, forceHide: Int, userData: gpointer) -> Unit>

/**
 * The event handler for the *deactivate* event. Arguments:
 * 1. menuShell: CPointer<GtkMenuShell>
 * 2. userData: gpointer
 */
public typealias DeactivateHandler =
    CFunction<(menuShell: CPointer<GtkMenuShell>, userData: gpointer) -> Unit>
