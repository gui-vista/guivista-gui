package io.gitlab.guiVista.gui.widget

import glib2.g_free
import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

public actual class ComboBoxText private constructor(
    ptr: CPointer<GtkComboBoxText>? = null,
    withEntry: Boolean = false
) : ComboBoxBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = when {
        ptr != null -> ptr.reinterpret()
        withEntry -> gtk_combo_box_text_new_with_entry()
        else -> gtk_combo_box_text_new()
    }
    override val gtkCellEditablePtr: CPointer<GtkCellEditable>?
        get() = gtkWidgetPtr?.reinterpret()
    override val gtkCellLayoutPtr: CPointer<GtkCellLayout>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkComboBoxTextPtr: CPointer<GtkComboBoxText>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual val activeText: String?
        get() {
            val ptr = gtk_combo_box_text_get_active_text(gtkComboBoxTextPtr)
            val result = ptr?.toKString()
            g_free(ptr)
            return result
        }

    public actual companion object {
        public actual fun create(): ComboBoxText = ComboBoxText()

        public actual fun createWithEntry(): ComboBoxText = ComboBoxText(withEntry = true)

        public fun fromPointer(ptr: CPointer<GtkComboBoxText>?): ComboBoxText = ComboBoxText(ptr = ptr)
    }

    public actual fun appendText(id: String?, text: String) {
        gtk_combo_box_text_append(combo_box = gtkComboBoxTextPtr, id = id, text = text)
    }

    public actual fun prependText(id: String?, text: String) {
        gtk_combo_box_text_prepend(combo_box = gtkComboBoxTextPtr, id = id, text = text)
    }

    public actual fun insertText(pos: Int, id: String?, text: String) {
        gtk_combo_box_text_insert(combo_box = gtkComboBoxTextPtr, position = pos, id = id, text = text)
    }

    public actual fun removeText(pos: Int) {
        gtk_combo_box_text_remove(gtkComboBoxTextPtr, pos)
    }

    public actual fun removeAllText() {
        gtk_combo_box_text_remove_all(gtkComboBoxTextPtr)
    }
}

public fun comboBoxTextWidget(
    ptr: CPointer<GtkComboBoxText>? = null,
    withEntry: Boolean = false,
    init: ComboBoxText.() -> Unit = {}
): ComboBoxText {
    val result = when {
        ptr != null -> ComboBoxText.fromPointer(ptr)
        withEntry -> ComboBoxText.createWithEntry()
        else -> ComboBoxText.create()
    }
    result.init()
    return result
}

public fun CPointer<GtkComboBoxText>?.toComboBoxText(): ComboBoxText = ComboBoxText.fromPointer(this)
