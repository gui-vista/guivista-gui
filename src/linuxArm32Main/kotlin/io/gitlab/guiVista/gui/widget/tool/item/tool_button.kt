package io.gitlab.guiVista.gui.widget.tool.item

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual class ToolButton private constructor(
    ptr: CPointer<GtkToolButton>? = null,
    iconWidget: WidgetBase? = null,
    label: String = ""
) : ToolButtonBase {
    override val gtkToolItemPtr: CPointer<GtkToolItem>? =
        ptr?.reinterpret() ?: gtk_tool_button_new(iconWidget?.gtkWidgetPtr, label)
    override val gtkToolButtonPtr: CPointer<GtkToolButton>?
        get() = gtkToolItemPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkToolButton>?): ToolButton = ToolButton(ptr = ptr)

        public actual fun create(iconWidget: WidgetBase, label: String): ToolButton =
            ToolButton(iconWidget = iconWidget, label = label)
    }
}

public fun toolButtonWidget(
    ptr: CPointer<GtkToolButton>? = null,
    iconWidget: WidgetBase? = null,
    label: String = "",
    init: ToolButton.() -> Unit = {}
): ToolButton {
    val toolButton =
        if (iconWidget != null && label.isNotEmpty()) ToolButton.create(iconWidget, label)
        else ToolButton.fromPointer(ptr)
    toolButton.init()
    return toolButton
}

public fun CPointer<GtkToolButton>?.toToolButton(): ToolButton = ToolButton.fromPointer(this)
