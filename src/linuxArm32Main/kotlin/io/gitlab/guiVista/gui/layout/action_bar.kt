package io.gitlab.guiVista.gui.layout

import gtk3.*
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class ActionBar private constructor(ptr: CPointer<GtkActionBar>? = null) : BinBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_action_bar_new()
    public val gtkActionBarPtr: CPointer<GtkActionBar>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var centerWidget: WidgetBase?
        get() {
            val ptr = gtk_action_bar_get_center_widget(gtkActionBarPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) = gtk_action_bar_set_center_widget(gtkActionBarPtr, value?.gtkWidgetPtr)

    public actual companion object {
        public actual fun create(): ActionBar = ActionBar()

        public fun fromPointer(ptr: CPointer<GtkActionBar>?): ActionBar = ActionBar(ptr)
    }

    public actual fun prependChild(child: WidgetBase) {
        gtk_action_bar_pack_end(gtkActionBarPtr, child.gtkWidgetPtr)
    }

    public actual fun appendChild(child: WidgetBase) {
        gtk_action_bar_pack_start(gtkActionBarPtr, child.gtkWidgetPtr)
    }
}

public fun actionBarLayout(actionBarPtr: CPointer<GtkActionBar>? = null, init: ActionBar.() -> Unit): ActionBar {
    val actionBar = if (actionBarPtr != null) ActionBar.fromPointer(actionBarPtr) else ActionBar.create()
    actionBar.init()
    return actionBar
}

public fun CPointer<GtkActionBar>?.toActionBar(): ActionBar = ActionBar.fromPointer(this)
