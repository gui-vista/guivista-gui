package io.gitlab.guiVista.gui.layout.pane

import gtk3.GtkOrientation
import gtk3.GtkPaned
import gtk3.GtkWidget
import gtk3.gtk_paned_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class DualPane private constructor(
    ptr: CPointer<GtkPaned>? = null,
    orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL
) : DualPaneBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_paned_new(orientation)
    override val gtkPanedPtr: CPointer<GtkPaned>?
        get() = gtkWidgetPtr?.reinterpret()

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkPaned>?): DualPane = DualPane(ptr = ptr)

        public fun create(orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL): DualPane =
            DualPane(orientation = orientation)
    }
}

public fun dualPaneLayout(
    panedPtr: CPointer<GtkPaned>? = null,
    orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL,
    init: DualPane.() -> Unit
): DualPane {
    val result = if (panedPtr != null) DualPane.fromPointer(panedPtr) else DualPane.create(orientation)
    result.init()
    return result
}

public fun CPointer<GtkPaned>?.toDualPane(): DualPane = DualPane.fromPointer(this)
