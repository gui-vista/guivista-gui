package io.gitlab.guiVista.gui.layout

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual interface BoxBase : ContainerBase, Orientable {
    override val gtkOrientable: CPointer<GtkOrientable>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkBoxPtr: CPointer<GtkBox>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var centerWidget: WidgetBase?
        get() {
            val ptr = gtk_box_get_center_widget(gtkBoxPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) = gtk_box_set_center_widget(gtkBoxPtr, value?.gtkWidgetPtr)
    public actual var spacing: Int
        set(value) = gtk_box_set_spacing(gtkBoxPtr, value)
        get() = gtk_box_get_spacing(gtkBoxPtr)
    public actual var homogeneous: Boolean
        set(value) = gtk_box_set_homogeneous(gtkBoxPtr, if (value) TRUE else FALSE)
        get() = gtk_box_get_homogeneous(gtkBoxPtr) == TRUE

    /**
     * The position of the baseline aligned widgets if extra space is available. Default value is
     * *GtkBaselinePosition.GTK_BASELINE_POSITION_CENTER*.
     *
     * Data binding property name: **baseline-position**
     */
    public var baselinePosition: GtkBaselinePosition
        set(value) = gtk_box_set_baseline_position(gtkBoxPtr, value)
        get() = gtk_box_get_baseline_position(gtkBoxPtr)

    public actual fun reorderChild(child: WidgetBase, pos: Int) {
        gtk_box_reorder_child(box = gtkBoxPtr, child = child.gtkWidgetPtr, position = pos)
    }

    public actual fun prependChild(
        child: WidgetBase,
        fill: Boolean,
        expand: Boolean,
        padding: UInt
    ): Unit = gtk_box_pack_end(
        box = gtkBoxPtr,
        child = child.gtkWidgetPtr,
        expand = if (expand) TRUE else FALSE,
        fill = if (fill) TRUE else FALSE,
        padding = padding
    )

    public actual fun appendChild(
        child: WidgetBase,
        fill: Boolean,
        expand: Boolean,
        padding: UInt
    ): Unit = gtk_box_pack_start(
        box = gtkBoxPtr,
        child = child.gtkWidgetPtr,
        expand = if (expand) TRUE else FALSE,
        fill = if (fill) TRUE else FALSE,
        padding = padding
    )
}
