package io.gitlab.guiVista.gui.layout

import gtk3.GtkBin
import gtk3.GtkWidget
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Bin private constructor(ptr: CPointer<GtkBin>?) : BinBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret()

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkBin>?): Bin = Bin(ptr)
    }
}
