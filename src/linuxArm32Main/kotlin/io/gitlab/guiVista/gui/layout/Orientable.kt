package io.gitlab.guiVista.gui.layout

import gtk3.GtkOrientable
import gtk3.GtkOrientation
import gtk3.gtk_orientable_get_orientation
import gtk3.gtk_orientable_set_orientation
import kotlinx.cinterop.CPointer

public actual interface Orientable {
    public val gtkOrientable: CPointer<GtkOrientable>?

    /** The orientation of the [Orientable]. */
    public var orientation: GtkOrientation
        get() = gtk_orientable_get_orientation(gtkOrientable)
        set(value) = gtk_orientable_set_orientation(gtkOrientable, value)
}
