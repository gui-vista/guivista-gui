package io.gitlab.guiVista.gui.layout

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Revealer private constructor(ptr: CPointer<GtkRevealer>? = null) : BinBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_revealer_new()
    public val gtkRevealerPtr: CPointer<GtkRevealer>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var revealChild: Boolean
        get() = gtk_revealer_get_reveal_child(gtkRevealerPtr) == TRUE
        set(value) = gtk_revealer_set_reveal_child(gtkRevealerPtr, if (value) TRUE else FALSE)
    public actual var transitionDuration: UInt
        get() = gtk_revealer_get_transition_duration(gtkRevealerPtr)
        set(value) = gtk_revealer_set_transition_duration(gtkRevealerPtr, value)
    public actual val childRevealed: Boolean
        get() = gtk_revealer_get_child_revealed(gtkRevealerPtr) == TRUE

    /**
     * The type of animation used to transition. Default value is `GTK_REVEALER_TRANSITION_TYPE_SLIDE_DOWN`.
     */
    public var transitionType: GtkRevealerTransitionType
        get() = gtk_revealer_get_transition_type(gtkRevealerPtr)
        set(value) = gtk_revealer_set_transition_type(gtkRevealerPtr, value)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkRevealer>?): Revealer = Revealer(ptr)

        public actual fun create(): Revealer = Revealer()
    }
}

public fun revealerLayout(
    revealerPtr: CPointer<GtkRevealer>? = null,
    init: Revealer.() -> Unit
): Revealer {
    val result = if (revealerPtr != null) Revealer.fromPointer(revealerPtr) else Revealer.create()
    result.init()
    return result
}

public fun CPointer<GtkRevealer>?.toRevealer(): Revealer = Revealer.fromPointer(this)
