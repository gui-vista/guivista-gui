package io.gitlab.guiVista.gui.layout

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.Adjustment
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual interface ContainerBase : WidgetBase {
    public val gtkContainerPtr: CPointer<GtkContainer>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual var borderWidth: UInt
        set(value) = gtk_container_set_border_width(gtkContainerPtr, value)
        get() = gtk_container_get_border_width(gtkContainerPtr)

    /**
     * Specify how resize events are handled. Default value is *GtkResizeMode.GTK_RESIZE_PARENT*.
     *
     * Data binding property name: **resize-mode**
     */
    public var resizeMode: GtkResizeMode
        get() = gtk_container_get_resize_mode(gtkContainerPtr)
        set(value) = gtk_container_set_resize_mode(gtkContainerPtr, value)

    public actual val children: DoublyLinkedList
        get() = DoublyLinkedList.fromPointer(gtk_container_get_children(gtkContainerPtr))

    public actual var focusChild: WidgetBase?
        get() {
            val tmp = gtk_container_get_focus_child(gtkContainerPtr)
            return if (tmp != null) Widget.fromPointer(tmp) else null
        }
        set(value) = gtk_container_set_focus_child(gtkContainerPtr, value?.gtkWidgetPtr)

    public actual var focusVAdjustment: Adjustment?
        get() {
            val tmp = gtk_container_get_focus_vadjustment(gtkContainerPtr)
            return if (tmp != null) Adjustment.fromPointer(tmp) else null
        }
        set(value) = gtk_container_set_focus_vadjustment(gtkContainerPtr, value?.gtkAdjustmentPtr)

    public actual var focusHAdjustment: Adjustment?
        get() {
            val tmp = gtk_container_get_focus_hadjustment(gtkContainerPtr)
            return if (tmp != null) Adjustment.fromPointer(tmp) else null
        }
        set(value) = gtk_container_set_focus_hadjustment(gtkContainerPtr, value?.gtkAdjustmentPtr)

    public actual fun childType(): ULong = gtk_container_child_type(gtkContainerPtr).toULong()

    public actual infix fun add(widget: WidgetBase) {
        gtk_container_add(gtkContainerPtr, widget.gtkWidgetPtr)
    }

    public actual fun checkResize() {
        gtk_container_check_resize(gtkContainerPtr)
    }

    public actual infix fun remove(widget: WidgetBase) {
        gtk_container_remove(gtkContainerPtr, widget.gtkWidgetPtr)
    }

    public actual operator fun plusAssign(widget: WidgetBase) {
        add(widget)
    }

    public actual operator fun minusAssign(widget: WidgetBase) {
        remove(widget)
    }
}
