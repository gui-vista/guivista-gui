package io.gitlab.guiVista.gui

import gtk3.GdkGLContext
import kotlinx.cinterop.CPointer

public actual class OpenGlContext private constructor(ptr: CPointer<GdkGLContext>?) {
    public val gdkGlContextPtr: CPointer<GdkGLContext>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<GdkGLContext>?): OpenGlContext = OpenGlContext(ptr)
    }
}
