package io.gitlab.guiVista.gui.dialog

import gtk3.*
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.gui.window.WindowBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class ColorChooserDialog private constructor(
    ptr: CPointer<GtkColorChooserDialog>? = null,
    title: String = "",
    parent: WindowBase? = null,
) : DialogBase, ColorChooser {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret()
        ?: gtk_color_chooser_dialog_new(title, parent?.gtkWindowPtr)
    public val gtkColorChooserDialogPtr: CPointer<GtkColorChooserDialog>?
        get() = gtkWidgetPtr?.reinterpret()
    override val gtkColorChooserPtr: CPointer<GtkColorChooser>?
        get() = gtkWidgetPtr?.reinterpret()

    actual override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gtkColorChooserDialogPtr, handlerId)
    }

    actual override fun close() {
        // Do nothing.
    }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkColorChooserDialog>?): ColorChooserDialog =
            ColorChooserDialog(ptr = ptr)

        public actual fun create(title: String, parent: WindowBase?): ColorChooserDialog =
            ColorChooserDialog(title = title, parent = parent)
    }
}

public fun colorChooserDialog(
    ptr: CPointer<GtkColorChooserDialog>? = null,
    title: String = "",
    parent: WindowBase? = null,
    init: ColorChooserDialog.() -> Unit = {}
): ColorChooserDialog {
    val dialog =
        if (ptr != null) {
            ColorChooserDialog.fromPointer(ptr)
        } else {
            ColorChooserDialog.create(title, parent)
        }
    dialog.init()
    return dialog
}

public fun CPointer<GtkColorChooserDialog>?.toColorChooserDialog(): ColorChooserDialog =
    ColorChooserDialog.fromPointer(this)
