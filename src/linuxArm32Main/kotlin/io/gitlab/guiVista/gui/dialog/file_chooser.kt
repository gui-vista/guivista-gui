package io.gitlab.guiVista.gui.dialog

import glib2.FALSE
import glib2.TRUE
import glib2.g_free
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.dataType.SinglyLinkedList
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.FileFilter
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import io.gitlab.guiVista.io.file.File
import io.gitlab.guiVista.io.file.FileBase

public actual interface FileChooser {
    public val gtkFileChooserPtr: CPointer<GtkFileChooser>?
    public actual val currentFolderFile: FileBase
        get() = File.fromPointer(gtk_file_chooser_get_current_folder_file(gtkFileChooserPtr))
    public actual val previewFile: FileBase?
        get() {
            val ptr = gtk_file_chooser_get_preview_file(gtkFileChooserPtr)
            return if (ptr != null) File.fromPointer(ptr) else null
        }
    public actual var currentName: String
        get() {
            val tmp = gtk_file_chooser_get_current_name(gtkFileChooserPtr)
            val result = tmp?.toKString() ?: ""
            g_free(tmp)
            return result
        }
        set(value) = gtk_file_chooser_set_current_name(gtkFileChooserPtr, value)
    public actual val previewFileName: String
        get() {
            val tmp = gtk_file_chooser_get_preview_filename(gtkFileChooserPtr)
            val result = tmp?.toKString() ?: ""
            g_free(tmp)
            return result
        }
    public actual val previewUri: String
        get() {
            val tmp = gtk_file_chooser_get_preview_uri(gtkFileChooserPtr)
            val result = tmp?.toKString() ?: ""
            g_free(tmp)
            return result
        }

    /**
     * The type of operation that the file selector is performing. Default value is *GTK_FILE_CHOOSER_ACTION_OPEN*.
     */
    public var action: GtkFileChooserAction
        get() = gtk_file_chooser_get_action(gtkFileChooserPtr)
        set(value) = gtk_file_chooser_set_action(gtkFileChooserPtr, value)
    public actual var createFolders: Boolean
        get() = gtk_file_chooser_get_create_folders(gtkFileChooserPtr) == TRUE
        set(value) = gtk_file_chooser_set_create_folders(gtkFileChooserPtr, if (value) TRUE else FALSE)
    public actual var doOverwriteConfirmation: Boolean
        get() = gtk_file_chooser_get_do_overwrite_confirmation(gtkFileChooserPtr) == TRUE
        set(value) = gtk_file_chooser_set_do_overwrite_confirmation(gtkFileChooserPtr, if (value) TRUE else FALSE)
    public actual var extraWidget: WidgetBase?
        get() {
            val ptr = gtk_file_chooser_get_extra_widget(gtkFileChooserPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) = gtk_file_chooser_set_extra_widget(gtkFileChooserPtr, value?.gtkWidgetPtr)
    public actual var filter: FileFilter?
        get() {
            val ptr = gtk_file_chooser_get_filter(gtkFileChooserPtr)
            return if (ptr != null) FileFilter.fromPointer(ptr) else null
        }
        set(value) = gtk_file_chooser_set_filter(gtkFileChooserPtr, value?.gtkFileFilterPtr)
    public actual var localOnly: Boolean
        get() = gtk_file_chooser_get_local_only(gtkFileChooserPtr) == TRUE
        set(value) = gtk_file_chooser_set_local_only(gtkFileChooserPtr, if (value) TRUE else FALSE)
    public actual var previewWidget: WidgetBase?
        get() {
            val ptr = gtk_file_chooser_get_preview_widget(gtkFileChooserPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) = gtk_file_chooser_set_preview_widget(gtkFileChooserPtr, value?.gtkWidgetPtr)
    public actual var previewWidgetActive: Boolean
        get() = gtk_file_chooser_get_preview_widget_active(gtkFileChooserPtr) == TRUE
        set(value) = gtk_file_chooser_set_preview_widget_active(gtkFileChooserPtr, if (value) TRUE else FALSE)
    public actual var selectMultiple: Boolean
        get() = gtk_file_chooser_get_select_multiple(gtkFileChooserPtr) == TRUE
        set(value) = gtk_file_chooser_set_select_multiple(gtkFileChooserPtr, if (value) TRUE else FALSE)
    public actual var showHidden: Boolean
        get() = gtk_file_chooser_get_show_hidden(gtkFileChooserPtr) == TRUE
        set(value) = gtk_file_chooser_set_show_hidden(gtkFileChooserPtr, if (value) TRUE else FALSE)
    public actual var usePreviewLabel: Boolean
        get() = gtk_file_chooser_get_use_preview_label(gtkFileChooserPtr) == TRUE
        set(value) = gtk_file_chooser_set_use_preview_label(gtkFileChooserPtr, if (value) TRUE else FALSE)

    public actual fun fetchFileName(): String {
        val tmp = gtk_file_chooser_get_filename(gtkFileChooserPtr)
        val result = tmp?.toKString() ?: ""
        g_free(tmp)
        return result
    }

    public actual fun changeFileName(fileName: String): Boolean =
        gtk_file_chooser_set_filename(gtkFileChooserPtr, fileName) == TRUE

    public actual fun selectFileName(fileName: String): Boolean =
        gtk_file_chooser_select_filename(gtkFileChooserPtr, fileName) == TRUE

    public actual fun unselectFileName(fileName: String) {
        gtk_file_chooser_unselect_filename(gtkFileChooserPtr, fileName)
    }

    public actual fun selectAll() {
        gtk_file_chooser_select_all(gtkFileChooserPtr)
    }

    public actual fun unselectAll() {
        gtk_file_chooser_unselect_all(gtkFileChooserPtr)
    }

    public actual fun fetchFileNames(): SinglyLinkedList =
        SinglyLinkedList(gtk_file_chooser_get_filenames(gtkFileChooserPtr))

    /**
     * Connects the *confirm-overwrite* event to a [handler] on a [FileChooser]. This event occurs when it is
     * appropriate to present a confirmation dialog when the user has selected a file name that already exists. Note
     * the event only gets emitted when the file chooser is in `GTK_FILE_CHOOSER_ACTION_SAVE` mode.
     *
     * Most applications just need to enable the [doOverwriteConfirmation] property, and they will automatically get a
     * stock confirmation dialog. Applications which need to customize this behavior should do that, and also connect
     * to the **confirm-overwrite** event. A event handler for this event **MUST** return a
     * `GtkFileChooserConfirmation` value, which indicates the action to take. If the handler determines that the user
     * wants to select a different filename, it should return `GTK_FILE_CHOOSER_CONFIRMATION_SELECT_AGAIN`. However if
     * it determines that the user is satisfied with his choice of file name, it should return
     * `GTK_FILE_CHOOSER_CONFIRMATION_ACCEPT_FILENAME`. On the other hand, if it determines that the stock confirmation
     * dialog should be used then it should return GTK_FILE_CHOOSER_CONFIRMATION_CONFIRM.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectConfirmOverwriteEvent(handler: CPointer<ConfirmOverwriteHandler>,
                                            userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkFileChooserPtr, signal = FileChooserEvent.confirmOverwrite, slot = handler,
            data = userData)

    /**
     * Connects the *current-folder-changed* event to a [handler] on a [FileChooser]. This event occurs when the current
     * folder in a [FileChooser] changes. This can happen due to the user performing some action that changes folders,
     * such as selecting a bookmark or visiting a folder on the file list. It can also happen as a result of calling a
     * function to explicitly change the current folder in a file chooser.
     *
     * Normally you do not need to connect to this event, unless you need to keep track of which folder a file chooser
     * is showing.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCurrentFolderChangedEvent(handler: CPointer<CurrentFolderChangedHandler>,
                                                userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkFileChooserPtr, signal = FileChooserEvent.currentFolderChanged, slot = handler,
            data = userData)

    /**
     * Connects the *file-activated* event to a [handler] on a [FileChooser]. This event occurs when the user
     * "activates" a file in the file chooser. This can happen by double-clicking on a file in the file list, or by
     * pressing **Enter**. Normally you do not need to connect to this event. It is used internally by
     * [GtkFileChooserDialog] to know when to activate the default button in the dialog.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectFileActivatedEvent(handler: CPointer<FileActivatedHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkFileChooserPtr, signal = FileChooserEvent.fileActivated, slot = handler,
            data = userData)

    /**
     * Connects the *selection-changed* event to a [handler] on a [FileChooser]. This event occurs when there is a
     * change in the set of selected files in a [FileChooser]. This can happen when the user modifies the selection
     * with the mouse or the keyboard, or when explicitly calling functions to change the selection.
     *
     * Normally you do not need to connect to this event as it is easier to wait for the file chooser to finish
     * running, and then to get the list of selected files using the functions mentioned below.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSelectionChangedEvent(handler: CPointer<SelectionChangedHandler>,
                                            userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkFileChooserPtr, signal = FileChooserEvent.selectionChanged, slot = handler,
            data = userData)

    /**
     * Connects the *update-preview* event to a [handler] on a [FileChooser]. This event occurs when the preview in a
     * file chooser should be regenerated. For example this can happen when the currently selected file changes. You
     * should use this event if you want your file chooser to have a preview widget. Once you have installed a preview
     * widget with [previewWidget], you should update it when this event is emitted. You can use the functions
     * `gtk_file_chooser_get_preview_filename()`, or `gtk_file_chooser_get_preview_uri()` to get the name of the file
     * to preview. Your widget may not be able to preview all kinds of files; your callback **MUST** set the
     * [previewWidgetActive] property to inform the file chooser about whether the preview was generated successfully
     * or not.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectUpdatePreviewEvent(handler: CPointer<UpdatePreviewHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(
            obj = gtkFileChooserPtr,
            signal = FileChooserEvent.updatePreview,
            slot = handler,
            data = userData
        )

    public actual fun changeCurrentFolder(fileName: String): Boolean =
        gtk_file_chooser_set_current_folder(gtkFileChooserPtr, fileName) == TRUE

    public actual fun fetchCurrentFolder(): String {
        val tmp = gtk_file_chooser_get_current_folder(gtkFileChooserPtr)
        val result = tmp?.toKString() ?: ""
        g_free(tmp)
        return result
    }

    public actual fun fetchUri(): String {
        val tmp = gtk_file_chooser_get_uri(gtkFileChooserPtr)
        val result = tmp?.toKString() ?: ""
        g_free(tmp)
        return result
    }

    public actual fun changeUri(uri: String): Boolean = gtk_file_chooser_set_uri(gtkFileChooserPtr, uri) == TRUE

    public actual fun selectUri(uri: String): Boolean = gtk_file_chooser_select_uri(gtkFileChooserPtr, uri) == TRUE

    public actual fun unselectUri(uri: String) {
        gtk_file_chooser_unselect_uri(gtkFileChooserPtr, uri)
    }

    public actual fun fetchMultipleUris(): SinglyLinkedList =
        SinglyLinkedList(gtk_file_chooser_get_uris(gtkFileChooserPtr))

    public actual fun changeCurrentFolderUri(uri: String): Boolean =
        gtk_file_chooser_set_current_folder_uri(gtkFileChooserPtr, uri) == TRUE

    public actual fun fetchCurrentFolderUri(): String {
        val tmp = gtk_file_chooser_get_current_folder_uri(gtkFileChooserPtr)
        val result = tmp?.toKString() ?: ""
        g_free(tmp)
        return result
    }

    public actual infix fun addFilter(filter: FileFilter) {
        gtk_file_chooser_add_filter(gtkFileChooserPtr, filter.gtkFileFilterPtr)
    }

    public actual infix fun removeFilter(filter: FileFilter) {
        gtk_file_chooser_remove_filter(gtkFileChooserPtr, filter.gtkFileFilterPtr)
    }

    public actual fun listFilters(): SinglyLinkedList =
        SinglyLinkedList(gtk_file_chooser_list_filters(gtkFileChooserPtr))

    public actual fun addShortcutFolder(folder: String, error: Error?): Boolean {
        // TODO: Cover error handling.
        return gtk_file_chooser_add_shortcut_folder(
            chooser = gtkFileChooserPtr,
            folder = folder,
            error = null
        ) == TRUE
    }

    public actual fun removeShortcutFolder(folder: String, error: Error?): Boolean {
        // TODO: Cover error handling.
        return gtk_file_chooser_remove_shortcut_folder(
            chooser = gtkFileChooserPtr,
            folder = folder,
            error = null
        ) == TRUE
    }

    public actual fun listShortcutFolders(): SinglyLinkedList? {
        val ptr = gtk_file_chooser_list_shortcut_folders(gtkFileChooserPtr)
        return if (ptr != null) SinglyLinkedList(ptr) else null
    }

    public actual fun addShortcutFolderUri(uri: String, error: Error?): Boolean {
        // TODO: Cover error handling.
        return gtk_file_chooser_add_shortcut_folder_uri(chooser = gtkFileChooserPtr, error = null, uri = uri) == TRUE
    }

    public actual fun removeShortcutFolderUri(uri: String, error: Error?): Boolean {
        // TODO: Cover error handling.
        return gtk_file_chooser_remove_shortcut_folder_uri(chooser = gtkFileChooserPtr, error = null, uri = uri) == TRUE
    }

    public actual fun listAllShortcutFolderUris(): SinglyLinkedList? {
        val ptr = gtk_file_chooser_list_shortcut_folder_uris(gtkFileChooserPtr)
        return if (ptr != null) SinglyLinkedList(ptr) else null
    }

    public actual fun fetchFile(): FileBase = File.fromPointer(gtk_file_chooser_get_file(gtkFileChooserPtr))

    public actual fun fetchAllFiles(): SinglyLinkedList =
        SinglyLinkedList(gtk_file_chooser_get_files(gtkFileChooserPtr))

    public actual fun selectFile(file: FileBase, error: Error?): Boolean {
        // TODO: Cover error handling.
        return gtk_file_chooser_select_file(chooser = gtkFileChooserPtr, file = file.gFilePtr, error = null) == TRUE
    }

    public actual fun changeCurrentFolderFile(file: FileBase, error: Error?): Boolean {
        // TODO: Cover error handling.
        return gtk_file_chooser_set_current_folder_file(
            chooser = gtkFileChooserPtr,
            error = null,
            file = file.gFilePtr
        ) == TRUE
    }

    public actual fun changeFile(file: FileBase, error: Error?): Boolean {
        // TODO: Cover error handling.
        return gtk_file_chooser_set_file(chooser = gtkFileChooserPtr, error = null, file = file.gFilePtr) == TRUE
    }

    public actual fun unselectFile(file: FileBase) {
        gtk_file_chooser_unselect_file(gtkFileChooserPtr, file.gFilePtr)
    }
}

/**
 * The event handler for the *confirm-overwrite* event. Arguments:
 * 1. chooser: CPointer<GtkFileChooser>
 * 2. userData: gpointer
 *
 * Returns GtkFileChooserConfirmation.
 */
public typealias ConfirmOverwriteHandler = CFunction<(
    chooser: CPointer<GtkFileChooser>,
    userData: gpointer
) -> GtkFileChooserConfirmation>

/**
 * The event handler for the *file-activated* event. Arguments:
 * 1. chooser: CPointer<GtkFileChooser>
 * 2. userData: gpointer
 */
public typealias FileActivatedHandler = CFunction<(chooser: CPointer<GtkFileChooser>, userData: gpointer) -> Unit>

/**
 * The event handler for the *selection-changed* event. Arguments:
 * 1. chooser: CPointer<GtkFileChooser>
 * 2. userData: gpointer
 */
public typealias SelectionChangedHandler = CFunction<(chooser: CPointer<GtkFileChooser>, userData: gpointer) -> Unit>

/**
 * The event handler for the *current-folder-changed* event. Arguments:
 * 1. chooser: CPointer<GtkFileChooser>
 * 2. userData: gpointer
 */
public typealias CurrentFolderChangedHandler = CFunction<(
    chooser: CPointer<GtkFileChooser>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *update-preview* event. Arguments:
 * 1. chooser: CPointer<GtkFileChooser>
 * 2. userData: gpointer
 */
public typealias UpdatePreviewHandler = CFunction<(chooser: CPointer<GtkFileChooser>, userData: gpointer) -> Unit>
