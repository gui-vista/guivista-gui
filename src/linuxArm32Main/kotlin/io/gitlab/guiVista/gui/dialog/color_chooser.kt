package io.gitlab.guiVista.gui.dialog

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.RgbaColor
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer

public actual interface ColorChooser : ObjectBase {
    public val gtkColorChooserPtr: CPointer<GtkColorChooser>?
    public actual var rgba: RgbaColor
        get() {
            val color = RgbaColor.create()
            gtk_color_chooser_get_rgba(gtkColorChooserPtr, color.gdkRgbaPtr)
            return color
        }
        set(value) = gtk_color_chooser_set_rgba(gtkColorChooserPtr, value.gdkRgbaPtr)
    public actual var useAlpha: Boolean
        get() = gtk_color_chooser_get_use_alpha(gtkColorChooserPtr) == TRUE
        set(value) = gtk_color_chooser_set_use_alpha(gtkColorChooserPtr, if (value) TRUE else FALSE)

    override fun close() {
        // Do nothing.
    }

    /**
     * Connects the *color-activated* event to a [handler] on a color chooser. This event is used when a color is
     * activated from the color chooser. This usually happens when the user clicks a color swatch, or a color is
     * selected and the user presses one of the following keys:
     * - **Space**
     * - **Shift+Space**
     * - **Enter**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectColorActivatedEvent(
        handler: CPointer<ColorActivatedHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkColorChooserPtr, signal = ColorChooserEvent.colorActivated, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkColorChooserPtr, handlerId)
    }
}

/**
 * The event handler for the *color-activated* event. Arguments:
 * 1. chooser: CPointer<GtkColorChooser>
 * 2. color: CPointer<GdkRGBA>
 * 3. userData: gpointer
 */
public typealias ColorActivatedHandler = CFunction<(
    chooser: CPointer<GtkColorChooser>,
    color: CPointer<GdkRGBA>,
    userData: gpointer
) -> Unit>
