package io.gitlab.guiVista.gui

import gio2.GApplication
import gio2.G_APPLICATION_FLAGS_NONE
import glib2.g_object_unref
import glib2.g_printerr
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.disposeEmptyDataRef
import io.gitlab.guiVista.io.application.Application
import io.gitlab.guiVista.io.application.ApplicationBase
import kotlinx.cinterop.staticCFunction
import kotlin.system.exitProcess

public var guiApplicationActivateHandler: (Application) -> Unit = {}

public actual class GuiApplication private constructor(
    ptr: CPointer<GApplication>? = null,
    id: String = "org.example.gui-app"
) : ApplicationBase {
    private var defaultActivateHandlerId = 0uL
    public val gtkApplicationPtr: CPointer<GtkApplication>? = ptr?.reinterpret() ?: createGtkApplicationPtr(id)
    override val gApplicationPtr: CPointer<GApplication>?
        get() = gtkApplicationPtr?.reinterpret()

    public fun assignDefaultActivateHandler() {
        if (defaultActivateHandlerId == 0uL) {
            defaultActivateHandlerId = connectActivateEvent(
                staticCFunction { app: CPointer<GApplication>, _: gpointer ->
                    guiApplicationActivateHandler(Application.fromPointer(app))
                }
            )
        }
    }

    @Suppress("unused")
    public actual fun use(init: GuiApplication.() -> Unit) {
        this.init()
        close()
    }

    private fun createGtkApplicationPtr(id: String): CPointer<GtkApplication>? {
        if (id.trim().isEmpty()) {
            g_printerr("GTK Application ID cannot be empty!\n")
            exitProcess(-1)
        }
        return gtk_application_new(id, G_APPLICATION_FLAGS_NONE)
    }

    override fun close() {
        if (defaultActivateHandlerId != 0uL) super.disconnectEvent(defaultActivateHandlerId)
        g_object_unref(gtkApplicationPtr)
        disposeEmptyDataRef()
    }

    public actual companion object {
        public actual fun create(id: String): GuiApplication = GuiApplication(id = id)

        public fun fromPointer(ptr: CPointer<GApplication>?): GuiApplication = GuiApplication(ptr = ptr)
    }
}
