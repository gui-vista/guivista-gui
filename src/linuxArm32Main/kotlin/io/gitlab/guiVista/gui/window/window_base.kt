package io.gitlab.guiVista.gui.window

import glib2.*
import gtk3.*
import kotlinx.cinterop.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.ImageBuffer
import io.gitlab.guiVista.gui.keyboard.AcceleratorGroup
import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual interface WindowBase : BinBase {
    public val gtkWindowPtr: CPointer<GtkWindow>?
    public actual var titleBar: WidgetBase?
        get() {
            val ptr = gtk_window_get_titlebar(gtkWindowPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) = gtk_window_set_titlebar(gtkWindowPtr, value?.gtkWidgetPtr)
    public actual var icon: ImageBuffer?
        get() {
            val ptr = gtk_window_get_icon(gtkWindowPtr)
            return if (ptr != null) ImageBuffer.fromPointer(ptr) else null
        }
        set(value) = gtk_window_set_icon(gtkWindowPtr, value?.gdkPixbufPtr)
    public actual var iconList: DoublyLinkedList?
        get() {
            val ptr = gtk_window_get_icon_list(gtkWindowPtr)
            return if (ptr != null) DoublyLinkedList.fromPointer(ptr) else null
        }
        set(value) = gtk_window_set_icon_list(gtkWindowPtr, value?.gListPtr)
    public actual var defaultWidget: WidgetBase?
        get() {
            val ptr = gtk_window_get_default_widget(gtkWindowPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) = gtk_window_set_default(gtkWindowPtr, value?.gtkWidgetPtr)
    public actual var deleteable: Boolean
        get() = gtk_window_get_deletable(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_deletable(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual var acceptFocus: Boolean
        set(value) = gtk_window_set_accept_focus(gtkWindowPtr, if (value) TRUE else FALSE)
        get() = gtk_window_get_accept_focus(gtkWindowPtr) == TRUE

    public actual var title: String
        set(value) = gtk_window_set_title(gtkWindowPtr, value)
        get() = gtk_window_get_title(gtkWindowPtr)?.toKString() ?: ""

    public actual var resizable: Boolean
        set(value) = gtk_window_set_resizable(gtkWindowPtr, if (value) TRUE else FALSE)
        get() = gtk_window_get_resizable(gtkWindowPtr) == TRUE

    public actual val isMaximized: Boolean
        get() = gtk_window_is_maximized(gtkWindowPtr) == TRUE

    public actual var attachedTo: WidgetBase?
        get() {
            val tmp = gtk_window_get_attached_to(gtkWindowPtr)
            return if (tmp != null) Widget.fromPointer(tmp) else null
        }
        set(value) = gtk_window_set_attached_to(gtkWindowPtr, value?.gtkWidgetPtr)

    public actual var decorated: Boolean
        get() = gtk_window_get_decorated(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_decorated(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual var deletable: Boolean
        get() = gtk_window_get_deletable(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_deletable(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual var destroyWithParent: Boolean
        get() = gtk_window_get_destroy_with_parent(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_destroy_with_parent(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual var focusOnMap: Boolean
        get() = gtk_window_get_focus_on_map(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_focus_on_map(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual var focusVisible: Boolean
        get() = gtk_window_get_focus_visible(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_focus_visible(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual var gravity: ULong
        get() = gtk_window_get_gravity(gtkWindowPtr).toULong()
        set(value) = gtk_window_set_gravity(gtkWindowPtr, value.toUInt())

    public actual var hideTitleBarWhenMaximized: Boolean
        get() = gtk_window_get_hide_titlebar_when_maximized(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_hide_titlebar_when_maximized(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual var iconName: String
        get() = gtk_window_get_icon_name(gtkWindowPtr)?.toKString() ?: ""
        set(value) = gtk_window_set_icon_name(gtkWindowPtr, value)

    public actual val isActive: Boolean
        get() = gtk_window_is_active(gtkWindowPtr) == TRUE

    public actual var mnemonicsVisible: Boolean
        get() = gtk_window_get_mnemonics_visible(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_mnemonics_visible(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual var modal: Boolean
        get() = gtk_window_get_modal(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_modal(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual var role: String
        get() = gtk_window_get_role(gtkWindowPtr)?.toKString() ?: ""
        set(value) = gtk_window_set_role(gtkWindowPtr, value)

    public actual var skipPagerHint: Boolean
        get() = gtk_window_get_skip_pager_hint(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_skip_pager_hint(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual var skipTaskBarHint: Boolean
        get() = gtk_window_get_skip_taskbar_hint(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_skip_taskbar_hint(gtkWindowPtr, if (value) TRUE else FALSE)

    /**
     * Hint to help the desktop environment understand what kind of window this is, and how to treat it. Default value
     * is *GdkWindowTypeHint.GDK_WINDOW_TYPE_HINT_NORMAL*.
     *
     * Data binding property name: **type-hint**
     */
    public var typeHint: GdkWindowTypeHint
        get() = gtk_window_get_type_hint(gtkWindowPtr)
        set(value) = gtk_window_set_type_hint(gtkWindowPtr, value)

    public actual var urgencyHint: Boolean
        get() = gtk_window_get_urgency_hint(gtkWindowPtr) == TRUE
        set(value) = gtk_window_set_urgency_hint(gtkWindowPtr, if (value) TRUE else FALSE)

    public actual val hasTopLevelFocus: Boolean
        get() = gtk_window_has_toplevel_focus(gtkWindowPtr) == TRUE

    public actual var focus: WidgetBase?
        get() {
            val tmp = gtk_window_get_focus(gtkWindowPtr)
            return if (tmp != null) Widget.fromPointer(tmp) else null
        }
        set(value) = gtk_window_set_focus(gtkWindowPtr, value?.gtkWidgetPtr)

    public actual companion object {
        public actual var defaultIconList: DoublyLinkedList?
            get() {
                val ptr = gtk_window_get_default_icon_list()
                return if (ptr != null) DoublyLinkedList.fromPointer(ptr) else null
            }
            set(value) = gtk_window_set_default_icon_list(value?.gListPtr)
        public actual var defaultIconName: String
            get() = gtk_window_get_default_icon_name()?.toKString() ?: ""
            set(value) = gtk_window_set_default_icon_name(value)
    }

    public actual infix fun addAccelGroup(accelGroup: AcceleratorGroup) {
        gtk_window_add_accel_group(gtkWindowPtr, accelGroup.gtkAccelGroupPtr)
    }

    public actual infix fun removeAccelGroup(accelGroup: AcceleratorGroup) {
        gtk_window_remove_accel_group(gtkWindowPtr, accelGroup.gtkAccelGroupPtr)
    }

    public actual fun fetchPosition(): Pair<Int, Int> = memScoped {
        val rootX = alloc<gintVar>()
        val rootY = alloc<gintVar>()
        gtk_window_get_position(window = gtkWindowPtr, root_x = rootX.ptr, root_y = rootY.ptr)
        return rootX.value to rootY.value
    }

    public actual fun move(x: Int, y: Int) {
        gtk_window_move(window = gtkWindowPtr, x = x, y = y)
    }

    /**
     * Changes the [window's][Window] position.
     * @param position The window position to use.
     */
    public infix fun changePosition(position: GtkWindowPosition) {
        gtk_window_set_position(gtkWindowPtr, position)
    }

    public actual infix fun changeStartupId(startupId: String) {
        gtk_window_set_startup_id(gtkWindowPtr, startupId)
    }

    public actual fun changeDefaultSize(width: Int, height: Int) {
        gtk_window_set_default_size(gtkWindowPtr, width, height)
    }

    /** Closes the window, and frees up resources. */
    override fun close() {
        gtk_window_close(gtkWindowPtr)
    }

    public actual fun maximize() {
        gtk_window_maximize(gtkWindowPtr)
    }

    public actual fun unmaximize() {
        gtk_window_unmaximize(gtkWindowPtr)
    }

    public actual infix fun addChild(widget: WidgetBase) {
        gtk_container_add(gtkContainerPtr, widget.gtkWidgetPtr)
    }

    /** Adds a new child to the window. */
    override operator fun plusAssign(widget: WidgetBase) {
        addChild(widget)
    }

    public actual infix fun removeChild(widget: WidgetBase) {
        gtk_container_remove(gtkContainerPtr, widget.gtkWidgetPtr)
    }

    /** Remove a child from the window. */
    override operator fun minusAssign(widget: WidgetBase) {
        removeChild(widget)
    }

    public actual fun resetFocus() {}

    public actual fun createMainLayout(): ContainerBase? = null

    public actual fun setupEvents() {}

    /**
     * Connects the *activate-default* event to a [handler] on a [WindowBase]. This event is used when the user
     * activates the window's default widget.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActivateDefaultEvent(handler: CPointer<ActivateDefaultHandler>,
                                           userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWindowPtr, signal = WindowBaseEvent.activateDefault, slot = handler, data = userData)

    /**
     * Connects the *activate-focus* event to a [handler] on a [WindowBase]. This event is used when the user activates
     * the currently focused widget of window.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActivateFocusEvent(handler: CPointer<ActivateFocusHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWindowPtr, signal = WindowBaseEvent.activateFocus, slot = handler, data = userData)

    /**
     * Connects the *enable-debugging* event to a [handler] on a [WindowBase]. This event is used when the user enables
     * or disables interactive debugging. If toggle is *true* then interactive debugging is toggled on/off. However if
     * toggle is *false* then the debugger will be pointed at the widget under the pointer.
     *
     * The default bindings for this event are **Ctrl+Shift+I** and **Ctrl+Shift+D**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectEnableDebuggingEvent(handler: CPointer<EnableDebuggingHandler>,
                                           userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWindowPtr, signal = WindowBaseEvent.enableDebugging, slot = handler, data = userData)

    /**
     * Connects the *keys-changed* event to a [handler] on a [WindowBase]. This event is used when the set of
     * accelerators, or mnemonics that are associated with the window changes.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectKeysChangedEvent(handler: CPointer<KeysChangedHandler>,
                                       userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWindowPtr, signal = WindowBaseEvent.keysChanged, slot = handler, data = userData)

    /**
     * Connects the *set-focus* event to a [handler] on a [WindowBase]. This event is used when focus is set on a widget
     * in a window.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSetFocusEvent(handler: CPointer<SetFocusHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkWindowPtr, signal = WindowBaseEvent.setFocus, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkWindowPtr, handlerId)
    }

    public actual fun addMnemonic(keyVal: UInt, target: WidgetBase) {
        gtk_window_add_mnemonic(window = gtkWindowPtr, keyval = keyVal, target = target.gtkWidgetPtr)
    }

    public actual fun removeMnemonic(keyVal: UInt, target: WidgetBase) {
        gtk_window_remove_mnemonic(window = gtkWindowPtr, keyval = keyVal, target = target.gtkWidgetPtr)
    }

    public actual fun createUi(init: WindowBase.() -> Unit) {}

    public actual fun present() {
        gtk_window_present(gtkWindowPtr)
    }

    public actual fun presentWithTime(timestamp: UInt) {
        gtk_window_present_with_time(gtkWindowPtr, timestamp)
    }

    public actual fun iconify() {
        gtk_window_iconify(gtkWindowPtr)
    }

    public actual fun deiconify() {
        gtk_window_deiconify(gtkWindowPtr)
    }

    public actual fun stick() {
        gtk_window_stick(gtkWindowPtr)
    }

    public actual fun unstick() {
        gtk_window_unstick(gtkWindowPtr)
    }

    public actual fun fullScreen() {
        gtk_window_fullscreen(gtkWindowPtr)
    }

    public actual fun unFullScreen() {
        gtk_window_unfullscreen(gtkWindowPtr)
    }

    public actual fun setupWindow() {}
}

public actual fun listTopLevelWindows(): DoublyLinkedList = DoublyLinkedList.fromPointer(gtk_window_list_toplevels())

/**
 * The event handler for the *activate-default* event. Arguments:
 * 1. window: CPointer<GtkWindow>
 * 2. userData: gpointer
 */
public typealias ActivateDefaultHandler = CFunction<(window: CPointer<GtkWindow>, userData: gpointer) -> Unit>

/**
 * The event handler for the *activate-focus* event. Arguments:
 * 1. window: CPointer<GtkWindow>
 * 2. userData: gpointer
 */
public typealias ActivateFocusHandler = CFunction<(window: CPointer<GtkWindow>, userData: gpointer) -> Unit>

/**
 * The event handler for the *enable-debugging* event. Arguments:
 * 1. window: CPointer<GtkWindow>
 * 2. toggle: Int (represents a Boolean)
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias EnableDebuggingHandler =
    CFunction<(window: CPointer<GtkWindow>, toggle: Int, userData: gpointer) -> Int>

/**
 * The event handler for the *keys-changed* event. Arguments:
 * 1. window: CPointer<GtkWindow>
 * 2. userData: gpointer
 */
public typealias KeysChangedHandler = CFunction<(window: CPointer<GtkWindow>, userData: gpointer) -> Unit>

/**
 * The event handler for the *set-focus* event. Arguments:
 * 1. window: CPointer<GtkWindow>
 * 2. widget: CPointer<GtkWidget>
 * 3. userData: gpointer
 */
public typealias SetFocusHandler =
    CFunction<(window: CPointer<GtkWindow>, widget: CPointer<GtkWidget>, userData: gpointer) -> Unit>
