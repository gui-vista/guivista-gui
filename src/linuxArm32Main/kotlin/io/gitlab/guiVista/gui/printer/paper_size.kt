package io.gitlab.guiVista.gui.printer

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import io.gitlab.guiVista.core.Closable
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString

public actual class PaperSize private constructor(
    ptr: CPointer<GtkPaperSize>? = null,
    name: String = "",
    ippName: String = "",
    ppdName: String = "",
    ppdDisplayName: String = "",
    width: Double = 0.0,
    height: Double = 0.0
) : Closable {
    public val gtkPaperSizePtr: CPointer<GtkPaperSize>? = when {
        name.isNotEmpty() -> {
            gtk_paper_size_new(name)
        }
        ippName.isNotEmpty() -> {
            gtk_paper_size_new_from_ipp(ipp_name = ippName, width = width, height = height)
        }
        ppdName.isNotEmpty() -> {
            gtk_paper_size_new_from_ppd(ppd_name = ppdName, ppd_display_name = ppdDisplayName, width = width,
                height = height)
        }
        else -> {
            ptr
        }
    }

    public actual val name: String
        get() = gtk_paper_size_get_name(gtkPaperSizePtr)?.toKString() ?: ""
    public actual val displayName: String
        get() = gtk_paper_size_get_display_name(gtkPaperSizePtr)?.toKString() ?: ""
    public actual val ppdName: String
        get() = gtk_paper_size_get_ppd_name(gtkPaperSizePtr)?.toKString() ?: ""
    public actual val isIpp: Boolean
        get() = gtk_paper_size_is_ipp(gtkPaperSizePtr) == TRUE
    public actual val isCustom: Boolean
        get() = gtk_paper_size_is_custom(gtkPaperSizePtr) == TRUE
    public actual val defaultSize: String
        get() = gtk_paper_size_get_default()?.toKString() ?: ""

    public actual companion object {
        public actual fun create(name: String): PaperSize = PaperSize(name = name)

        public actual fun fromPpd(ppdName: String, ppdDisplayName: String, width: Double, height: Double): PaperSize =
            PaperSize(
                ppdName = ppdName,
                ppdDisplayName = ppdDisplayName,
                width = width,
                height = height
            )

        public actual fun fromIpp(ippName: String): PaperSize = PaperSize(ippName = ippName)

        public actual fun copy(original: PaperSize): PaperSize =
            fromPointer(gtk_paper_size_copy(original.gtkPaperSizePtr))

        public fun fromPointer(ptr: CPointer<GtkPaperSize>?): PaperSize = PaperSize(ptr = ptr)
    }

    public actual fun fetchPaperSizes(includeCustom: Boolean): DoublyLinkedList =
        DoublyLinkedList.fromPointer(gtk_paper_size_get_paper_sizes(if (includeCustom) TRUE else FALSE))

    override fun close() {
        gtk_paper_size_free(gtkPaperSizePtr)
    }

    /**
     * Changes the dimensions of a size to width x height.
     * @param width The new width in units of [unit].
     * @param height The new height in units of [unit].
     * @param unit The unit for [width] and [height].
     */
    public fun changeSize(width: Double, height: Double, unit: GtkUnit) {
        gtk_paper_size_set_size(size = gtkPaperSizePtr, width = width, height = height, unit = unit)
    }

    /**
     * Gets the default top margin for the [PaperSize].
     * @param unit The unit for the return value, not `GTK_UNIT_NONE`.
     * @return The default top margin.
     */
    public fun fetchDefaultTopMargin(unit: GtkUnit): Double =
        gtk_paper_size_get_default_top_margin(gtkPaperSizePtr, unit)

    /**
     * Gets the default bottom margin for the [PaperSize].
     * @param unit The unit for the return value, not `GTK_UNIT_NONE`.
     * @return The default bottom margin.
     */
    public fun fetchDefaultBottomMargin(unit: GtkUnit): Double =
        gtk_paper_size_get_default_bottom_margin(gtkPaperSizePtr, unit)

    /**
     * Gets the default left margin for the [PaperSize].
     * @param unit The unit for the return value, not `GTK_UNIT_NONE`.
     * @return The default left margin.
     */
    public fun fetchDefaultLeftMargin(unit: GtkUnit): Double =
        gtk_paper_size_get_default_left_margin(gtkPaperSizePtr, unit)

    /**
     * Gets the default right margin for the [PaperSize].
     * @param unit The unit for the return value, not `GTK_UNIT_NONE`.
     * @return The default right margin.
     */
    public fun fetchDefaultRightMargin(unit: GtkUnit): Double =
        gtk_paper_size_get_default_right_margin(gtkPaperSizePtr, unit)
}

public fun CPointer<GtkPaperSize>?.toPaperSize(): PaperSize = PaperSize.fromPointer(this)
