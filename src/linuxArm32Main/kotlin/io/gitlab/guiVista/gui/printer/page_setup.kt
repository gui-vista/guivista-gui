package io.gitlab.guiVista.gui.printer

import glib2.TRUE
import glib2.g_object_unref
import gtk3.*
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.cValuesOf

public actual class PageSetup private constructor(
    ptr: CPointer<GtkPageSetup>? = null,
    fileName: String = "",
    error: Error? = null
) : ObjectBase {
    public val gtkPageSetupPtr: CPointer<GtkPageSetup>? = when {
        ptr != null -> ptr
        fileName.isNotEmpty() -> gtk_page_setup_new_from_file(fileName, cValuesOf(error?.gErrorPtr))
        else -> gtk_page_setup_new()
    }
    public actual var paperSize: PaperSize
        get() = PaperSize.fromPointer(gtk_page_setup_get_paper_size(gtkPageSetupPtr))
        set(value) = gtk_page_setup_set_paper_size(gtkPageSetupPtr, value.gtkPaperSizePtr)

    /** The page orientation. */
    public var orientation: GtkPageOrientation
        get() = gtk_page_setup_get_orientation(gtkPageSetupPtr)
        set(value) = gtk_page_setup_set_orientation(gtkPageSetupPtr, value)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkPageSetup>?): PageSetup = PageSetup(ptr = ptr)

        public actual fun create(): PageSetup = PageSetup()

        public actual fun copy(original: PageSetup): PageSetup =
            fromPointer(gtk_page_setup_copy(original.gtkPageSetupPtr))

        public actual fun fromFile(fileName: String, error: Error?): PageSetup? {
            val ptr = gtk_page_setup_new_from_file(fileName, cValuesOf(error?.gErrorPtr))
            return if (ptr != null) fromPointer(ptr) else null
        }
    }

    public actual fun loadToFile(fileName: String, error: Error?): Boolean =
        gtk_page_setup_load_file(setup = gtkPageSetupPtr,
            file_name = fileName,
            error = cValuesOf(error?.gErrorPtr)
        ) == TRUE

    public actual fun saveToFile(fileName: String, error: Error?): Boolean =
        gtk_page_setup_to_file(
            setup = gtkPageSetupPtr,
            error = cValuesOf(error?.gErrorPtr),
            file_name = fileName
        ) == TRUE

    public actual fun changePaperSizeAndDefaultMargins(paperSize: PaperSize) {
        gtk_page_setup_set_paper_size_and_default_margins(gtkPageSetupPtr, paperSize.gtkPaperSizePtr)
    }

    override fun close() {
        g_object_unref(gtkPageSetupPtr)
    }

    /**
     * Gets the top margin in units of [unit].
     * @param unit The unit for the return value.
     * @return The top margin.
     */
    public fun fetchTopMargin(unit: GtkUnit): Double = gtk_page_setup_get_top_margin(gtkPageSetupPtr, unit)

    /**
     * Gets the bottom margin in units of [unit].
     * @param unit The unit for the return value.
     * @return The bottom margin.
     */
    public fun fetchBottomMargin(unit: GtkUnit): Double = gtk_page_setup_get_bottom_margin(gtkPageSetupPtr, unit)

    /**
     * Gets the left margin in units of [unit].
     * @param unit The unit for the return value.
     * @return The left margin.
     */
    public fun fetchLeftMargin(unit: GtkUnit): Double = gtk_page_setup_get_left_margin(gtkPageSetupPtr, unit)

    /**
     * Gets the right margin in units of [unit].
     * @param unit The unit for the return value.
     * @return The right margin.
     */
    public fun fetchRightMargin(unit: GtkUnit): Double = gtk_page_setup_get_right_margin(gtkPageSetupPtr, unit)

    /**
     * Sets the top margin of the [PageSetup].
     * @param margin The new margin in units of [unit].
     * @param unit The units for [margin].
     */
    public fun changeTopMargin(margin: Double, unit: GtkUnit) {
        gtk_page_setup_set_top_margin(setup = gtkPageSetupPtr, margin = margin, unit = unit)
    }

    /**
     * Sets the bottom margin of the [PageSetup].
     * @param margin The new margin in units of [unit].
     * @param unit The units for [margin].
     */
    public fun changeBottomMargin(margin: Double, unit: GtkUnit) {
        gtk_page_setup_set_bottom_margin(setup = gtkPageSetupPtr, margin = margin, unit = unit)
    }

    /**
     * Sets the left margin of the [PageSetup].
     * @param margin The new margin in units of [unit].
     * @param unit The units for [margin].
     */
    public fun changeLeftMargin(margin: Double, unit: GtkUnit) {
        gtk_page_setup_set_left_margin(setup = gtkPageSetupPtr, margin = margin, unit = unit)
    }

    /**
     * Sets the right margin of the [PageSetup].
     * @param margin The new margin in units of [unit].
     * @param unit The units for [margin].
     */
    public fun changeRightMargin(margin: Double, unit: GtkUnit) {
        gtk_page_setup_set_right_margin(setup = gtkPageSetupPtr, margin = margin, unit = unit)
    }

    /**
     * Changes all margins for [PageSetup].
     * @param top The top margin.
     * @param bottom The bottom margin.
     * @param left The left margin.
     * @param right The right margin.
     * @param unit The units to use.
     */
    public fun changeAllMargins(top: Double, bottom: Double, left: Double, right: Double, unit: GtkUnit) {
        changeTopMargin(top, unit)
        changeBottomMargin(bottom, unit)
        changeLeftMargin(left, unit)
        changeRightMargin(right, unit)
    }

    /**
     * Returns the paper width in units of [unit]. Note that this function takes orientation, but not margins into
     * consideration.
     * @param unit The unit for the return value.
     * @return The paper width.
     * @see fetchPageWidth
     */
    public fun fetchPaperWidth(unit: GtkUnit): Double = gtk_page_setup_get_paper_width(gtkPageSetupPtr, unit)

    /**
     * Returns the paper height in units of [unit]. Note that this function takes orientation, but not margins into
     * consideration.
     * @see fetchPageHeight
     */
    public fun fetchPaperHeight(unit: GtkUnit): Double = gtk_page_setup_get_paper_height(gtkPageSetupPtr, unit)

    /**
     * Returns the page width in units of [unit]. Note that this function takes orientation, and margins into
     * consideration.
     * @see fetchPaperWidth
     */
    public fun fetchPageWidth(unit: GtkUnit): Double = gtk_page_setup_get_page_width(gtkPageSetupPtr, unit)

    /**
     * Returns the page height in units of [unit]. Note that this function takes orientation, and margins into
     * consideration.
     * @see fetchPaperHeight
     */
    public fun fetchPageHeight(unit: GtkUnit): Double = gtk_page_setup_get_page_height(gtkPageSetupPtr, unit)
}

public fun CPointer<GtkPageSetup>?.toPageSetup(): PageSetup = PageSetup.fromPointer(this)
