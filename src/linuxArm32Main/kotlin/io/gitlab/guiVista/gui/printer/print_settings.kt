package io.gitlab.guiVista.gui.printer

import glib2.FALSE
import glib2.TRUE
import glib2.g_object_unref
import gtk3.*
import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.cValuesOf
import kotlinx.cinterop.toKString

public actual class PrintSettings private constructor(ptr: CPointer<GtkPrintSettings>? = null) : ObjectBase {
    public val gtkPrintSettingsPtr: CPointer<GtkPrintSettings>? = ptr ?: gtk_print_settings_new()

    /** The orientation. */
    public var orientation: GtkPageOrientation
        get() = gtk_print_settings_get_orientation(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_orientation(gtkPrintSettingsPtr, value)

    /** Whether to print the output in duplex. */
    public var duplex: GtkPrintDuplex
        get() = gtk_print_settings_get_duplex(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_duplex(gtkPrintSettingsPtr, value)

    /** The print quality. */
    public var quality: GtkPrintQuality
        get() = gtk_print_settings_get_quality(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_quality(gtkPrintSettingsPtr, value)

    /** Layout of page in number-up mode. */
    public var sheetLayout: GtkNumberUpLayout
        get() = gtk_print_settings_get_number_up_layout(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_number_up_layout(gtkPrintSettingsPtr, value)

    /** Which pages to print. */
    public var printPages: GtkPrintPages
        get() = gtk_print_settings_get_print_pages(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_print_pages(gtkPrintSettingsPtr, value)

    /** The set of pages to print. */
    public var pageSet: GtkPageSet
        get() = gtk_print_settings_get_page_set(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_page_set(gtkPrintSettingsPtr, value)

    public actual var paperSize: PaperSize
        get() = PaperSize.fromPointer(gtk_print_settings_get_paper_size(gtkPrintSettingsPtr))
        set(value) = gtk_print_settings_set_paper_size(gtkPrintSettingsPtr, value.gtkPaperSizePtr)
    public actual var printer: String
        get() = gtk_print_settings_get_printer(gtkPrintSettingsPtr)?.toKString() ?: ""
        set(value) = gtk_print_settings_set_printer(gtkPrintSettingsPtr, value)
    public actual var useColor: Boolean
        get() = gtk_print_settings_get_use_color(gtkPrintSettingsPtr) == TRUE
        set(value) = gtk_print_settings_set_use_color(gtkPrintSettingsPtr, if (value) TRUE else FALSE)
    public actual var collate: Boolean
        get() = gtk_print_settings_get_collate(gtkPrintSettingsPtr) == TRUE
        set(value) = gtk_print_settings_set_collate(gtkPrintSettingsPtr, if (value) TRUE else FALSE)
    public actual var reverse: Boolean
        get() = gtk_print_settings_get_reverse(gtkPrintSettingsPtr) == TRUE
        set(value) = gtk_print_settings_set_reverse(gtkPrintSettingsPtr, if (value) TRUE else FALSE)
    public actual var totalCopies: Int
        get() = gtk_print_settings_get_n_copies(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_n_copies(gtkPrintSettingsPtr, value)
    public actual var pagesPerSheet: Int
        get() = gtk_print_settings_get_number_up(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_number_up(gtkPrintSettingsPtr, value)
    public actual var resolution: Int
        get() = gtk_print_settings_get_resolution(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_resolution(gtkPrintSettingsPtr, value)
    public actual val hResolution: Int
        get() = gtk_print_settings_get_resolution_x(gtkPrintSettingsPtr)
    public actual val vResolution: Int
        get() = gtk_print_settings_get_resolution_y(gtkPrintSettingsPtr)
    public actual var printLpi: Double
        get() = gtk_print_settings_get_printer_lpi(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_printer_lpi(gtkPrintSettingsPtr, value)
    public actual var scale: Double
        get() = gtk_print_settings_get_scale(gtkPrintSettingsPtr)
        set(value) = gtk_print_settings_set_scale(gtkPrintSettingsPtr, value)
    public actual var defaultSource: String
        get() = gtk_print_settings_get_default_source(gtkPrintSettingsPtr)?.toKString() ?: ""
        set(value) = gtk_print_settings_set_default_source(gtkPrintSettingsPtr, value)
    public actual var mediaType: String
        get() = gtk_print_settings_get_media_type(gtkPrintSettingsPtr)?.toKString() ?: ""
        set(value) = gtk_print_settings_set_media_type(gtkPrintSettingsPtr, value)
    public actual var dither: String
        get() = gtk_print_settings_get_dither(gtkPrintSettingsPtr)?.toKString() ?: ""
        set(value) = gtk_print_settings_set_dither(gtkPrintSettingsPtr, value)
    public actual var finishings: String
        get() = gtk_print_settings_get_finishings(gtkPrintSettingsPtr)?.toKString() ?: ""
        set(value) = gtk_print_settings_set_finishings(gtkPrintSettingsPtr, value)
    public actual var outputBin: String
        get() = gtk_print_settings_get_output_bin(gtkPrintSettingsPtr)?.toKString() ?: ""
        set(value) = gtk_print_settings_set_output_bin(gtkPrintSettingsPtr, value)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkPrintSettings>?): PrintSettings = PrintSettings(ptr = ptr)

        public actual fun create(): PrintSettings = PrintSettings()

        public actual fun fromFile(fileName: String, error: Error?): PrintSettings? {
            val ptr = gtk_print_settings_new_from_file(fileName, cValuesOf(error?.gErrorPtr))
            return if (ptr != null) fromPointer(ptr) else null
        }

        public actual fun copy(original: PrintSettings): PrintSettings =
            fromPointer(gtk_print_settings_copy(original.gtkPrintSettingsPtr))
    }

    public actual fun fetchStringValue(key: String): String =
        gtk_print_settings_get(gtkPrintSettingsPtr, key)?.toKString() ?: ""

    public actual fun changeStringValue(key: String, value: String) {
        gtk_print_settings_set(settings = gtkPrintSettingsPtr, key = key, value = value)
    }

    public actual fun unsetValue(key: String) {
        gtk_print_settings_unset(gtkPrintSettingsPtr, key)
    }

    public actual fun fetchBooleanValue(key: String): Boolean =
        gtk_print_settings_get_bool(gtkPrintSettingsPtr, key) == TRUE

    public actual fun changeBooleanValue(key: String, value: Boolean) {
        gtk_print_settings_set_bool(settings = gtkPrintSettingsPtr, key = key, value = if (value) TRUE else FALSE)
    }

    public actual fun fetchDoubleValue(key: String, default: Double): Double =
        gtk_print_settings_get_double_with_default(settings = gtkPrintSettingsPtr, key = key, def = default)

    public actual fun changeDoubleValue(key: String, value: Double) {
        gtk_print_settings_set_double(settings = gtkPrintSettingsPtr, key = key, value = value)
    }

    /**
     * Returns the value associated with [key], interpreted as a length. The returned value is converted to units.
     * @param key The key to use.
     * @param unit The unit of the return value.
     * @return The length value of [key], converted to [unit].
     */
    public fun fetchLength(key: String, unit: GtkUnit): Double =
        gtk_print_settings_get_length(settings = gtkPrintSettingsPtr, key = key, unit = unit)

    /**
     * Associates a length in units of unit with [key].
     * @param key The key to use.
     * @param value A length.
     * @param unit The unit of length.
     */
    public fun changeLength(key: String, value: Double, unit: GtkUnit) {
        gtk_print_settings_set_length(settings = gtkPrintSettingsPtr, key = key, value = value, unit = unit)
    }

    public actual fun fetchIntValue(key: String, default: Int): Int =
        gtk_print_settings_get_int_with_default(settings = gtkPrintSettingsPtr, key = key, def = default)

    public actual fun changeIntValue(key: String, value: Int) {
        gtk_print_settings_set_int(settings = gtkPrintSettingsPtr, key = key, value = value)
    }

    public actual fun changeHAndVResolution(hResolution: Int, vResolution: Int) {
        gtk_print_settings_set_resolution_xy(settings = gtkPrintSettingsPtr, resolution_x = hResolution,
            resolution_y = vResolution)
    }

    public actual fun loadFile(fileName: String, error: Error?): Boolean =
        gtk_print_settings_load_file(settings = gtkPrintSettingsPtr,
            error = cValuesOf(error?.gErrorPtr),
            file_name = fileName
        ) == TRUE

    public actual fun saveToFile(fileName: String, error: Error?): Boolean =
        gtk_print_settings_to_file(
            settings = gtkPrintSettingsPtr,
            file_name = fileName,
            error = cValuesOf(error?.gErrorPtr)
        ) == TRUE

    override fun close() {
        g_object_unref(gtkPrintSettingsPtr)
    }

    /**
     * Gets the value of paper width, converted to [unit].
     * @param unit The unit for the return value.
     * @return The paper width in units of [unit].
     */
    public fun fetchPaperWidth(unit: GtkUnit): Double = gtk_print_settings_get_paper_width(gtkPrintSettingsPtr, unit)

    /**
     * Gets the value of paper height, converted to [unit].
     * @param unit The unit for the return value.
     * @return The paper height in units of [unit].
     */
    public fun fetchPaperHeight(unit: GtkUnit): Double = gtk_print_settings_get_paper_height(gtkPrintSettingsPtr, unit)

    /**
     * Sets the paper width.
     * @param width The paper width.
     * @param unit The units of [width].
     */
    public fun changePaperWidth(width: Double, unit: GtkUnit) {
        gtk_print_settings_set_paper_width(settings = gtkPrintSettingsPtr, width = width, unit = unit)
    }

    /**
     * Sets the paper height.
     * @param height The paper height.
     * @param unit The units of [height].
     */
    public fun changePaperHeight(height: Double, unit: GtkUnit) {
        gtk_print_settings_set_paper_height(settings = gtkPrintSettingsPtr, height = height, unit = unit)
    }
}

public fun CPointer<GtkPrintSettings>?.toPrintSettings(): PrintSettings = PrintSettings.fromPointer(this)
