package io.gitlab.guiVista.gui.keyboard

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer

public actual object AcceleratorMap : ObjectBase {
    public actual fun load(fileName: String) {
        gtk_accel_map_load(fileName)
    }

    public actual fun save(fileName: String) {
        gtk_accel_map_save(fileName)
    }

    override fun close() {
        // Do nothing since AcceleratorMap is a Singleton.
    }

    public actual fun addEntry(accelPath: String, accelKey: UInt, accelMods: UInt) {
        gtk_accel_map_add_entry(accel_path = accelPath, accel_key = accelKey, accel_mods = accelMods)
    }

    public actual fun lookupEntry(accelPath: String): Boolean = gtk_accel_map_lookup_entry(accelPath, null) == TRUE

    public actual fun changeEntry(accelPath: String, accelKey: UInt, accelMods: UInt, replace: Boolean): Boolean =
        gtk_accel_map_change_entry(accel_path = accelPath, accel_key = accelKey, accel_mods = accelMods,
            replace = if (replace) TRUE else FALSE) == TRUE

    public actual fun loadFileDescriptor(fd: Int) {
        gtk_accel_map_load_fd(fd)
    }

    public actual fun saveFileDescriptor(fd: Int) {
        gtk_accel_map_save_fd(fd)
    }

    public actual fun lockPath(accelPath: String) {
        gtk_accel_map_lock_path(accelPath)
    }

    public actual fun unlockPath(accelPath: String) {
        gtk_accel_map_unlock_path(accelPath)
    }

    /**
     * Connects the *changed* event to a [handler] on an [AcceleratorMap]. The *changed* event is used to of a change in
     * the global accelerator map. The path is also used as the detail for the event, so it is possible to connect to
     * `changed::accel_path`.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectChangedEvent(handler: CPointer<ChangedHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtk_accel_map_get(), signal = AcceleratorMapEvent.changed, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtk_accel_map_get(), handlerId)
    }
}

/**
 * The event handler for the *changed* event. Arguments:
 * 1. obj: CPointer<GtkAccelMap>
 * 2. accelPath: CPointer<ByteVar> (represents a String)
 * 3. accelKey: UInt
 * 4. accelMods: GdkModifierType
 * 5. userData: gpointer
 */
public typealias ChangedHandler = CFunction<(
    accelMap: CPointer<GtkAccelMap>,
    accelPath: CPointer<ByteVar>,
    accelKey: UInt,
    accelMods: GdkModifierType,
    userData: gpointer
) -> Unit>
