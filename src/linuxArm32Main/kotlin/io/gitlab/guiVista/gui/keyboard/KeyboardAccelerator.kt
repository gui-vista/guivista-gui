package io.gitlab.guiVista.gui.keyboard

import glib2.TRUE
import gtk3.*
import kotlinx.cinterop.toKString

public actual object KeyboardAccelerator {
    public actual var defaultModMask: UInt
        get() = gtk_accelerator_get_default_mod_mask()
        set(value) = gtk_accelerator_set_default_mod_mask(value)

    public actual fun valid(keyVal: UInt, modifiers: UInt): Boolean =
        gtk_accelerator_valid(keyVal, modifiers) == TRUE

    public actual fun name(acceleratorKey: UInt, acceleratorMods: UInt): String =
        gtk_accelerator_name(acceleratorKey, acceleratorMods)?.toKString() ?: ""

    public actual fun fetchLabel(acceleratorKey: UInt, acceleratorMods: UInt): String =
        gtk_accelerator_get_label(acceleratorKey, acceleratorMods)?.toKString() ?: ""
}
