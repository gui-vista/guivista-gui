package io.gitlab.guiVista.gui

import glib2.FALSE
import glib2.TRUE
import glib2.g_object_unref
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.*

public actual class StatusIcon private constructor(ptr: CPointer<GtkStatusIcon>?) : ObjectBase {
    public val gtkStatusIconPtr: CPointer<GtkStatusIcon>? = ptr

    public actual val embedded: Boolean
        get() = gtk_status_icon_is_embedded(gtkStatusIconPtr) == TRUE

    public actual var hasTooltip: Boolean
        get() = gtk_status_icon_get_has_tooltip(gtkStatusIconPtr) == TRUE
        set(value) = gtk_status_icon_set_has_tooltip(gtkStatusIconPtr, if (value) TRUE else FALSE)

    public actual var iconName: String
        get() = gtk_status_icon_get_icon_name(gtkStatusIconPtr)?.toKString() ?: ""
        set(value) = gtk_status_icon_set_from_icon_name(gtkStatusIconPtr, value)

    public actual var imageBuffer: ImageBuffer
        get() = ImageBuffer.fromPointer(gtk_status_icon_get_pixbuf(gtkStatusIconPtr))
        set(value) = gtk_status_icon_set_from_pixbuf(gtkStatusIconPtr, value.gdkPixbufPtr)

    public actual val size: Int
        get() = gtk_status_icon_get_size(gtkStatusIconPtr)

    public actual var title: String
        get() = gtk_status_icon_get_title(gtkStatusIconPtr)?.toKString() ?: ""
        set(value) = gtk_status_icon_set_title(gtkStatusIconPtr, value)

    public actual var tooltipMarkup: String
        get() = gtk_status_icon_get_tooltip_markup(gtkStatusIconPtr)?.toKString() ?: ""
        set(value) = gtk_status_icon_set_tooltip_markup(gtkStatusIconPtr, value)

    public actual var tooltipText: String
        get() = gtk_status_icon_get_tooltip_text(gtkStatusIconPtr)?.toKString() ?: ""
        set(value) = gtk_status_icon_set_tooltip_text(gtkStatusIconPtr, value)

    public actual var visible: Boolean
        get() = gtk_status_icon_get_visible(gtkStatusIconPtr) == TRUE
        set(value) = gtk_status_icon_set_visible(gtkStatusIconPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkStatusIcon>?): StatusIcon = StatusIcon(ptr)

        public actual fun create(): StatusIcon = StatusIcon(gtk_status_icon_new())

        public actual fun fromImageBuffer(imageBuffer: ImageBuffer): StatusIcon =
            StatusIcon(gtk_status_icon_new_from_pixbuf(imageBuffer.gdkPixbufPtr))

        public actual fun fromFile(fileName: String): StatusIcon =
            StatusIcon(gtk_status_icon_new_from_file(fileName))

        public actual fun fromIconName(iconName: String): StatusIcon =
            StatusIcon(gtk_status_icon_new_from_icon_name(iconName))
    }

    override fun close() {
        g_object_unref(gtkStatusIconPtr)
    }

    override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gtkStatusIconPtr, handlerId)
    }

    /**
     * Connects the *activate* event to a [handler] on a [StatusIcon]. This event occurs when the user activates the
     * status icon. If and how status icons can activated is platform-dependent.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActivateEvent(handler: CPointer<ActivateHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkStatusIconPtr, signal = StatusIconEvent.activate, slot = handler, data = userData)

    /**
     * Connects the *button-press-event* event to a [handler] on a [StatusIcon]. This event occurs when a button
     * (typically from a mouse) is pressed. Whether this event is fired is platform-dependent. Use the **activate**,
     * and **popup-menu** events in preference.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectButtonPressEvent(handler: CPointer<ButtonPressEventHandler>,
                                       userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkStatusIconPtr, signal = StatusIconEvent.buttonPressEvent, slot = handler,
            data = userData)

    /**
     * Connects the *button-release-event* event to a [handler] on a [StatusIcon]. This event occurs when a button
     * (typically from a mouse) is released. Whether this event is fired is platform-dependent. Use the **activate**,
     * and **popup-menu** events in preference.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectButtonReleaseEvent(handler: CPointer<ButtonReleaseEventHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkStatusIconPtr, signal = StatusIconEvent.buttonReleaseEvent, slot = handler,
            data = userData)

    /**
     * Connects the *query-tooltip* event to a [handler] on a [StatusIcon]. This event occurs when the hover timeout
     * has expired with the cursor hovering above the status icon, or when the status icon got focus in keyboard mode.
     * Using the given coordinates the event handler should determine whether a tooltip should be shown for the status
     * icon. If this is the case then *true* should be returned, *false* otherwise. Note that if keyboardMode is *true*
     * then the values of xPos, and yPos are undefined (all set to *0*) and should not be used.
     *
     * The event handler is free to manipulate tooltip with the therefore destined function calls. Whether this event
     * is fired is platform-dependent. For plain text tooltips use [tooltipText] in preference.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectQueryTooltipEvent(handler: CPointer<QueryTooltipHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkStatusIconPtr, signal = StatusIconEvent.queryTooltip, slot = handler,
            data = userData)

    /**
     * Connects the *scroll-event* event to a [handler] on a [StatusIcon]. This event occurs when a button in the 4 to
     * 7 range is pressed. Wheel mice are usually configured to generate button press events for buttons 4 and 5 when
     * the wheel is turned. Whether this event is emitted is platform-dependent.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectScrollEvent(handler: CPointer<ScrollEventHandler>,
                                  userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkStatusIconPtr, signal = StatusIconEvent.scrollEvent, slot = handler,
            data = userData)

    /**
     * Connects the *size-changed* event to a [handler] on a [StatusIcon]. This event occurs when the size available
     * for the image changes, e.g. because the notification area got resized.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSizeChangedEvent(handler: CPointer<SizeChangedHandler>,
                                       userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkStatusIconPtr, signal = StatusIconEvent.sizeChanged, slot = handler,
            data = userData)

    /**
     * Connects the *popup-menu* event to a [handler] on a [StatusIcon]. This event occurs when the user brings up the
     * context menu of the status icon. Whether status icons can have context menus, and how these are activated is
     * platform dependent. The button and activateTime parameters should be passed as the last to arguments to
     * [io.gitlab.guiVista.gui.widget.menu.MenuBase.popup].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPopupMenuEvent(handler: CPointer<PopupMenuHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkStatusIconPtr, signal = StatusIconEvent.popupMenu, slot = handler,
            data = userData)
}

/**
 * The event handler for the *activate* event. Arguments:
 * 1. statusIcon: CPointer<GtkStatusIcon>
 * 2. userData: gpointer
 */
public typealias ActivateHandler = CFunction<(statusIcon: CPointer<GtkStatusIcon>, userData: gpointer) -> Unit>

/**
 * The event handler for the *button-press-event* event. Arguments:
 * 1. statusIcon: CPointer<GtkStatusIcon>
 * 2. event: CPointer<GdkEvent>
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias ButtonPressEventHandler = CFunction<(
    statusIcon: CPointer<GtkStatusIcon>,
    event: CPointer<GdkEvent>,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *button-release-event* event. Arguments:
 * 1. statusIcon: CPointer<GtkStatusIcon>
 * 2. event: CPointer<GdkEvent>
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias ButtonReleaseEventHandler = CFunction<(
    statusIcon: CPointer<GtkStatusIcon>,
    event: CPointer<GdkEvent>,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *query-tooltip* event. Arguments:
 * 1. statusIcon: CPointer<GtkStatusIcon>
 * 2. xPos: Int
 * 3. yPos: Int
 * 4. keyboardMode: Int (represents a Boolean)
 * 5. tooltip: CPointer<GtkTooltip>
 * 6. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias QueryTooltipHandler = CFunction<(
    statusIcon: CPointer<GtkStatusIcon>,
    xPos: Int,
    yPos: Int,
    keyboardMode: Int,
    tooltip: CPointer<GtkTooltip>,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *scroll-event* event. Arguments:
 * 1. statusIcon: CPointer<GtkStatusIcon>
 * 2. event: CPointer<GdkEvent>
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias ScrollEventHandler = CFunction<(
    statusIcon: CPointer<GtkStatusIcon>,
    event: CPointer<GdkEvent>,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *size-changed* event. Arguments:
 * 1. statusIcon: CPointer<GtkStatusIcon>
 * 2. size: Int
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias SizeChangedHandler = CFunction<(
    statusIcon: CPointer<GtkStatusIcon>,
    size: Int,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *popup-menu* event. Arguments:
 * 1. statusIcon: CPointer<GtkStatusIcon>
 * 2. button: UInt
 * 3. activateTime: UInt
 * 4. userData: gpointer
 */
public typealias PopupMenuHandler = CFunction<(
    statusIcon: CPointer<GtkStatusIcon>,
    button: UInt,
    activateTime: UInt,
    userData: gpointer
) -> Unit>

public fun CPointer<GtkStatusIcon>?.toStatusIcon(): StatusIcon = StatusIcon.fromPointer(this)
