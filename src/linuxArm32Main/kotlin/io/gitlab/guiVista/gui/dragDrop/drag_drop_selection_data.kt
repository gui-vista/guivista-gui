package io.gitlab.guiVista.gui.dragDrop

import glib2.FALSE
import glib2.TRUE
import glib2.g_free
import glib2.g_strfreev
import gtk3.*
import io.gitlab.guiVista.core.Closable
import io.gitlab.guiVista.gui.Atom
import io.gitlab.guiVista.gui.ImageBuffer
import io.gitlab.guiVista.gui.text.TextBuffer
import kotlinx.cinterop.*

public actual class DragDropSelectionData private constructor(ptr: CPointer<GtkSelectionData>?) : Closable {
    public val gtkSelectionDataPtr: CPointer<GtkSelectionData>? = ptr
    public actual val dataType: Atom
        get() = Atom.fromPointer(gtk_selection_data_get_data_type(gtkSelectionDataPtr))
    public actual val format: Int
        get() = gtk_selection_data_get_format(gtkSelectionDataPtr)

    public actual fun fetchTarget(): Atom = Atom.fromPointer(gtk_selection_data_get_target(gtkSelectionDataPtr))

    public actual fun fetchData(): Array<UByte> {
        val tmpList = mutableListOf<UByte>()
        val ptr = gtk_selection_data_get_data(gtkSelectionDataPtr)
        var pos = 0
        var finished = false
        while (!finished) {
            val item = ptr?.get(pos)
            pos++
            if (item != null) tmpList += item else finished = true
        }
        return tmpList.toTypedArray()
    }

    public actual fun fetchDataAsAtom(): Atom = Atom.fromPointer(gtk_selection_data_get_selection(gtkSelectionDataPtr))

    public actual fun fetchDataLength(): Int = gtk_selection_data_get_length(gtkSelectionDataPtr)

    @Suppress("ReplaceRangeToWithUntil")
    public actual fun fetchMultipleTargets(): Pair<Boolean, Array<Atom>> = memScoped {
        val totalItems = alloc<IntVar>()
        val items = alloc<CPointerVar<GdkAtomVar>>()
        val success = gtk_selection_data_get_targets(selection_data = gtkSelectionDataPtr, n_atoms = totalItems.ptr,
            targets = items.ptr) == TRUE
        val tmpList = mutableListOf<Atom>()
        (0..(totalItems.value - 1)).forEach { pos ->
            val tmpItem = items.value?.get(pos)
            if (tmpItem != null) tmpList += Atom.fromPointer(tmpItem)
        }
        success to tmpList.toTypedArray()
    }

    public actual fun fetchDataAsImageBuffer(): ImageBuffer? {
        val ptr = gtk_selection_data_get_pixbuf(gtkSelectionDataPtr)
        return if (ptr != null) ImageBuffer.fromPointer(ptr) else null
    }

    public actual fun fetchDataAsText(): String {
        val ptr = gtk_selection_data_get_text(gtkSelectionDataPtr)
        val result = ptr?.reinterpret<ByteVar>()?.toKString() ?: ""
        g_free(ptr)
        return result
    }

    public actual fun fetchDataAsUris(): Array<String> {
        val tmpList = mutableListOf<String>()
        val ptr = gtk_selection_data_get_uris(gtkSelectionDataPtr)
        var finished = false
        var pos = 0
        while (!finished) {
            val item = ptr?.get(pos)
            pos++
            if (item != null) tmpList += item.toKString() else finished = true
        }
        if (ptr != null) g_strfreev(ptr)
        return tmpList.toTypedArray()
    }

    public actual fun changeDataAsImageBuffer(imageBuffer: ImageBuffer): Boolean =
        gtk_selection_data_set_pixbuf(gtkSelectionDataPtr, imageBuffer.gdkPixbufPtr) == TRUE

    public actual fun changeDataAsString(str: String): Boolean =
        gtk_selection_data_set_text(selection_data = gtkSelectionDataPtr, str = str, len = -1) == TRUE

    public actual fun changeDataAsUris(uris: Array<String>): Boolean = memScoped {
        gtk_selection_data_set_uris(gtkSelectionDataPtr, uris.toCStringArray(this)) == TRUE
    }

    public actual fun targetsIncludeImage(writable: Boolean): Boolean =
        gtk_selection_data_targets_include_image(gtkSelectionDataPtr, if (writable) TRUE else FALSE) == TRUE

    public actual fun targetsIncludeRichText(buffer: TextBuffer): Boolean =
        gtk_selection_data_targets_include_rich_text(gtkSelectionDataPtr, buffer.gtkTextBufferPtr) == TRUE

    public actual fun targetsIncludeText(): Boolean =
        gtk_selection_data_targets_include_text(gtkSelectionDataPtr) == TRUE

    public actual fun targetsIncludeUri(): Boolean =
        gtk_selection_data_targets_include_uri(gtkSelectionDataPtr) == TRUE

    override fun close() {
        gtk_selection_data_free(gtkSelectionDataPtr)
    }

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkSelectionData>?): DragDropSelectionData = DragDropSelectionData(ptr)
    }
}

public fun CPointer<GtkSelectionData>?.toDragDropSelectionData(): DragDropSelectionData =
    DragDropSelectionData.fromPointer(this)
