package io.gitlab.guiVista.gui.dragDrop

import glib2.g_object_unref
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer

public actual class DragDropContext private constructor(ptr: CPointer<GdkDragContext>?) : ObjectBase {
    public val gdkDragContextPtr: CPointer<GdkDragContext>? = ptr
    public actual val actions: UInt
        get() = gdk_drag_context_get_actions(gdkDragContextPtr)
    public actual val selectedAction: UInt
        get() = gdk_drag_context_get_selected_action(gdkDragContextPtr)
    public actual val suggestedAction: UInt
        get() = gdk_drag_context_get_suggested_action(gdkDragContextPtr)
    public actual val targets: DoublyLinkedList
        get() = DoublyLinkedList.fromPointer(gdk_drag_context_list_targets(gdkDragContextPtr))

    public actual fun changeHotspot(xPos: Int, yPos: Int) {
        gdk_drag_context_set_hotspot(context = gdkDragContextPtr, hot_x = xPos, hot_y = yPos)
    }

    override fun close() {
        if (gdkDragContextPtr != null) g_object_unref(gdkDragContextPtr)
    }

    public companion object {
        public fun fromPointer(ptr: CPointer<GdkDragContext>?): DragDropContext = DragDropContext(ptr)
    }

    /**
     * Connects the *action-changed* event to a [handler] on this [DragDropContext]. This event occurs when a new
     * action is being chosen for the **Drag N Drop** operation. This event will only be fired if the [DragDropContext]
     * manages the drag and drop operation. See `gdk_drag_context_manage_dnd()` for more information.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActionChangedEvent(handler: CPointer<ActionChangedHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gdkDragContextPtr, signal = DragDropContextEvent.actionChanged, slot = handler,
            data = userData)

    /**
     * Connects the *cancel* event to a [handler] on this [DragDropContext]. This event occurs when the **Drag N Drop**
     * operation was cancelled. This event will only be fired if the [DragDropContext]
     * manages the drag and drop operation. See `gdk_drag_context_manage_dnd()` for more information.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCancelEvent(handler: CPointer<CancelHandler>,
                                  userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gdkDragContextPtr, signal = DragDropContextEvent.cancel, slot = handler,
            data = userData)

    /**
     * Connects the *dnd-finished* event to a [handler] on this [DragDropContext]. This event occurs when the
     * **Drag N Drop** operation was finished, with the drag destination reading all data. The drag source can now free
     * all miscellaneous data. This event will only be fired if the [DragDropContext]
     * manages the drag and drop operation. See `gdk_drag_context_manage_dnd()` for more information.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectDragDropFinishedEvent(handler: CPointer<DragDropFinishedHandler>,
                                            userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gdkDragContextPtr, signal = DragDropContextEvent.dragDropFinished, slot = handler,
            data = userData)

    /**
     * Connects the *drop-performed* event to a [handler] on this [DragDropContext]. This event occurs when the
     * **Drag N Drop** operation was performed on an accepting client. This event will only be fired if the
     * [DragDropContext] manages the drag and drop operation. See `gdk_drag_context_manage_dnd()` for more information.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectDropPerformedEvent(handler: CPointer<DropPerformedHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gdkDragContextPtr, signal = DragDropContextEvent.dropPerformed, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gdkDragContextPtr, handlerId)
    }
}

/**
 * The event handler for the *action-changed* event. Arguments:
 * 1. ctx: CPointer<GdkDragContext>
 * 2. action: GdkDragAction
 * 3. userData: gpointer
 */
public typealias ActionChangedHandler = CFunction<(
    ctx: CPointer<GdkDragContext>,
    action: GdkDragAction,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *cancel* event. Arguments:
 * 1. ctx: CPointer<GdkDragContext>
 * 2. reason: GdkDragCancelReason
 * 3. userData: gpointer
 */
public typealias CancelHandler = CFunction<(
    ctx: CPointer<GdkDragContext>,
    reason: GdkDragCancelReason,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *drop-performed* event. Arguments:
 * 1. ctx: CPointer<GdkDragContext>
 * 2. time: Int
 * 3. userData: gpointer
 */
public typealias DropPerformedHandler = CFunction<(
    ctx: CPointer<GdkDragContext>,
    time: Int,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *dnd-finished* event. Arguments:
 * 1. ctx: CPointer<GdkDragContext>
 * 2. userData: gpointer
 */
public typealias DragDropFinishedHandler = CFunction<(ctx: CPointer<GdkDragContext>, userData: gpointer) -> Unit>

public fun CPointer<GdkDragContext>?.toDragDropContext(): DragDropContext = DragDropContext.fromPointer(this)
