package io.gitlab.guiVista.gui.dragDrop

import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.ImageBuffer
import io.gitlab.guiVista.gui.widget.WidgetBase
import io.gitlab.guiVista.io.icon.IconBase
import kotlinx.cinterop.*

public actual fun WidgetBase.addDragImageTargets() {
    gtk_drag_source_add_image_targets(gtkWidgetPtr)
}

public actual fun WidgetBase.addDragTextTargets() {
    gtk_drag_source_add_text_targets(gtkWidgetPtr)
}

public actual fun WidgetBase.addDragUriTargets() {
    gtk_drag_source_add_uri_targets(gtkWidgetPtr)
}

public actual var WidgetBase.dragTargets: DragDropTargetList?
    get() {
        val ptr = gtk_drag_source_get_target_list(gtkWidgetPtr)
        return if (ptr != null) DragDropTargetList.fromPointer(ptr) else null
    }
    set(value) = gtk_drag_source_set_target_list(gtkWidgetPtr, value?.gtkTargetListPtr)

public actual fun WidgetBase.setDragSource(
    startBtnMask: UInt,
    dragActions: UInt,
    vararg targets: DragDropTargetEntry
): Unit = memScoped {
    val totalTargets = targets.size
    val tmp = allocArray<GtkTargetEntry>(totalTargets)
    targets.forEachIndexed { pos, item ->
        tmp[pos].flags = item.flags
        tmp[pos].info = item.info
        tmp[pos].target = item.target.cstr.ptr
    }
    gtk_drag_source_set(
        widget = gtkWidgetPtr,
        start_button_mask = startBtnMask,
        n_targets = totalTargets,
        actions = dragActions,
        targets = tmp
    )
}

public actual fun WidgetBase.unsetDragSource() {
    gtk_drag_source_unset(gtkWidgetPtr)
}

public actual fun WidgetBase.changeDragIcon(icon: IconBase) {
    gtk_drag_source_set_icon_gicon(gtkWidgetPtr, icon.gIconPtr)
}

public actual fun WidgetBase.changeDragIconName(iconName: String) {
    gtk_drag_source_set_icon_name(gtkWidgetPtr, iconName)
}

public actual fun WidgetBase.changeDragIconBuffer(iconBuffer: ImageBuffer) {
    gtk_drag_source_set_icon_pixbuf(gtkWidgetPtr, iconBuffer.gdkPixbufPtr)
}

/**
 * Connects the *drag-begin* event to a [handler] on a widget. This event is used when a drag is started. A
 * typical reason to connect to this event is to set up a custom drag icon with e.g. [changeDragIconBuffer]. Note that
 * some widgets set up a drag icon in the default handler of this event, so you may have to use
 * `g_signal_connect_after()` to override what the default handler did.
 * @param handler The event handler for the event.
 * @param userData User data to pass through to the [handler].
 */
public fun WidgetBase.connectDragBeginEvent(handler: CPointer<DragBeginHandler>,
                                            userData: gpointer = fetchEmptyDataPointer()): ULong =
    connectGSignal(obj = gtkWidgetPtr, signal = DragDropEvent.dragBegin, slot = handler, data = userData)

/**
 * Connects the *drag-data-get* event to a [handler] on a widget. This event is used when the drop site requests the
 * data which is dragged. It is the responsibility of the event handler to fill data with the data in the format, which
 * is indicated by info.
 * @param handler The event handler for the event.
 * @param userData User data to pass through to the [handler].
 * @see DragDropSelectionData.changeDataAsString
 */
public fun WidgetBase.connectDragDataGetEvent(handler: CPointer<DragDataGetHandler>,
                                              userData: gpointer = fetchEmptyDataPointer()): ULong =
    connectGSignal(obj = gtkWidgetPtr, signal = DragDropEvent.dragDataGet, slot = handler, data = userData)

/**
 * Connects the *drag-data-delete* event to a [handler] on a widget. This event is fired on the drag source when a
 * drag with the action `GDK_ACTION_MOVE` is successfully completed. The event handler is responsible for deleting the
 * data that has been dropped. What **delete** means depends on the context of the drag operation.
 * @param handler The event handler for the event.
 * @param userData User data to pass through to the [handler].
 */
public fun WidgetBase.connectDragDataDeleteEvent(handler: CPointer<DragDataDeleteHandler>,
                                                 userData: gpointer = fetchEmptyDataPointer()): ULong =
    connectGSignal(obj = gtkWidgetPtr, signal = DragDropEvent.dragDataDelete, slot = handler, data = userData)

/**
 * Connects the *drag-end* event to a [handler] on a widget. This event is fired on the drag source when a drag is
 * finished. A typical reason to connect to this event is to undo things done in `GtkWidget::drag-begin`.
 * @param handler The event handler for the event.
 * @param userData User data to pass through to the [handler].
 */
public fun WidgetBase.connectDragEndEvent(handler: CPointer<DragEndHandler>,
                                          userData: gpointer = fetchEmptyDataPointer()): ULong =
    connectGSignal(obj = gtkWidgetPtr, signal = DragDropEvent.dragEnd, slot = handler, data = userData)

/**
 * The event handler for the *drag-begin* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. ctx: CPointer<GdkDragContext>
 * 3. userData: gpointer
 */
public typealias DragBeginHandler = CFunction<(
    widget: CPointer<GtkWidget>,
    ctx: CPointer<GdkDragContext>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *drag-end* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. ctx: CPointer<GdkDragContext>
 * 3. userData: gpointer
 */
public typealias DragEndHandler = CFunction<(
    widget: CPointer<GtkWidget>,
    ctx: CPointer<GdkDragContext>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *drag-data-delete* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. ctx: CPointer<GdkDragContext>
 * 3. userData: gpointer
 */
public typealias DragDataDeleteHandler = CFunction<(
    widget: CPointer<GtkWidget>,
    ctx: CPointer<GdkDragContext>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *drag-data-get* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. ctx: CPointer<GdkDragContext>
 * 3. data: CPointer<GtkSelectionData>
 * 4. info: UInt
 * 5. time: UInt
 * 6. userData: gpointer
 */
public typealias DragDataGetHandler = CFunction<(
    widget: CPointer<GtkWidget>,
    ctx: CPointer<GdkDragContext>,
    data: CPointer<GtkSelectionData>,
    info: UInt,
    time: UInt,
    userData: gpointer
) -> Unit>
