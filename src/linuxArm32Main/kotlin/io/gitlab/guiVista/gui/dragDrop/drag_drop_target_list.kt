package io.gitlab.guiVista.gui.dragDrop

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import io.gitlab.guiVista.core.Closable
import io.gitlab.guiVista.gui.Atom
import io.gitlab.guiVista.gui.text.TextBuffer
import kotlinx.cinterop.*

public actual class DragDropTargetList private constructor(ptr: CPointer<GtkTargetList>?) : Closable {
    public val gtkTargetListPtr: CPointer<GtkTargetList>? = ptr

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkTargetList>?): DragDropTargetList = DragDropTargetList(ptr)

        public actual fun create(vararg targets: DragDropTargetEntry): DragDropTargetList = memScoped {
            val totalTargets = targets.size
            val tmpArray = allocArray<GtkTargetEntry>(totalTargets)
            targets.forEachIndexed { pos, item ->
                tmpArray[pos].flags = item.flags
                tmpArray[pos].info = item.info
                tmpArray[pos].target = item.target.cstr.ptr
            }
            return DragDropTargetList(gtk_target_list_new(tmpArray, totalTargets.toUInt()))
        }
    }

    public actual fun add(target: Atom, flags: UInt, info: UInt) {
        gtk_target_list_add(list = gtkTargetListPtr, target = target.gdkAtomPtr, flags = flags, info = info)
    }

    public actual fun addImageTargets(info: UInt, writable: Boolean) {
        gtk_target_list_add_image_targets(list = gtkTargetListPtr, info = info,
            writable = if (writable) TRUE else FALSE)
    }

    public actual fun addRichTextTargets(info: UInt, deserializable: Boolean, buffer: TextBuffer) {
        gtk_target_list_add_rich_text_targets(
            list = gtkTargetListPtr,
            deserializable = if (deserializable) TRUE else FALSE,
            buffer = buffer.gtkTextBufferPtr,
            info = info
        )
    }

    public actual fun addTable(vararg targets: DragDropTargetEntry): Unit = memScoped {
        val totalTargets = targets.size
        val tmpArray = allocArray<GtkTargetEntry>(totalTargets)
        targets.forEachIndexed { pos, item ->
            tmpArray[pos].flags = item.flags
            tmpArray[pos].info = item.info
            tmpArray[pos].target = item.target.cstr.ptr
        }
        gtk_target_list_add_table(list = gtkTargetListPtr, targets = tmpArray, totalTargets.toUInt())
    }

    public actual fun addTextTargets(info: UInt) {
        gtk_target_list_add_text_targets(gtkTargetListPtr, info)
    }

    public actual fun addUriTargets(info: UInt) {
        gtk_target_list_add_uri_targets(gtkTargetListPtr, info)
    }

    public actual fun find(target: Atom): Pair<Boolean, Int> = memScoped {
        val tmpInfo = alloc<UIntVar>()
        val found = gtk_target_list_find(list = gtkTargetListPtr, target = target.gdkAtomPtr, info = tmpInfo.ptr) == TRUE
        found to tmpInfo.value.toInt()
    }

    public actual fun remove(target: Atom) {
        gtk_target_list_remove(gtkTargetListPtr, target.gdkAtomPtr)
    }

    override fun close() {
        if (gtkTargetListPtr != null) gtk_target_list_unref(gtkTargetListPtr)
    }
}

public fun CPointer<GtkTargetList>?.toDragDropTargetList(): DragDropTargetList =
    DragDropTargetList.fromPointer(this)
