package io.gitlab.guiVista.gui.tree

import gtk3.GtkTreeSortable
import kotlinx.cinterop.CPointer

public actual class TreeSortable private constructor(ptr: CPointer<GtkTreeSortable>?) : TreeSortableBase {
    override val gtkTreeSortablePtr: CPointer<GtkTreeSortable>? = ptr

    override fun close() {
        // Do nothing.
    }

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkTreeSortable>?): TreeSortable = TreeSortable(ptr)
    }
}

public fun CPointer<GtkTreeSortable>?.toTreeSortable(): TreeSortable = TreeSortable.fromPointer(this)
