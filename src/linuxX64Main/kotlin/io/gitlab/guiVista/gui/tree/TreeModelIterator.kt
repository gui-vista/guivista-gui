package io.gitlab.guiVista.gui.tree

import gtk3.GtkTreeIter
import gtk3.gtk_tree_iter_free
import io.gitlab.guiVista.core.Closable
import kotlinx.cinterop.Arena
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.alloc
import kotlinx.cinterop.ptr

public actual class TreeModelIterator private constructor(ptr: CPointer<GtkTreeIter>? = null) : Closable {
    private val arena = Arena()
    public val gtkTreeIterPtr: CPointer<GtkTreeIter> = ptr ?: arena.alloc<GtkTreeIter>().ptr

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkTreeIter>?): TreeModelIterator = TreeModelIterator(ptr)

        public actual fun create(): TreeModelIterator = TreeModelIterator()
    }

    override fun close() {
        gtk_tree_iter_free(gtkTreeIterPtr)
    }
}
