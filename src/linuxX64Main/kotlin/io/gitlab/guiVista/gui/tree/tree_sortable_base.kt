package io.gitlab.guiVista.gui.tree

import glib2.GDestroyNotify
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.*

public actual interface TreeSortableBase : ObjectBase {
    public val gtkTreeSortablePtr: CPointer<GtkTreeSortable>?
    public actual val hasDefaultSortFunc: Boolean
        get() = gtk_tree_sortable_has_default_sort_func(gtkTreeSortablePtr) == TRUE

    /**
     * Fills in the sort column ID, and the order with the current sort column, and the order. Note that
     * ordinarySortId will be *true* unless the sortColumnId is `GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID`, or
     * `GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID`.
     * @return A Triple containing the following:
     * 1. sortColumnId - The sort column id.
     * 2. order - The sorting type.
     * 3. ordinarySortId - Will be *true* if the sort column isn't one of the special sort column ID's.
     */
    public fun fetchSortColumnId(): Triple<Int, GtkSortType, Boolean> = memScoped {
        val sortColumnId = alloc<IntVar>().apply { value = 0 }
        val order = alloc<GtkSortType.Var>()
        val ordinarySortId = gtk_tree_sortable_get_sort_column_id(
            sortable = gtkTreeSortablePtr,
            sort_column_id = sortColumnId.ptr,
            order = order.ptr
        ) == TRUE
        Triple(sortColumnId.value, order.value, ordinarySortId)
    }

    /**
     * Sets the current sort column to be [sortColumnId]. The sortable will resort itself to reflect this change, after
     * firing a **sort-column-changed** event. Note that [sortColumnId] may either be a regular column id, or one of
     * the following special values:
     * - GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID: the default sort function will be used, if it is set
     * - GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID: no sorting will occur
     * @param sortColumnId The sort column ID to set.
     * @param order The sort order of the column.
     */
    public fun changeSortColumnId(sortColumnId: Int, order: GtkSortType) {
        gtk_tree_sortable_set_sort_column_id(sortable = gtkTreeSortablePtr, sort_column_id = sortColumnId,
            order = order)
    }

    /**
     * Sets the comparison function used when sorting to be [sortFunc]. If the current sort column id of sortable is
     * the same as [sortColumnId] then the model will sort using this function.
     * @param sortColumnId The sort column id to set the function for.
     * @param sortFunc The comparison function.
     * @param userData User data to pass to [sortFunc].
     * @param destroyFunc Destroy notifier of [userData] or *null*.
     */
    public fun changeSortFunc(
        sortColumnId: Int,
        sortFunc: GtkTreeIterCompareFunc,
        userData: gpointer = fetchEmptyDataPointer(),
        destroyFunc: GDestroyNotify? = null
    ) {
        gtk_tree_sortable_set_sort_func(
            sortable = gtkTreeSortablePtr,
            sort_column_id = sortColumnId,
            sort_func = sortFunc,
            user_data = userData,
            destroy = destroyFunc
        )
    }

    /**
     * Sets the default comparison function used when sorting to be [sortFunc]. If the current sort column id of
     * sortable is `GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID` then the model will sort using this function. If
     * [sortFunc] is *null* then there will be no default comparison function. This means that once the model has been
     * sorted it can’t go back to the default state. In this case when the current sort column id of sortable is
     * `GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID` the model will be unsorted.
     * @param sortFunc The comparison function.
     * @param userData User data to pass to [sortFunc].
     * @param destroyFunc Destroy notifier of user_data or *null*.
     */
    public fun changeDefaultSortFunc(
        sortFunc: GtkTreeIterCompareFunc? = null,
        userData: gpointer = fetchEmptyDataPointer(),
        destroyFunc: GDestroyNotify? = null
    ) {
        gtk_tree_sortable_set_default_sort_func(
            sortable = gtkTreeSortablePtr,
            sort_func = sortFunc,
            user_data = userData,
            destroy = destroyFunc
        )
    }

    /**
     * Connects the *sort-column-changed* event to a [handler] on a tree sortable. This event occurs when the sort
     * column, or sort order of this sortable is changed. The event is fired before the contents of sortable are
     * resorted.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSortColumnChangedEvent(handler: CPointer<SortColumnChangedHandler>,
                                             userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeSortablePtr, signal = TreeSortableBaseEvent.sortColumnChanged, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkTreeSortablePtr, handlerId)
    }
}

/**
 * The event handler for the *sort-column-changed* event. Arguments:
 * 1. sortable: CPointer<GtkTreeSortable>
 * 2. userData: gpointer
 */
public typealias SortColumnChangedHandler = CFunction<(sortable: CPointer<GtkTreeSortable>, userData: gpointer) -> Unit>
