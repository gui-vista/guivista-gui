package io.gitlab.guiVista.gui.tree

import glib2.TRUE
import gtk3.*
import io.gitlab.guiVista.core.Closable
import kotlinx.cinterop.CPointer

public actual class TreeRowReference private constructor(
    ptr: CPointer<GtkTreeRowReference>? = null,
    model: TreeModelBase? = null,
    path: TreePath? = null
) : Closable {
    public val gtkTreeRowReferencePtr: CPointer<GtkTreeRowReference>? = when {
        model != null && path != null -> gtk_tree_row_reference_new(model.gtkTreeModelPtr, path.gtkTreePathPtr)
        else -> ptr
    }
    public actual val model: TreeModelBase
        get() = TreeModel.fromPointer(gtk_tree_row_reference_get_model(gtkTreeRowReferencePtr))
    public actual val path: TreePath?
        get() {
            val ptr = gtk_tree_row_reference_get_path(gtkTreeRowReferencePtr)
            return if (ptr != null) TreePath.fromPointer(ptr) else null
        }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkTreeRowReference>?): TreeRowReference = TreeRowReference(ptr = ptr)

        public actual fun create(model: TreeModelBase, path: TreePath): TreeRowReference? {
            val ptr = gtk_tree_row_reference_new(model.gtkTreeModelPtr, path.gtkTreePathPtr)
            return if (ptr != null) TreeRowReference(ptr) else null
        }
    }

    public actual fun valid(): Boolean = gtk_tree_row_reference_valid(gtkTreeRowReferencePtr) == TRUE

    public actual fun copy(): TreeRowReference = TreeRowReference(gtk_tree_row_reference_copy(gtkTreeRowReferencePtr))

    override fun close() {
        gtk_tree_row_reference_free(gtkTreeRowReferencePtr)
    }
}

public fun CPointer<GtkTreeRowReference>?.toTreeRowReference(): TreeRowReference =
    TreeRowReference.fromPointer(this)
