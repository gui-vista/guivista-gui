package io.gitlab.guiVista.gui.tree

import gtk3.GtkTreeModel
import kotlinx.cinterop.CPointer

public actual class TreeModel private constructor(ptr: CPointer<GtkTreeModel>?) : TreeModelBase {
    override val gtkTreeModelPtr: CPointer<GtkTreeModel>? = ptr

    override fun close() {
        // Do nothing since the tree model doesn't need to be freed.
    }

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkTreeModel>?): TreeModel = TreeModel(ptr)
    }
}

public fun treeModel(treeModelPtr: CPointer<GtkTreeModel>? = null, init: TreeModel.() -> Unit = {}): TreeModel {
    val result = TreeModel.fromPointer(treeModelPtr)
    result.init()
    return result
}

public fun CPointer<GtkTreeModel>?.toTreeModel(): TreeModel = TreeModel.fromPointer(this)
