package io.gitlab.guiVista.gui.event

import gtk3.GdkEvent
import gtk3.GdkEventDND
import gtk3.gdk_event_free
import gtk3.gdk_event_new
import io.gitlab.guiVista.gui.dragDrop.DragDropContext
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.reinterpret

public actual class DragDropEvent private constructor(ptr: CPointer<GdkEventDND>?) : EventBase {
    public val gdkEventDNDPtr: CPointer<GdkEventDND>? = ptr

    override val gdkEventPtr: CPointer<GdkEvent>?
        get() = gdkEventDNDPtr?.reinterpret()

    public actual val sendEvent: Boolean
        get() = gdkEventDNDPtr?.pointed?.send_event == 1.toByte()

    public actual val time: UInt
        get() = gdkEventDNDPtr?.pointed?.time ?: 0u

    public actual val rootXPos: Short
        get() = gdkEventDNDPtr?.pointed?.x_root ?: 0.toShort()

    public actual val rootYPos: Short
        get() = gdkEventDNDPtr?.pointed?.y_root ?: 0.toShort()

    public actual val context: DragDropContext
        get() = DragDropContext.fromPointer(gdkEventDNDPtr?.pointed?.context)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GdkEventDND>?): DragDropEvent = DragDropEvent(ptr)

        public actual fun create(type: Int): DragDropEvent = DragDropEvent(gdk_event_new(type)?.reinterpret())
    }
}

public fun CPointer<GdkEventDND>?.toDragDropEvent(): DragDropEvent = DragDropEvent.fromPointer(this)
