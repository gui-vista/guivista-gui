package io.gitlab.guiVista.gui.event

import gtk3.GdkEvent
import gtk3.GdkEventMotion
import gtk3.gdk_event_free
import gtk3.gdk_event_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.value

public actual class MotionEvent private constructor(ptr: CPointer<GdkEventMotion>?) : EventBase {
    public val gdkEventMotionPtr: CPointer<GdkEventMotion>? = ptr

    override val gdkEventPtr: CPointer<GdkEvent>?
        get() = gdkEventMotionPtr?.reinterpret()

    public actual val sendEvent: Boolean
        get() = gdkEventMotionPtr?.pointed?.send_event == 1.toByte()

    public actual val time: UInt
        get() = gdkEventMotionPtr?.pointed?.time ?: 0u

    public actual val xPos: Double
        get() = gdkEventMotionPtr?.pointed?.x ?: 0.0

    public actual val yPos: Double
        get() = gdkEventMotionPtr?.pointed?.y ?: 0.0

    public actual val axes: Double?
        get() = gdkEventMotionPtr?.pointed?.axes?.pointed?.value

    public actual val state: UInt
        get() = gdkEventMotionPtr?.pointed?.state ?: 0u

    public actual val isHint: Boolean
        get() = gdkEventMotionPtr?.pointed?.is_hint == 1.toShort()

    public actual val xPosRoot: Double
        get() = gdkEventMotionPtr?.pointed?.x_root ?: 0.0

    public actual val yPosRoot: Double
        get() = gdkEventMotionPtr?.pointed?.y_root ?: 0.0

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GdkEventMotion>?): MotionEvent = MotionEvent(ptr)

        public actual fun create(type: Int): MotionEvent = MotionEvent(gdk_event_new(type)?.reinterpret())
    }
}

public fun CPointer<GdkEventMotion>?.toMotionEvent(): MotionEvent = MotionEvent.fromPointer(this)
