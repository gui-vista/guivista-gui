package io.gitlab.guiVista.gui.cell.renderer

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class ToggleCellRenderer private constructor(ptr: CPointer<GtkCellRendererToggle>? = null) :
    CellRendererBase {
    override val gtkCellRendererPtr: CPointer<GtkCellRenderer>? = ptr?.reinterpret() ?: gtk_cell_renderer_toggle_new()
    public val gtkCellRendererTogglePtr: CPointer<GtkCellRendererToggle>?
        get() = gtkCellRendererPtr?.reinterpret()
    public actual var radio: Boolean
        get() = gtk_cell_renderer_toggle_get_radio(gtkCellRendererTogglePtr) == TRUE
        set(value) = gtk_cell_renderer_toggle_set_radio(gtkCellRendererTogglePtr, if (value) TRUE else FALSE)
    public actual var active: Boolean
        get() = gtk_cell_renderer_toggle_get_active(gtkCellRendererTogglePtr) == TRUE
        set(value) = gtk_cell_renderer_toggle_set_active(gtkCellRendererTogglePtr, if (value) TRUE else FALSE)
    public actual var activatable: Boolean
        get() = gtk_cell_renderer_toggle_get_activatable(gtkCellRendererTogglePtr) == TRUE
        set(value) = gtk_cell_renderer_toggle_set_activatable(gtkCellRendererTogglePtr, if (value) TRUE else FALSE)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkCellRendererToggle>?): ToggleCellRenderer = ToggleCellRenderer(ptr)

        public actual fun create(): ToggleCellRenderer = ToggleCellRenderer()
    }

    /**
     * Connects the *toggled* event to a [handler] on a toggle cell renderer. This event occurs when the cell is
     * toggled. It is the responsibility of the application to update the model with the correct value to store at the
     * path. Often this is simply the opposite of the value currently stored at the path.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectToggledEvent(
        handler: CPointer<ToggledHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong = connectGSignal(obj = gtkCellRendererTogglePtr, signal = ToggleCellRendererEvent.toggled, slot = handler,
        data = userData)


    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkCellRendererTogglePtr, handlerId)
    }
}

public fun toggleCellRenderer(
    ptr: CPointer<GtkCellRendererToggle>? = null,
    init: ToggleCellRenderer.() -> Unit = {}
): ToggleCellRenderer {
    val result = if (ptr != null) ToggleCellRenderer.fromPointer(ptr) else ToggleCellRenderer.create()
    result.init()
    return result
}

/**
 * The event handler for the *toggled* event. Arguments:
 * 1. renderer: CPointer<GtkCellRendererToggle>
 * 2. path: CPointer<ByteVar> (represents a String)
 * 3. userData: gpointer
 */
public typealias ToggledHandler = CFunction<(
    renderer: CPointer<GtkCellRendererToggle>,
    path: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>

public fun CPointer<GtkCellRendererToggle>?.toToggleCellRenderer(): ToggleCellRenderer =
    ToggleCellRenderer.fromPointer(this)
