package io.gitlab.guiVista.gui.cell

import glib2.gpointer
import gtk3.GdkEvent
import gtk3.GtkCellEditable
import gtk3.gtk_cell_editable_start_editing
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer

public actual interface CellEditableBase : ObjectBase {
    public val gtkCellEditablePtr: CPointer<GtkCellEditable>?

    /**
     * Begins editing on a editable cell. The [event] is the `GdkEvent` that began the editing process. It may be
     * *null* in the instance that editing was initiated through pragmatic means.
     * @param event The `GdkEvent` or *null*.
     */
    public fun startEditing(event: CPointer<GdkEvent>? = null) {
        gtk_cell_editable_start_editing(gtkCellEditablePtr, event)
    }

    /**
     * Connects the *editing-done* event to a [handler] on a editable cell. This event is a sign for the cell renderer
     * to update its value from the editable cell. Implementations of [CellEditableBase] are responsible for firing
     * this event when they are done editing, e.g. [io.gitlab.guiVista.gui.widget.dataEntry.Entry] is emitting it when
     * the user presses **Enter**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectEditingDoneEvent(
        handler: CPointer<EditingDoneHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkCellEditablePtr, signal = CellEditableEvent.editingDone, slot = handler,
            data = userData)

    /**
     * Connects the *remove-widget* event to a [handler] on a editable cell. This event is meant to indicate that the
     * cell is finished editing, and the widget may now be destroyed. Implementations of [CellEditableBase] are
     * responsible for firing this signal when they are done editing. It must be fired after the **editing-done**
     * event, to give the cell renderer a chance to update the cell's value before the widget is removed.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRemoveWidgetEvent(
        handler: CPointer<RemoveWidgetHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkCellEditablePtr, signal = CellEditableEvent.removeWidget, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkCellEditablePtr, handlerId)
    }
}

/**
 * The event handler for the *editing-done* event. Arguments:
 * 1. cellEditable: CPointer<GtkCellEditable>
 * 2. userData: gpointer
 */
public typealias EditingDoneHandler = CFunction<(cellEditable: CPointer<GtkCellEditable>, userData: gpointer) -> Unit>

/**
 * The event handler for the *remove-widget* event. Arguments:
 * 1. cellEditable: CPointer<GtkCellEditable>
 * 2. userData: gpointer
 */
public typealias RemoveWidgetHandler = CFunction<(cellEditable: CPointer<GtkCellEditable>, userData: gpointer) -> Unit>
