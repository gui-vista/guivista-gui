package io.gitlab.guiVista.gui.cell

import gtk3.GtkCellArea
import kotlinx.cinterop.CPointer

public actual class CellArea private constructor(ptr: CPointer<GtkCellArea>?) : CellAreaBase {
    override val gtkCellAreaPtr: CPointer<GtkCellArea>? = ptr

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkCellArea>?): CellArea = CellArea(ptr)
    }
}
