package io.gitlab.guiVista.gui.cell.renderer

import gtk3.GtkCellRenderer
import gtk3.GtkCellRendererSpinner
import gtk3.gtk_cell_renderer_spinner_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class SpinnerCellRenderer private constructor(ptr: CPointer<GtkCellRendererSpinner>? = null) :
    CellRendererBase {
    override val gtkCellRendererPtr: CPointer<GtkCellRenderer>? = ptr?.reinterpret() ?: gtk_cell_renderer_spinner_new()
    public val gtkCellRendererSpinnerPtr: CPointer<GtkCellRendererSpinner>?
        get() = gtkCellRendererPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkCellRendererSpinner>?): SpinnerCellRenderer = SpinnerCellRenderer(ptr)

        public actual fun create(): SpinnerCellRenderer = SpinnerCellRenderer()
    }
}

public fun spinnerCellRenderer(
    ptr: CPointer<GtkCellRendererSpinner>? = null,
    init: SpinnerCellRenderer.() -> Unit = {}
): SpinnerCellRenderer {
    val result = if (ptr != null) SpinnerCellRenderer.fromPointer(ptr) else SpinnerCellRenderer.create()
    result.init()
    return result
}

public fun CPointer<GtkCellRendererSpinner>?.toSpinnerCellRenderer(): SpinnerCellRenderer =
    SpinnerCellRenderer.fromPointer(this)
