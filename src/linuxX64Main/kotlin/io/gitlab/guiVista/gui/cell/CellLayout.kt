package io.gitlab.guiVista.gui.cell

import glib2.FALSE
import glib2.GDestroyNotify
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.cell.renderer.CellRendererBase
import kotlinx.cinterop.CPointer

public actual interface CellLayout {
    public val gtkCellLayoutPtr: CPointer<GtkCellLayout>?
    public actual val cells: DoublyLinkedList
        get() = DoublyLinkedList.fromPointer(gtk_cell_layout_get_cells(gtkCellLayoutPtr))
    public actual val area: CellAreaBase?
        get() {
            val ptr = gtk_cell_layout_get_area(gtkCellLayoutPtr)
            return if (ptr != null) CellArea.fromPointer(ptr) else null
        }

    /**
     * Sets the `GtkCellLayoutDataFunc` to use for the cell layout. This function is used instead of the standard
     * attributes mapping for setting the column value, and should set the value of cell layout’s cell renderer(s) as
     * appropriate. Note that [cellDataFunc] may be *null* to remove a previously set function.
     * @param cell The cell to use.
     * @param cellDataFunc The `GtkCellLayoutDataFunc` to use or *null*.
     * @param userData User data for [cellDataFunc].
     * @param destroyFunc Destroy notify for [cellDataFunc].
     */
    public fun changeCellDataFunc(
        cell: CellRendererBase,
        cellDataFunc: GtkCellLayoutDataFunc?,
        userData: gpointer = fetchEmptyDataPointer(),
        destroyFunc: GDestroyNotify? = null
    ) {
        gtk_cell_layout_set_cell_data_func(
            cell_layout = gtkCellLayoutPtr,
            cell = cell.gtkCellRendererPtr,
            func = cellDataFunc,
            func_data = userData,
            destroy = destroyFunc
        )
    }

    public actual fun prependCell(cell: CellRendererBase, expand: Boolean) {
        gtk_cell_layout_pack_end(cell_layout = gtkCellLayoutPtr, cell = cell.gtkCellRendererPtr,
            expand = if (expand) TRUE else FALSE)
    }

    public actual fun appendCell(cell: CellRendererBase, expand: Boolean) {
        gtk_cell_layout_pack_start(cell_layout = gtkCellLayoutPtr, cell = cell.gtkCellRendererPtr,
            expand = if (expand) TRUE else FALSE)
    }

    public actual fun reorder(cell: CellRendererBase, pos: Int) {
        gtk_cell_layout_reorder(cell_layout = gtkCellLayoutPtr, cell = cell.gtkCellRendererPtr, position = pos)
    }

    public actual fun clear() {
        gtk_cell_layout_clear(gtkCellLayoutPtr)
    }

    public actual fun addAttribute(cell: CellRendererBase, attr: String, column: Int) {
        gtk_cell_layout_add_attribute(cell_layout = gtkCellLayoutPtr, cell = cell.gtkCellRendererPtr, attribute = attr,
            column = column)
    }

    public actual fun addMultipleAttributes(cell: CellRendererBase, vararg attributes: Pair<String, Int>) {
        attributes.forEach { (attrName, column) -> addAttribute(cell = cell, attr = attrName, column = column) }
    }

    public actual fun clearAttributes(cell: CellRendererBase) {
        gtk_cell_layout_clear_attributes(gtkCellLayoutPtr, cell.gtkCellRendererPtr)
    }
}
