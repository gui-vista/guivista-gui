package io.gitlab.guiVista.gui.cell

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.*
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.cell.renderer.CellRenderer
import io.gitlab.guiVista.gui.cell.renderer.CellRendererBase
import io.gitlab.guiVista.gui.widget.WidgetBase
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreeModelIterator
import kotlinx.cinterop.*

public actual interface CellAreaBase : CellLayout, InitiallyUnowned {
    public val gtkCellAreaPtr: CPointer<GtkCellArea>?
    override val gtkCellLayoutPtr: CPointer<GtkCellLayout>?
        get() = gtkCellAreaPtr?.reinterpret()
    public actual val currentPathString: String
        get() = gtk_cell_area_get_current_path_string(gtkCellAreaPtr)?.toKString() ?: ""
    public actual val isActivatable: Boolean
        get() = gtk_cell_area_is_activatable(gtkCellAreaPtr) == TRUE
    public actual var focusCell: CellRendererBase
        get() = CellRenderer.fromPointer(gtk_cell_area_get_focus_cell(gtkCellAreaPtr))
        set(value) = gtk_cell_area_set_focus_cell(gtkCellAreaPtr, value.gtkCellRendererPtr)
    public actual val editedCell: CellRendererBase
        get() = CellRenderer.fromPointer(gtk_cell_area_get_edited_cell(gtkCellAreaPtr))
    public actual val editWidget: CellEditableBase?
        get() {
            val ptr = gtk_cell_area_get_edit_widget(gtkCellAreaPtr)
            return if (ptr != null) CellEditable.fromPointer(ptr) else null
        }

    public actual val requestMode: UInt
        get() = gtk_cell_area_get_request_mode(gtkCellAreaPtr)

    public actual infix fun addCellRenderer(renderer: CellRendererBase) {
        gtk_cell_area_add(gtkCellAreaPtr, renderer.gtkCellRendererPtr)
    }

    public actual infix fun removeCellRenderer(renderer: CellRendererBase) {
        gtk_cell_area_remove(gtkCellAreaPtr, renderer.gtkCellRendererPtr)
    }

    public actual infix fun hasRenderer(renderer: CellRendererBase): Boolean =
        gtk_cell_area_has_renderer(gtkCellAreaPtr, renderer.gtkCellRendererPtr) == TRUE

    public actual fun createContext(): CellAreaContext =
        CellAreaContext.fromPointer(gtk_cell_area_create_context(gtkCellAreaPtr))

    public actual fun copyContext(context: CellAreaContext): CellAreaContext =
        CellAreaContext.fromPointer(gtk_cell_area_copy_context(gtkCellAreaPtr, context.gtkCellAreaContextPtr))

    public actual fun fetchPreferredWidth(context: CellAreaContext, widget: WidgetBase): Pair<Int, Int> = memScoped {
        val minWidth = alloc<IntVar>().apply { value = 0 }
        val naturalWidth = alloc<IntVar>().apply { value = 0 }
        gtk_cell_area_get_preferred_width(
            area = gtkCellAreaPtr,
            context = context.gtkCellAreaContextPtr,
            widget = widget.gtkWidgetPtr,
            minimum_width = minWidth.ptr,
            natural_width = naturalWidth.ptr
        )
        minWidth.value to naturalWidth.value
    }

    public actual fun fetchPreferredHeightForWidth(
        context: CellAreaContext,
        widget: WidgetBase,
        width: Int
    ): Pair<Int, Int> = memScoped {
        val minHeight = alloc<IntVar>().apply { value = 0 }
        val naturalHeight = alloc<IntVar>().apply { value = 0 }
        gtk_cell_area_get_preferred_height_for_width(
            area = gtkCellAreaPtr,
            context = context.gtkCellAreaContextPtr,
            widget = widget.gtkWidgetPtr,
            minimum_height = minHeight.ptr,
            natural_height = naturalHeight.ptr,
            width = width
        )
        minHeight.value to naturalHeight.value
    }

    public actual fun fetchPreferredHeight(context: CellAreaContext, widget: WidgetBase): Pair<Int, Int> = memScoped {
        val minHeight = alloc<IntVar>().apply { value = 0 }
        val naturalHeight = alloc<IntVar>().apply { value = 0 }
        gtk_cell_area_get_preferred_height(
            area = gtkCellAreaPtr,
            context = context.gtkCellAreaContextPtr,
            widget = widget.gtkWidgetPtr,
            minimum_height = minHeight.ptr,
            natural_height = naturalHeight.ptr
        )
        minHeight.value to naturalHeight.value
    }

    public actual fun fetchPreferredWidthForHeight(
        context: CellAreaContext,
        widget: WidgetBase,
        height: Int
    ): Pair<Int, Int> = memScoped {
        val minWidth = alloc<IntVar>().apply { value = 0 }
        val naturalWidth = alloc<IntVar>().apply { value = 0 }
        gtk_cell_area_get_preferred_width_for_height(
            area = gtkCellAreaPtr,
            context = context.gtkCellAreaContextPtr,
            widget = widget.gtkWidgetPtr,
            minimum_width = minWidth.ptr,
            natural_width = naturalWidth.ptr,
            height = height
        )
        minWidth.value to naturalWidth.value
    }

    public actual fun applyAttributes(
        treeModel: TreeModelBase,
        iter: TreeModelIterator,
        isExpander: Boolean,
        isExpanded: Boolean
    ) {
        gtk_cell_area_apply_attributes(
            area = gtkCellAreaPtr,
            tree_model = treeModel.gtkTreeModelPtr,
            iter = iter.gtkTreeIterPtr,
            is_expander = if (isExpander) TRUE else FALSE,
            is_expanded = if (isExpanded) TRUE else FALSE
        )
    }

    public actual fun connectAttribute(renderer: CellRendererBase, attr: String, column: Int) {
        gtk_cell_area_attribute_connect(
            area = gtkCellAreaPtr,
            renderer = renderer.gtkCellRendererPtr,
            attribute = attr,
            column = column
        )
    }

    public actual fun disconnectAttribute(renderer: CellRendererBase, attr: String) {
        gtk_cell_area_attribute_disconnect(
            area = gtkCellAreaPtr,
            renderer = renderer.gtkCellRendererPtr,
            attribute = attr
        )
    }

    public actual fun fetchColumnForAttribute(renderer: CellRendererBase, attr: String) {
        gtk_cell_area_attribute_get_column(area = gtkCellAreaPtr, renderer = renderer.gtkCellRendererPtr,
            attribute = attr)
    }

    public actual fun changeProperty(renderer: CellRendererBase, propName: String, value: ValueBase) {
        gtk_cell_area_cell_set_property(
            area = gtkCellAreaPtr,
            renderer = renderer.gtkCellRendererPtr,
            property_name = propName,
            value = value.gValuePtr
        )
    }

    public actual fun fetchProperty(renderer: CellRendererBase, propName: String, propType: ULong): ValueBase {
        val result = Value.create(propType)
        gtk_cell_area_cell_get_property(
            area = gtkCellAreaPtr,
            renderer = renderer.gtkCellRendererPtr,
            property_name = propName,
            value = result.gValuePtr
        )
        return result
    }

    public actual fun addFocusSibling(renderer: CellRendererBase, sibling: CellRendererBase) {
        gtk_cell_area_add_focus_sibling(area = gtkCellAreaPtr, renderer = renderer.gtkCellRendererPtr,
            sibling = sibling.gtkCellRendererPtr)
    }

    public actual fun removeFocusSibling(renderer: CellRendererBase, sibling: CellRendererBase) {
        gtk_cell_area_remove_focus_sibling(area = gtkCellAreaPtr, renderer = renderer.gtkCellRendererPtr,
            sibling = sibling.gtkCellRendererPtr)
    }

    public actual fun isFocusSibling(renderer: CellRendererBase, sibling: CellRendererBase): Boolean =
        gtk_cell_area_is_focus_sibling(
            area = gtkCellAreaPtr,
            renderer = renderer.gtkCellRendererPtr,
            sibling = sibling.gtkCellRendererPtr
        ) == TRUE

    public actual fun fetchFocusSiblings(renderer: CellRendererBase): DoublyLinkedList =
        DoublyLinkedList.fromPointer(gtk_cell_area_get_focus_siblings(gtkCellAreaPtr, renderer.gtkCellRendererPtr))

    public actual fun fetchFocusFromSibling(renderer: CellRendererBase): CellRendererBase? {
        val ptr = gtk_cell_area_get_focus_from_sibling(gtkCellAreaPtr, renderer.gtkCellRendererPtr)
        return if (ptr != null) CellRenderer.fromPointer(ptr) else null
    }

    public actual fun stopEditing(cancelled: Boolean) {
        gtk_cell_area_stop_editing(gtkCellAreaPtr, if (cancelled) TRUE else FALSE)
    }

    /**
     * Connects the *add-editable* event to a [handler] on a cell area. This event occurs when editing has started
     * on the cell renderer, and that the editable should be added to the owning cell layout widget at the cell area.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectAddEditableEvent(handler: CPointer<AddEditableHandler>,
                                       userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkCellAreaPtr, signal = CellAreaBaseEvent.addEditable, slot = handler, data = userData)

    /**
     * Connects the *apply-attributes* event to a [handler] on a cell area. This event occurs whenever applying
     * attributes to the cell area from the model.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectApplyAttributesEvent(
        handler: CPointer<ApplyAttributesHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkCellAreaPtr, signal = CellAreaBaseEvent.applyAttributes, slot = handler,
            data = userData)

    /**
     * Connects the *focus-changed* event to a [handler] on a cell area. This event occurs either as a result of focus
     * handling, or event handling. It's possible that the event is fired even if the currently focused cell renderer
     * didn't change. This is because the focus may change to the same cell renderer in the same cell area for a
     * different row of data.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectFocusChangedEvent(
        handler: CPointer<FocusChangedHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkCellAreaPtr, signal = CellAreaBaseEvent.focusChanged, slot = handler, data = userData)

    /**
     * Connects the *remove-editable* event to a [handler] on a cell area. This event occurs when editing finished on
     * the cell renderer, and that the editable should be removed from the owning cell layout widget.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRemoveEditableEvent(
        handler: CPointer<RemoveEditableHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkCellAreaPtr, signal = CellAreaBaseEvent.removeEditable, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkCellAreaPtr, handlerId)
    }
}

/**
 * The event handler for the *add-editable* event. Arguments:
 * 1. area: CPointer<GtkCellArea>
 * 2. renderer: CPointer<GtkCellRenderer>
 * 3. editable: CPointer<GtkEditable>
 * 4. cellArea: CPointer<GdkRectangle>
 * 5. path: CPointer<ByteVar> (represents a String)
 * 6. userData: gpointer
 */
public typealias AddEditableHandler = CFunction<(
    area: CPointer<GtkCellArea>,
    renderer: CPointer<GtkCellRenderer>,
    editable: CPointer<GtkEditable>,
    cellArea: CPointer<GdkRectangle>,
    path: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *apply-attributes* event. Arguments:
 * 1. area: CPointer<GtkCellArea>
 * 2. model: CPointer<GtkTreeModel>
 * 3. iter: CPointer<GtkTreeIter>
 * 4. isExpander: Int (represents a Boolean)
 * 5. isExpanded: Int (represents a Boolean)
 * 6. userData: gpointer
 */
public typealias ApplyAttributesHandler = CFunction<(
    area: CPointer<GtkCellArea>,
    model: CPointer<GtkTreeModel>,
    iter: CPointer<GtkTreeIter>,
    isExpander: Int,
    isExpanded: Int,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *focus-changed* event. Arguments:
 * 1. area: CPointer<GtkCellArea>
 * 2. renderer: CPointer<GtkCellRenderer>
 * 3. path: CPointer<ByteVar> (represents a String)
 * 4. userData: gpointer
 */
public typealias FocusChangedHandler = CFunction<(
    area: CPointer<GtkCellArea>,
    renderer: CPointer<GtkCellRenderer>,
    path: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *remove-editable* event. Arguments:
 * 1. area: CPointer<GtkCellArea>
 * 2. renderer: CPointer<GtkCellRenderer>
 * 3. editable: CPointer<GtkCellEditable>
 * 4. userData: gpointer
 */
public typealias RemoveEditableHandler = CFunction<(
    area: CPointer<GtkCellArea>,
    renderer: CPointer<GtkCellRenderer>,
    editable: CPointer<GtkCellEditable>,
    userData: gpointer
) -> Unit>
