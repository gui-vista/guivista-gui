package io.gitlab.guiVista.gui

import gtk3.gdk_threads_add_idle
import kotlinx.cinterop.*
import platform.posix.pthread_create
import platform.posix.pthread_tVar

/**
 * Runs a [function][func] on a new background thread.
 * @param func The function to run.
 * @param userData User data to pass through to [func].
 * @return A Triple containing the following:
 * 1. success - Will be *true* if no error occurred.
 * 2. threadId - The ID of the background thread.
 * 3. errorCode - The error code (if an error occurred).
 */
public fun runOnBackgroundThread(
    func: CPointer<CFunction<(COpaquePointer?) -> COpaquePointer?>>,
    userData: COpaquePointer? = null
): Triple<Boolean, ULong, Int> {
    val thread = memScoped { alloc<pthread_tVar>() }
    val rc = pthread_create(thread.ptr, null, func, userData)
    return Triple(rc == 0, thread.value, rc)
}

/**
 * Runs a [function][func] on the UI thread (this is the main thread).
 * @param func The function to run.
 * @param userData User data to pass through to [func].
 * @return The ID (greater than *0*) of the event source.
 */
public fun runOnUiThread(func: CPointer<CFunction<(COpaquePointer?) -> Int>>, userData: COpaquePointer? = null): UInt =
    gdk_threads_add_idle(func, userData)
