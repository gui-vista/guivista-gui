package io.gitlab.guiVista.gui

import glib2.TRUE
import glib2.g_object_unref
import gtk3.*
import io.gitlab.guiVista.core.ValueBase
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreeModelIterator
import kotlinx.cinterop.*

public actual class ListStore private constructor(
    ptr: CPointer<GtkListStore>? = null,
    types: ULongArray = ULongArray(0)
) : TreeModelBase {
    public val gtkListStorePtr: CPointer<GtkListStore>? = ptr ?: gtk_list_store_newv(types.size, types.toCValues())
    override val gtkTreeModelPtr: CPointer<GtkTreeModel>?
        get() = gtkListStorePtr?.reinterpret()

    override fun close() {
        g_object_unref(gtkListStorePtr)
    }

    public actual companion object {
        public actual fun create(vararg types: ULong): ListStore = ListStore(types = types)

        public fun fromPointer(ptr: CPointer<GtkListStore>?): ListStore = ListStore(ptr = ptr)
    }

    public actual fun changeStringValue(iter: TreeModelIterator, column: Int, value: String): Unit = memScoped {
        gtk_list_store_set(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, column, value.cstr.ptr, -1)
    }

    public actual fun changeUIntValue(iter: TreeModelIterator, column: Int, value: UInt) {
        gtk_list_store_set(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeIntValue(iter: TreeModelIterator, column: Int, value: Int) {
        gtk_list_store_set(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeULongValue(iter: TreeModelIterator, column: Int, value: ULong) {
        gtk_list_store_set(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeLongValue(iter: TreeModelIterator, column: Int, value: Long) {
        gtk_list_store_set(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeFloatValue(iter: TreeModelIterator, column: Int, value: Float) {
        gtk_list_store_set(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeDoubleValue(iter: TreeModelIterator, column: Int, value: Double) {
        gtk_list_store_set(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeUShortValue(iter: TreeModelIterator, column: Int, value: UShort) {
        gtk_list_store_set(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeShortValue(iter: TreeModelIterator, column: Int, value: Short) {
        gtk_list_store_set(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, column, value, -1)
    }

    public actual fun changeValue(iter: TreeModelIterator, column: Int, value: ValueBase) {
        gtk_list_store_set_value(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, column = column,
            value = value.gValuePtr)
    }

    public actual fun changeMultipleValues(
        iter: TreeModelIterator,
        columns: IntArray,
        values: Array<ValueBase>
    ) {
        @Suppress("ReplaceRangeToWithUntil")
        (0..(columns.size - 1)).forEach { changeValue(iter = iter, column = columns[it], value = values[it]) }
    }

    public actual fun remove(iter: TreeModelIterator): Boolean =
        gtk_list_store_remove(gtkListStorePtr, iter.gtkTreeIterPtr) == TRUE

    public actual fun insert(iter: TreeModelIterator, pos: Int) {
        gtk_list_store_insert(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr, position = pos)
    }

    public actual fun insertBefore(iter: TreeModelIterator, sibling: TreeModelIterator?) {
        gtk_list_store_insert_before(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr,
            sibling = sibling?.gtkTreeIterPtr)
    }

    public actual fun insertAfter(iter: TreeModelIterator, sibling: TreeModelIterator?) {
        gtk_list_store_insert_after(list_store = gtkListStorePtr, iter = iter.gtkTreeIterPtr,
            sibling = sibling?.gtkTreeIterPtr)
    }

    public actual fun insertWithValues(iter: TreeModelIterator, pos: Int, columns: IntArray, values: Array<ValueBase>) {
        insert(iter, pos)
        changeMultipleValues(iter = iter, columns = columns, values = values)
    }

    public actual fun prepend(iter: TreeModelIterator) {
        gtk_list_store_prepend(gtkListStorePtr, iter.gtkTreeIterPtr)
    }

    public actual fun append(iter: TreeModelIterator) {
        gtk_list_store_append(gtkListStorePtr, iter.gtkTreeIterPtr)
    }

    public actual fun clear() {
        gtk_list_store_clear(gtkListStorePtr)
    }

    public actual fun reorder(newOrder: IntArray) {
        newOrder.usePinned { pin ->
            gtk_list_store_reorder(gtkListStorePtr, pin.addressOf(0))
        }
    }

    public actual fun swap(iterA: TreeModelIterator, iterB: TreeModelIterator) {
        gtk_list_store_swap(store = gtkListStorePtr, a = iterA.gtkTreeIterPtr, b = iterB.gtkTreeIterPtr)
    }

    public actual fun moveBefore(iter: TreeModelIterator, pos: TreeModelIterator?) {
        gtk_list_store_move_before(store = gtkListStorePtr, iter = iter.gtkTreeIterPtr,
            position = pos?.gtkTreeIterPtr)
    }

    public actual fun moveAfter(iter: TreeModelIterator, pos: TreeModelIterator?) {
        gtk_list_store_move_after(store = gtkListStorePtr, iter = iter.gtkTreeIterPtr,
            position = pos?.gtkTreeIterPtr)
    }
}

public fun CPointer<GtkListStore>?.toListStore(): ListStore = ListStore.fromPointer(this)
