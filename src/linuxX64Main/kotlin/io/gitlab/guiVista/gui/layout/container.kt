package io.gitlab.guiVista.gui.layout

import gtk3.GtkContainer
import gtk3.GtkWidget
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Container private constructor(
    widgetPtr: CPointer<GtkWidget>? = null,
    containerPtr: CPointer<GtkContainer>? = null
) : ContainerBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = widgetPtr ?: containerPtr?.reinterpret()

    public companion object {
        public fun fromContainerPointer(ptr: CPointer<GtkContainer>?): Container = Container(containerPtr = ptr)

        public fun fromWidgetPointer(ptr: CPointer<GtkWidget>?): Container = Container(widgetPtr = ptr)
    }
}

public fun CPointer<GtkContainer>?.toContainer(): Container = Container.fromContainerPointer(this)
