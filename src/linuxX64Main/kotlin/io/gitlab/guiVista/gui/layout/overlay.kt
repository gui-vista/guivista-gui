package io.gitlab.guiVista.gui.layout

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Overlay private constructor(ptr: CPointer<GtkOverlay>? = null) : BinBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_overlay_new()
    public val gtkOverlayPtr: CPointer<GtkOverlay>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public actual fun create(): Overlay = Overlay()

        public fun fromPointer(ptr: CPointer<GtkOverlay>?): Overlay = Overlay(ptr)
    }

    public actual fun addOverlay(widget: WidgetBase) {
        gtk_overlay_add_overlay(gtkOverlayPtr, widget.gtkWidgetPtr)
    }

    public actual fun reorderOverlay(child: WidgetBase, pos: Int) {
        gtk_overlay_reorder_overlay(overlay = gtkOverlayPtr, child = child.gtkWidgetPtr, index_ = pos)
    }

    public actual fun fetchOverlayPassThrough(widget: WidgetBase): Boolean =
        gtk_overlay_get_overlay_pass_through(gtkOverlayPtr, widget.gtkWidgetPtr) == TRUE

    public actual fun changeOverlayPassThrough(widget: WidgetBase, passThrough: Boolean) {
        gtk_overlay_set_overlay_pass_through(overlay = gtkOverlayPtr, widget = widget.gtkWidgetPtr,
            pass_through = if (passThrough) TRUE else FALSE)
    }

    /**
     * Connects the *get-child-position* event to a [handler] on an [Overlay]. This event is used to determine the
     * position and size of any overlay child widgets. A handler for this event should fill allocation with the desired
     * position, and size for widget, relative to the 'main' child of the overlay.
     *
     * The default handler for this event uses the widget's **halign**, and **valign** properties to determine the
     * position, and gives the widget its natural size (except that an alignment of `GTK_ALIGN_FILL` will cause the
     * overlay to be full-width/height). If the main child is a [io.gitlab.guiVista.gui.window.ScrolledWindow] then
     * the overlays are placed relative to its contents.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectGetChildPositionEvent(handler: CPointer<GetChildPositionHandler>,
                                            userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkOverlayPtr, signal = OverlayEvent.getChildPosition, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkOverlayPtr, handlerId)
    }
}

public fun overlayLayout(overlayPtr: CPointer<GtkOverlay>? = null, init: Overlay.() -> Unit): Overlay {
    val overlay = if (overlayPtr != null) Overlay.fromPointer(overlayPtr) else Overlay.create()
    overlay.init()
    return overlay
}

/**
 * The event handler for the *get-child-position* event. Arguments:
 * 1. overlay: CPointer<GtkOverlay>
 * 2. widget: CPointer<GtkWidget>
 * 3. allocation: CPointer<GdkRectangle>
 * 4. userData: gpointer
 * Returns a value of *true* (an Int that represents a Boolean) if the allocation has been filled.
 */
public typealias GetChildPositionHandler = CFunction<(
    overlay: CPointer<GtkOverlay>,
    widget: CPointer<GtkWidget>,
    allocation: CPointer<GdkRectangle>,
    userData: gpointer
) -> Int>

public fun CPointer<GtkOverlay>?.toOverlay(): Overlay = Overlay.fromPointer(this)
