package io.gitlab.guiVista.gui.layout.stack

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

public actual class Stack private constructor(ptr: CPointer<GtkStack>? = null) : ContainerBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_stack_new()
    public val gtkStackPtr: CPointer<GtkStack>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual var visibleChild: WidgetBase?
        get() {
            val ptr = gtk_stack_get_visible_child(gtkStackPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) = gtk_stack_set_visible_child(gtkStackPtr, value?.gtkWidgetPtr)
    public actual var visibleChildName: String
        get() = gtk_stack_get_visible_child_name(gtkStackPtr)?.toKString() ?: ""
        set(value) = gtk_stack_set_visible_child_name(gtkStackPtr, value)
    public actual var homogeneous: Boolean
        get() = gtk_stack_get_homogeneous(gtkStackPtr) == TRUE
        set(value) = gtk_stack_set_homogeneous(gtkStackPtr, if (value) TRUE else FALSE)
    public actual var hHomogeneous: Boolean
        get() = gtk_stack_get_hhomogeneous(gtkStackPtr) == TRUE
        set(value) = gtk_stack_set_homogeneous(gtkStackPtr, if (value) TRUE else FALSE)
    public actual var vHomogeneous: Boolean
        get() = gtk_stack_get_vhomogeneous(gtkStackPtr) == TRUE
        set(value) = gtk_stack_set_vhomogeneous(gtkStackPtr, if (value) TRUE else FALSE)
    public actual var transitionDuration: UInt
        get() = gtk_stack_get_transition_duration(gtkStackPtr)
        set(value) = gtk_stack_set_transition_duration(gtkStackPtr, value)
    public actual var interpolateSize: Boolean
        get() = gtk_stack_get_interpolate_size(gtkStackPtr) == TRUE
        set(value) = gtk_stack_set_interpolate_size(gtkStackPtr, if (value) TRUE else FALSE)

    /** The type of animation used to transition. Default value is `GTK_STACK_TRANSITION_TYPE_NONE`. */
    public var transitionType: GtkStackTransitionType
        get() = gtk_stack_get_transition_type(gtkStackPtr)
        set(value) = gtk_stack_set_transition_type(gtkStackPtr, value)

    public actual companion object {
        public actual fun create(): Stack = Stack()

        public fun fromPointer(ptr: CPointer<GtkStack>?): Stack = Stack(ptr)
    }

    public actual fun addNamed(child: WidgetBase, name: String) {
        gtk_stack_add_named(stack = gtkStackPtr, child = child.gtkWidgetPtr, name = name)
    }

    public actual fun addTitled(child: WidgetBase, name: String, title: String) {
        gtk_stack_add_titled(stack = gtkStackPtr, child = child.gtkWidgetPtr, name = name, title = title)
    }

    /**
     * Makes the child with the given [name] visible. Note that the child widget has to be visible itself in order to
     * become the visible child of this [Stack].
     * @see WidgetBase.showAll
     * @param name The name of the child to make visible.
     * @param transition The transition type to use.
     */
    public fun changeVisibleChildFull(name: String, transition: GtkStackTransitionType) {
        gtk_stack_set_visible_child_full(stack = gtkStackPtr, name = name, transition = transition)
    }
}

public fun stackLayout(stackPtr: CPointer<GtkStack>? = null, init: Stack.() -> Unit): Stack {
    val stack = if (stackPtr != null) Stack.fromPointer(stackPtr) else Stack.create()
    stack.init()
    return stack
}

public fun CPointer<GtkStack>?.toStack(): Stack = Stack.fromPointer(this)
