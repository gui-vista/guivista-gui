package io.gitlab.guiVista.gui.layout

import gtk3.GtkButtonBox
import gtk3.GtkOrientation
import gtk3.GtkWidget
import gtk3.gtk_button_box_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class ButtonBox private constructor(
    ptr: CPointer<GtkButtonBox>? = null,
    orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL
) : ButtonBoxBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_button_box_new(orientation)

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkButtonBox>?): ButtonBox = ButtonBox(ptr = ptr)

        public fun create(orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL): ButtonBox =
            ButtonBox(orientation = orientation)
    }
}

public fun buttonBoxLayout(
    buttonBoxPtr: CPointer<GtkButtonBox>? = null,
    orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL,
    init: ButtonBox.() -> Unit
): ButtonBox {
    val result = if (buttonBoxPtr != null) ButtonBox.fromPointer(buttonBoxPtr) else ButtonBox.create(orientation)
    result.init()
    return result
}

public fun CPointer<GtkButtonBox>?.toButtonBox(): ButtonBox = ButtonBox.fromPointer(this)
