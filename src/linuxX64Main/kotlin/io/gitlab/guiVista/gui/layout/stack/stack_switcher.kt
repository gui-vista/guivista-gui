package io.gitlab.guiVista.gui.layout.stack

import gtk3.*
import io.gitlab.guiVista.gui.layout.BoxBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class StackSwitcher private constructor(ptr: CPointer<GtkStackSwitcher>? = null) : BoxBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_stack_switcher_new()
    public val gtkStackSwitcherPtr: CPointer<GtkStackSwitcher>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var stack: Stack?
        get() {
            val ptr = gtk_stack_switcher_get_stack(gtkStackSwitcherPtr)
            return if (ptr != null) Stack.fromPointer(ptr) else null
        }
        set(value) = gtk_stack_switcher_set_stack(gtkStackSwitcherPtr, value?.gtkStackPtr)

    public actual companion object {
        public fun fromCPointer(ptr: CPointer<GtkStackSwitcher>?): StackSwitcher = StackSwitcher(ptr)

        public actual fun create(): StackSwitcher = StackSwitcher()
    }
}

public fun CPointer<GtkStackSwitcher>?.toStackSwitcher(): StackSwitcher = StackSwitcher.fromCPointer(this)
