package io.gitlab.guiVista.gui.layout

import gtk3.GtkBin
import gtk3.gtk_bin_get_child
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual interface BinBase : ContainerBase {
    public val gtkBinPtr: CPointer<GtkBin>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual val child: WidgetBase?
        get() {
            val ptr = gtk_bin_get_child(gtkBinPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
}
