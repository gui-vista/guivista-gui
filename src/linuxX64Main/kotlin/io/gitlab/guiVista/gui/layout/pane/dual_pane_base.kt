package io.gitlab.guiVista.gui.layout.pane

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.layout.Orientable
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual interface DualPaneBase : ContainerBase, Orientable {
    override val gtkOrientable: CPointer<GtkOrientable>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkPanedPtr: CPointer<GtkPaned>?
    public actual var position: Int
        get() = gtk_paned_get_position(gtkPanedPtr)
        set(value) = gtk_paned_set_position(gtkPanedPtr, value)
    public actual var wideHandle: Boolean
        get() = gtk_paned_get_wide_handle(gtkPanedPtr) == TRUE
        set(value) = gtk_paned_set_wide_handle(gtkPanedPtr, if (value) TRUE else FALSE)
    public actual val firstChild: WidgetBase?
        get() {
            val ptr = gtk_paned_get_child1(gtkPanedPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
    public actual val secondChild: WidgetBase?
        get() {
            val ptr = gtk_paned_get_child2(gtkPanedPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }

    public actual fun add1(child: WidgetBase) {
        gtk_paned_add1(gtkPanedPtr, child.gtkWidgetPtr)
    }

    public actual fun add2(child: WidgetBase) {
        gtk_paned_add2(gtkPanedPtr, child.gtkWidgetPtr)
    }

    public actual fun pack1(child: WidgetBase, resize: Boolean, shrink: Boolean) {
        gtk_paned_pack1(paned = gtkPanedPtr, child = child.gtkWidgetPtr, resize = if (resize) TRUE else FALSE,
            shrink = if (shrink) TRUE else FALSE)
    }

    public actual fun pack2(child: WidgetBase, resize: Boolean, shrink: Boolean) {
        gtk_paned_pack2(paned = gtkPanedPtr, child = child.gtkWidgetPtr, resize = if (resize) TRUE else FALSE,
            shrink = if (shrink) TRUE else FALSE)
    }

    /**
     * Connects the *accept-position* event to a [handler] on a [DualPane]. This event is used to accept the current
     * position of the handle when moving it using key bindings. The default binding for this signal is *Return*
     * (*Enter*) or *Space*.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectAcceptPositionEvent(handler: CPointer<AcceptPositionHandler>,
                                          userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkPanedPtr, signal = DualPaneBaseEvent.acceptPosition, slot = handler, data = userData)

    /**
     * Connects the *cancel-position* event to a [handler] on a [DualPane]. This event is used to cancel moving the
     * position of the handle using key bindings. The position of the handle will be reset to the value prior to
     * moving it. The default binding for this signal is *Escape*.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCancelPositionEvent(handler: CPointer<CancelPositionHandler>,
                                          userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkPanedPtr, signal = DualPaneBaseEvent.cancelPosition, slot = handler, data = userData)

    /**
     * Connects the *cycle-child-focus* event to a [handler] on a [DualPane]. This event is a keybinding event which
     * gets emitted to cycle the focus between the children of the [DualPane]. The default binding is *F6*.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCycleChildFocusEvent(handler: CPointer<CycleChildFocusHandler>,
                                           userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkPanedPtr, signal = DualPaneBaseEvent.cycleChildFocus, slot = handler, data = userData)

    /**
     * Connects the *cycle-handle-focus* event to a [handler] on a [DualPane]. This event is used to cycle whether the
     * [DualPane] should grab focus to allow the user to change position of the handle by using key bindings. The
     * default binding for this signal is *F8*.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCycleHandleFocusEvent(handler: CPointer<CycleHandleFocusHandler>,
                                            userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkPanedPtr, signal = DualPaneBaseEvent.cycleHandleFocus, slot = handler, data = userData)

    /**
     * Connects the *move-handle* event to a [handler] on a [DualPane]. This event is used to move the handle when the
     * user is using key bindings to move it.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectMoveHandleEvent(handler: CPointer<MoveHandleHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkPanedPtr, signal = DualPaneBaseEvent.moveHandle, slot = handler, data = userData)

    /**
     * Connects the *toggle-handle-focus* event to a [handler] on a [DualPane]. This event is used to accept the
     * current position of the handle, and then move focus to the next widget in the focus chain. The default binding
     * is *Tab*.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectToggleHandleFocusEvent(handler: CPointer<ToggleHandleFocusHandler>,
                                             userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkPanedPtr, signal = DualPaneBaseEvent.toggleHandleFocus, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkPanedPtr, handlerId)
    }
}

/**
 * The event handler for the *accept-position* event. Arguments:
 * 1. widget: CPointer<GtkPaned>
 * 2. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias AcceptPositionHandler = CFunction<(widget: CPointer<GtkPaned>, userData: gpointer) -> Int>

/**
 * The event handler for the *cancel-position* event. Arguments:
 * 1. widget: CPointer<GtkPaned>
 * 2. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias CancelPositionHandler = CFunction<(widget: CPointer<GtkPaned>, userData: gpointer) -> Int>

/**
 * The event handler for the *cycle-child-focus* event. Arguments:
 * 1. widget: CPointer<GtkPaned>
 * 2. reversed: Int (represents a Boolean)
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias CycleChildFocusHandler = CFunction<(
    widget: CPointer<GtkPaned>,
    reversed: Int,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *cycle-handle-focus* event. Arguments:
 * 1. widget: CPointer<GtkPaned>
 * 2. reversed: Int (represents a Boolean)
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias CycleHandleFocusHandler = CFunction<(
    widget: CPointer<GtkPaned>,
    reversed: Int,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *move-handle* event. Arguments:
 * 1. widget: CPointer<GtkPaned>
 * 2. scrollType: GtkScrollType
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias MoveHandleHandler = CFunction<(
    widget: CPointer<GtkPaned>,
    scrollType: GtkScrollType,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *toggle-handle-focus* event. Arguments:
 * 1. widget: CPointer<GtkPaned>
 * 2. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias ToggleHandleFocusHandler = CFunction<(widget: CPointer<GtkPaned>, userData: gpointer) -> Int>
