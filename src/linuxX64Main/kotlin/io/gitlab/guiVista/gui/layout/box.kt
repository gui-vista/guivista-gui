package io.gitlab.guiVista.gui.layout

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Box private constructor(
    ptr: CPointer<GtkBox>? = null,
    orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL
) : BoxBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_box_new(orientation, 0)

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkBox>?): Box = Box(ptr)

        public fun create(
            ptr: CPointer<GtkBox>?,
            orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL
        ): Box = Box(ptr, orientation)
    }
}

public fun boxLayout(
    boxPtr: CPointer<GtkBox>? = null,
    orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL,
    init: Box.() -> Unit
): Box {
    val box = Box.create(boxPtr, orientation)
    box.init()
    return box
}

public fun CPointer<GtkBox>?.toBox(): Box = Box.fromPointer(this)
