package io.gitlab.guiVista.gui.layout.stack

import gtk3.*
import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.layout.ContainerBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class StackSideBar private constructor(ptr: CPointer<GtkStackSidebar>? = null) : BinBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_stack_sidebar_new()
    public val gtkStackSideBarPtr: CPointer<GtkStackSidebar>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var stack: Stack?
        get() {
            val ptr = gtk_stack_sidebar_get_stack(gtkStackSideBarPtr)
            return if (ptr != null) Stack.fromPointer(ptr) else null
        }
        set(value) = gtk_stack_sidebar_set_stack(gtkStackSideBarPtr, value?.gtkStackPtr)

    public actual companion object {
        public actual fun create(): StackSideBar = StackSideBar()

        public fun fromPointer(ptr: CPointer<GtkStackSidebar>?): StackSideBar = StackSideBar(ptr)
    }
}

public fun CPointer<GtkStackSidebar>?.toStackSideBar(): StackSideBar = StackSideBar.fromPointer(this)
