package io.gitlab.guiVista.gui.layout

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

public actual class Expander private constructor(
    ptr: CPointer<GtkExpander>? = null,
    label: String = "",
    mnemonic: String = ""
) : BinBase {
    @Suppress("IfThenToElvis")
    override val gtkWidgetPtr: CPointer<GtkWidget>? =
        when {
            ptr != null -> ptr.reinterpret()
            label.isNotEmpty() -> gtk_expander_new(label)
            else -> gtk_expander_new_with_mnemonic(mnemonic)
        }
    public val gtkExpanderPtr: CPointer<GtkExpander>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual var expanded: Boolean
        get() = gtk_expander_get_expanded(gtkExpanderPtr) == TRUE
        set(value) = gtk_expander_set_expanded(gtkExpanderPtr, if (value) TRUE else FALSE)

    public actual var label: String
        get() = gtk_expander_get_label(gtkExpanderPtr)?.toKString() ?: ""
        set(value) = gtk_expander_set_label(gtkExpanderPtr, value)

    public actual var labelFill: Boolean
        get() = gtk_expander_get_label_fill(gtkExpanderPtr) == TRUE
        set(value) = gtk_expander_set_label_fill(gtkExpanderPtr, if (value) TRUE else FALSE)

    public actual var labelWidget: WidgetBase?
        get() {
            val ptr = gtk_expander_get_label_widget(gtkExpanderPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) = gtk_expander_set_label_widget(gtkExpanderPtr, value?.gtkWidgetPtr)

    public actual var resizeTopLevel: Boolean
        get() = gtk_expander_get_resize_toplevel(gtkExpanderPtr) == TRUE
        set(value) = gtk_expander_set_resize_toplevel(gtkExpanderPtr, if (value) TRUE else FALSE)

    public actual var useMarkup: Boolean
        get() = gtk_expander_get_use_markup(gtkExpanderPtr) == TRUE
        set(value) = gtk_expander_set_use_markup(gtkExpanderPtr, if (value) TRUE else FALSE)

    public actual var useUnderline: Boolean
        get() = gtk_expander_get_use_underline(gtkExpanderPtr) == TRUE
        set(value) = gtk_expander_set_use_underline(gtkExpanderPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public actual fun fromLabel(label: String): Expander = Expander(label = label)

        public actual fun fromMnemonic(mnemonic: String): Expander = Expander(mnemonic = mnemonic)

        public fun fromPointer(ptr: CPointer<GtkExpander>?): Expander = Expander(ptr = ptr)
    }

    /**
     * Connects the *activate* event to a [handler] on a [Expander]. This event is occurs when the expander is
     * activated.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActivatedEvent(handler: CPointer<ActivateHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkExpanderPtr, signal = ExpanderEvent.activate, slot = handler, data = userData)


    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkExpanderPtr, handlerId)
    }
}

public fun expanderLayout(
    expanderPtr: CPointer<GtkExpander>? = null,
    label: String = "",
    mnemonic: String = "",
    init: Expander.() -> Unit
): Expander {
    val expander = when {
        expanderPtr != null -> Expander.fromPointer(expanderPtr)
        label.isNotEmpty() -> Expander.fromLabel(label)
        else -> Expander.fromMnemonic(mnemonic)
    }
    expander.init()
    return expander
}

/**
 * The event handler for the *activate* event. Arguments:
 * 1. expander: CPointer<GtkExpander>
 * 2. userData: gpointer
 */
public typealias ActivateHandler = CFunction<(
    expander: CPointer<GtkExpander>,
    userData: gpointer
) -> Unit>

public fun CPointer<GtkExpander>?.toExpander(): Expander = Expander.fromPointer(this)
