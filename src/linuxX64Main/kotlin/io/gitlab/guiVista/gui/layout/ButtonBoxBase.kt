package io.gitlab.guiVista.gui.layout

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual interface ButtonBoxBase : BoxBase, Orientable {
    override val gtkOrientable: CPointer<GtkOrientable>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkButtonBoxPtr: CPointer<GtkButtonBox>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var layout: UInt
        get() = gtk_button_box_get_layout(gtkButtonBoxPtr)
        set(value) = gtk_button_box_set_layout(gtkButtonBoxPtr, value)

    public actual fun changeIsChildSecondary(child: WidgetBase, isSecondary: Boolean) {
        gtk_button_box_set_child_secondary(widget = gtkButtonBoxPtr, child = child.gtkWidgetPtr,
            is_secondary = if (isSecondary) TRUE else FALSE)
    }

    public actual fun fetchIsChildSecondary(child: WidgetBase): Boolean =
        gtk_button_box_get_child_secondary(gtkButtonBoxPtr, child.gtkWidgetPtr) == TRUE

    public actual fun fetchIsChildNonHomogeneous(child: WidgetBase): Boolean =
        gtk_button_box_get_child_non_homogeneous(gtkButtonBoxPtr, child.gtkWidgetPtr) == TRUE

    public actual fun changeIsChildNonHomogeneous(child: WidgetBase, isNonHomogeneous: Boolean) {
        gtk_button_box_set_child_non_homogeneous(widget = gtkButtonBoxPtr, child = child.gtkWidgetPtr,
            non_homogeneous = if (isNonHomogeneous) TRUE else FALSE)
    }
}
