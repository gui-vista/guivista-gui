package io.gitlab.guiVista.gui

import glib2.FALSE
import glib2.TRUE
import glib2.g_free
import glib2.g_strfreev
import gtk3.*
import io.gitlab.guiVista.core.Closable
import kotlinx.cinterop.*

public actual class ImageBufferFormat private constructor(ptr: CPointer<GdkPixbufFormat>?) : Closable {
    public val gdkPixbufFormatPtr: CPointer<GdkPixbufFormat>? = ptr
    public actual val name: String
        get() = gdk_pixbuf_format_get_name(gdkPixbufFormatPtr)?.toKString() ?: ""
    public actual val desc: String
        get() = gdk_pixbuf_format_get_description(gdkPixbufFormatPtr)?.toKString() ?: ""
    public actual val isWritable: Boolean
        get() = gdk_pixbuf_format_is_writable(gdkPixbufFormatPtr) == TRUE
    public actual val isScalable: Boolean
        get() = gdk_pixbuf_format_is_scalable(gdkPixbufFormatPtr) == TRUE
    public actual var disabled: Boolean
        get() = gdk_pixbuf_format_is_disabled(gdkPixbufFormatPtr) == TRUE
        set(value) = gdk_pixbuf_format_set_disabled(gdkPixbufFormatPtr, if (value) TRUE else FALSE)
    public actual val license: String
        get() {
            val ptr = gdk_pixbuf_format_get_license(gdkPixbufFormatPtr)
            val result = ptr?.toKString() ?: ""
            g_free(ptr)
            return result
        }

    public actual fun copy(): ImageBufferFormat = fromPointer(gdk_pixbuf_format_copy(gdkPixbufFormatPtr))

    public actual fun fetchMimeTypes(): Array<String> = memScoped {
        val ptr = gdk_pixbuf_format_get_mime_types(gdkPixbufFormatPtr)
        val tmpList = mutableListOf<String>()
        var pos = 0
        var tmpItem: CPointer<ByteVar>?
        do {
            tmpItem = ptr?.get(pos)
            if (tmpItem != null) tmpList += tmpItem.toKString()
            pos++
        } while (tmpItem != null)
        g_strfreev(ptr)
        tmpList.toTypedArray()
    }

    public actual fun fetchExtensions(): Array<String> = memScoped {
        val ptr = gdk_pixbuf_format_get_extensions(gdkPixbufFormatPtr)
        val tmpList = mutableListOf<String>()
        var pos = 0
        var tmpItem: CPointer<ByteVar>?
        do {
            tmpItem = ptr?.get(pos)
            if (tmpItem != null) tmpList += tmpItem.toKString()
            pos++
        } while (tmpItem != null)
        g_strfreev(ptr)
        tmpList.toTypedArray()
    }

    override fun close() {
        gdk_pixbuf_format_free(gdkPixbufFormatPtr)
    }

    public companion object {
        public fun fromPointer(ptr: CPointer<GdkPixbufFormat>?): ImageBufferFormat = ImageBufferFormat(ptr)
    }
}

public fun CPointer<GdkPixbufFormat>?.toImageBufferFormat(): ImageBufferFormat =
    ImageBufferFormat.fromPointer(this)
