package io.gitlab.guiVista.gui

import gtk3.*
import io.gitlab.guiVista.core.InitiallyUnowned
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString

public actual class FileFilter private constructor(ptr: CPointer<GtkFileFilter>? = null) : InitiallyUnowned {
    public val gtkFileFilterPtr: CPointer<GtkFileFilter>? = ptr ?: gtk_file_filter_new()
    public actual var name: String
        get() = gtk_file_filter_get_name(gtkFileFilterPtr)?.toKString() ?: ""
        set(value) = gtk_file_filter_set_name(gtkFileFilterPtr, value)

    public actual infix fun addMimeType(mimeType: String) {
        gtk_file_filter_add_mime_type(gtkFileFilterPtr, mimeType)
    }

    public actual infix fun addPattern(pattern: String) {
        gtk_file_filter_add_pattern(gtkFileFilterPtr, pattern)
    }

    public actual fun addPixBufFormats() {
        gtk_file_filter_add_pixbuf_formats(gtkFileFilterPtr)
    }

    public actual companion object {
        public actual fun create(): FileFilter = FileFilter()

        public fun fromPointer(ptr: CPointer<GtkFileFilter>?): FileFilter = FileFilter(ptr)
    }
}

public fun fileFilter(ptr: CPointer<GtkFileFilter>? = null, init: FileFilter.() -> Unit): FileFilter {
    val filter = if (ptr != null) FileFilter.fromPointer(ptr) else FileFilter.create()
    filter.init()
    return filter
}

public fun CPointer<GtkFileFilter>?.toFileFilter(): FileFilter = FileFilter.fromPointer(this)
