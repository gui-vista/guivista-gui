package io.gitlab.guiVista.gui

import glib2.FALSE
import gtk3.GdkAtom
import gtk3.gdk_atom_intern
import gtk3.gdk_atom_intern_static_string
import gtk3.gdk_atom_name
import kotlinx.cinterop.toKString

public actual class Atom private constructor(atom: GdkAtom?) {
    public val gdkAtomPtr: GdkAtom? = atom
    public actual val name: String
        get() = gdk_atom_name(gdkAtomPtr)?.toKString() ?: ""

    public actual companion object {
        public fun fromPointer(ptr: GdkAtom?): Atom = Atom(ptr)

        public actual fun internString(name: String): Atom = Atom(gdk_atom_intern(name, FALSE))

        public actual fun internStaticString(name: String): Atom = Atom(gdk_atom_intern_static_string(name))
    }
}
