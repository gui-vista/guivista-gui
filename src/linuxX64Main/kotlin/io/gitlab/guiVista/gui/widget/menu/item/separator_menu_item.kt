package io.gitlab.guiVista.gui.widget.menu.item

import gtk3.GtkSeparatorMenuItem
import gtk3.GtkWidget
import gtk3.gtk_separator_menu_item_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class SeparatorMenuItem private constructor(ptr: CPointer<GtkSeparatorMenuItem>? = null) : MenuItemBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? =
        ptr?.reinterpret() ?: gtk_separator_menu_item_new()
    public val gtkSeparatorMenuItemPtr: CPointer<GtkSeparatorMenuItem>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkSeparatorMenuItem>?): SeparatorMenuItem = SeparatorMenuItem(ptr)

        public actual fun create(): SeparatorMenuItem = SeparatorMenuItem()
    }
}

public fun separatorMenuItem(
    ptr: CPointer<GtkSeparatorMenuItem>? = null,
    init: SeparatorMenuItem.() -> Unit = {}
): SeparatorMenuItem {
    val menuItem = if (ptr != null) SeparatorMenuItem.fromPointer(ptr) else SeparatorMenuItem.create()
    menuItem.init()
    return menuItem
}

public fun CPointer<GtkSeparatorMenuItem>?.toSeparatorMenuItem(): SeparatorMenuItem =
    SeparatorMenuItem.fromPointer(this)
