package io.gitlab.guiVista.gui.widget.dataEntry

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import io.gitlab.guiVista.gui.event.EventBase
import io.gitlab.guiVista.gui.layout.BinBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class SearchBar private constructor(ptr: CPointer<GtkSearchBar>? = null) : BinBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_search_bar_new()
    public val gtkSearchBarPtr: CPointer<GtkSearchBar>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var showCloseButton: Boolean
        get() = gtk_search_bar_get_show_close_button(gtkSearchBarPtr) == TRUE
        set(value) = gtk_search_bar_set_show_close_button(gtkSearchBarPtr, if (value) TRUE else FALSE)
    public actual var searchMode: Boolean
        get() = gtk_search_bar_get_search_mode(gtkSearchBarPtr) == TRUE
        set(value) = gtk_search_bar_set_search_mode(gtkSearchBarPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public actual fun create(): SearchBar = SearchBar()

        public fun fromPointer(ptr: CPointer<GtkSearchBar>?): SearchBar = SearchBar(ptr)
    }

    public actual fun connectEntry(entry: EntryBase) {
        gtk_search_bar_connect_entry(gtkSearchBarPtr, entry.gtkEntryPtr)
    }

    public actual fun handleEvent(event: EventBase): Boolean =
        gtk_search_bar_handle_event(gtkSearchBarPtr, event.gdkEventPtr) == TRUE
}

public fun searchBarWidget(searchBarPtr: CPointer<GtkSearchBar>? = null, init: SearchBar.() -> Unit = {}): SearchBar {
    val searchBar = if (searchBarPtr != null) SearchBar.fromPointer(searchBarPtr) else SearchBar.create()
    searchBar.init()
    return searchBar
}

public fun CPointer<GtkSearchBar>?.toSearchBar(): SearchBar = SearchBar.fromPointer(this)
