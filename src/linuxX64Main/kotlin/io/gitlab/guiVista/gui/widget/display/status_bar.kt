package io.gitlab.guiVista.gui.widget.display

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.layout.Orientable
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual class StatusBar private constructor(ptr: CPointer<GtkStatusbar>? = null) : ContainerBase, Orientable {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_statusbar_new()
    override val gtkOrientable: CPointer<GtkOrientable>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkStatusBarPtr: CPointer<GtkStatusbar>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual val messageArea: WidgetBase?
        get() {
            val ptr = gtk_statusbar_get_message_area(gtkStatusBarPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }

    public actual companion object {
        public actual fun create(): StatusBar = StatusBar()

        public fun fromPointer(ptr: CPointer<GtkStatusbar>?): StatusBar = StatusBar(ptr)
    }

    public actual infix fun fetchContextId(contextDescription: String): UInt =
        gtk_statusbar_get_context_id(gtkStatusBarPtr, contextDescription)

    public actual fun push(contextId: UInt, text: String): UInt =
        gtk_statusbar_push(statusbar = gtkStatusBarPtr, context_id = contextId, text = text)

    public actual fun pop(contextId: UInt) {
        gtk_statusbar_pop(statusbar = gtkStatusBarPtr, context_id = contextId)
    }

    public actual fun remove(contextId: UInt, messageId: UInt) {
        gtk_statusbar_remove(statusbar = gtkStatusBarPtr, context_id = contextId, message_id = messageId)
    }

    public actual fun removeAll(contextId: UInt) {
        gtk_statusbar_remove_all(gtkStatusBarPtr, contextId)
    }
}

public fun statusBarWidget(ptr: CPointer<GtkStatusbar>? = null, init: StatusBar.() -> Unit): StatusBar {
    val statusBar = if (ptr != null) StatusBar.fromPointer(ptr) else StatusBar.create()
    statusBar.init()
    return statusBar
}

public fun CPointer<GtkStatusbar>?.toStatusBar(): StatusBar = StatusBar.fromPointer(this)
