package io.gitlab.guiVista.gui.widget

import glib2.TRUE
import glib2.g_object_unref
import gtk3.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.RgbaColor
import kotlinx.cinterop.CPointer

public actual class StyleContext private constructor(ptr: CPointer<GtkStyleContext>? = null) : ObjectBase {
    public val gtkStyleContextPtr: CPointer<GtkStyleContext>? = ptr ?: gtk_style_context_new()
    public actual val classList: DoublyLinkedList
        get() = DoublyLinkedList.fromPointer(gtk_style_context_list_classes(gtkStyleContextPtr))

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkStyleContext>?): StyleContext = StyleContext(ptr)

        public actual fun create(): StyleContext = StyleContext()
    }

    public actual infix fun addClass(className: String) {
        gtk_style_context_add_class(gtkStyleContextPtr, className)
    }

    public actual infix fun hasClass(className: String): Boolean =
        gtk_style_context_has_class(gtkStyleContextPtr, className) == TRUE

    public actual infix fun lookupColor(colorName: String): Pair<Boolean, RgbaColor> {
        val color = RgbaColor.create()
        val colorFound = gtk_style_context_lookup_color(
            context = gtkStyleContextPtr,
            color_name = colorName,
            color = color.gdkRgbaPtr
        ) == TRUE
        return colorFound to color
    }

    public actual infix fun removeClass(className: String) {
        gtk_style_context_remove_class(gtkStyleContextPtr, className)
    }

    public actual fun restore() {
        gtk_style_context_restore(gtkStyleContextPtr)
    }

    public actual fun save() {
        gtk_style_context_save(gtkStyleContextPtr)
    }

    public actual fun changeScale(scale: Int) {
        gtk_style_context_set_scale(gtkStyleContextPtr, scale)
    }

    override fun close() {
        g_object_unref(gtkStyleContextPtr)
    }
}

public fun CPointer<GtkStyleContext>?.toStyleContext(): StyleContext = StyleContext.fromPointer(this)
