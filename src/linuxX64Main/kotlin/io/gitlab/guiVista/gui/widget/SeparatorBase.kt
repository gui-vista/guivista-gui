package io.gitlab.guiVista.gui.widget

import gtk3.GtkOrientable
import gtk3.GtkSeparator
import io.gitlab.guiVista.gui.layout.Orientable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual interface SeparatorBase : WidgetBase, Orientable {
    override val gtkOrientable: CPointer<GtkOrientable>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkSeparatorPtr: CPointer<GtkSeparator>?
}
