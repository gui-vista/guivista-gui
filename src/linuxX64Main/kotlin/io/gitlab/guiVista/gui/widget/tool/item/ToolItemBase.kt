package io.gitlab.guiVista.gui.widget.tool.item

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.gui.SizeGroup
import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual interface ToolItemBase : BinBase {
    public val gtkToolItemPtr: CPointer<GtkToolItem>?
    override val gtkWidgetPtr: CPointer<GtkWidget>?
        get() = gtkToolItemPtr?.reinterpret()
    public actual val textSizeGroup: SizeGroup
        get() = SizeGroup.fromPointer(gtk_tool_item_get_text_size_group(gtkToolItemPtr))

    /**
     * The relief style of the tool item . Custom implementations of [ToolItemBase] should call this function in the
     * handler of the “toolbar_reconfigured” signal to find out the relief style of buttons.
     * @see [io.gitlab.guiVista.gui.widget.button.Button.relief]
     */
    public val reliefStyle: GtkReliefStyle
        get() = gtk_tool_item_get_relief_style(gtkToolItemPtr)
    public actual var isImportant: Boolean
        get() = gtk_tool_item_get_is_important(gtkToolItemPtr) == TRUE
        set(value) = gtk_tool_item_set_is_important(gtkToolItemPtr, if (value) TRUE else FALSE)
    public actual var visibleHorizontal: Boolean
        get() = gtk_tool_item_get_visible_horizontal(gtkToolItemPtr) == TRUE
        set(value) = gtk_tool_item_set_visible_horizontal(gtkToolItemPtr, if (value) TRUE else FALSE)
    public actual var visibleVertical: Boolean
        get() = gtk_tool_item_get_visible_vertical(gtkToolItemPtr) == TRUE
        set(value) = gtk_tool_item_set_visible_vertical(gtkToolItemPtr, if (value) TRUE else FALSE)
    public actual var expand: Boolean
        get() = gtk_tool_item_get_expand(gtkToolItemPtr) == TRUE
        set(value) = gtk_tool_item_set_expand(gtkToolItemPtr, if (value) TRUE else FALSE)
    public actual var homogeneous: Boolean
        get() = gtk_tool_item_get_homogeneous(gtkToolItemPtr) == TRUE
        set(value) = gtk_tool_item_set_homogeneous(gtkToolItemPtr, if (value) TRUE else FALSE)
    public actual var useDragWindow: Boolean
        get() = gtk_tool_item_get_use_drag_window(gtkToolItemPtr) == TRUE
        set(value) = gtk_tool_item_set_use_drag_window(gtkToolItemPtr, if (value) TRUE else FALSE)

    /**
     * Fetches the icon size used for tool item. Custom implementations of [ToolItemBase] should call this function to
     * find out what size icons they should use.
     */
    public val iconSize: GtkIconSize
        get() = gtk_tool_item_get_icon_size(gtkToolItemPtr)
    /**
     * Fetches the orientation used for tool item. Custom implementations of [ToolItemBase] should call this function
     * to find out what size icons they should use.
     */
    public val orientation: GtkOrientation
        get() = gtk_tool_item_get_orientation(gtkToolItemPtr)
    /**
     * Returns the toolbar style used for tool item. Custom implementations of [ToolItemBase] should call this function
     * in the handler of the GtkToolItem::toolbar_reconfigured signal to find out in what style the toolbar is
     * displayed and change themselves accordingly. Below are the styles:
     * - **GTK_TOOLBAR_BOTH**: The tool item should show both an icon and a label, stacked vertically
     * - **GTK_TOOLBAR_ICONS**: The toolbar shows only icons
     * - **GTK_TOOLBAR_TEXT**: The tool item should only show text
     * - **GTK_TOOLBAR_BOTH_HORIZ**: The tool item should show both an icon and a label, arranged horizontally
     */
    public val toolBarStyle: GtkToolbarStyle
        get() = gtk_tool_item_get_toolbar_style(gtkToolItemPtr)
    public actual val textAlignment: Float
        get() = gtk_tool_item_get_text_alignment(gtkToolItemPtr)

    /**
     * Fetches the text orientation used for tool item. Custom subclasses of [ToolItemBase] should call this function
     * to find out how text should be orientated.
     */
    public val textOrientation: GtkOrientation
        get() = gtk_tool_item_get_text_orientation(gtkToolItemPtr)

    public actual fun rebuildMenu() {
        gtk_tool_item_rebuild_menu(gtkToolItemPtr)
    }

    public actual infix fun changeTooltipText(text: String) {
        gtk_tool_item_set_tooltip_text(gtkToolItemPtr, text)
    }

    public actual infix fun changeTooltipMarkup(markup: String) {
        gtk_tool_item_set_tooltip_markup(gtkToolItemPtr, markup)
    }

    public actual fun changeProxyMenuItem(id: String, menuItem: WidgetBase) {
        gtk_tool_item_set_proxy_menu_item(tool_item = gtkToolItemPtr, menu_item_id = id,
            menu_item = menuItem.gtkWidgetPtr)
    }

    public actual fun fetchProxyMenuItemById(id: String): WidgetBase? {
        val ptr = gtk_tool_item_get_proxy_menu_item(gtkToolItemPtr, id)
        return if (ptr != null) Widget.fromPointer(ptr) else null
    }

    public actual fun fetchProxyMenuItem(): WidgetBase? {
        val ptr = gtk_tool_item_retrieve_proxy_menu_item(gtkToolItemPtr)
        return if (ptr != null) Widget.fromPointer(ptr) else null
    }
}
