package io.gitlab.guiVista.gui.widget.button

import gtk3.GtkVolumeButton
import gtk3.GtkWidget
import gtk3.gtk_volume_button_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class VolumeButton private constructor(ptr: CPointer<GtkVolumeButton>? = null) : ScaleButtonBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_volume_button_new()
    public val gtkVolumeButtonPtr: CPointer<GtkVolumeButton>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkVolumeButton>?): VolumeButton = VolumeButton(ptr)

        public actual fun create(): VolumeButton = VolumeButton()
    }
}

public fun volumeButtonWidget(
    volumeButtonPtr: CPointer<GtkVolumeButton>? = null,
    init: VolumeButton.() -> Unit = {}
): VolumeButton {
    val volumeButton = if (volumeButtonPtr != null) VolumeButton.fromPointer(volumeButtonPtr) else VolumeButton.create()
    volumeButton.init()
    return volumeButton
}

public fun CPointer<GtkVolumeButton>?.toVolumeButton(): VolumeButton = VolumeButton.fromPointer(this)
