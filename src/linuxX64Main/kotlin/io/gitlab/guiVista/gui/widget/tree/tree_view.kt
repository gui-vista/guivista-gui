package io.gitlab.guiVista.gui.widget.tree

import glib2.FALSE
import glib2.GDestroyNotify
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.Tooltip
import io.gitlab.guiVista.gui.cell.renderer.CellRendererBase
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.tree.TreeModel
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreeModelIterator
import io.gitlab.guiVista.gui.tree.TreePath
import io.gitlab.guiVista.gui.widget.dataEntry.Entry
import io.gitlab.guiVista.gui.widget.dataEntry.EntryBase
import kotlinx.cinterop.*

public actual class TreeView private constructor(
    ptr: CPointer<GtkTreeView>? = null,
    model: TreeModelBase? = null
) : ContainerBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = when {
        ptr != null -> ptr.reinterpret()
        model != null -> gtk_tree_view_new_with_model(model.gtkTreeModelPtr)
        else -> gtk_tree_view_new()
    }
    public val gtkTreeViewPtr: CPointer<GtkTreeView>?
        get() = gtkWidgetPtr?.reinterpret()

    /** Whether grid lines should be drawn in the tree view. Default value is `GTK_TREE_VIEW_GRID_LINES_NONE`. */
    public var gridLines: GtkTreeViewGridLines
        get() = gtk_tree_view_get_grid_lines(gtkTreeViewPtr)
        set(value) = gtk_tree_view_set_grid_lines(gtkTreeViewPtr, value)
    public actual val selection: TreeSelection
        get() = TreeSelection.fromPointer(gtk_tree_view_get_selection(gtkTreeViewPtr))
    public actual var reorderable: Boolean
        get() = gtk_tree_view_get_reorderable(gtkTreeViewPtr) == TRUE
        set(value) = gtk_tree_view_set_reorderable(gtkTreeViewPtr, if (value) TRUE else FALSE)
    public actual val totalColumns: UInt
        get() = gtk_tree_view_get_n_columns(gtkTreeViewPtr)
    public actual var levelIndentation: Int
        get() = gtk_tree_view_get_level_indentation(gtkTreeViewPtr)
        set(value) = gtk_tree_view_set_level_indentation(gtkTreeViewPtr, value)
    public actual var showExpanders: Boolean
        get() = gtk_tree_view_get_show_expanders(gtkTreeViewPtr) == TRUE
        set(value) = gtk_tree_view_set_show_expanders(gtkTreeViewPtr, if (value) TRUE else FALSE)
    public actual var model: TreeModelBase
        get() = TreeModel.fromPointer(gtk_tree_view_get_model(gtkTreeViewPtr))
        set(value) = gtk_tree_view_set_model(gtkTreeViewPtr, value.gtkTreeModelPtr)
    public actual var headersVisible: Boolean
        get() = gtk_tree_view_get_headers_visible(gtkTreeViewPtr) == TRUE
        set(value) = gtk_tree_view_set_headers_visible(gtkTreeViewPtr, if (value) TRUE else FALSE)
    public actual var headersClickable: Boolean
        get() = gtk_tree_view_get_headers_clickable(gtkTreeViewPtr) == TRUE
        set(value) = gtk_tree_view_set_headers_clickable(gtkTreeViewPtr, if (value) TRUE else FALSE)
    public actual var activateOnSingleClick: Boolean
        get() = gtk_tree_view_get_activate_on_single_click(gtkTreeViewPtr) == TRUE
        set(value) = gtk_tree_view_set_activate_on_single_click(gtkTreeViewPtr, if (value) TRUE else FALSE)
    public actual var expanderColumn: TreeViewColumn
        get() = TreeViewColumn.fromPointer(gtk_tree_view_get_expander_column(gtkTreeViewPtr))
        set(value) = gtk_tree_view_set_expander_column(gtkTreeViewPtr, value.gtkTreeViewColumnPtr)
    public actual var searchColumn: Int
        get() = gtk_tree_view_get_search_column(gtkTreeViewPtr)
        set(value) = gtk_tree_view_set_search_column(gtkTreeViewPtr, value)
    public actual var searchEntry: EntryBase?
        get() {
            val ptr = gtk_tree_view_get_search_entry(gtkTreeViewPtr)
            return if (ptr != null) Entry.fromPointer(ptr) else null
        }
        set(value) = gtk_tree_view_set_search_entry(gtkTreeViewPtr, value?.gtkEntryPtr)
    public actual var hoverSelection: Boolean
        get() = gtk_tree_view_get_hover_selection(gtkTreeViewPtr) == TRUE
        set(value) = gtk_tree_view_set_hover_selection(gtkTreeViewPtr, if (value) TRUE else FALSE)
    public actual var hoverExpand: Boolean
        get() = gtk_tree_view_get_hover_expand(gtkTreeViewPtr) == TRUE
        set(value) = gtk_tree_view_set_hover_expand(gtkTreeViewPtr, if (value) TRUE else FALSE)
    public actual var rubberBanding: Boolean
        get() = gtk_tree_view_get_rubber_banding(gtkTreeViewPtr) == TRUE
        set(value) = gtk_tree_view_set_rubber_banding(gtkTreeViewPtr, if (value) TRUE else FALSE)
    public actual var enableTreeLines: Boolean
        get() = gtk_tree_view_get_enable_tree_lines(gtkTreeViewPtr) == TRUE
        set(value) = gtk_tree_view_set_enable_tree_lines(gtkTreeViewPtr, if (value) TRUE else FALSE)
    public actual var tooltipColumn: Int
        get() = gtk_tree_view_get_tooltip_column(gtkTreeViewPtr)
        set(value) = gtk_tree_view_set_tooltip_column(gtkTreeViewPtr, value)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkTreeView>?): TreeView = TreeView(ptr)

        public actual fun create(): TreeView = TreeView()

        public actual fun createWithModel(model: TreeModelBase): TreeView = TreeView(model = model)
    }

    public actual fun appendColumn(column: TreeViewColumn): Int =
        gtk_tree_view_append_column(gtkTreeViewPtr, column.gtkTreeViewColumnPtr)

    public actual fun removeColumn(column: TreeViewColumn): Int =
        gtk_tree_view_remove_column(gtkTreeViewPtr, column.gtkTreeViewColumnPtr)

    public actual fun insertColumn(column: TreeViewColumn, pos: Int): Int =
        gtk_tree_view_insert_column(tree_view = gtkTreeViewPtr, column = column.gtkTreeViewColumnPtr, position = pos)

    public actual fun fetchColumn(pos: Int): TreeViewColumn? {
        val ptr = gtk_tree_view_get_column(gtkTreeViewPtr, pos)
        return if (ptr != null) TreeViewColumn.fromPointer(ptr) else null
    }

    public actual fun fetchMultipleColumns(): DoublyLinkedList =
        DoublyLinkedList.fromPointer(gtk_tree_view_get_columns(gtkTreeViewPtr))

    public actual fun moveColumnAfter(column: TreeViewColumn, baseColumn: TreeViewColumn?) {
        gtk_tree_view_move_column_after(
            tree_view = gtkTreeViewPtr,
            column = column.gtkTreeViewColumnPtr,
            base_column = baseColumn?.gtkTreeViewColumnPtr
        )
    }

    public actual fun scrollToPoint(treeX: Int, treeY: Int) {
        gtk_tree_view_scroll_to_point(tree_view = gtkTreeViewPtr, tree_x = treeX, tree_y = treeY)
    }

    public actual fun scrollToCell(path: TreePath?, column: TreeViewColumn?, useAlign: Boolean, rowAlign: Float,
                                   colAlign: Float) {
        gtk_tree_view_scroll_to_cell(
            tree_view = gtkTreeViewPtr,
            path = path?.gtkTreePathPtr,
            column = column?.gtkTreeViewColumnPtr,
            use_align = if (useAlign) TRUE else FALSE,
            row_align = rowAlign,
            col_align = colAlign
        )
    }

    public actual fun changeCursor(path: TreePath, focusColumn: TreeViewColumn?, startEditing: Boolean) {
        gtk_tree_view_set_cursor(
            tree_view = gtkTreeViewPtr,
            path = path.gtkTreePathPtr,
            focus_column = focusColumn?.gtkTreeViewColumnPtr,
            start_editing = if (startEditing) TRUE else FALSE
        )
    }

    public actual fun changeCursorOnCell(path: TreePath, focusColumn: TreeViewColumn?, focusCell: CellRendererBase?,
                                         startEditing: Boolean) {
        gtk_tree_view_set_cursor_on_cell(
            tree_view = gtkTreeViewPtr,
            path = path.gtkTreePathPtr,
            focus_column = focusColumn?.gtkTreeViewColumnPtr,
            focus_cell = focusCell?.gtkCellRendererPtr,
            start_editing = if (startEditing) TRUE else FALSE
        )
    }

    public actual fun fetchCursor(): Pair<TreePath?, TreeViewColumn?> = memScoped {
        val path = alloc<CPointerVar<GtkTreePath>>()
        val focusColumn = alloc<CPointerVar<GtkTreeViewColumn>>()
        gtk_tree_view_get_cursor(
            tree_view = gtkTreeViewPtr,
            path = path.ptr,
            focus_column = focusColumn.ptr
        )
        TreePath.fromPointer(path.value) to TreeViewColumn.fromPointer(focusColumn.value)
    }

    public actual fun activateRow(path: TreePath, column: TreeViewColumn) {
        gtk_tree_view_row_activated(tree_view = gtkTreeViewPtr, path = path.gtkTreePathPtr,
            column = column.gtkTreeViewColumnPtr)
    }

    public actual fun expandAll() {
        gtk_tree_view_expand_all(gtkTreeViewPtr)
    }

    public actual fun collapseAll() {
        gtk_tree_view_collapse_all(gtkTreeViewPtr)
    }

    public actual fun expandToPath(path: TreePath) {
        gtk_tree_view_expand_to_path(gtkTreeViewPtr, path.gtkTreePathPtr)
    }

    public actual fun expandRow(path: TreePath, openAll: Boolean): Boolean =
        gtk_tree_view_expand_row(
            tree_view = gtkTreeViewPtr,
            path = path.gtkTreePathPtr,
            open_all = if (openAll) TRUE else FALSE
        ) == TRUE

    public actual fun collapseRow(path: TreePath): Boolean =
        gtk_tree_view_collapse_row(gtkTreeViewPtr, path.gtkTreePathPtr) == TRUE

    public actual fun rowExpanded(path: TreePath): Boolean =
        gtk_tree_view_row_expanded(gtkTreeViewPtr, path.gtkTreePathPtr) == TRUE

    public actual fun fetchPathAtPosition(xPos: Int, yPos: Int): PathAtPositionResult = memScoped {
        val path = alloc<CPointerVar<GtkTreePath>>()
        val column = alloc<CPointerVar<GtkTreeViewColumn>>()
        val cellXPos = alloc<IntVar>().apply { value = 0 }
        val cellYPos = alloc<IntVar>().apply { value = 0 }
        val rowExists = gtk_tree_view_get_path_at_pos(
            tree_view = gtkTreeViewPtr,
            x = xPos,
            y = yPos,
            path = path.ptr,
            column = column.ptr,
            cell_x = cellXPos.ptr,
            cell_y = cellYPos.ptr
        ) == TRUE
        PathAtPositionResult(
            rowExists = rowExists,
            path = TreePath.fromPointer(path.value),
            column = TreeViewColumn.fromPointer(column.value),
            cellXPos = cellXPos.value,
            cellYPos = cellYPos.value
        )
    }

    public actual fun isBlankAtPosition(xPos: Int, yPos: Int): BlankAtPositionResult = memScoped {
        val path = alloc<CPointerVar<GtkTreePath>>()
        val column = alloc<CPointerVar<GtkTreeViewColumn>>()
        val cellXPos = alloc<IntVar>().apply { value = 0 }
        val cellYPos = alloc<IntVar>().apply { value = 0 }
        val blankArea = gtk_tree_view_is_blank_at_pos(
            tree_view = gtkTreeViewPtr,
            x = xPos,
            y = yPos,
            path = path.ptr,
            column = column.ptr,
            cell_x = cellXPos.ptr,
            cell_y = cellYPos.ptr
        ) == TRUE
        BlankAtPositionResult(
            blankArea = blankArea,
            path = TreePath.fromPointer(path.value),
            column = TreeViewColumn.fromPointer(column.value),
            cellYPos = cellYPos.value,
            cellXPos = cellXPos.value
        )
    }

    public actual fun fetchVisibleRange(): Triple<TreePath, TreePath, Boolean> = memScoped {
        val startPath = alloc<CPointerVar<GtkTreePath>>()
        val endPath = alloc<CPointerVar<GtkTreePath>>()
        val validPaths = gtk_tree_view_get_visible_range(
            tree_view = gtkTreeViewPtr,
            start_path = startPath.ptr,
            end_path = endPath.ptr
        ) == TRUE
        Triple(TreePath.fromPointer(startPath.value), TreePath.fromPointer(endPath.value), validPaths)
    }

    public actual fun convertTreeToWidgetCoords(treeXPos: Int, treeYPos: Int): Pair<Int, Int> = memScoped {
        val widgetXPos = alloc<IntVar>().apply { value = 0 }
        val widgetYPos = alloc<IntVar>().apply { value = 0 }
        gtk_tree_view_convert_tree_to_widget_coords(
            tree_view = gtkTreeViewPtr,
            tx = treeXPos,
            ty = treeYPos,
            wx = widgetXPos.ptr,
            wy = widgetYPos.ptr
        )
        widgetXPos.value to widgetYPos.value
    }

    public actual fun convertWidgetToTreeCoords(widgetXPos: Int, widgetYPos: Int): Pair<Int, Int> = memScoped {
        val treeXPos = alloc<IntVar>().apply { value = 0 }
        val treeYPos = alloc<IntVar>().apply { value = 0 }
        gtk_tree_view_convert_widget_to_tree_coords(
            tree_view = gtkTreeViewPtr,
            wx = widgetXPos,
            wy = widgetYPos,
            tx = treeXPos.ptr,
            ty = treeYPos.ptr
        )
        treeXPos.value to treeYPos.value
    }

    public actual fun changeTooltipRow(tooltip: Tooltip, path: TreePath) {
        gtk_tree_view_set_tooltip_row(tree_view = gtkTreeViewPtr, tooltip = tooltip.gtkTooltipPtr,
            path = path.gtkTreePathPtr)
    }

    public actual fun changeTooltipCell(tooltip: Tooltip, path: TreePath?, column: TreeViewColumn?,
                                        cell: CellRendererBase?) {
        gtk_tree_view_set_tooltip_cell(
            tree_view = gtkTreeViewPtr,
            tooltip = tooltip.gtkTooltipPtr,
            path = path?.gtkTreePathPtr,
            column = column?.gtkTreeViewColumnPtr,
            cell = cell?.gtkCellRendererPtr
        )
    }

    public actual fun fetchTooltipContext(
        xPos: Int,
        yPos: Int,
        keyboardTip: Boolean
    ): TooltipContextResult = memScoped {
        val model = alloc<CPointerVar<GtkTreeModel>>()
        val path = alloc<CPointerVar<GtkTreePath>>()
        val iter = alloc<GtkTreeIter>()
        val isRow = gtk_tree_view_get_tooltip_context(
            tree_view = gtkTreeViewPtr,
            x = cValuesOf(xPos),
            y = cValuesOf(yPos),
            keyboard_tip = if (keyboardTip) TRUE else FALSE,
            model = model.ptr,
            path = path.ptr,
            iter = iter.ptr
        ) == TRUE
        TooltipContextResult(
            model = TreeModel.fromPointer(model.value),
            path = TreePath.fromPointer(path.value),
            iter = TreeModelIterator.fromPointer(iter.ptr),
            isRow = isRow
        )
    }

    /**
     * Convenience function that inserts a new column into the [TreeView] with the given cell renderer, and a
     * `GtkTreeCellDataFunc` to set cell renderer attributes (normally using data from the model). See also
     * `gtk_tree_view_column_set_cell_data_func()`, and `gtk_tree_view_column_pack_start()`. If tree_view has
     * **fixed_height** mode enabled then the new column will have its **sizing** property set to be
     * `GTK_TREE_VIEW_COLUMN_FIXED`.
     * @param title Column title.
     * @param cell The cell renderer for the column.
     * @param dataFunc The function to set attributes of the [cell renderer][cell].
     * @param destroyFunc Destroy notifier for [userData].
     * @param pos Position to insert, *-1* for append.
     * @param userData User data for [dataFunc].
     * @return Number of columns in the tree view post-insert.
     */
    public fun insertColumnWithDataFunc(
        title: String,
        cell: CellRendererBase,
        dataFunc: GtkTreeCellDataFunc,
        destroyFunc: GDestroyNotify,
        pos: Int = -1,
        userData: gpointer = fetchEmptyDataPointer()
    ): Int = gtk_tree_view_insert_column_with_data_func(
        tree_view = gtkTreeViewPtr,
        position = pos,
        title = title,
        cell = cell.gtkCellRendererPtr,
        func = dataFunc,
        data = userData,
        dnotify = destroyFunc
    )

    /**
     * Returns the compare function currently in use.
     * @return The currently used compare function for the search code.
     */
    public fun fetchSearchEqualFunc(): GtkTreeViewSearchEqualFunc? = gtk_tree_view_get_search_equal_func(gtkTreeViewPtr)

    /**
     * Sets the compare function for the interactive search capabilities. Note this is like comparing a [String] for
     * equality where `GtkTreeViewSearchEqualFunc` returns *false* on matches.
     * @param searchEqualFunc The compare function to use during the search.
     * @param userData The user data to for [searchEqualFunc].
     * @param destroyFunc Destroy notifier for [userData], may be *null*.
     */
    public fun changeSearchEqualFunc(
        searchEqualFunc: GtkTreeViewSearchEqualFunc,
        userData: gpointer = fetchEmptyDataPointer(),
        destroyFunc: GDestroyNotify? = null
    ) {
        gtk_tree_view_set_search_equal_func(
            tree_view = gtkTreeViewPtr,
            search_equal_func = searchEqualFunc,
            search_user_data = userData,
            search_destroy = destroyFunc
        )
    }

    /**
     * Returns the positioning function currently in use.
     * @return The currently used function for positioning the search dialog.
     */
    public fun fetchSearchPositionFunc(): GtkTreeViewSearchPositionFunc? =
        gtk_tree_view_get_search_position_func(gtkTreeViewPtr)

    /**
     * Sets the function to use when positioning the search dialog.
     * @param searchPositionFunc The function to use to position the search dialog, or *null* to use the default search
     * position function.
     * @param userData The user data for [searchPositionFunc].
     * @param destroyFunc Destroy notifier for [userData], or *null*.
     */
    public fun changeSearchPositionFunc(
        searchPositionFunc: GtkTreeViewSearchPositionFunc? = null,
        userData: gpointer = fetchEmptyDataPointer(),
        destroyFunc: GDestroyNotify? = null
    ) {
        gtk_tree_view_set_search_position_func(
            tree_view = gtkTreeViewPtr,
            func = searchPositionFunc,
            data = userData,
            destroy = destroyFunc
        )
    }

    /**
     * Returns the current row separator function.
     * @return The current row separator function.
     */
    public fun fetchRowSeparatorFunc(): GtkTreeViewRowSeparatorFunc? =
        gtk_tree_view_get_row_separator_func(gtkTreeViewPtr)

    /**
     * Sets the row separator function which is used to determine whether a row should be drawn as a separator. If the
     * row separator function is *null* then no separators are drawn. This is the default value.
     * @param rowSeparatorFunc The row separator function.
     * @param userData User data for [rowSeparatorFunc].
     * @param destroyFunc Destroy notifier for [userData], or *null*.
     */
    public fun changeRowSeparatorFunc(
        rowSeparatorFunc: GtkTreeViewRowSeparatorFunc,
        userData: gpointer = fetchEmptyDataPointer(),
        destroyFunc: GDestroyNotify? = null
    ) {
        gtk_tree_view_set_row_separator_func(
            tree_view = gtkTreeViewPtr,
            func = rowSeparatorFunc,
            data = userData,
            destroy = destroyFunc
        )
    }

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkTreeViewPtr, handlerId)
    }

    /**
     * Connects the *columns-changed* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectColumnsChangedEvent(handler: CPointer<ColumnsChangedHandler>,
                                          userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.columnsChanged, slot = handler, data = userData)

    /**
     * Connects the *cursor-changed* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCursorChangedEvent(handler: CPointer<CursorChangedHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.cursorChanged, slot = handler, data = userData)

    /**
     * Connects the *expand-collapse-cursor-row* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectExpandCollapseCursorRowEvent(handler: CPointer<ExpandCollapseCursorRowHandler>,
                                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.expandCollapseCursorRow, slot = handler,
            data = userData)

    /**
     * Connects the *move-cursor* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectMoveCursorEvent(handler: CPointer<MoveCursorHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.moveCursor, slot = handler, data = userData)

    /**
     * Connects the *row-activated* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRowActivatedEvent(handler: CPointer<RowActivatedHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.rowActivated, slot = handler, data = userData)

    /**
     * Connects the *row-collapsed* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRowCollapsedEvent(handler: CPointer<RowCollapsedHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.rowCollapsed, slot = handler, data = userData)

    /**
     * Connects the *row-expanded* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectRowExpandedEvent(handler: CPointer<RowExpandedHandler>,
                                       userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.rowExpanded, slot = handler, data = userData)

    /**
     * Connects the *select-all* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSelectAllEvent(handler: CPointer<SelectAllHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.selectAll, slot = handler, data = userData)

    /**
     * Connects the *select-cursor-parent* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSelectCursorParentEvent(handler: CPointer<SelectCursorParentHandler>,
                                              userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.selectCursorParent, slot = handler, data = userData)

    /**
     * Connects the *select-cursor-row* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSelectCursorRowEvent(handler: CPointer<SelectCursorRowHandler>,
                                           userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.selectCursorRow, slot = handler, data = userData)

    /**
     * Connects the *start-interactive-search* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectStartInteractiveSearchEvent(handler: CPointer<StartInteractiveSearchHandler>,
                                                  userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.startInteractiveSearch, slot = handler,
            data = userData)

    /**
     * Connects the *toggle-cursor-row* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectToggleCursorRowEvent(handler: CPointer<ToggleCursorRowHandler>,
                                           userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.toggleCursorRow, slot = handler, data = userData)

    /**
     * Connects the *unselect-all* event to a [handler] on a [TreeView].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectUnselectAllEvent(handler: CPointer<UnselectAllHandler>,
                                       userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkTreeViewPtr, signal = TreeViewEvent.unselectAll, slot = handler, data = userData)
}

public fun treeViewWidget(
    ptr: CPointer<GtkTreeView>? = null,
    model: TreeModelBase? = null,
    init: TreeView.() -> Unit = {}
): TreeView {
    val result = when {
        ptr != null -> TreeView.fromPointer(ptr)
        model != null -> TreeView.createWithModel(model)
        else -> TreeView.create()
    }
    result.init()
    return result
}

/**
 * The event handler for the *columns-changed* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. userData: gpointer
 */
public typealias ColumnsChangedHandler = CFunction<(treeView: CPointer<GtkTreeView>, userData: gpointer) -> Unit>

/**
 * The event handler for the *cursor-changed* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. userData: gpointer
 */
public typealias CursorChangedHandler = CFunction<(treeView: CPointer<GtkTreeView>, userData: gpointer) -> Unit>

/**
 * The event handler for the *expand-collapse-cursor-row* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. arg1: Int (represents a Boolean)
 * 3. arg2: Int (represents a Boolean)
 * 4. arg3: Int (represents a Boolean)
 * 5. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias ExpandCollapseCursorRowHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    arg1: Int,
    arg2: Int,
    arg3: Int,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *move-cursor* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. step: GtkMovementStep
 * 3. direction: Int
 * 4. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias MoveCursorHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    step: GtkMovementStep,
    direction: Int,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *row-activated* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. path: CPointer<GtkTreePath>
 * 3. column: CPointer<GtkTreeViewColumn>
 * 4. userData: gpointer
 */
public typealias RowActivatedHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    path: CPointer<GtkTreePath>,
    column: CPointer<GtkTreeViewColumn>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *row-collapsed* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. iter: CPointer<GtkTreeIter>
 * 3. path: CPointer<GtkTreePath>
 * 4. userData: gpointer
 */
public typealias RowCollapsedHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    iter: CPointer<GtkTreeIter>,
    path: CPointer<GtkTreePath>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *row-expanded* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. iter: CPointer<GtkTreeIter>
 * 3. path: CPointer<GtkTreePath>
 * 4. userData: gpointer
 */
public typealias RowExpandedHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    iter: CPointer<GtkTreeIter>,
    path: CPointer<GtkTreePath>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *select-all* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias SelectAllHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *select-cursor-parent* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias SelectCursorParentHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *select-cursor-row* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. arg1: Int (represents a Boolean)
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias SelectCursorRowHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    arg1: Int,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *start-interactive-search* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias StartInteractiveSearchHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *toggle-cursor-row* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias ToggleCursorRowHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *unselect-all* event. Arguments:
 * 1. treeView: CPointer<GtkTreeView>
 * 2. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias UnselectAllHandler = CFunction<(
    treeView: CPointer<GtkTreeView>,
    userData: gpointer
) -> Int>

public fun CPointer<GtkTreeView>?.toTreeView(): TreeView = TreeView.fromPointer(this)
