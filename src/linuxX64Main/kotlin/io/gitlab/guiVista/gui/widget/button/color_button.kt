package io.gitlab.guiVista.gui.widget.button

import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.RgbaColor
import io.gitlab.guiVista.gui.dialog.ColorChooser
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

public actual class ColorButton private constructor(
    ptr: CPointer<GtkColorButton>? = null,
    rgba: RgbaColor? = null
) : ButtonBase, ColorChooser {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = when {
        ptr != null -> ptr.reinterpret()
        rgba != null -> gtk_color_button_new_with_rgba(rgba.gdkRgbaPtr)
        else -> gtk_color_button_new()
    }
    override val gtkColorChooserPtr: CPointer<GtkColorChooser>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkColorButtonPtr: CPointer<GtkColorButton>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var title: String
        get() = gtk_color_button_get_title(gtkColorButtonPtr)?.toKString() ?: ""
        set(value) = gtk_color_button_set_title(gtkColorButtonPtr, value)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkColorButton>?): ColorButton = ColorButton(ptr = ptr)

        public actual fun create(): ColorButton = ColorButton()

        public actual fun createWithRgba(rgba: RgbaColor): ColorButton = ColorButton(rgba = rgba)
    }

    actual override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gtkColorButtonPtr, handlerId)
    }

    actual override fun close() {
        // Do nothing.
    }

    /**
     * Connects the *color-set* event to a [handler] on a [ColorButton]. This event is used when the user selects
     * a color. Note that this event is only fired when the user changes the color. If you need to react to
     * programmatic color changes as well, use the `notify::color` event.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectColorSetEvent(
        handler: CPointer<ColorSetHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkColorButtonPtr, signal = ColorButtonEvent.colorSet, slot = handler, data = userData)
}

public fun colorButtonWidget(
    ptr: CPointer<GtkColorButton>? = null,
    rgba: RgbaColor? = null,
    init: ColorButton.() -> Unit = {}
): ColorButton {
    val result = when {
        ptr != null -> ColorButton.fromPointer(ptr)
        rgba != null -> ColorButton.createWithRgba(rgba)
        else -> ColorButton.create()
    }
    result.init()
    return result
}

/**
 * The event handler for the *color-set* event. Arguments:
 * 1. colorBtn: CPointer<GtkColorButton>
 * 2. userData: gpointer
 */
public typealias ColorSetHandler = CFunction<(colorBtn: CPointer<GtkColorButton>, userData: gpointer) -> Unit>

public fun CPointer<GtkColorButton>?.toColorButton(): ColorButton = ColorButton.fromPointer(this)
