package io.gitlab.guiVista.gui.widget.button

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer

public actual interface ToggleButtonBase : ButtonBase {
    public val gtkToggleButtonPtr: CPointer<GtkToggleButton>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var active: Boolean
        get() = gtk_toggle_button_get_active(gtkToggleButtonPtr) == TRUE
        set(value) = gtk_toggle_button_set_active(gtkToggleButtonPtr, if (value) TRUE else FALSE)
    public actual var inconsistent: Boolean
        get() = gtk_toggle_button_get_inconsistent(gtkToggleButtonPtr) == TRUE
        set(value) = gtk_toggle_button_set_inconsistent(gtkToggleButtonPtr, if (value) TRUE else FALSE)
    public actual var mode: Boolean
        get() = gtk_toggle_button_get_mode(gtkToggleButtonPtr) == TRUE
        set(value) = gtk_toggle_button_set_mode(gtkToggleButtonPtr, if (value) TRUE else FALSE)

    /**
     * Connects the *toggled* event to a [handler] on a [ToggleButton]. This event is used when the
     * [toggle button's][ToggleButton] state has changed.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID for the [handler].
     */
    public fun connectToggledEvent(handler: CPointer<ToggledHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkToggleButtonPtr, signal = ToggleButtonBaseEvent.toggled, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkToggleButtonPtr, handlerId)
    }
}

/**
 * The event handler for the *toggled* event. Arguments:
 * 1. toggleButton: CPointer<GtkToggleButton>
 * 2. userData: gpointer
 */
public typealias ToggledHandler = CFunction<(toggleButton: CPointer<GtkToggleButton>, userData: gpointer) -> Unit>
