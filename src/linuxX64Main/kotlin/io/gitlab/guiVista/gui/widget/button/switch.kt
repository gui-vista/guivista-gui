package io.gitlab.guiVista.gui.widget.button

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual class Switch private constructor(ptr: CPointer<GtkSwitch>? = null) : WidgetBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_switch_new()
    public val gtkSwitchPtr: CPointer<GtkSwitch>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var active: Boolean
        get() = gtk_switch_get_active(gtkSwitchPtr) == TRUE
        set(value) = gtk_switch_set_active(gtkSwitchPtr, if (value) TRUE else FALSE)
    public actual var state: Boolean
        get() = gtk_switch_get_state(gtkSwitchPtr) == TRUE
        set(value) = gtk_switch_set_state(gtkSwitchPtr, if (value) TRUE else FALSE)

    /**
     * Connects the *state-set* event to a [handler] on a [Switch]. This event is used when the user changes the switch
     * position. The default handler keeps the state in sync with the [active property][active]. To implement delayed
     * state change applications can connect to this event, initiate the change of the underlying state, and call
     * `gtk_switch_set_state()` when the underlying state change is complete. The event handler should return *true*
     * to prevent the default handler from running.
     *
     * Visually, the underlying state is represented by the true color of the switch, while the
     * [active property][active] is represented by the position of the [Switch].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectStateSetEvent(handler: CPointer<StateSetHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkSwitchPtr, signal = SwitchEvent.stateSet, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkSwitchPtr, handlerId)
    }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkSwitch>?): Switch = Switch(ptr)

        public actual fun create(): Switch = Switch()
    }
}

public fun switchWidget(ptr: CPointer<GtkSwitch>? = null, init: Switch.() -> Unit = {}): Switch {
    val switch = if (ptr != null) Switch.fromPointer(ptr) else Switch.create()
    switch.init()
    return switch
}

/**
 * The event handler for the *state-set* event. Arguments:
 * 1. switch: CPointer<GtkSwitch>
 * 2. state: Int (represents a Boolean)
 * 3. userData: gpointer
 *
 * Return *true* (an Int that represents a Boolean) to stop the event emission.
 */
public typealias StateSetHandler = CFunction<(switch: CPointer<GtkSwitch>, state: Boolean, userData: gpointer) -> Int>

public fun CPointer<GtkSwitch>?.toSwitch(): Switch = Switch.fromPointer(this)
