package io.gitlab.guiVista.gui.widget.display

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual class Spinner private constructor(ptr: CPointer<GtkSpinner>? = null) : WidgetBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_spinner_new()
    public val gtkSpinnerPtr: CPointer<GtkSpinner>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual fun start() {
        gtk_spinner_start(gtkSpinnerPtr)
    }

    public actual fun stop() {
        gtk_spinner_stop(gtkSpinnerPtr)
    }

    public actual companion object {
        public actual fun create(): Spinner = Spinner()

        public fun fromPointer(ptr: CPointer<GtkSpinner>?): Spinner = Spinner(ptr)
    }
}

public fun spinnerWidget(ptr: CPointer<GtkSpinner>? = null, init: Spinner.() -> Unit = {}): Spinner {
    val spinner = if (ptr != null) Spinner.fromPointer(ptr) else Spinner.create()
    spinner.init()
    return spinner
}

public fun CPointer<GtkSpinner>?.toSpinner(): Spinner = Spinner.fromPointer(this)
