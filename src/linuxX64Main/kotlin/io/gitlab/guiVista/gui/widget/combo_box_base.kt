package io.gitlab.guiVista.gui.widget

import glib2.FALSE
import glib2.GDestroyNotify
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.cell.CellEditableBase
import io.gitlab.guiVista.gui.cell.CellLayout
import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.tree.TreeModel
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreeModelIterator
import kotlinx.cinterop.*

public actual interface ComboBoxBase : BinBase, CellEditableBase, CellLayout {
    public val gtkComboBoxPtr: CPointer<GtkComboBox>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var wrapWidth: Int
        get() = gtk_combo_box_get_wrap_width(gtkComboBoxPtr)
        set(value) = gtk_combo_box_set_wrap_width(gtkComboBoxPtr, value)
    public actual var rowSpanColumn: Int
        get() = gtk_combo_box_get_row_span_column(gtkComboBoxPtr)
        set(value) = gtk_combo_box_set_row_span_column(gtkComboBoxPtr, value)
    public actual var columnSpanColumn: Int
        get() = gtk_combo_box_get_column_span_column(gtkComboBoxPtr)
        set(value) = gtk_combo_box_set_column_span_column(gtkComboBoxPtr, value)
    public actual var active: Int
        get() = gtk_combo_box_get_active(gtkComboBoxPtr)
        set(value) = gtk_combo_box_set_active(gtkComboBoxPtr, value)
    public actual var idColumn: Int
        get() = gtk_combo_box_get_id_column(gtkComboBoxPtr)
        set(value) = gtk_combo_box_set_id_column(gtkComboBoxPtr, value)
    public actual var activeId: String?
        get() = gtk_combo_box_get_active_id(gtkComboBoxPtr)?.toKString()
        set(value) {
            gtk_combo_box_set_active_id(gtkComboBoxPtr, value)
        }
    public actual var model: TreeModelBase
        get() = TreeModel.fromPointer(gtk_combo_box_get_model(gtkComboBoxPtr))
        set(value) = gtk_combo_box_set_model(gtkComboBoxPtr, value.gtkTreeModelPtr)
    public actual val hasEntry: Boolean
        get() = gtk_combo_box_get_has_entry(gtkComboBoxPtr) == TRUE
    public actual var entryTextColumn: Int
        get() = gtk_combo_box_get_entry_text_column(gtkComboBoxPtr)
        set(value) = gtk_combo_box_set_entry_text_column(gtkComboBoxPtr, value)
    public actual var popupFixedWidth: Boolean
        get() = gtk_combo_box_get_popup_fixed_width(gtkComboBoxPtr) == TRUE
        set(value) = gtk_combo_box_set_popup_fixed_width(gtkComboBoxPtr, if (value) TRUE else FALSE)
    public actual var activeIterator: TreeModelIterator?
        get() = memScoped {
            val iter = alloc<GtkTreeIter>()
            val active = gtk_combo_box_get_active_iter(gtkComboBoxPtr, iter.ptr) == TRUE
            return if (active) TreeModelIterator.fromPointer(iter.ptr) else null
        }
        set(value) = gtk_combo_box_set_active_iter(gtkComboBoxPtr, value?.gtkTreeIterPtr)

    /**
     * Whether the dropdown button of the combo box should be always sensitive (`GTK_SENSITIVITY_ON`), never sensitive
     * (`GTK_SENSITIVITY_OFF`), or only if there is at least one item to display (`GTK_SENSITIVITY_AUTO`).
     *
     * Data binding property name: **button-sensitivity**
     */
    public var buttonSensitivity: GtkSensitivityType
        get() = gtk_combo_box_get_button_sensitivity(gtkComboBoxPtr)
        set(value) = gtk_combo_box_set_button_sensitivity(gtkComboBoxPtr, value)

    public fun fetchRowSeparatorFunc(): GtkTreeViewRowSeparatorFunc? =
        gtk_combo_box_get_row_separator_func(gtkComboBoxPtr)

    public fun changeRowSeparatorFunc(
        rowSeparatorFunc: GtkTreeViewRowSeparatorFunc?,
        data: gpointer = fetchEmptyDataPointer(),
        destroyFunc: GDestroyNotify? = null
    ) {
        gtk_combo_box_set_row_separator_func(combo_box = gtkComboBoxPtr, data = data, func = rowSeparatorFunc,
            destroy = destroyFunc)
    }

    /**
     * Connects the *changed* event to a [handler] on a combo box. This event is used when the active item is changed.
     * This can be due to the user selecting a different item from the list, or due to using the [activeIterator]
     * property. It will also be fired while typing into the entry of a combo box with an entry.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectChangedEvent(handler: CPointer<ChangedHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkComboBoxPtr, signal = ComboBoxBaseEvent.changed, slot = handler, data = userData)

    /**
     * Connects the *format-entry-text* event to a [handler] on a combo box. This event is for combo boxes that are
     * created with an entry, and the event allows you to change how the text displayed in a combo box's entry is
     * displayed. Connect an event handler which returns an allocated String representing the path. That string will
     * then be used to set the text in the combo box's entry. The default event handler uses the text from the
     * [entryTextColumn] model column.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @see hasEntry
     */
    public fun connectFormatEntryTextEvent(
        handler: CPointer<FormatEntryTextHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkComboBoxPtr, signal = ComboBoxBaseEvent.formatEntryText, slot = handler,
            data = userData)

    /**
     * Connects the *move-active* event to a [handler] on a combo box. This event is a keybinding event which gets
     * fired to move the active selection.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectMoveActiveEvent(handler: CPointer<MoveActiveHandler>,
                                      userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkComboBoxPtr, signal = ComboBoxBaseEvent.moveActive, slot = handler, data = userData)

    /**
     * Connects the *popdown* event to a [handler] on a combo box. This event is a keybinding event which gets
     * fired to popdown the combo box list. The default bindings for this event are **Alt+Up**, and **Escape**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPopdownEvent(handler: CPointer<PopdownHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkComboBoxPtr, signal = ComboBoxBaseEvent.popdown, slot = handler, data = userData)

    /**
     * Connects the *popup* event to a [handler] on a combo box. This event is a keybinding event which gets
     * fired to popup the combo box list. The default binding for this event is **Alt+Down**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPopupEvent(handler: CPointer<PopupHandler>,
                                 userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkComboBoxPtr, signal = ComboBoxBaseEvent.popup, slot = handler, data = userData)

    actual override fun disconnectEvent(handlerId: ULong) {
        super<BinBase>.disconnectEvent(handlerId)
        disconnectGSignal(gtkComboBoxPtr, handlerId)
    }

    public fun disconnectCellEditableEvent(handlerId: ULong) {
        super<CellEditableBase>.disconnectEvent(handlerId)
        disconnectGSignal(gtkCellEditablePtr, handlerId)
    }
}

/**
 * The event handler for the *changed* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. userData: gpointer
 */
public typealias ChangedHandler = CFunction<(widget: CPointer<GtkWidget>, userData: gpointer) -> Unit>

/**
 * The event handler for the *format-entry-text* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. path: CPointer<ByteVar>
 * 3. userData: gpointer
 */
public typealias FormatEntryTextHandler = CFunction<(
    widget: CPointer<GtkWidget>,
    path: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *move-active* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. scrollType: GtkScrollType
 * 3. userData: gpointer
 */
public typealias MoveActiveHandler = CFunction<(
    widget: CPointer<GtkWidget>,
    scrollType: GtkScrollType,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *popdown* event. Arguments:
 * 1. comboBox: CPointer<GtkComboBox>
 * 2. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias PopdownHandler = CFunction<(comboBox: CPointer<GtkComboBox>, userData: gpointer) -> Int>

/**
 * The event handler for the *popup* event. Arguments:
 * 1. comboBox: CPointer<GtkComboBox>
 * 2. userData: gpointer
 */
public typealias PopupHandler = CFunction<(comboBox: CPointer<GtkComboBox>, userData: gpointer) -> Unit>
