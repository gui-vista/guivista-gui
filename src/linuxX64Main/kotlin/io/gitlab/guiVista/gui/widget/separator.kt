package io.gitlab.guiVista.gui.widget

import gtk3.GtkOrientation
import gtk3.GtkSeparator
import gtk3.GtkWidget
import gtk3.gtk_separator_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Separator private constructor(
    ptr: CPointer<GtkSeparator>? = null,
    orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL
) : SeparatorBase {
    override val gtkSeparatorPtr: CPointer<GtkSeparator>? = ptr ?: gtk_separator_new(orientation)?.reinterpret()
    override val gtkWidgetPtr: CPointer<GtkWidget>?
        get() = gtkSeparatorPtr?.reinterpret()

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkSeparator>?): Separator = Separator(ptr = ptr)

        public fun create(orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL): Separator =
            Separator(orientation = orientation)
    }
}

public fun separatorWidget(
    ptr: CPointer<GtkSeparator>? = null,
    orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL,
    init: Separator.() -> Unit = {}
): Separator {
    val result = if (ptr != null) Separator.fromPointer(ptr) else Separator.create(orientation)
    result.init()
    return result
}

public fun CPointer<GtkSeparator>?.toSeparator(): Separator = Separator.fromPointer(this)
