package io.gitlab.guiVista.gui.widget.menu

import gtk3.GtkMenu
import gtk3.GtkWidget
import gtk3.gtk_menu_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Menu private constructor(
    menuPtr: CPointer<GtkMenu>? = null,
    widgetPtr: CPointer<GtkWidget>? = null
) : MenuBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? =
        if (menuPtr != null && widgetPtr == null) menuPtr.reinterpret()
        else if (widgetPtr != null && menuPtr == null) widgetPtr
        else gtk_menu_new()

    public actual companion object {
        public fun fromMenuPointer(ptr: CPointer<GtkMenu>?): Menu = Menu(menuPtr = ptr)

        public fun fromWidgetPointer(ptr: CPointer<GtkWidget>?): Menu = Menu(widgetPtr = ptr)

        public actual fun create(): Menu = Menu()
    }
}

public fun menuWidget(
    menuPtr: CPointer<GtkMenu>? = null,
    widgetPtr: CPointer<GtkWidget>? = null,
    init: Menu.() -> Unit
): Menu {
    val menu =
        if (menuPtr != null && widgetPtr == null) Menu.fromMenuPointer(menuPtr)
        else if (widgetPtr != null && menuPtr == null) Menu.fromWidgetPointer(widgetPtr)
        else Menu.create()
    menu.init()
    return menu
}

public fun CPointer<GtkMenu>?.toMenu(): Menu = Menu.fromMenuPointer(this)
