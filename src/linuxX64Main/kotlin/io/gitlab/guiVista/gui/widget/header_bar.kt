package io.gitlab.guiVista.gui.widget

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import io.gitlab.guiVista.gui.layout.ContainerBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString

public actual class HeaderBar private constructor(ptr: CPointer<GtkHeaderBar>? = null) : ContainerBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_header_bar_new()
    public val gtkHeaderBarPtr: CPointer<GtkHeaderBar>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var title: String
        get() = gtk_header_bar_get_title(gtkHeaderBarPtr)?.toKString() ?: ""
        set(value) = gtk_header_bar_set_title(gtkHeaderBarPtr, value)
    public actual var subtitle: String
        get() = gtk_header_bar_get_subtitle(gtkHeaderBarPtr)?.toKString() ?: ""
        set(value) = gtk_header_bar_set_subtitle(gtkHeaderBarPtr, value)
    public actual var customTitle: WidgetBase?
        get() {
            val ptr = gtk_header_bar_get_custom_title(gtkHeaderBarPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) =
            if (value != null) gtk_header_bar_set_custom_title(gtkHeaderBarPtr, value.gtkWidgetPtr)
            else gtk_header_bar_set_custom_title(gtkHeaderBarPtr, null)
    public actual var declarationLayout: String
        get() = gtk_header_bar_get_decoration_layout(gtkHeaderBarPtr)?.toKString() ?: ""
        set(value) = gtk_header_bar_set_decoration_layout(gtkHeaderBarPtr, value.ifEmpty { null })
    public actual var hasSubtitle: Boolean
        get() = gtk_header_bar_get_has_subtitle(gtkHeaderBarPtr) == TRUE
        set(value) = gtk_header_bar_set_has_subtitle(gtkHeaderBarPtr, if (value) TRUE else FALSE)
    public actual var showCloseButton: Boolean
        get() = gtk_header_bar_get_show_close_button(gtkHeaderBarPtr) == TRUE
        set(value) = gtk_header_bar_set_show_close_button(gtkHeaderBarPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkHeaderBar>?): HeaderBar = HeaderBar(ptr)

        public actual fun create(): HeaderBar = HeaderBar()
    }

    public actual fun prependChild(child: WidgetBase) {
        gtk_header_bar_pack_end(gtkHeaderBarPtr, child.gtkWidgetPtr)
    }

    public actual fun appendChild(child: WidgetBase) {
        gtk_header_bar_pack_start(gtkHeaderBarPtr, child.gtkWidgetPtr)
    }
}

public fun headerBarWidget(headerBarPtr: CPointer<GtkHeaderBar>? = null, init: HeaderBar.() -> Unit = {}): HeaderBar {
    val headerBar = if (headerBarPtr != null) HeaderBar.fromPointer(headerBarPtr) else HeaderBar.create()
    headerBar.init()
    return headerBar
}

public fun CPointer<GtkHeaderBar>?.toHeaderBar(): HeaderBar = HeaderBar.fromPointer(this)
