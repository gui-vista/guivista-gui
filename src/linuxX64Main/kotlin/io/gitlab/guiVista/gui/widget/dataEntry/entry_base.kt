package io.gitlab.guiVista.gui.widget.dataEntry

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.cell.CellEditableBase
import io.gitlab.guiVista.gui.text.Editable
import io.gitlab.guiVista.gui.text.EntryBuffer
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual interface EntryBase : Editable, WidgetBase, CellEditableBase {
    override val gtkEditablePtr: CPointer<GtkEditable>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkEntryPtr: CPointer<GtkEntry>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var buffer: EntryBuffer
        get() = EntryBuffer.fromPointer(gtk_entry_get_buffer(gtkEntryPtr))
        set(value) = gtk_entry_set_buffer(gtkEntryPtr, value.gtkEntryBufferPtr)
    public actual var activatesDefault: Boolean
        get() = gtk_entry_get_activates_default(gtkEntryPtr) == TRUE
        set(value) = gtk_entry_set_activates_default(gtkEntryPtr, if (value) TRUE else FALSE)
    public actual var hasFrame: Boolean
        get() = gtk_entry_get_has_frame(gtkEntryPtr) == TRUE
        set(value) = gtk_entry_set_has_frame(gtkEntryPtr, if (value) TRUE else FALSE)
    public actual var inputHints: UInt
        get() = gtk_entry_get_input_hints(gtkEntryPtr)
        set(value) = gtk_entry_set_input_hints(gtkEntryPtr, value)

    /**
     * The purpose of this text field. This property can be used by on-screen keyboards, and other input methods to
     * adjust their behaviour. Note that setting the purpose to `GTK_INPUT_PURPOSE_PASSWORD` or `GTK_INPUT_PURPOSE_PIN`
     * is independent from setting `visibility`. Default value is [GtkInputPurpose.GTK_INPUT_PURPOSE_FREE_FORM].
     *
     * Data binding property name: **input-purpose**
     */
    public var inputPurpose: GtkInputPurpose
        get() = gtk_entry_get_input_purpose(gtkEntryPtr)
        set(value) = gtk_entry_set_input_purpose(gtkEntryPtr, value)
    public actual var invisibleChar: Char
        get() = gtk_entry_get_invisible_char(gtkEntryPtr).toInt().toChar()
        set(value) = gtk_entry_set_invisible_char(gtkEntryPtr, value.toInt().toUInt())
    public actual var overwriteMode: Boolean
        get() = gtk_entry_get_overwrite_mode(gtkEntryPtr) == TRUE
        set(value) = gtk_entry_set_overwrite_mode(gtkEntryPtr, if (value) TRUE else FALSE)
    public actual var progressFraction: Double
        get() = gtk_entry_get_progress_fraction(gtkEntryPtr)
        set(value) {
            if (value !in (0.0..1.0)) {
                throw IllegalArgumentException("The value for progressFraction must be between 0.0 to 1.0")
            }
            gtk_entry_set_progress_fraction(gtkEntryPtr, value)
        }
    public actual var progressPulseStep: Double
        get() = gtk_entry_get_progress_pulse_step(gtkEntryPtr)
        set(value) {
            if (value !in (0.0..1.0)) {
                throw IllegalArgumentException("The value for progressPulseStep must be between 0.0 to 1.0")
            }
            gtk_entry_set_progress_pulse_step(gtkEntryPtr, value)
        }
    public actual val textLength: UShort
        get() = gtk_entry_get_text_length(gtkEntryPtr)
    public actual var visibility: Boolean
        get() = gtk_entry_get_visibility(gtkEntryPtr) == TRUE
        set(value) = gtk_entry_set_visibility(gtkEntryPtr, if (value) TRUE else FALSE)
    public actual var alignment: Float
        get() = gtk_entry_get_alignment(gtkEntryPtr)
        set(value) = gtk_entry_set_alignment(gtkEntryPtr, value)
    public actual var text: String
        set(value) = gtk_entry_set_text(gtkEntryPtr, value)
        get() = gtk_entry_get_text(gtkEntryPtr)?.toKString() ?: ""
    public actual var placeholderText: String
        set(value) = gtk_entry_set_placeholder_text(gtkEntryPtr, value)
        get() = gtk_entry_get_placeholder_text(gtkEntryPtr)?.toKString() ?: ""
    public actual var maxLength: Int
        set(value) = gtk_entry_set_max_length(gtkEntryPtr, value)
        get() = gtk_entry_get_max_length(gtkEntryPtr)
    public actual var maxWidthChars: Int
        set(value) {
            if (value >= -1) gtk_entry_set_max_width_chars(gtkEntryPtr, value)
        }
        get() = gtk_entry_get_max_width_chars(gtkEntryPtr)
    public actual var widthChars: Int
        set(value) {
            if (value >= -1) gtk_entry_set_width_chars(gtkEntryPtr, value)
        }
        get() = gtk_entry_get_width_chars(gtkEntryPtr)

    public actual fun progressPulse() {
        gtk_entry_progress_pulse(gtkEntryPtr)
    }

    public actual fun unsetInvisibleChar() {
        gtk_entry_unset_invisible_char(gtkEntryPtr)
    }

    /**
     * Sets whether the icon is activatable.
     * @param iconPos Icon position.
     * @param activatable If *true* then the icon should be activatable.
     */
    public fun changeIconActivatable(iconPos: GtkEntryIconPosition, activatable: Boolean) {
        gtk_entry_set_icon_activatable(entry = gtkEntryPtr, icon_pos = iconPos,
            activatable = if (activatable) TRUE else FALSE)
    }

    /**
     * Returns whether the icon is activatable.
     * @return A value of *true* if the icon is activatable.
     */
    public fun fetchIconActivatable(iconPos: GtkEntryIconPosition): Boolean =
        gtk_entry_get_icon_activatable(gtkEntryPtr, iconPos) == TRUE

    public actual fun grabFocusWithoutSelecting() {
        gtk_entry_grab_focus_without_selecting(gtkEntryPtr)
    }

    actual override fun disconnectEvent(handlerId: ULong) {
        super<WidgetBase>.disconnectEvent(handlerId)
        disconnectGSignal(gtkEntryPtr, handlerId)
    }

    public fun disconnectEditableEvent(handlerId: ULong) {
        super<Editable>.disconnectEvent(handlerId)
        disconnectGSignal(gtkEditablePtr, handlerId)
    }

    /**
     * Connects the *activate* event to a [handler] on a [Entry]. This event is used when the user hits the **Enter**
     * key, and the event is commonly used by applications to intercept activation of entries. The default bindings for
     * this event are all forms of the **Enter** key.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID for the [handler].
     */
    public fun connectActivateEvent(handler: CPointer<ActivateHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkEntryPtr, signal = EntryBaseEvent.activate, slot = handler, data = userData)

    /**
     * Connects the *backspace* event to a [handler] on a [Entry]. This event is used when when the user asks for it.
     * The default bindings for this event are **Backspace** and **Shift-Backspace**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID for the [handler].
     */
    public fun connectBackspaceEvent(handler: CPointer<BackspaceHandler>,
                                     userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkEntryPtr, signal = EntryBaseEvent.backspace, slot = handler, data = userData)

    /**
     * Connects the *copy-clipboard* event to a [handler] on a [Entry]. This event is used when something is copied to
     * the clipboard. The default bindings for this event are **Ctrl-c** and **Ctrl-Insert**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID for the [handler].
     */
    public fun connectCopyClipboardEvent(handler: CPointer<CopyClipboardHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkEntryPtr, signal = EntryBaseEvent.copyClipboard, slot = handler, data = userData)

    /**
     * Connects the *cut-clipboard* event to a [handler] on a [Entry]. This event is used when something is cut from
     * the clipboard. The default bindings for this event are **Ctrl-x** and **Shift-Delete**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID for the [handler].
     */
    public fun connectCutClipboardEvent(handler: CPointer<CutClipboardHandler>,
                                        userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkEntryPtr, signal = EntryBaseEvent.cutClipboard, slot = handler, data = userData)

    /**
     * Connects the *paste-clipboard* event to a [handler] on a [Entry]. This event is used when pasting the contents of
     * the clipboard into the text view. The default bindings for this event are **Ctrl-v** and **Shift-Insert**.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     * @return The handler ID for the [handler].
     */
    public fun connectPasteClipboardEvent(handler: CPointer<PasteClipboardHandler>,
                                          userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkEntryPtr, signal = EntryBaseEvent.pasteClipboard, slot = handler, data = userData)
}

/**
 * The event handler for the *activate* event. Arguments:
 * 1. entry: CPointer<GtkEntry>
 * 2. userData: gpointer
 */
public typealias ActivateHandler = CFunction<(entry: CPointer<GtkEntry>, userData: gpointer) -> Unit>

/**
 * The event handler for the *backspace* event. Arguments:
 * 1. entry: CPointer<GtkEntry>
 * 2. userData: gpointer
 */
public typealias BackspaceHandler = CFunction<(entry: CPointer<GtkEntry>, userData: gpointer) -> Unit>

/**
 * The event handler for the *copy-clipboard* event. Arguments:
 * 1. entry: CPointer<GtkEntry>
 * 2. userData: gpointer
 */
public typealias CopyClipboardHandler = CFunction<(entry: CPointer<GtkEntry>, userData: gpointer) -> Unit>

/**
 * The event handler for the *cut-clipboard* event. Arguments:
 * 1. entry: CPointer<GtkEntry>
 * 2. userData: gpointer
 */
public typealias CutClipboardHandler = CFunction<(entry: CPointer<GtkEntry>, userData: gpointer) -> Unit>

/**
 * The event handler for the *paste-clipboard* event. Arguments:
 * 1. entry: CPointer<GtkEntry>
 * 2. userData: gpointer
 */
public typealias PasteClipboardHandler = CFunction<(entry: CPointer<GtkEntry>, userData: gpointer) -> Unit>
