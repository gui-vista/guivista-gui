package io.gitlab.guiVista.gui.widget

import gtk3.*
import io.gitlab.guiVista.gui.cell.CellAreaBase
import io.gitlab.guiVista.gui.tree.TreeModelBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class ComboBox private constructor(
    ptr: CPointer<GtkComboBox>? = null,
    withEntry: Boolean = false,
    model: TreeModelBase? = null,
    area: CellAreaBase? = null
) : ComboBoxBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = when {
        ptr != null -> ptr.reinterpret()
        area != null && !withEntry -> gtk_combo_box_new_with_area(area.gtkCellAreaPtr)
        area != null && withEntry -> gtk_combo_box_new_with_area_and_entry(area.gtkCellAreaPtr)
        model != null && withEntry -> gtk_combo_box_new_with_model_and_entry(model.gtkTreeModelPtr)
        model != null && !withEntry -> gtk_combo_box_new_with_model(model.gtkTreeModelPtr)
        withEntry && model == null -> gtk_combo_box_new_with_entry()
        else -> gtk_combo_box_new()
    }
    override val gtkCellEditablePtr: CPointer<GtkCellEditable>?
        get() = gtkWidgetPtr?.reinterpret()
    override val gtkCellLayoutPtr: CPointer<GtkCellLayout>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkComboBox>?): ComboBox = ComboBox(ptr = ptr)

        public actual fun create(): ComboBox = ComboBox()

        public actual fun createWithEntry(): ComboBox = ComboBox(withEntry = true)

        public actual fun createWithModel(model: TreeModelBase): ComboBox =
            ComboBox(model = model)

        public actual fun createWithModelAndEntry(model: TreeModelBase): ComboBox =
            ComboBox(model = model, withEntry = true)

        public actual fun createWithArea(area: CellAreaBase): ComboBox = ComboBox(area = area)

        public actual fun createWithAreaAndEntry(area: CellAreaBase): ComboBox = ComboBox(area = area, withEntry = true)
    }
}

public fun comboBoxWidget(
    comboBoxPtr: CPointer<GtkComboBox>? = null,
    withEntry: Boolean = false,
    model: TreeModelBase? = null,
    area: CellAreaBase? = null,
    init: ComboBox.() -> Unit = {}
): ComboBox {
    val comboBox = when {
        comboBoxPtr != null -> ComboBox.fromPointer(comboBoxPtr)
        model != null && withEntry -> ComboBox.createWithModelAndEntry(model)
        model != null && !withEntry -> ComboBox.createWithModel(model)
        area != null && !withEntry -> ComboBox.createWithArea(area)
        area != null && withEntry -> ComboBox.createWithAreaAndEntry(area)
        withEntry && model == null -> ComboBox.createWithEntry()
        else -> ComboBox.create()
    }
    comboBox.init()
    return comboBox
}

public fun CPointer<GtkComboBox>?.toComboBox(): ComboBox = ComboBox.fromPointer(this)
