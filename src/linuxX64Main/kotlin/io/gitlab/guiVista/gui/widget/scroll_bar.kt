package io.gitlab.guiVista.gui.widget

import gtk3.GtkOrientation
import gtk3.GtkScrollbar
import gtk3.GtkWidget
import gtk3.gtk_scrollbar_new
import io.gitlab.guiVista.gui.Adjustment
import io.gitlab.guiVista.gui.layout.Orientable
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class ScrollBar private constructor(
    ptr: CPointer<GtkScrollbar>? = null,
    orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_VERTICAL,
    adjustment: Adjustment? = null
) : Range, Orientable {
    public val gtkScrollbarPtr: CPointer<GtkScrollbar>? = ptr
        ?: gtk_scrollbar_new(orientation, adjustment?.gtkAdjustmentPtr)?.reinterpret()
    override val gtkWidgetPtr: CPointer<GtkWidget>?
        get() = gtkScrollbarPtr?.reinterpret()

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkScrollbar>?): ScrollBar = ScrollBar(ptr = ptr)

        public fun create(
            orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_VERTICAL,
            adjustment: Adjustment? = null
        ): ScrollBar = ScrollBar(orientation = orientation, adjustment = adjustment)
    }
}

public fun scrollBar(
    ptr: CPointer<GtkScrollbar>? = null,
    orientation: GtkOrientation = GtkOrientation.GTK_ORIENTATION_VERTICAL,
    adjustment: Adjustment? = null,
    init: ScrollBar.() -> Unit
): ScrollBar {
    val result = if (ptr != null) ScrollBar.fromPointer(ptr) else ScrollBar.create(orientation, adjustment)
    result.init()
    return result
}

public fun CPointer<GtkScrollbar>?.toScrollBar(): ScrollBar = ScrollBar.fromPointer(this)
