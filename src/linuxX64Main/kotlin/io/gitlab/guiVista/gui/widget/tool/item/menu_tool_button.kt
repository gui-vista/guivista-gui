package io.gitlab.guiVista.gui.widget.tool.item

import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual class MenuToolButton(
    ptr: CPointer<GtkMenuToolButton>? = null,
    iconWidget: WidgetBase? = null,
    label: String = ""
) : ToolButtonBase {
    override val gtkToolItemPtr: CPointer<GtkToolItem>? = ptr?.reinterpret()
        ?: gtk_menu_tool_button_new(iconWidget?.gtkWidgetPtr, label)
    public val gtkMenuToolButtonPtr: CPointer<GtkMenuToolButton>?
        get() = gtkToolItemPtr?.reinterpret()
    override val gtkToolButtonPtr: CPointer<GtkToolButton>?
        get() = gtkToolItemPtr?.reinterpret()
    public var menu: CPointer<GtkWidget>?
        get() = gtk_menu_tool_button_get_menu(gtkMenuToolButtonPtr)
        set(value) = gtk_menu_tool_button_set_menu(gtkMenuToolButtonPtr, value)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkMenuToolButton>?): MenuToolButton = MenuToolButton(ptr = ptr)

        public actual fun create(iconWidget: WidgetBase, label: String): MenuToolButton =
            MenuToolButton(iconWidget = iconWidget, label = label)
    }

    public actual infix fun changeArrowTooltipText(text: String) {
        gtk_menu_tool_button_set_arrow_tooltip_text(gtkMenuToolButtonPtr, text)
    }

    /**
     * Connects the *show-menu* event to a [handler] on a [MenuToolButton]. This event is used to populate the menu on
     * demand when [menu] is set. Note that even if you populate the menu dynamically in this way, you **must** set an
     * empty menu on the [MenuToolButton] beforehand, since the arrow is made insensitive if the menu is not set.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectShowMenuEvent(handler: CPointer<ShowMenuHandler>, userData: gpointer): ULong =
        connectGSignal(obj = gtkMenuToolButtonPtr, signal = MenuToolButtonEvent.showMenu, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkMenuToolButtonPtr, handlerId)
    }
}

public fun menuToolButtonWidget(
    ptr: CPointer<GtkMenuToolButton>? = null,
    iconWidget: WidgetBase? = null,
    label: String = "",
    init: MenuToolButton.() -> Unit = {}
): MenuToolButton {
    val menuToolButton =
        if (iconWidget != null && label.isNotEmpty()) MenuToolButton.create(iconWidget, label)
        else MenuToolButton.fromPointer(ptr)
    menuToolButton.init()
    return menuToolButton
}

/**
 * The event handler for the *show-menu* event. Arguments:
 * 1. button: CPointer<GtkMenuToolButton>
 * 2. userData: gpointer
 */
public typealias ShowMenuHandler = CFunction<(button: CPointer<GtkMenuToolButton>, userData: gpointer) -> Unit>

public fun CPointer<GtkMenuToolButton>?.toMenuToolButton(): MenuToolButton = MenuToolButton.fromPointer(this)
