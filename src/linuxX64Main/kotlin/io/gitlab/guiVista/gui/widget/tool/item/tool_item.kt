package io.gitlab.guiVista.gui.widget.tool.item

import gtk3.GtkToolItem
import gtk3.gtk_tool_item_new
import kotlinx.cinterop.CPointer

public actual class ToolItem private constructor(ptr: CPointer<GtkToolItem>? = null) : ToolItemBase {
    override val gtkToolItemPtr: CPointer<GtkToolItem>? = ptr ?: gtk_tool_item_new()

    public actual companion object {
        public actual fun create(): ToolItem = ToolItem()

        public fun fromPointer(ptr: CPointer<GtkToolItem>?): ToolItem = ToolItem(ptr)
    }
}

public fun toolItemWidget(ptr: CPointer<GtkToolItem>? = null, init: ToolItem.() -> Unit = {}): ToolItem {
    val toolItem = if (ptr != null) ToolItem.fromPointer(ptr) else ToolItem.create()
    toolItem.init()
    return toolItem
}

public fun CPointer<GtkToolItem>?.toToolItem(): ToolItem = ToolItem.fromPointer(this)
