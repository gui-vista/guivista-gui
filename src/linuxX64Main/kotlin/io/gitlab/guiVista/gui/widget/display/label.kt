package io.gitlab.guiVista.gui.widget.display

import gtk3.GtkLabel
import gtk3.GtkWidget
import gtk3.gtk_label_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Label private constructor(ptr: CPointer<GtkLabel>? = null, text: String = "") : LabelBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_label_new(text)

    public actual companion object {
        public actual fun create(text: String): Label = Label(text = text)

        public fun fromPointer(ptr: CPointer<GtkLabel>?): Label = Label(ptr = ptr)
    }
}

public fun labelWidget(ptr: CPointer<GtkLabel>? = null, text: String = "", init: Label.() -> Unit = {}): Label {
    val lbl = if (ptr != null) Label.fromPointer(ptr) else Label.create(text)
    lbl.init()
    return lbl
}

public fun CPointer<GtkLabel>?.toLabel(): Label = Label.fromPointer(this)
