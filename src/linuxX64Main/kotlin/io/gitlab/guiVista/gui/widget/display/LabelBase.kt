package io.gitlab.guiVista.gui.widget.display

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual interface LabelBase : WidgetBase {
    public val gtkLabelPtr: CPointer<GtkLabel>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var text: String
        set(value) = gtk_label_set_text(gtkLabelPtr, value)
        get() = gtk_label_get_text(gtkLabelPtr)?.toKString() ?: ""

    /**
     * The alignment of the lines in the text of the label relative to each other. This does NOT affect the alignment
     * of the label within its allocation. See `GtkLabel.xalign` for that. Default value is
     * *GtkJustification.GTK_JUSTIFY_LEFT*.
     *
     * Data binding property name: **justify**
     */
    public var justify: GtkJustification
        set(value) = gtk_label_set_justify(gtkLabelPtr, value)
        get() = gtk_label_get_justify(gtkLabelPtr)
    public actual var maxWidthChars: Int
        set(value) {
            if (value >= -1) gtk_label_set_max_width_chars(gtkLabelPtr, value)
        }
        get() = gtk_label_get_max_width_chars(gtkLabelPtr)
    public actual var widthChars: Int
        set(value) {
            if (value >= -1) gtk_label_set_width_chars(gtkLabelPtr, value)
        }
        get() = gtk_label_get_width_chars(gtkLabelPtr)
    public actual var angle: Double
        get() = gtk_label_get_angle(gtkLabelPtr)
        set(value) {
            if (value in (0.0..360.0)) gtk_label_set_angle(gtkLabelPtr, value)
        }

    /**
     * The preferred place to "ellipsize" the string, if the label does not have enough room to display the entire
     * string, specified as a PangoEllipsizeMode. Note that setting this property to a value other than
     * PANGO_ELLIPSIZE_NONE has the side-effect that the label requests only enough space to display the ellipsis
     * "...". In particular this means that "ellipsizing" labels do not work well in notebook tabs, unless the
     * GtkNotebook tab-expand child property is set to *true*. Other ways to set a label's width are
     * `gtk_widget_set_size_request()`, and [widthChars].
     *
     * Default value is *PangoEllipsizeMode.PANGO_ELLIPSIZE_NONE*. Data binding property name: **ellipsize**
     */
    public var ellipsize: PangoEllipsizeMode
        get() = gtk_label_get_ellipsize(gtkLabelPtr)
        set(value) = gtk_label_set_ellipsize(gtkLabelPtr, value)
    public actual var label: String
        get() = gtk_label_get_label(gtkLabelPtr)?.toKString() ?: ""
        set(value) = gtk_label_set_label(gtkLabelPtr, value)
    public actual var lines: Int
        get() = gtk_label_get_lines(gtkLabelPtr)
        set(value) {
            if (value >= -1) gtk_label_set_lines(gtkLabelPtr, value)
        }
    public actual val mnemonicKeyVal: UInt
        get() = gtk_label_get_mnemonic_keyval(gtkLabelPtr)

    public actual var mnemonicWidget: WidgetBase?
        get() {
            val tmp = gtk_label_get_mnemonic_widget(gtkLabelPtr)
            return if (tmp != null) Widget.fromPointer(tmp) else null
        }
        set(value) = gtk_label_set_mnemonic_widget(gtkLabelPtr, value?.gtkWidgetPtr)
    public actual var selectable: Boolean
        get() = gtk_label_get_selectable(gtkLabelPtr) == TRUE
        set(value) = gtk_label_set_selectable(gtkLabelPtr, if (value) TRUE else FALSE)
    public actual var singleLineMode: Boolean
        get() = gtk_label_get_single_line_mode(gtkLabelPtr) == TRUE
        set(value) = gtk_label_set_single_line_mode(gtkLabelPtr, if (value) TRUE else FALSE)
    public actual var trackVisitedLinks: Boolean
        get() = gtk_label_get_track_visited_links(gtkLabelPtr) == TRUE
        set(value) = gtk_label_set_track_visited_links(gtkLabelPtr, if (value) TRUE else FALSE)
    public actual var useMarkup: Boolean
        get() = gtk_label_get_use_markup(gtkLabelPtr) == TRUE
        set(value) = gtk_label_set_use_markup(gtkLabelPtr, if (value) TRUE else FALSE)
    public actual var useUnderline: Boolean
        get() = gtk_label_get_use_underline(gtkLabelPtr) == TRUE
        set(value) = gtk_label_set_use_underline(gtkLabelPtr, if (value) TRUE else FALSE)
    public actual var lineWrap: Boolean
        get() = gtk_label_get_line_wrap(gtkLabelPtr) == TRUE
        set(value) = gtk_label_set_line_wrap(gtkLabelPtr, if (value) TRUE else FALSE)

    /**
     * If line wrapping is on (see the [lineWrap] property) this controls how the line wrapping is done. Default value
     * is *PangoWrapMode.PANGO_WRAP_WORD* (wrap on word boundaries).
     */
    public var lineWrapMode: PangoWrapMode
        get() = gtk_label_get_line_wrap_mode(gtkLabelPtr)
        set(value) = gtk_label_set_line_wrap_mode(gtkLabelPtr, value)
    public actual var xAlign: Float
        get() = gtk_label_get_xalign(gtkLabelPtr)
        set(value) {
            if (value in (0.0f..1.0f)) gtk_label_set_xalign(gtkLabelPtr, value)
        }
    public actual var yAlign: Float
        get() = gtk_label_get_yalign(gtkLabelPtr)
        set(value) {
            if (value in (0.0f..1.0f)) gtk_label_set_yalign(gtkLabelPtr, value)
        }

    public actual fun changeMarkupWithMnemonic(str: String) {
        gtk_label_set_markup_with_mnemonic(gtkLabelPtr, str)
    }
}
