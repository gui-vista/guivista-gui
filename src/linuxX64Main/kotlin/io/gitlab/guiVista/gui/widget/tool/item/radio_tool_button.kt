package io.gitlab.guiVista.gui.widget.tool.item

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.dataType.SinglyLinkedList

public actual class RadioToolButton private constructor(
    ptr: CPointer<GtkRadioToolButton>? = null,
    group: RadioToolButton? = null
) : ToggleToolButtonBase {
    @Suppress("IfThenToElvis")
    override val gtkToolItemPtr: CPointer<GtkToolItem>? = when {
        ptr != null -> ptr.reinterpret()
        group != null -> gtk_radio_tool_button_new_from_widget(group.gtkRadioToolButtonPtr)
        else -> gtk_radio_tool_button_new(null)
    }
    override val gtkToolButtonPtr: CPointer<GtkToolButton>?
        get() = gtkToolItemPtr?.reinterpret()
    override val gtkToggleToolButtonPtr: CPointer<GtkToggleToolButton>?
        get() = gtkToolItemPtr?.reinterpret()
    public val gtkRadioToolButtonPtr: CPointer<GtkRadioToolButton>?
        get() = gtkToolItemPtr?.reinterpret()
    public actual var group: SinglyLinkedList?
        get() {
            val ptr = gtk_radio_tool_button_get_group(gtkRadioToolButtonPtr)
            return if (ptr != null) SinglyLinkedList(ptr) else null
        }
        set(value) = gtk_radio_tool_button_set_group(gtkRadioToolButtonPtr, value?.gSListPtr)

    public actual companion object {
        public actual fun create(): RadioToolButton = RadioToolButton()

        public actual fun fromWidget(group: RadioToolButton): RadioToolButton = RadioToolButton(group = group)

        public fun fromPointer(ptr: CPointer<GtkRadioToolButton>?): RadioToolButton = RadioToolButton(ptr = ptr)
    }
}

public fun radioToolButtonWidget(
    ptr: CPointer<GtkRadioToolButton>? = null,
    group: RadioToolButton? = null,
    init: RadioToolButton.() -> Unit = {}
): RadioToolButton {
    val button =
        if (ptr != null) RadioToolButton.fromPointer(ptr)
        else if (group != null) RadioToolButton.fromWidget(group)
        else RadioToolButton.create()
    button.init()
    return button
}

public fun CPointer<GtkRadioToolButton>?.toRadioToolButton(): RadioToolButton = RadioToolButton.fromPointer(this)
