package io.gitlab.guiVista.gui.widget

import gtk3.GtkColorChooser
import gtk3.GtkColorChooserWidget
import gtk3.GtkWidget
import gtk3.gtk_color_chooser_widget_new
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.gui.dialog.ColorChooser
import io.gitlab.guiVista.gui.layout.BoxBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class ColorChooserWidget private constructor(ptr: CPointer<GtkColorChooserWidget>? = null) :
    BoxBase, ColorChooser {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_color_chooser_widget_new()
    override val gtkColorChooserPtr: CPointer<GtkColorChooser>?
        get() = gtkWidgetPtr?.reinterpret()
    public val gtkColorChooserWidgetPtr: CPointer<GtkColorChooserWidget>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkColorChooserWidget>?): ColorChooserWidget = ColorChooserWidget(ptr)

        public actual fun create(): ColorChooserWidget = ColorChooserWidget()
    }

    actual override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gtkColorChooserWidgetPtr, handlerId)
    }

    actual override fun close() {
        // Do nothing.
    }
}

public fun colorChooserWidget(
    ptr: CPointer<GtkColorChooserWidget>? = null,
    init: ColorChooserWidget.() -> Unit = {}
): ColorChooserWidget {
    val result = if (ptr != null) ColorChooserWidget.fromPointer(ptr) else ColorChooserWidget.create()
    result.init()
    return result
}

public fun CPointer<GtkColorChooserWidget>?.toColorChooserWidget(): ColorChooserWidget =
    ColorChooserWidget.fromPointer(this)
