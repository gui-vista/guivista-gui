package io.gitlab.guiVista.gui.widget.tool.item

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class ToggleToolButton private constructor(
    ptr: CPointer<GtkToggleToolButton>? = null
) : ToggleToolButtonBase {
    override val gtkToolItemPtr: CPointer<GtkToolItem>? =
        ptr?.reinterpret() ?: gtk_toggle_tool_button_new()
    override val gtkToggleToolButtonPtr: CPointer<GtkToggleToolButton>?
        get() = gtkToolItemPtr?.reinterpret()
    override val gtkToolButtonPtr: CPointer<GtkToolButton>?
        get() = gtkToolItemPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkToggleToolButton>?): ToggleToolButton = ToggleToolButton(ptr)

        public actual fun create(): ToggleToolButton = ToggleToolButton()
    }
}

public fun toggleToolButtonWidget(
    ptr: CPointer<GtkToggleToolButton>? = null,
    init: ToggleToolButton.() -> Unit = {}
): ToggleToolButton {
    val button = if (ptr != null) ToggleToolButton.fromPointer(ptr) else ToggleToolButton.create()
    button.init()
    return button
}

public fun CPointer<GtkToggleToolButton>?.toToggleToolButton(): ToggleToolButton = ToggleToolButton.fromPointer(this)
