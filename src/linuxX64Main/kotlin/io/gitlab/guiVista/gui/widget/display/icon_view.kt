package io.gitlab.guiVista.gui.widget.display

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.cell.CellAreaBase
import io.gitlab.guiVista.gui.cell.renderer.CellRenderer
import io.gitlab.guiVista.gui.cell.renderer.CellRendererBase
import io.gitlab.guiVista.gui.dragDrop.DragDropTargetEntry
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.tree.TreeModel
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreePath
import kotlinx.cinterop.*

public actual class IconView private constructor(ptr: CPointer<GtkIconView>?) : ContainerBase {
    public val gtkIconViewPtr: CPointer<GtkIconView>? = ptr
    override val gtkWidgetPtr: CPointer<GtkWidget>?
        get() = gtkIconViewPtr?.reinterpret()

    public actual val selectedItems: DoublyLinkedList
        get() = DoublyLinkedList.fromPointer(gtk_icon_view_get_selected_items(gtkIconViewPtr))

    /**
     * The selection mode of icon view. If the mode is `GTK_SELECTION_MULTIPLE` then **rubberband** selection is
     * enabled. For the other modes only keyboard selection is possible.
     */
    public var selectionMode: GtkSelectionMode
        get() = gtk_icon_view_get_selection_mode(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_selection_mode(gtkIconViewPtr, value)

    /** How the cells (i.e. the icon and the text) of the item are positioned relative to each other. */
    public var itemOrientation: GtkOrientation
        get() = gtk_icon_view_get_item_orientation(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_item_orientation(gtkIconViewPtr, value)
    public actual var activateOnSingleClick: Boolean
        get() = gtk_icon_view_get_activate_on_single_click(gtkIconViewPtr) == TRUE
        set(value) = gtk_icon_view_set_activate_on_single_click(gtkIconViewPtr, if (value) TRUE else FALSE)
    public actual var columnSpacing: Int
        get() = gtk_icon_view_get_column_spacing(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_column_spacing(gtkIconViewPtr, value)
    public actual var columns: Int
        get() = gtk_icon_view_get_columns(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_columns(gtkIconViewPtr, value)
    public actual var itemPadding: Int
        get() = gtk_icon_view_get_item_padding(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_item_padding(gtkIconViewPtr, value)
    public actual var itemWidth: Int
        get() = gtk_icon_view_get_item_width(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_item_width(gtkIconViewPtr, value)
    public actual var margin: Int
        get() = gtk_icon_view_get_margin(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_margin(gtkIconViewPtr, value)
    public actual var markupColumn: Int
        get() = gtk_icon_view_get_markup_column(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_markup_column(gtkIconViewPtr, value)
    public actual var model: TreeModelBase
        get() = TreeModel.fromPointer(gtk_icon_view_get_model(gtkIconViewPtr))
        set(value) = gtk_icon_view_set_model(gtkIconViewPtr, value.gtkTreeModelPtr)
    public actual var imageBufferColumn: Int
        get() = gtk_icon_view_get_pixbuf_column(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_pixbuf_column(gtkIconViewPtr, value)
    public actual var reorderable: Boolean
        get() = gtk_icon_view_get_reorderable(gtkIconViewPtr) == TRUE
        set(value) = gtk_icon_view_set_reorderable(gtkIconViewPtr, if (value) TRUE else FALSE)
    public actual var rowSpacing: Int
        get() = gtk_icon_view_get_row_spacing(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_row_spacing(gtkIconViewPtr, value)
    public actual var spacing: Int
        get() = gtk_icon_view_get_spacing(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_spacing(gtkIconViewPtr, value)
    public actual var textColumn: Int
        get() = gtk_icon_view_get_text_column(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_text_column(gtkIconViewPtr, value)
    public actual var tooltipColumn: Int
        get() = gtk_icon_view_get_tooltip_column(gtkIconViewPtr)
        set(value) = gtk_icon_view_set_tooltip_column(gtkIconViewPtr, value)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkIconView>?): IconView = IconView(ptr)

        public actual fun create(): IconView = IconView(gtk_icon_view_new()?.reinterpret())

        public actual fun createWithArea(area: CellAreaBase): IconView =
            IconView(gtk_icon_view_new_with_area(area.gtkCellAreaPtr)?.reinterpret())

        public actual fun createWithModel(model: TreeModelBase): IconView =
            IconView(gtk_icon_view_new_with_model(model.gtkTreeModelPtr)?.reinterpret())
    }

    public actual fun convertWidgetToBinWindowCoordinates(
        widgetXPos: Int,
        widgetYPos: Int
    ): Pair<Int, Int> = memScoped {
        val winXPos = alloc<IntVar>()
        val winYPos = alloc<IntVar>()
        gtk_icon_view_convert_widget_to_bin_window_coords(
            icon_view = gtkIconViewPtr,
            wx = widgetXPos,
            wy = widgetYPos,
            bx = winXPos.ptr,
            by = winYPos.ptr
        )
        return winXPos.value to winYPos.value
    }

    public actual fun enableModelDropDestination(
        actions: UInt,
        vararg targets: DragDropTargetEntry
    ): Unit = memScoped {
        val totalTargets = targets.size
        val tmpArray = allocArray<GtkTargetEntry>(totalTargets)
        targets.forEachIndexed { pos, item ->
            tmpArray[pos].target = item.target.cstr.ptr
            tmpArray[pos].info = item.info
            tmpArray[pos].flags = item.flags
        }
        gtk_icon_view_enable_model_drag_dest(icon_view = gtkIconViewPtr, actions = actions, targets = tmpArray,
            n_targets = totalTargets)
    }

    public actual fun enableModelDragSource(
        startBtnMask: UInt,
        actions: UInt,
        vararg targets: DragDropTargetEntry
    ): Unit = memScoped {
        val totalTargets = targets.size
        val tmpArray = allocArray<GtkTargetEntry>(totalTargets)
        targets.forEachIndexed { pos, item ->
            tmpArray[pos].target = item.target.cstr.ptr
            tmpArray[pos].info = item.info
            tmpArray[pos].flags = item.flags
        }
        gtk_icon_view_enable_model_drag_source(
            start_button_mask = startBtnMask,
            icon_view = gtkIconViewPtr,
            actions = actions,
            targets = tmpArray,
            n_targets = totalTargets
        )
    }

    /**
     * Creates a Cairo Surface representation of the item at [path]. This image is used for a drag icon.
     * @param path A tree path in the icon view.
     * @return A new Cairo Surface of the drag icon.
     */
    public fun createDragIcon(path: TreePath): CPointer<cairo_surface_t>? =
        gtk_icon_view_create_drag_icon(gtkIconViewPtr, path.gtkTreePathPtr)

    /**
     * Determines the destination item for a given position.
     * @param xPos The X coordinate position to determine the destination item for.
     * @param yPos The Y coordinate position to determine the destination item for.
     * @return A Triple containing the following:
     * 1. itemExists - Will be *true* if there is an item at the given position.
     * 2. path - The path of the item or *null*.
     * 3. pos - The drop position.
     */
    public fun fetchDestinationItemAtPos(
        xPos: Int,
        yPos: Int
    ): Triple<Boolean, TreePath?, GtkIconViewDropPosition> = memScoped {
        val tmpPath = alloc<CPointerVar<GtkTreePath>>()
        val tmpPos = alloc<GtkIconViewDropPosition.Var>()
        val itemExists = gtk_icon_view_get_dest_item_at_pos(
            icon_view = gtkIconViewPtr,
            drag_x = xPos,
            drag_y = yPos,
            path = tmpPath.ptr,
            pos = tmpPos.ptr
        ) == TRUE
        val path = if (tmpPath.value != null) TreePath.fromPointer(tmpPath.value) else null
        Triple(itemExists, path, tmpPos.value)
    }

    /**
     * Gets information about the item that is highlighted for feedback.
     * @return A Pair containing the following:
     * 1. path - The path of the highlighted item or *null*.
     * 2. pos - The drop position.
     */
    public fun fetchDropDestinationItem(): Pair<TreePath?, GtkIconViewDropPosition> = memScoped {
        val tmpPath = alloc<CPointerVar<GtkTreePath>>()
        val tmpPos = alloc<GtkIconViewDropPosition.Var>()
        gtk_icon_view_get_drag_dest_item(icon_view = gtkIconViewPtr, path = tmpPath.ptr, pos = tmpPos.ptr)
        val path = if (tmpPath.value != null) TreePath.fromPointer(tmpPath.value) else null
        path to tmpPos.value
    }

    public actual fun fetchCursor(): Triple<Boolean, TreePath?, CellRendererBase?> = memScoped {
        val tmpPath = alloc<CPointerVar<GtkTreePath>>()
        val tmpCell = alloc<CPointerVar<GtkCellRenderer>>()
        val cursorSet = gtk_icon_view_get_cursor(
            icon_view = gtkIconViewPtr,
            path = tmpPath.ptr,
            cell = tmpCell.ptr
        ) == TRUE
        val path = if (tmpPath.value != null) TreePath.fromPointer(tmpPath.value) else null
        val cell = if (tmpCell.value != null) CellRenderer.fromPointer(tmpCell.value) else null
        Triple(cursorSet, path, cell)
    }

    public actual fun fetchItemAtPosition(
        xPos: Int,
        yPos: Int
    ): Triple<Boolean, TreePath?, CellRendererBase?> = memScoped {
        val tmpPath = alloc<CPointerVar<GtkTreePath>>()
        val tmpCell = alloc<CPointerVar<GtkCellRenderer>>()
        val itemExists = gtk_icon_view_get_item_at_pos(
            icon_view = gtkIconViewPtr,
            x = xPos,
            y = yPos,
            path = tmpPath.ptr,
            cell = tmpCell.ptr
        ) == TRUE
        val path = if (tmpPath.value != null) TreePath.fromPointer(tmpPath.value) else null
        val cell = if (tmpCell.value != null) CellRenderer.fromPointer(tmpCell.value) else null
        Triple(itemExists, path, cell)
    }

    public actual fun fetchItemColumn(path: TreePath): Int =
        gtk_icon_view_get_item_column(gtkIconViewPtr, path.gtkTreePathPtr)

    public actual fun fetchItemRow(path: TreePath): Int =
        gtk_icon_view_get_item_row(gtkIconViewPtr, path.gtkTreePathPtr)

    public actual fun fetchPathAtPosition(xPos: Int, yPos: Int): TreePath? {
        val ptr = gtk_icon_view_get_path_at_pos(icon_view = gtkIconViewPtr, x = xPos, y = yPos)
        return if (ptr != null) TreePath.fromPointer(ptr) else null
    }

    public actual fun fetchVisibleRange(): Triple<Boolean, TreePath?, TreePath?> = memScoped {
        val tmpStartPath = alloc<CPointerVar<GtkTreePath>>()
        val tmpEndPath = alloc<CPointerVar<GtkTreePath>>()
        val validPaths = gtk_icon_view_get_visible_range(
            icon_view = gtkIconViewPtr,
            start_path = tmpStartPath.ptr,
            end_path = tmpEndPath.ptr
        ) == TRUE
        val startPath = if (tmpStartPath.value != null) TreePath.fromPointer(tmpStartPath.value) else null
        val endPath = if (tmpEndPath.value != null) TreePath.fromPointer(tmpEndPath.value) else null
        Triple(validPaths, startPath, endPath)
    }

    public actual fun itemActivated(path: TreePath) {
        gtk_icon_view_item_activated(gtkIconViewPtr, path.gtkTreePathPtr)
    }

    public actual fun pathIsSelected(path: TreePath): Boolean =
        gtk_icon_view_path_is_selected(gtkIconViewPtr, path.gtkTreePathPtr) == TRUE

    public actual fun scrollToPath(path: TreePath, useAlign: Boolean, rowAlign: Float, colAlign: Float) {
        gtk_icon_view_scroll_to_path(
            icon_view = gtkIconViewPtr,
            path = path.gtkTreePathPtr,
            use_align = if (useAlign) TRUE else FALSE,
            row_align = rowAlign,
            col_align = colAlign
        )
    }

    public actual fun selectAll() {
        gtk_icon_view_select_all(gtkIconViewPtr)
    }

    public actual fun selectPath(path: TreePath) {
        gtk_icon_view_select_path(gtkIconViewPtr, path.gtkTreePathPtr)
    }

    public actual fun changeCursor(path: TreePath, cell: CellRendererBase?, startEditing: Boolean) {
        gtk_icon_view_set_cursor(
            icon_view = gtkIconViewPtr,
            path = path.gtkTreePathPtr,
            cell = cell?.gtkCellRendererPtr,
            start_editing = if (startEditing) TRUE else FALSE
        )
    }

    public actual fun unselectAll() {
        gtk_icon_view_unselect_all(gtkIconViewPtr)
    }

    public actual fun unselectPath(path: TreePath) {
        gtk_icon_view_unselect_path(gtkIconViewPtr, path.gtkTreePathPtr)
    }

    override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gtkIconViewPtr, handlerId)
    }

    /**
     * Connects the *item-activated* event to a [handler] on a [IconView]. This event is fired when the function
     * [itemActivated] is called, where the user double clicks an item with the [activateOnSingleClick] property set to
     * *false*, or where the user single clicks an item when the [activateOnSingleClick] property set to *true*. It is
     * also fired when a non-editable item is selected and one of the keys: **Space**, **Return** or **Enter** is
     * pressed.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectItemActivatedEvent(handler: CPointer<ItemActivatedHandler>,
                                         userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkIconViewPtr, signal = IconViewEvent.itemActivated, slot = handler, data = userData)

    /**
     * Connects the *selection-changed* event to a [handler] on a [IconView]. This event is fired when the selection
     * (i.e. the set of selected items) changes.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectSelectionChangedEvent(handler: CPointer<SelectionChangedHandler>,
                                            userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkIconViewPtr, signal = IconViewEvent.selectionChanged, slot = handler, data = userData)
}

/**
 * The event handler for the *item-activated* event. Arguments:
 * 1. iconView: CPointer<GtkIconView>
 * 2. path: CPointer<GtkTreePath>
 * 3. userData: gpointer
 */
public typealias ItemActivatedHandler = CFunction<(
    iconView: CPointer<GtkIconView>,
    path: CPointer<GtkTreePath>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *selection-changed* event. Arguments:
 * 1. iconView: CPointer<GtkIconView>
 * 2. userData: gpointer
 */
public typealias SelectionChangedHandler = CFunction<(iconView: CPointer<GtkIconView>, userData: gpointer) -> Unit>

public fun CPointer<GtkIconView>?.toIconView(): IconView = IconView.fromPointer(this)
