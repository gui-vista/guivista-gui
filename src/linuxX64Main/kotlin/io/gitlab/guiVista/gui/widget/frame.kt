package io.gitlab.guiVista.gui.widget

import gtk3.GtkFrame
import gtk3.GtkWidget
import gtk3.gtk_frame_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Frame private constructor(ptr: CPointer<GtkFrame>? = null, label: String = "") : FrameBase {
    override val gtkFramePtr: CPointer<GtkFrame>? =
        ptr ?: gtk_frame_new(label.ifEmpty { null })?.reinterpret()
    override val gtkWidgetPtr: CPointer<GtkWidget>?
        get() = gtkFramePtr?.reinterpret()

    public actual companion object {
        public actual fun create(label: String): Frame = Frame(label = label)

        public fun fromPointer(ptr: CPointer<GtkFrame>?): Frame = Frame(ptr = ptr)
    }
}

public fun frameWidget(ptr: CPointer<GtkFrame>? = null, label: String = "", init: Frame.() -> Unit = {}): Frame {
    val frame = if (ptr != null) Frame.fromPointer(ptr) else Frame.create(label)
    frame.init()
    return frame
}

public fun CPointer<GtkFrame>?.toFrame(): Frame = Frame.fromPointer(this)
