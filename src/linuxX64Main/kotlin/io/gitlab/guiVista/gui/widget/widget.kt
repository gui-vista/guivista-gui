package io.gitlab.guiVista.gui.widget

import glib2.gpointer
import gtk3.GtkWidget
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.staticCFunction

public var widgetActivateHandler: (WidgetBase) -> Unit = {}

public actual class Widget private constructor(ptr: CPointer<GtkWidget>?) : WidgetBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr
    private var defaultActivateHandlerId = 0uL

    public fun assignDefaultActivateHandler() {
        if (defaultActivateHandlerId == 0uL) {
            defaultActivateHandlerId = connectActivateEvent(staticCFunction { widget: CPointer<GtkWidget>, _: gpointer ->
                widgetActivateHandler(Widget(widget))
            })
        }
    }

    public actual companion object {
        public actual fun fromWidgetBase(widgetBase: WidgetBase): Widget = Widget(widgetBase.gtkWidgetPtr)

        public fun fromPointer(ptr: CPointer<GtkWidget>?): Widget = Widget(ptr)
    }
}

public fun CPointer<GtkWidget>?.toWidget(): Widget = Widget.fromPointer(this)
