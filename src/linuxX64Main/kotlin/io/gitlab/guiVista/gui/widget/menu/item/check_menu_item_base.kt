package io.gitlab.guiVista.gui.widget.menu.item

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer

public actual interface CheckMenuItemBase : MenuItemBase {
    public val gtkCheckMenuItemPtr: CPointer<GtkCheckMenuItem>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var active: Boolean
        get() = gtk_check_menu_item_get_active(gtkCheckMenuItemPtr) == TRUE
        set(value) = gtk_check_menu_item_set_active(gtkCheckMenuItemPtr, if (value) TRUE else FALSE)
    public actual var drawAsRadio: Boolean
        get() = gtk_check_menu_item_get_draw_as_radio(gtkCheckMenuItemPtr) == TRUE
        set(value) = gtk_check_menu_item_set_draw_as_radio(gtkCheckMenuItemPtr, if (value) TRUE else FALSE)
    public actual var inconsistent: Boolean
        get() = gtk_check_menu_item_get_inconsistent(gtkCheckMenuItemPtr) == TRUE
        set(value) = gtk_check_menu_item_set_inconsistent(gtkCheckMenuItemPtr, if (value) TRUE else FALSE)

    /**
     * Connects the *toggled* event to a [handler] on a check menu item. This event is used when that state of the check
     * box is changed.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectToggledEvent(handler: CPointer<ToggledHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkCheckMenuItemPtr, signal = CheckMenuItemBaseEvent.toggled, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkCheckMenuItemPtr, handlerId)
    }
}

/**
 * The event handler for the *toggled* event. Arguments:
 * 1. checkMenuItem: CPointer<GtkCheckMenuItem>
 * 2. userData: gpointer
 */
public typealias ToggledHandler = CFunction<(checkMenuItem: CPointer<GtkCheckMenuItem>, userData: gpointer) -> Unit>
