package io.gitlab.guiVista.gui.widget.tool.item

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer

public actual interface ToggleToolButtonBase : ToolButtonBase {
    public val gtkToggleToolButtonPtr: CPointer<GtkToggleToolButton>?
    public actual var active: Boolean
        get() = gtk_toggle_tool_button_get_active(gtkToggleToolButtonPtr) == TRUE
        set(value) = gtk_toggle_tool_button_set_active(gtkToggleToolButtonPtr, if (value) TRUE else FALSE)

    /**
     * Connects the *toggled* event to a [handler] on a toggle tool button. This event is used when the button changes
     * state.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectToggledEvent(handler: CPointer<ToggledHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkToggleToolButtonPtr, signal = ToggleToolButtonBaseEvent.toggled, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkToggleToolButtonPtr, handlerId)
    }
}

/**
 * The event handler for the *toggled* event. Arguments:
 * 1. button: CPointer<GtkToggleToolButton>
 * 2. userData: gpointer
 */
public typealias ToggledHandler = CFunction<(button: CPointer<GtkToggleToolButton>, userData: gpointer) -> Unit>
