package io.gitlab.guiVista.gui.widget.button

import glib2.FALSE
import glib2.TRUE
import gtk3.*
import io.gitlab.guiVista.gui.layout.Container
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.widget.Popover
import io.gitlab.guiVista.gui.widget.PopoverBase
import io.gitlab.guiVista.gui.widget.menu.Menu
import io.gitlab.guiVista.gui.widget.menu.MenuBase
import io.gitlab.guiVista.io.application.menu.MenuModel
import io.gitlab.guiVista.io.application.menu.MenuModelBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class MenuButton private constructor(ptr: CPointer<GtkMenuButton>? = null) : ToggleButtonBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_menu_button_new()
    public val gtkMenuButtonPtr: CPointer<GtkMenuButton>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var alignWidget: ContainerBase
        get() = Container.fromWidgetPointer(gtk_menu_button_get_align_widget(gtkMenuButtonPtr))
        set(value) = gtk_menu_button_set_align_widget(gtkMenuButtonPtr, value.gtkWidgetPtr)
    public actual var menuModel: MenuModelBase
        get() = MenuModel(gtk_menu_button_get_menu_model(gtkMenuButtonPtr))
        set(value) = gtk_menu_button_set_menu_model(gtkMenuButtonPtr, value.gMenuModelPtr)
    public actual var popover: PopoverBase
        get() = Popover.fromPointer(gtk_menu_button_get_popover(gtkMenuButtonPtr))
        set(value) = gtk_menu_button_set_popover(gtkMenuButtonPtr, value.gtkWidgetPtr)
    public actual var popup: MenuBase
        get() = Menu.fromMenuPointer(gtk_menu_button_get_popup(gtkMenuButtonPtr))
        set(value) = gtk_menu_button_set_popup(gtkMenuButtonPtr, value.gtkWidgetPtr)
    public actual var usePopover: Boolean
        get() = gtk_menu_button_get_use_popover(gtkMenuButtonPtr) == TRUE
        set(value) = gtk_menu_button_set_use_popover(gtkMenuButtonPtr, if (value) TRUE else FALSE)

    /**
     * The `GtkArrowType` representing the direction in which the menu, or popover will be popped out. Default value is
     * `GTK_ARROW_DOWN`.
     *
     * Data binding property name: **direction**
     */
    public var direction: GtkArrowType
        get() = gtk_menu_button_get_direction(gtkMenuButtonPtr)
        set(value) = gtk_menu_button_set_direction(gtkMenuButtonPtr, value)

    public actual companion object {
        public actual fun create(): MenuButton = MenuButton()

        public fun fromPointer(ptr: CPointer<GtkMenuButton>?): MenuButton = MenuButton(ptr)
    }
}

public fun menuButtonWidget(ptr: CPointer<GtkMenuButton>? = null, init: MenuButton.() -> Unit = {}): MenuButton {
    val menuButton = if (ptr != null) MenuButton.fromPointer(ptr) else MenuButton.create()
    menuButton.init()
    return menuButton
}

public fun CPointer<GtkMenuButton>?.toMenuButton(): MenuButton = MenuButton.fromPointer(this)
