package io.gitlab.guiVista.gui.widget

import gio2.GFile
import gio2.GMountOperation
import gio2.GVolume
import glib2.FALSE
import glib2.GObject
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.dataType.SinglyLinkedList
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.window.ScrolledWindowBase
import io.gitlab.guiVista.io.file.File
import io.gitlab.guiVista.io.file.FileBase
import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class PlacesSideBar private constructor(ptr: CPointer<GtkPlacesSidebar>? = null) : ScrolledWindowBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_places_sidebar_new()
    public val gtkPlacesSideBarPtr: CPointer<GtkPlacesSidebar>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual var openFlags: UInt
        get() = gtk_places_sidebar_get_open_flags(gtkPlacesSideBarPtr)
        set(value) = gtk_places_sidebar_set_open_flags(gtkPlacesSideBarPtr, value)
    public actual var location: FileBase
        get() = File.fromPointer(gtk_places_sidebar_get_location(gtkPlacesSideBarPtr))
        set(value) = gtk_places_sidebar_set_location(gtkPlacesSideBarPtr, value.gFilePtr)
    public actual var showRecent: Boolean
        get() = gtk_places_sidebar_get_show_recent(gtkPlacesSideBarPtr) == TRUE
        set(value) = gtk_places_sidebar_set_show_recent(gtkPlacesSideBarPtr, if (value) TRUE else FALSE)
    public actual var showDesktop: Boolean
        get() = gtk_places_sidebar_get_show_desktop(gtkPlacesSideBarPtr) == TRUE
        set(value) = gtk_places_sidebar_set_show_desktop(gtkPlacesSideBarPtr, if (value) TRUE else FALSE)
    public actual var localOnly: Boolean
        get() = gtk_places_sidebar_get_local_only(gtkPlacesSideBarPtr) == TRUE
        set(value) = gtk_places_sidebar_set_local_only(gtkPlacesSideBarPtr, if (value) TRUE else FALSE)
    public actual var showEnterLocation: Boolean
        get() = gtk_places_sidebar_get_show_enter_location(gtkPlacesSideBarPtr) == TRUE
        set(value) = gtk_places_sidebar_set_show_enter_location(gtkPlacesSideBarPtr, if (value) TRUE else FALSE)
    public actual var showTrash: Boolean
        get() = gtk_places_sidebar_get_show_trash(gtkPlacesSideBarPtr) == TRUE
        set(value) = gtk_places_sidebar_set_show_trash(gtkPlacesSideBarPtr, if (value) TRUE else FALSE)
    public actual var showOtherLocations: Boolean
        get() = gtk_places_sidebar_get_show_other_locations(gtkPlacesSideBarPtr) == TRUE
        set(value) = gtk_places_sidebar_set_show_other_locations(gtkPlacesSideBarPtr, if (value) TRUE else FALSE)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkPlacesSidebar>?): PlacesSideBar = PlacesSideBar(ptr)

        public actual fun create(): PlacesSideBar = PlacesSideBar()
    }

    public actual infix fun addShortcut(location: FileBase) {
        gtk_places_sidebar_add_shortcut(gtkPlacesSideBarPtr, location.gFilePtr)
    }

    public actual infix fun removeShortcut(location: FileBase) {
        gtk_places_sidebar_remove_shortcut(gtkPlacesSideBarPtr, location.gFilePtr)
    }

    public actual fun listShortcuts(): SinglyLinkedList =
        SinglyLinkedList(gtk_places_sidebar_list_shortcuts(gtkPlacesSideBarPtr))

    public actual fun fetchBookmarkByIndex(index: Int): FileBase? {
        val ptr = gtk_places_sidebar_get_nth_bookmark(gtkPlacesSideBarPtr, index)
        return if (ptr != null) File.fromPointer(ptr) else null
    }

    override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gtkPlacesSideBarPtr, handlerId)
    }

    /**
     * Connects the *mount* event to a [handler] on a [PlacesSideBar]. This event is used when it starts a new
     * operation because the user clicked on some location that needs mounting. In this way the application using the
     * [PlacesSideBar] can track the progress of the operation and, for example show a notification.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectMountEvent(
        handler: CPointer<MountHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkPlacesSideBarPtr, signal = PlacesSideBarEvent.mount, slot = handler, data = userData)

    /**
     * Connects the *open-location* event to a [handler] on a [PlacesSideBar]. This event is used when the user selects
     * a location in it. The calling application should display the contents of that location. For example a file
     * manager should show a list of files in the specified location.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectOpenLocationEvent(
        handler: CPointer<OpenLocationHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkPlacesSideBarPtr, signal = PlacesSideBarEvent.openLocation, slot = handler,
            data = userData)

    /**
     * Connects the *populate-popup* event to a [handler] on a [PlacesSideBar]. This event is used when the user
     * invokes a contextual popup on one of its items. In the event handler the application may add extra items to the
     * menu as appropriate. For example a file manager may want to add a **Properties** command to the menu. It is not
     * necessary to store selectedItem for each menu item; during their callbacks, the application can use
     * [location] to get the file to which the item refers.
     *
     * The selectedItem argument may be *null* in case the selection refers to a volume. In this case selectedVolume
     * will be non null. In this case the calling application will have to `g_object_ref()` selectedVolume, and keep it
     * around to use it in the callback.
     *
     * The container and all its contents are destroyed after the user dismisses the popup. The popup is re-created
     * (and thus, this event is fired) every time the user activates the contextual menu.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectPopulatePopupEvent(
        handler: CPointer<PopulatePopupHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkPlacesSideBarPtr, signal = PlacesSideBarEvent.populatePopup, slot = handler,
            data = userData)

    /**
     * Connects the *show-enter-location* event to a [handler] on a [PlacesSideBar]. This event is used when it needs
     * the calling application to present an way to directly enter a location. For example the application may bring up
     * a dialog box asking for a URL like "http://http.example.com".
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectShowEnterLocationEvent(
        handler: CPointer<ShowEnterLocationHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkPlacesSideBarPtr, signal = PlacesSideBarEvent.showEnterLocation, slot = handler,
            data = userData)

    /**
     * Connects the *show-error-message* event to a [handler] on a [PlacesSideBar]. This event is used when it needs
     * the calling application to present an error message. Most of these messages refer to mounting or unmounting
     * media. For example when a drive cannot be started for some reason.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectShowErrorMessageEvent(
        handler: CPointer<ShowErrorMessageHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkPlacesSideBarPtr, signal = PlacesSideBarEvent.showErrorMessage, slot = handler,
            data = userData)

    /**
     * Connects the *show-other-locations-with-flags* event to a [handler] on a [PlacesSideBar]. This event is used
     * when it needs the calling application to present a way to show other locations e.g. drives and network access
     * points. For example the application may bring up a page showing persistent volumes and discovered network
     * addresses.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectShowOtherLocationsWithFlagsEvent(
        handler: CPointer<ShowOtherLocationsWithFlagsHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(
            obj = gtkPlacesSideBarPtr,
            signal = PlacesSideBarEvent.showOtherLocationsWithFlags,
            slot = handler,
            data = userData
        )

    /**
     * Connects the *unmount* event to a [handler] on a [PlacesSideBar]. This event is used when it starts a new
     * operation because the user for example ejected some drive or unmounted a mount. In this way the application
     * using the [PlacesSideBar] can track the progress of the operation and, for example, show a notification.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectUnmountEvent(
        handler: CPointer<UnmountHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkPlacesSideBarPtr, signal = PlacesSideBarEvent.unmount, slot = handler,
            data = userData)
}

/**
 * The event handler for the *mount* event. Arguments:
 * 1. sidebar: CPointer<GtkPlacesSidebar>
 * 2. mountOperation: CPointer<GMountOperation>
 * 3. userData: gpointer
 */
public typealias MountHandler = CFunction<(
    sidebar: CPointer<GtkPlacesSidebar>,
    mountOperation: CPointer<GMountOperation>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *open-location* event. Arguments:
 * 1. sidebar: CPointer<GtkPlacesSidebar>
 * 2. location: CPointer<GObject>
 * 3. openFlags: GtkPlacesOpenFlags
 * 4. userData: gpointer
 */
public typealias OpenLocationHandler = CFunction<(
    sidebar: CPointer<GtkPlacesSidebar>,
    location: CPointer<GObject>,
    openFlags: GtkPlacesOpenFlags,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *populate-popup* event. Arguments:
 * 1. sidebar: CPointer<GtkPlacesSidebar>
 * 2. container: CPointer<GtkWidget>
 * 3. selectedItem: CPointer<GFile>?
 * 4. selectedVolume: CPointer<GVolume>?
 * 5. userData: gpointer
 */
public typealias PopulatePopupHandler = CFunction<(
    sidebar: CPointer<GtkPlacesSidebar>,
    container: CPointer<GtkWidget>,
    selectedItem: CPointer<GFile>?,
    selectedVolume: CPointer<GVolume>?,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *show-enter-location* event. Arguments:
 * 1. sidebar: CPointer<GtkPlacesSidebar>
 * 2. userData: gpointer
 */
public typealias ShowEnterLocationHandler = CFunction<(
    sidebar: CPointer<GtkPlacesSidebar>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *show-error-message* event. Arguments:
 * 1. sidebar: CPointer<GtkPlacesSidebar>
 * 2. primary: CPointer<ByteVar> (represents a String)
 * 3. secondary: CPointer<ByteVar> (represents a String)
 * 4. userData: gpointer
 */
public typealias ShowErrorMessageHandler = CFunction<(
    sidebar: CPointer<GtkPlacesSidebar>,
    primary: CPointer<ByteVar>,
    secondary: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *show-other-locations-with-flags* event. Arguments:
 * 1. sidebar: CPointer<GtkPlacesSidebar>
 * 2. openFlags: GtkPlacesOpenFlags
 * 3. userData: gpointer
 */
public typealias ShowOtherLocationsWithFlagsHandler = CFunction<(
    sidebar: CPointer<GtkPlacesSidebar>,
    openFlags: GtkPlacesOpenFlags,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *unmount* event. Arguments:
 * 1. sidebar: CPointer<GtkPlacesSidebar>
 * 2. openFlags: GtkPlacesOpenFlags
 * 3. userData: gpointer
 */
public typealias UnmountHandler = CFunction<(
    sidebar: CPointer<GtkPlacesSidebar>,
    mountOperation: CPointer<GMountOperation>,
    userData: gpointer
) -> Unit>

public fun placesSideBarWidget(
    ptr: CPointer<GtkPlacesSidebar>? = null,
    init: PlacesSideBar.() -> Unit = {}
): PlacesSideBar {
    val result = if (ptr != null) PlacesSideBar.fromPointer(ptr) else PlacesSideBar.create()
    result.init()
    return result
}

public fun CPointer<GtkPlacesSidebar>?.toPlacesSideBar(): PlacesSideBar = PlacesSideBar.fromPointer(this)
