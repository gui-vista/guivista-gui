package io.gitlab.guiVista.gui.widget.tool.item

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase

public actual interface ToolButtonBase : ToolItemBase {
    public val gtkToolButtonPtr: CPointer<GtkToolButton>?
    public actual var iconName: String
        get() = gtk_tool_button_get_icon_name(gtkToolButtonPtr)?.toKString() ?: ""
        set(value) = gtk_tool_button_set_icon_name(gtkToolButtonPtr, value)
    public actual var iconWidget: WidgetBase?
        get() {
            val tmp = gtk_tool_button_get_icon_widget(gtkToolButtonPtr)
            return if (tmp != null) Widget.fromPointer(tmp) else null
        }
        set(value) = gtk_tool_button_set_icon_widget(gtkToolButtonPtr, value?.gtkWidgetPtr)
    public actual var label: String
        get() = gtk_tool_button_get_label(gtkToolButtonPtr)?.toKString() ?: ""
        set(value) = gtk_tool_button_set_label(gtkToolButtonPtr, value)
    public actual var labelWidget: WidgetBase?
        get() {
            val tmp = gtk_tool_button_get_label_widget(gtkToolButtonPtr)
            return if (tmp != null) Widget.fromPointer(tmp) else null
        }
        set(value) = gtk_tool_button_set_label_widget(gtkToolButtonPtr, value?.gtkWidgetPtr)
    public actual var useUnderline: Boolean
        get() = gtk_tool_button_get_use_underline(gtkToolButtonPtr) == TRUE
        set(value) = gtk_tool_button_set_use_underline(gtkToolButtonPtr, if (value) TRUE else FALSE)

    /**
     * Connects the *clicked* event to a [handler] on a tool button. This event is used when a user has clicked on the
     * tool button.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectClickedEvent(handler: CPointer<ClickedHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkToolButtonPtr, signal = ToolButtonBaseEvent.clicked, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkToolButtonPtr, handlerId)
    }
}

/**
 * The event handler for the *clicked* event. Arguments:
 * 1. toolButton: CPointer<GtkToolButton>
 * 2. userData: gpointer
 */
public typealias ClickedHandler = CFunction<(toolButton: CPointer<GtkToolButton>, userData: gpointer) -> Unit>
