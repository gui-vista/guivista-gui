package io.gitlab.guiVista.gui.widget

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.io.application.menu.MenuModel
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual interface PopoverBase : BinBase {
    public val gtkPopoverPtr: CPointer<GtkPopover>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var modal: Boolean
        get() = gtk_popover_get_modal(gtkPopoverPtr) == TRUE
        set(value) = gtk_popover_set_modal(gtkPopoverPtr, if (value) TRUE else FALSE)
    public actual var relativeTo: WidgetBase
        get() = Widget.fromPointer(gtk_popover_get_relative_to(gtkPopoverPtr))
        set(value) = gtk_popover_set_relative_to(gtkPopoverPtr, value.gtkWidgetPtr)
    public actual var transitionsEnabled: Boolean
        get() = gtk_popover_get_transitions_enabled(gtkPopoverPtr) == TRUE
        set(value) = gtk_popover_set_transitions_enabled(gtkPopoverPtr, if (value) TRUE else FALSE)

    /**
     * A constraint for the popover position. Default value is `GTK_POPOVER_CONSTRAINT_WINDOW`.
     *
     * Data binding property name: **constrain-to**
     */
    public var constrainTo: GtkPopoverConstraint
        get() = gtk_popover_get_constrain_to(gtkPopoverPtr)
        set(value) = gtk_popover_set_constrain_to(gtkPopoverPtr, value)

    /**
     * The preferred position of the popover. Default value is `GTK_POS_TOP`.
     *
     * Data binding property name: **position**
     */
    public var position: GtkPositionType
        get() = gtk_popover_get_position(gtkPopoverPtr)
        set(value) = gtk_popover_set_position(gtkPopoverPtr, value)

    public actual fun bindModel(model: MenuModel?, actionNamespace: String?) {
        gtk_popover_bind_model(popover = gtkPopoverPtr, model = model?.gMenuModelPtr,
            action_namespace = actionNamespace)
    }

    /**
     * Connects the *closed* event to a [handler] on a popover.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectClosedEvent(handler: CPointer<ClosedHandler>,
                                  userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkPopoverPtr, signal = PopoverBaseEvent.closed, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkPopoverPtr, handlerId)
    }
}

/**
 * The event handler for the *closed* event. Arguments:
 * 1. popover: CPointer<GtkPopover>
 * 2. userData: gpointer
 */
public typealias ClosedHandler = CFunction<(popover: CPointer<GtkPopover>, userData: gpointer) -> Unit>
