package io.gitlab.guiVista.gui.widget

import gtk3.*
import io.gitlab.guiVista.gui.layout.BinBase
import kotlinx.cinterop.*
import platform.posix.float_tVar

public actual interface FrameBase : BinBase {
    public val gtkFramePtr: CPointer<GtkFrame>?
    public actual var label: String
        get() = gtk_frame_get_label(gtkFramePtr)?.toKString() ?: ""
        set(value) = gtk_frame_set_label(gtkFramePtr, value)
    public actual var labelWidget: WidgetBase?
        get() {
            val ptr = gtk_frame_get_label_widget(gtkFramePtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
        set(value) = gtk_frame_set_label_widget(gtkFramePtr, value?.gtkWidgetPtr)

    /**
     * Appearance of the frame border. Default value is `GTK_SHADOW_ETCHED_IN`.
     *
     * Data binding property name: **shadow-type**
     */
    public var shadowType: GtkShadowType
        get() = gtk_frame_get_shadow_type(gtkFramePtr)
        set(value) = gtk_frame_set_shadow_type(gtkFramePtr, value)

    public actual fun changeLabelAlign(xAlign: Float, yAlign: Float) {
        gtk_frame_set_label_align(frame = gtkFramePtr, xalign = xAlign, yalign = yAlign)
    }

    public actual fun fetchLabelAlign(): Pair<Float, Float> = memScoped {
        val xAlign = alloc<float_tVar>()
        val yAlign = alloc<float_tVar>()
        gtk_frame_get_label_align(frame = gtkFramePtr, xalign = xAlign.ptr, yalign = yAlign.ptr)
        xAlign.value to yAlign.value
    }
}
