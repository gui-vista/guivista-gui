package io.gitlab.guiVista.gui.widget.menu

import gtk3.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class MenuBar private constructor(ptr: CPointer<GtkMenuBar>? = null) : MenuShell {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_menu_bar_new()
    public val gtkMenuBarPtr: CPointer<GtkMenuBar>?
        get() = gtkWidgetPtr?.reinterpret()

    /**
     * The child pack direction of the [MenuBar]. It determines how the widgets contained in child menu items are
     * arranged. Default value is *GtkPackDirection.GTK_PACK_DIRECTION_LTR*.
     *
     * Data binding property name: **child-pack-direction**
     */
    public var childPackDirection: GtkPackDirection
        get() = gtk_menu_bar_get_child_pack_direction(gtkMenuBarPtr)
        set(value) = gtk_menu_bar_set_child_pack_direction(gtkMenuBarPtr, value)

    /**
     * The pack direction of the menu bar. It determines how menu items are arranged in the menu bar. Default value is
     * *GtkPackDirection.GTK_PACK_DIRECTION_LTR*.
     *
     * Data binding property name: **pack-direction**
     */
    public var packDirection: GtkPackDirection
        get() = gtk_menu_bar_get_pack_direction(gtkMenuBarPtr)
        set(value) = gtk_menu_bar_set_pack_direction(gtkMenuBarPtr, value)

    public actual companion object {
        public actual fun create(): MenuBar = MenuBar()

        public fun fromPointer(ptr: CPointer<GtkMenuBar>?): MenuBar = MenuBar(ptr)
    }
}

public fun menuBarWidget(ptr: CPointer<GtkMenuBar>? = null, init: MenuBar.() -> Unit): MenuBar {
    val menuBar = if (ptr != null) MenuBar.fromPointer(ptr) else MenuBar.create()
    menuBar.init()
    return menuBar
}

public fun CPointer<GtkMenuBar>?.toMenuBar(): MenuBar = MenuBar.fromPointer(this)
