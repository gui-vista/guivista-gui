package io.gitlab.guiVista.gui.widget.tool

import gtk3.*
import kotlinx.cinterop.CPointer
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.gui.SizeGroup

public actual interface ToolShell : ObjectBase {
    public val gtkToolShellPtr: CPointer<GtkToolShell>?
    /**
     * Retrieves the icon size for the [ToolShell]. Tool items must not call this function directly, but rely on
     * [ToolItem's][io.gitlab.guiVista.gui.widget.tool.item.ToolItem]
     * [iconSize][io.gitlab.guiVista.gui.widget.tool.item.ToolItem.iconSize] property instead.
     */
    public val iconSize: GtkIconSize
        get() = gtk_tool_shell_get_icon_size(gtkToolShellPtr)
    /**
     * Retrieves the current orientation for the [ToolShell]. Tool items must not call this function directly, but rely
     * on [ToolItem's][io.gitlab.guiVista.gui.widget.tool.item.ToolItem]
     * [orientation][io.gitlab.guiVista.gui.widget.tool.item.ToolItem.orientation] property instead.
     */
    public val orientation: GtkOrientation
        get() = gtk_tool_shell_get_orientation(gtkToolShellPtr)
    /**
     * Returns the relief style of buttons on [ToolShell]. Tool items must not call this function directly, but rely on
     * [ToolItem's][io.gitlab.guiVista.gui.widget.tool.item.ToolItem]
     * [reliefStyle][io.gitlab.guiVista.gui.widget.tool.item.ToolItem.reliefStyle] property instead.
     */
    public val reliefStyle: GtkReliefStyle
        get() = gtk_tool_shell_get_relief_style(gtkToolShellPtr)
    /**
     * Retrieves whether the [ToolShell] has text, icons, or both. Tool items must not call this function directly, but
     * rely on [ToolItem's][io.gitlab.guiVista.gui.widget.tool.item.ToolItem]
     * [toolbarStyle][io.gitlab.guiVista.gui.widget.tool.item.ToolItem.toolBarStyle] property instead.
     */
    public val style: GtkToolbarStyle
        get() = gtk_tool_shell_get_style(gtkToolShellPtr)
    public actual val textAlignment: Float
        get() = gtk_tool_shell_get_text_alignment(gtkToolShellPtr)

    /**
     * Retrieves the current text orientation for the [ToolShell]. Tool items must not call this function directly, but
     * rely on [ToolItem's][io.gitlab.guiVista.gui.widget.tool.item.ToolItem]
     * [textOrientation][io.gitlab.guiVista.gui.widget.tool.item.ToolItem.textOrientation] property instead.
     */
    public val textOrientation: GtkOrientation
        get() = gtk_tool_shell_get_text_orientation(gtkToolShellPtr)
    public actual val textSizeGroup: SizeGroup
        get() = SizeGroup.fromPointer(gtk_tool_shell_get_text_size_group(gtkToolShellPtr))

    public actual fun rebuildMenu() {
        gtk_tool_shell_rebuild_menu(gtkToolShellPtr)
    }
}
