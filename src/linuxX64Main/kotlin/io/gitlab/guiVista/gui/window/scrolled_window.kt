package io.gitlab.guiVista.gui.window

import gtk3.GtkScrolledWindow
import gtk3.GtkWidget
import gtk3.gtk_scrolled_window_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.gui.Adjustment

public actual class ScrolledWindow private constructor(
    ptr: CPointer<GtkScrolledWindow>? = null,
    newHAdjustment: Adjustment? = null,
    newVAdjustment: Adjustment? = null
) : ScrolledWindowBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret()
        ?: gtk_scrolled_window_new(newHAdjustment?.gtkAdjustmentPtr, newVAdjustment?.gtkAdjustmentPtr)

    public actual companion object {
        public actual fun create(newHAdjustment: Adjustment?, newVAdjustment: Adjustment?): ScrolledWindow =
            ScrolledWindow(newHAdjustment = newHAdjustment, newVAdjustment = newVAdjustment)

        public fun fromPointer(ptr: CPointer<GtkScrolledWindow>?): ScrolledWindow = ScrolledWindow(ptr = ptr)
    }
}

public fun CPointer<GtkScrolledWindow>?.toScrolledWindow(): ScrolledWindow = ScrolledWindow.fromPointer(this)
