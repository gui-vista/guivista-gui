package io.gitlab.guiVista.gui.printer

import glib2.TRUE
import gtk3.GtkPrintOperationPreview
import gtk3.gtk_print_operation_preview_end_preview
import gtk3.gtk_print_operation_preview_is_selected
import gtk3.gtk_print_operation_preview_render_page
import kotlinx.cinterop.CPointer

public actual interface PrintOperationPreview {
    public val gtkPrintOperationPreviewPtr: CPointer<GtkPrintOperationPreview>?

    public actual fun endPreview() {
        gtk_print_operation_preview_end_preview(gtkPrintOperationPreviewPtr)
    }

    public actual fun pageSelectedInPreview(pageNum: Int): Boolean =
        gtk_print_operation_preview_is_selected(gtkPrintOperationPreviewPtr, pageNum) == TRUE

    public actual fun renderPage(pageNum: Int) {
        gtk_print_operation_preview_render_page(gtkPrintOperationPreviewPtr, pageNum)
    }
}
