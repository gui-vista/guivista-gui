package io.gitlab.guiVista.gui.printer

import glib2.FALSE
import glib2.TRUE
import glib2.g_object_unref
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.*
import io.gitlab.guiVista.gui.window.WindowBase
import kotlinx.cinterop.*

public actual class PrintOperation private constructor(
    ptr: CPointer<GtkPrintOperation>? = null
) : ObjectBase, PrintOperationPreview {
    public val gtkPrintOperationPtr: CPointer<GtkPrintOperation>? = ptr ?: gtk_print_operation_new()
    override val gtkPrintOperationPreviewPtr: CPointer<GtkPrintOperationPreview>?
        get() = gtkPrintOperationPtr?.reinterpret()

    /**
     * The status of the print operation.
     *
     * Data binding property name: **status**
     * @see statusString
     */
    public val status: GtkPrintStatus
        get() = gtk_print_operation_get_status(gtkPrintOperationPtr)
    public actual var hasSelection: Boolean
        get() = gtk_print_operation_get_has_selection(gtkPrintOperationPtr) == TRUE
        set(value) = gtk_print_operation_set_has_selection(gtkPrintOperationPtr, if (value) TRUE else FALSE)
    public actual var defaultPageSetup: PageSetup?
        get() {
            val ptr = gtk_print_operation_get_default_page_setup(gtkPrintOperationPtr)
            return if (ptr != null) PageSetup.fromPointer(ptr) else null
        }
        set(value) = gtk_print_operation_set_default_page_setup(gtkPrintOperationPtr, value?.gtkPageSetupPtr)
    public actual var printSettings: PrintSettings
        get() = PrintSettings.fromPointer(gtk_print_operation_get_print_settings(gtkPrintOperationPtr))
        set(value) = gtk_print_operation_set_print_settings(gtkPrintOperationPtr, value.gtkPrintSettingsPtr)
    public actual val totalPagesToPrint: Int
        get() = gtk_print_operation_get_n_pages_to_print(gtkPrintOperationPtr)
    public actual var supportSelection: Boolean
        get() = gtk_print_operation_get_support_selection(gtkPrintOperationPtr) == TRUE
        set(value) = gtk_print_operation_set_support_selection(gtkPrintOperationPtr, if (value) TRUE else FALSE)
    public actual var embedPageSetup: Boolean
        get() = gtk_print_operation_get_embed_page_setup(gtkPrintOperationPtr) == TRUE
        set(value) = gtk_print_operation_set_embed_page_setup(gtkPrintOperationPtr, if (value) TRUE else FALSE)
    public actual val statusString: String
        get() = gtk_print_operation_get_status_string(gtkPrintOperationPtr)?.toKString() ?: ""
    public actual val isFinished: Boolean
        get() = gtk_print_operation_is_finished(gtkPrintOperationPtr) == TRUE

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkPrintOperation>?): PrintOperation = PrintOperation(ptr)

        public actual fun create(): PrintOperation = PrintOperation()
    }

    public actual fun changeJobName(jobName: String) {
        gtk_print_operation_set_job_name(gtkPrintOperationPtr, jobName)
    }

    public actual fun changeTotalPages(totalPages: Int) {
        gtk_print_operation_set_n_pages(gtkPrintOperationPtr, totalPages)
    }

    public actual fun changeCurrentPage(currentPage: Int) {
        gtk_print_operation_set_current_page(gtkPrintOperationPtr, currentPage)
    }

    public actual fun changeUseFullPage(useFullPage: Boolean) {
        gtk_print_operation_set_use_full_page(gtkPrintOperationPtr, if (useFullPage) TRUE else FALSE)
    }

    public actual fun changeExportFileName(fileName: String) {
        gtk_print_operation_set_export_filename(gtkPrintOperationPtr, fileName)
    }

    public actual fun changeShowProgress(showProgress: Boolean) {
        gtk_print_operation_set_show_progress(gtkPrintOperationPtr, if (showProgress) TRUE else FALSE)
    }

    public actual fun changeTrackPrintStatus(trackStatus: Boolean) {
        gtk_print_operation_set_track_print_status(gtkPrintOperationPtr, if (trackStatus) TRUE else FALSE)
    }

    public actual fun changeCustomTabLabel(label: String) {
        gtk_print_operation_set_custom_tab_label(gtkPrintOperationPtr, label.ifEmpty { null })
    }

    public actual fun cancel() {
        gtk_print_operation_cancel(gtkPrintOperationPtr)
    }

    public actual fun drawPageFinish() {
        gtk_print_operation_draw_page_finish(gtkPrintOperationPtr)
    }

    public actual fun setupDeferDrawing() {
        gtk_print_operation_set_defer_drawing(gtkPrintOperationPtr)
    }

    override fun close() {
        g_object_unref(gtkPrintOperationPtr)
    }

    public actual fun changeAllowAsync(allowAsync: Boolean) {
        gtk_print_operation_set_allow_async(gtkPrintOperationPtr, if (allowAsync) TRUE else FALSE)
    }

    /**
     * Runs the print operation by first letting the user modify print settings in the print dialog, and then print the
     * document. Normally this function doesn't return until the rendering of all pages is complete. You can connect to
     * the **status-changed** event to obtain some information about the progress of the print operation. Furthermore
     * it may use a recursive main loop to show the print dialog.
     *
     * If you call [changeAllowAsync] then the operation will run asynchronously if this is supported on the platform.
     * The **done** signal will be fired with the result of the operation when the it is done (i.e. when the dialog is
     * canceled, or when the print succeeds or fails).
     * @param action The action to start.
     * @param parent The parent of the dialog.
     * @param error The error to use or *null*.
     * @return The result of the print operation. A return value of `GTK_PRINT_OPERATION_RESULT_APPLY` indicates that
     * the printing was completed successfully. In this case it is a good idea to obtain the used print settings with
     * [printSettings], and store them for reuse with the next print operation. A value of
     * `GTK_PRINT_OPERATION_RESULT_IN_PROGRESS` means the operation is running asynchronously, and will fire the
     * **done** event when done.
     */
    public fun run(
        action: GtkPrintOperationAction,
        parent: WindowBase,
        error: Error? = null
    ): GtkPrintOperationResult = gtk_print_operation_run(
        op = gtkPrintOperationPtr,
        action = action,
        parent = parent.gtkWindowPtr,
        error = cValuesOf(error?.gErrorPtr)
    )

    /**
     * Connects the *begin-print* event to a [handler] on a [PrintOperation]. This event occurs after the user has
     * finished changing print settings in the dialog, before the actual rendering starts. A typical use for
     * **begin-print** is to use the parameters from the `GtkPrintContext`, and paginate the document accordingly, and
     * then set the number of pages with [changeTotalPages].
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectBeginPrintEvent(
        handler: CPointer<BeginPrintHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkPrintOperationPtr, signal = PrintOperationEvent.beginPrint, slot = handler,
            data = userData)

    /**
     * Connects the *end-print* event to a [handler] on a [PrintOperation]. This event occurs after all pages have been
     * rendered. A handler for this event can clean up any resources that have been allocated in the **begin-print**
     * handler.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectEndPrintEvent(
        handler: CPointer<EndPrintHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkPrintOperationPtr, signal = PrintOperationEvent.endPrint, slot = handler,
            data = userData)

    /**
     * Connects the *draw-page* event to a [handler] on a [PrintOperation]. This event occurs for every page that is
     * printed. The event handler **MUST** render the page_nr 's page onto the cairo context obtained from context
     * using `gtk_print_context_get_cairo_context()`.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectDrawPageEvent(
        handler: CPointer<DrawPageHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkPrintOperationPtr, signal = PrintOperationEvent.drawPage, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkPrintOperationPtr, handlerId)
    }
}

public actual fun runPageSetupDialog(
    parent: WindowBase,
    pageSetup: PageSetup,
    printSettings: PrintSettings
): PageSetup = PageSetup.fromPointer(
    gtk_print_run_page_setup_dialog(
        parent = parent.gtkWindowPtr,
        page_setup = pageSetup.gtkPageSetupPtr,
        settings = printSettings.gtkPrintSettingsPtr
    )
)

/**
 * Runs a page setup dialog, letting the user modify the values from [pageSetup]. In contrast to [runPageSetupDialog],
 * this function returns after showing the page setup dialog on platforms that support this, and calls [doneFunc] from
 * a event handler for the **response** event of the dialog.
 * @param parent The parent or *null*.
 * @param pageSetup An existing [PageSetup] or *null*.
 * @param printSettings The print settings to use.
 * @param doneFunc A function to call when the user saves the modified page setup.
 * @param userData The user data to pass to [doneFunc].
 *
 */
public fun runPageSetupDialogAsync(
    parent: WindowBase?,
    pageSetup: PageSetup?,
    printSettings: PrintSettings,
    doneFunc: GtkPageSetupDoneFunc,
    userData: gpointer = fetchEmptyDataPointer()
) {
    gtk_print_run_page_setup_dialog_async(
        parent = parent?.gtkWindowPtr,
        page_setup = pageSetup?.gtkPageSetupPtr,
        settings = printSettings.gtkPrintSettingsPtr,
        done_cb = doneFunc,
        data = userData
    )
}

/**
 * The event handler for the *begin-print* event. Arguments:
 * 1. operation: CPointer<GtkPrintOperation>
 * 2. ctx: CPointer<GtkPrintContext>
 * 3. userData: gpointer
 */
public typealias BeginPrintHandler = CFunction<(
    operation: CPointer<GtkPrintOperation>,
    ctx: CPointer<GtkPrintContext>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *end-print* event. Arguments:
 * 1. operation: CPointer<GtkPrintOperation>
 * 2. ctx: CPointer<GtkPrintContext>
 * 3. userData: gpointer
 */
public typealias EndPrintHandler = CFunction<(
    operation: CPointer<GtkPrintOperation>,
    ctx: CPointer<GtkPrintContext>,
    userData: gpointer
) -> Unit>

/**
 * The event handler for the *draw-page* event. Arguments:
 * 1. operation: CPointer<GtkPrintOperation>
 * 2. ctx: CPointer<GtkPrintContext>
 * 3. pageNum: Int
 * 4. userData: gpointer
 */
public typealias DrawPageHandler = CFunction<(
    operation: CPointer<GtkPrintOperation>,
    ctx: CPointer<GtkPrintContext>,
    pageNum: Int,
    userData: gpointer
) -> Unit>

public fun CPointer<GtkPrintOperation>?.toPrintOperation(): PrintOperation = PrintOperation.fromPointer(this)
