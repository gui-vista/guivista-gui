package io.gitlab.guiVista.gui.dialog

import gtk3.GtkFontChooser
import gtk3.GtkFontChooserDialog
import gtk3.GtkWidget
import gtk3.gtk_font_chooser_dialog_new
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.gui.window.WindowBase
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class FontChooserDialog private constructor(
    ptr: CPointer<GtkFontChooserDialog>? = null,
    title: String = "",
    parent: WindowBase? = null
) : DialogBase, FontChooser {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret()
        ?: gtk_font_chooser_dialog_new(title, parent?.gtkWindowPtr)
    public val gtkFontChooserDialogPtr: CPointer<GtkFontChooserDialog>?
        get() = gtkWidgetPtr?.reinterpret()
    override val gtkFontChooserPtr: CPointer<GtkFontChooser>?
        get() = gtkWidgetPtr?.reinterpret()

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkFontChooserDialog>?): FontChooserDialog = FontChooserDialog(ptr = ptr)

        public actual fun create(title: String, parent: WindowBase?): FontChooserDialog =
            FontChooserDialog(title = title, parent = parent)
    }

    actual override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gtkFontChooserDialogPtr, handlerId)
    }
}

public fun fontChooserDialog(
    ptr: CPointer<GtkFontChooserDialog>? = null,
    title: String = "",
    parent: WindowBase? = null,
    init: FontChooserDialog.() -> Unit = {}
): FontChooserDialog {
    val dialog =
        if (ptr != null) {
            FontChooserDialog.fromPointer(ptr)
        } else {
            FontChooserDialog.create(title, parent)
        }
    dialog.init()
    return dialog
}

public fun CPointer<GtkFontChooserDialog>?.toFontChooserDialog(): FontChooserDialog =
    FontChooserDialog.fromPointer(this)
