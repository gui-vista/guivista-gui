package io.gitlab.guiVista.gui.dialog

import gtk3.GtkDialog
import gtk3.GtkWidget
import gtk3.gtk_dialog_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Dialog private constructor(ptr: CPointer<GtkDialog>? = null) : DialogBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_dialog_new()

    public companion object {
        public fun fromPointer(ptr: CPointer<GtkDialog>?): Dialog = Dialog(ptr)
    }
}
