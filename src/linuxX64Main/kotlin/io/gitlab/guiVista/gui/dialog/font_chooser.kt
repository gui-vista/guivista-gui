package io.gitlab.guiVista.gui.dialog

import glib2.FALSE
import glib2.TRUE
import glib2.g_free
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString

public actual interface FontChooser : ObjectBase {
    public val gtkFontChooserPtr: CPointer<GtkFontChooser>?
    public actual var font: String
        get() {
            val ptr = gtk_font_chooser_get_font(gtkFontChooserPtr)
            val result = ptr?.toKString() ?: ""
            g_free(ptr)
            return result
        }
        set(value) = gtk_font_chooser_set_font(gtkFontChooserPtr, value)
    public actual val fontSize: Int
        get() = gtk_font_chooser_get_font_size(gtkFontChooserPtr)
    public actual var previewText: String
        get() = gtk_font_chooser_get_preview_text(gtkFontChooserPtr)?.toKString() ?: ""
        set(value) = gtk_font_chooser_set_preview_text(gtkFontChooserPtr, value)
    public actual var showPreviewEntry: Boolean
        get() = gtk_font_chooser_get_show_preview_entry(gtkFontChooserPtr) == TRUE
        set(value) = gtk_font_chooser_set_show_preview_entry(gtkFontChooserPtr, if (value) TRUE else FALSE)

    /**
     * Connects the *font-activated* event to a [handler] on a [FontChooser]. This event is used when a font is
     * activated. This usually happens when the user double clicks an item, or an item is selected and the user presses
     * one of the following keys:
     * - **Space**
     * - **Shift+Space**
     * - **Enter**
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectFontActivatedEvent(
        handler: CPointer<FontActivatedHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkFontChooserPtr, signal = FontChooserEvent.fontActivated, slot = handler,
            data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkFontChooserPtr, handlerId)
    }
}

/**
 * The event handler for the *font-activated* event. Arguments:
 * 1. chooser: CPointer<GtkFontChooser>
 * 2. fontName: CPointer<ByteVar> (represents a String)
 * 3. userData: gpointer
 */
public typealias FontActivatedHandler = CFunction<(
    chooser: CPointer<GtkFontChooser>,
    fontName: CPointer<ByteVar>,
    userData: gpointer
) -> Unit>
