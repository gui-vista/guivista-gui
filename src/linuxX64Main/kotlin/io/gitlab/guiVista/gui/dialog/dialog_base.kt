package io.gitlab.guiVista.gui.dialog

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import kotlinx.cinterop.CFunction
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.widget.Widget
import io.gitlab.guiVista.gui.widget.WidgetBase
import io.gitlab.guiVista.gui.window.WindowBase

public actual interface DialogBase : WindowBase {
    public val gtkDialogPtr: CPointer<GtkDialog>?
        get() = gtkWidgetPtr?.reinterpret()
    override val gtkWindowPtr: CPointer<GtkWindow>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual val headerBar: WidgetBase?
        get() {
            val ptr = gtk_dialog_get_header_bar(gtkDialogPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }
    public actual val contentArea: WidgetBase?
        get() {
            val ptr = gtk_dialog_get_content_area(gtkDialogPtr)
            return if (ptr != null) Widget.fromPointer(ptr) else null
        }

    public actual fun run(): Int = gtk_dialog_run(gtkDialogPtr)

    public actual fun response(responseId: Int) {
        gtk_dialog_response(gtkDialogPtr, responseId)
    }

    public actual fun addButton(buttonText: String, responseId: Int): WidgetBase? {
        val ptr = gtk_dialog_add_button(dialog = gtkDialogPtr, button_text = buttonText, response_id = responseId)
        return if (ptr != null) Widget.fromPointer(ptr) else null
    }

    public actual fun addActionWidget(child: WidgetBase, responseId: Int) {
        gtk_dialog_add_action_widget(dialog = gtkDialogPtr, child = child.gtkWidgetPtr, response_id = responseId)
    }

    public actual fun changeDefaultResponse(responseId: Int) {
        gtk_dialog_set_default_response(gtkDialogPtr, responseId)
    }

    public actual fun changeResponseSensitive(responseId: Int, setting: Boolean) {
        gtk_dialog_set_response_sensitive(dialog = gtkDialogPtr, response_id = responseId,
            setting = if (setting) TRUE else FALSE)
    }

    public actual fun fetchResponseForWidget(widget: WidgetBase): Int =
        gtk_dialog_get_response_for_widget(gtkDialogPtr, widget.gtkWidgetPtr)

    public actual fun fetchWidgetForResponse(responseId: Int): WidgetBase? {
        val ptr = gtk_dialog_get_widget_for_response(gtkDialogPtr, responseId)
        return if (ptr != null) Widget.fromPointer(ptr) else null
    }

    override fun createUi(init: WindowBase.() -> Unit) {
        visible = true
    }

    /**
     * Connects the *close* event to a [handler] on a [Dialog]. This event occurs when the user uses a keybinding to
     * close the dialog. The default binding for this event is the **Escape** key.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectCloseEvent(handler: CPointer<CloseHandler>,
                                 userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkDialogPtr, signal = DialogBaseEvent.close, slot = handler, data = userData)

    /**
     * Connects the *response* event to a [handler] on a [Dialog]. This event occurs when an action widget is clicked,
     * the dialog receives a delete event, or the application programmer calls [response]. On a delete event the
     * response ID is `GTK_RESPONSE_DELETE_EVENT`. Otherwise it depends on which action widget was clicked.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectResponseEvent(handler: CPointer<ResponseHandler>,
                                    userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkDialogPtr, signal = DialogBaseEvent.response, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkDialogPtr, handlerId)
    }
}

/**
 * The event handler for the *close* event. Arguments:
 * 1. dialog: CPointer<GtkDialog>
 * 2. userData: gpointer
 */
public typealias CloseHandler = CFunction<(dialog: CPointer<GtkDialog>, userData: gpointer) -> Unit>

/**
 * The event handler for the *response* event. Arguments:
 * 1. dialog: CPointer<GtkDialog>
 * 2. responseId: Int
 * 3. userData: gpointer
 */
public typealias ResponseHandler = CFunction<(dialog: CPointer<GtkDialog>, responseId: Int, userData: gpointer) -> Unit>
