package io.gitlab.guiVista.gui.dialog

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.disconnectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.ImageBuffer
import io.gitlab.guiVista.gui.window.WindowBase
import kotlinx.cinterop.*

public actual class AboutDialog private constructor(ptr: CPointer<GtkAboutDialog>? = null) : DialogBase {
    override val gtkWidgetPtr: CPointer<GtkWidget>? = ptr?.reinterpret() ?: gtk_about_dialog_new()
    public val gtkAboutDialogPtr: CPointer<GtkAboutDialog>?
        get() = gtkWidgetPtr?.reinterpret()
    public actual var programName: String
        get() = gtk_about_dialog_get_program_name(gtkAboutDialogPtr)?.toKString() ?: ""
        set(value) = gtk_about_dialog_set_program_name(gtkAboutDialogPtr, value)
    public actual var version: String
        get() = gtk_about_dialog_get_version(gtkAboutDialogPtr)?.toKString() ?: ""
        set(value) = gtk_about_dialog_set_version(gtkAboutDialogPtr, value)
    public actual var copyright: String
        get() = gtk_about_dialog_get_copyright(gtkAboutDialogPtr)?.toKString() ?: ""
        set(value) = gtk_about_dialog_set_copyright(gtkAboutDialogPtr, value)
    public actual var comments: String
        get() = gtk_about_dialog_get_comments(gtkAboutDialogPtr)?.toKString() ?: ""
        set(value) = gtk_about_dialog_set_comments(gtkAboutDialogPtr, value)
    public actual var license: String
        get() = gtk_about_dialog_get_license(gtkAboutDialogPtr)?.toKString() ?: ""
        set(value) = gtk_about_dialog_set_license(gtkAboutDialogPtr, value)
    public actual var website: String
        get() = gtk_about_dialog_get_website(gtkAboutDialogPtr)?.toKString() ?: ""
        set(value) = gtk_about_dialog_set_website(gtkAboutDialogPtr, value)
    public actual var websiteLabel: String
        get() = gtk_about_dialog_get_website_label(gtkAboutDialogPtr)?.toKString() ?: ""
        set(value) = gtk_about_dialog_set_website_label(gtkAboutDialogPtr, value)
    public actual var artists: Array<String>
        get() {
            val ptr = gtk_about_dialog_get_artists(gtkAboutDialogPtr)
            val tmp = mutableListOf<String>()
            var pos = 0
            while (ptr?.get(pos) != null) {
                pos++
                tmp += ptr[pos]?.toKString() ?: ""
            }
            return tmp.toTypedArray()
        }
        set(value) = memScoped {
            gtk_about_dialog_set_artists(gtkAboutDialogPtr, value.toCStringArray(this))
        }
    public actual var authors: Array<String>
        get() {
            val ptr = gtk_about_dialog_get_authors(gtkAboutDialogPtr)
            val tmp = mutableListOf<String>()
            var pos = 0
            while (ptr?.get(pos) != null) {
                pos++
                tmp += ptr[pos]?.toKString() ?: ""
            }
            return tmp.toTypedArray()
        }
        set(value) = memScoped {
            gtk_about_dialog_set_authors(gtkAboutDialogPtr, value.toCStringArray(this))
        }
    public actual var translatorCredits: String
        get() = gtk_about_dialog_get_translator_credits(gtkAboutDialogPtr)?.toKString() ?: ""
        set(value) = gtk_about_dialog_set_translator_credits(gtkAboutDialogPtr, value)
    public actual var logo: ImageBuffer?
        get() {
            val ptr = gtk_about_dialog_get_logo(gtkAboutDialogPtr)
            return if (ptr != null) ImageBuffer.fromPointer(ptr) else null
        }
        set(value) = gtk_about_dialog_set_logo(gtkAboutDialogPtr, value?.gdkPixbufPtr)
    public actual var logoIconName: String
        get() = gtk_about_dialog_get_logo_icon_name(gtkAboutDialogPtr)?.toKString() ?: ""
        set(value) = gtk_about_dialog_set_logo_icon_name(gtkAboutDialogPtr, value)
    public actual var wrapLicense: Boolean
        get() = gtk_about_dialog_get_wrap_license(gtkAboutDialogPtr) == TRUE
        set(value) = gtk_about_dialog_set_wrap_license(gtkAboutDialogPtr, if (value) TRUE else FALSE)
    public actual var documenters: Array<String>
        get() {
            val ptr = gtk_about_dialog_get_documenters(gtkAboutDialogPtr)
            val tmp = mutableListOf<String>()
            var pos = 0
            while (ptr?.get(pos) != null) {
                pos++
                tmp += ptr[pos]?.toKString() ?: ""
            }
            return tmp.toTypedArray()
        }
        set(value) = memScoped {
            gtk_about_dialog_set_documenters(gtkAboutDialogPtr, value.toCStringArray(this))
        }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkAboutDialog>?): AboutDialog = AboutDialog(ptr)

        public actual fun create(): AboutDialog = AboutDialog()
    }

    public actual fun addCreditSection(sectionName: String, vararg people: String): Unit = memScoped {
        val tmp = mutableListOf<String>()
        people.forEach { item -> tmp += item }
        val copy = tmp.toTypedArray()
        gtk_about_dialog_add_credit_section(about = gtkAboutDialogPtr, section_name = sectionName,
            people = copy.toCStringArray(this))
    }

    public actual fun show(programName: String, parent: WindowBase?): Unit = memScoped {
        gtk_show_about_dialog(parent = parent?.gtkWindowPtr, first_property_name = "program-name",
            if (programName.isEmpty()) null else programName.cstr.ptr)
    }

    /**
     * Connects the *activate-link* event to a [handler] on an [AboutDialog]. This event occurs when a URI is
     * activated. Applications may connect to it to override the default behaviour, which is to call `gtk_show_uri()`.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectActivateLinkEvent(
        handler: CPointer<ActivateLinkHandler>,
        userData: gpointer = fetchEmptyDataPointer()
    ): ULong =
        connectGSignal(obj = gtkAboutDialogPtr, signal = AboutDialogEvent.activateLink, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        super.disconnectEvent(handlerId)
        disconnectGSignal(gtkAboutDialogPtr, handlerId)
    }
}

/**
 * Creates an [AboutDialog]. Will display the dialog if [show] is *true*.
 * @param programName The program version.
 * @param ptr If not null then the [AboutDialog] is created from the pointer.
 * @param parent The parent window to use, or *null* to not use the parent.
 * @param show If *true* then the dialog is displayed.
 * @param init The initialization block for the dialog.
 * @return A new [AboutDialog] object.
 */
public fun aboutDialog(
    programName: String = "",
    ptr: CPointer<GtkAboutDialog>? = null,
    parent: WindowBase? = null,
    show: Boolean = false,
    init: AboutDialog.() -> Unit = {}
): AboutDialog {
    val dialog = if (ptr != null) AboutDialog.fromPointer(ptr) else AboutDialog.create()
    dialog.init()
    if (show) dialog.show(programName, parent)
    return dialog
}

/**
 * The event handler for the *activate-link* event. Arguments:
 * 1. dialog: CPointer<GtkAboutDialog>
 * 2. uri: CPointer<ByteVar> (represents a String)
 * 3. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias ActivateLinkHandler =
    CFunction<(dialog: CPointer<GtkAboutDialog>, uri: CPointer<ByteVar>, userData: gpointer) -> Int>

public fun CPointer<GtkAboutDialog>?.toAboutDialog(): AboutDialog = AboutDialog.fromPointer(this)
