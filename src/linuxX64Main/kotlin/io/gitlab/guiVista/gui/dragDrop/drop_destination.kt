package io.gitlab.guiVista.gui.dragDrop

import glib2.FALSE
import glib2.TRUE
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.connectGSignal
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.Atom
import io.gitlab.guiVista.gui.widget.WidgetBase
import kotlinx.cinterop.*

public actual fun WidgetBase.setDropDestination(
    defaultFlags: UInt,
    dragActions: UInt,
    vararg targets: DragDropTargetEntry
): Unit = memScoped {
    val totalTargets = targets.size
    val tmp = allocArray<GtkTargetEntry>(totalTargets)
    targets.forEachIndexed { pos, item ->
        tmp[pos].target = item.target.cstr.ptr
        tmp[pos].info = item.info
        tmp[pos].flags = item.flags
    }
    gtk_drag_dest_set(
        widget = this@setDropDestination.gtkWidgetPtr,
        flags = defaultFlags,
        actions = dragActions,
        targets = null,
        n_targets = totalTargets
    )
}

public actual fun WidgetBase.unsetDropDestination() {
    gtk_drag_dest_unset(gtkWidgetPtr)
}

public actual fun WidgetBase.addDropImageTargets() {
    gtk_drag_dest_add_image_targets(gtkWidgetPtr)
}

public actual fun WidgetBase.addDropTextTargets() {
    gtk_drag_dest_add_text_targets(gtkWidgetPtr)
}

public actual fun WidgetBase.addDropUriTargets() {
    gtk_drag_dest_add_uri_targets(gtkWidgetPtr)
}

public actual fun WidgetBase.findDropTarget(ctx: DragDropContext, targetList: DragDropTargetList): Atom {
    val ptr = gtk_drag_dest_find_target(
        widget = gtkWidgetPtr,
        context = ctx.gdkDragContextPtr,
        target_list = targetList.gtkTargetListPtr
    )
    return Atom.fromPointer(ptr)
}

public actual var WidgetBase.dropTargets: DragDropTargetList?
    get() {
        val ptr = gtk_drag_dest_get_target_list(gtkWidgetPtr)
        return if (ptr != null) DragDropTargetList.fromPointer(ptr) else null
    }
    set(value) = gtk_drag_dest_set_target_list(gtkWidgetPtr, value?.gtkTargetListPtr)

public actual var WidgetBase.trackDragMotion: Boolean
    get() = gtk_drag_dest_get_track_motion(gtkWidgetPtr) == TRUE
    set(value) = gtk_drag_dest_set_track_motion(gtkWidgetPtr, if (value) TRUE else FALSE)

/**
 * Connects the *drag-motion* event to a [handler] on a widget. This event is fired on the drop site when the user
 * moves the cursor over the widget during a drag. The event handler **MUST** determine whether the cursor position is
 * in a drop zone or not. If it is not in a drop zone then it returns *0* (equivalent to *false*), and no further
 * processing is necessary. Otherwise the handler returns *1* (equivalent to *true*). In this case the handler is
 * responsible for providing the necessary information for displaying feedback to the user by calling
 * `gdk_drag_status()`.
 * @param handler The event handler for the event.
 * @param userData User data to pass through to the [handler].
 */
public fun WidgetBase.connectDragMotionEvent(handler: CPointer<DragMotionHandler>,
                                             userData: gpointer = fetchEmptyDataPointer()): ULong =
    connectGSignal(obj = gtkWidgetPtr, signal = DragDropEvent.dragMotion, slot = handler, data = userData)

/**
 * Connects the *drag-drop* event to a [handler] on a widget. This event is fired on the drop site when the user drops
 * the data onto the widget. The event handler **MUST** determine whether the cursor position is in a drop zone or
 * not. If it is not in a drop zone then it returns *0* (equivalent to *false*) and no further processing is necessary.
 * Otherwise the handler returns *1* (equivalent to *true*). In this case the handler must ensure that
 * `gtk_drag_finish()` is called to let the source know that the drop is done. The call to `gtk_drag_finish()` can be
 * done either directly or in a `GtkWidget::drag-data-received` handler, which gets triggered by calling
 * `gtk_drag_get_data()` to receive the data for one or more of the supported targets.
 * @param handler The event handler for the event.
 * @param userData User data to pass through to the [handler].
 */
public fun WidgetBase.connectDragDropEvent(handler: CPointer<DragDropHandler>,
                                           userData: gpointer = fetchEmptyDataPointer()): ULong =
    connectGSignal(obj = gtkWidgetPtr, signal = DragDropEvent.dragDrop, slot = handler, data = userData)

/**
 * Connects the *drag-data-received* event to a [handler] on a widget. This event is fired on the drop site when the
 * dragged data has been received. If the data was received in order to determine whether the drop will be accepted
 * then the handler is expected to call `gdk_drag_status()` and not finish the drag. However if the data was received
 * in response to a `GtkWidget::drag-drop` event (and this is the last target to be received) then the handler for this
 * event is expected to process the received data and then call `gtk_drag_finish()`, setting the success parameter
 * depending on whether the data was processed successfully.
 *
 * Applications **MUST** create some means to determine why the event was fired, and therefore whether to call
 * `gdk_drag_status()` or `gtk_drag_finish()`.
 * @param handler The event handler for the event.
 * @param userData User data to pass through to the [handler].
 */
public fun WidgetBase.connectDragDataReceivedEvent(handler: CPointer<DragDataReceivedHandler>,
                                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
    connectGSignal(obj = gtkWidgetPtr, signal = DragDropEvent.dragDataReceived, slot = handler, data = userData)

/**
 * The event handler for the *drag-motion* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. ctx: CPointer<GdkDragContext>
 * 3. xPos: Int
 * 4. yPos: Int
 * 5. time: UInt
 * 6. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias DragMotionHandler = CFunction<(
    widget: CPointer<GtkWidget>,
    ctx: CPointer<GdkDragContext>,
    xPos: Int,
    yPos: Int,
    time: UInt,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *drag-drop* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. ctx: CPointer<GdkDragContext>
 * 3. xPos: Int
 * 4. yPos: Int
 * 5. time: UInt
 * 6. userData: gpointer
 * Returns an Int (represents a Boolean).
 */
public typealias DragDropHandler = CFunction<(
    widget: CPointer<GtkWidget>,
    ctx: CPointer<GdkDragContext>,
    xPos: Int,
    yPos: Int,
    time: UInt,
    userData: gpointer
) -> Int>

/**
 * The event handler for the *drag-data-received* event. Arguments:
 * 1. widget: CPointer<GtkWidget>
 * 2. ctx: CPointer<GdkDragContext>
 * 3. xPos: Int
 * 4. yPos: Int
 * 5. selectionData: CPointer<GtkSelectionData>
 * 6. info: UInt
 * 7. time: UInt
 * 8. userData: gpointer
 */
public typealias DragDataReceivedHandler = CFunction<(
    widget: CPointer<GtkWidget>,
    ctx: CPointer<GdkDragContext>,
    xPos: Int,
    yPos: Int,
    selectionData: CPointer<GtkSelectionData>,
    info: UInt,
    time: UInt,
    userData: gpointer
) -> Unit>
