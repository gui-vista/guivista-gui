package io.gitlab.guiVista.gui.dragDrop

import gtk3.GtkTargetEntry
import gtk3.gtk_target_entry_free
import gtk3.gtk_target_entry_new
import io.gitlab.guiVista.core.Closable
import kotlinx.cinterop.*

public actual class DragDropTargetEntry private constructor(ptr: CPointer<GtkTargetEntry>?) : Closable {
    public val gtkTargetEntryPtr: CPointer<GtkTargetEntry>? = ptr
    public actual var target: String
        get() = gtkTargetEntryPtr?.pointed?.target?.toKString() ?: ""
        set(value) = memScoped {
            gtkTargetEntryPtr?.pointed?.target = value.cstr.ptr
        }
    public actual var flags: UInt
        get() = gtkTargetEntryPtr?.pointed?.flags ?: 0u
        set(value) {
            gtkTargetEntryPtr?.pointed?.flags = value
        }
    public actual var info: UInt
        get() = gtkTargetEntryPtr?.pointed?.info ?: 0u
        set(value) {
            gtkTargetEntryPtr?.pointed?.info = value
        }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkTargetEntry>?): DragDropTargetEntry = DragDropTargetEntry(ptr)

        public actual fun create(target: String, flags: UInt, info: UInt): DragDropTargetEntry {
            val ptr = gtk_target_entry_new(target = target, flags = flags, info = info)
            return DragDropTargetEntry(ptr)
        }
    }

    override fun close() {
        if (gtkTargetEntryPtr != null) gtk_target_entry_free(gtkTargetEntryPtr)
    }
}

public fun CPointer<GtkTargetEntry>?.toDragDropTargetEntry(): DragDropTargetEntry =
    DragDropTargetEntry.fromPointer(this)
