package io.gitlab.guiVista.gui

import glib2.*
import gtk3.*
import io.gitlab.guiVista.core.*
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import kotlinx.cinterop.*

public actual class IconTheme private constructor(ptr: CPointer<GtkIconTheme>?) : ObjectBase {
    public val gtkIconThemePtr: CPointer<GtkIconTheme>? = ptr
    public actual val exampleIconName: String
        get() = gtk_icon_theme_get_example_icon_name(gtkIconThemePtr)?.toKString() ?: ""

    public actual val contextList: DoublyLinkedList
        get() = DoublyLinkedList.fromPointer(gtk_icon_theme_list_contexts(gtkIconThemePtr))

    public actual companion object {
        public fun fromPointer(ptr: CPointer<GtkIconTheme>?): IconTheme = IconTheme(ptr)

        public actual fun create(): IconTheme = IconTheme(gtk_icon_theme_new())

        public actual fun fetchDefault(): IconTheme = IconTheme(gtk_icon_theme_get_default())
    }

    override fun close() {
        g_object_unref(gtkIconThemePtr)
    }

    public actual fun fetchIconSizes(iconName: String): Array<Int> {
        val tmpList = mutableListOf<Int>()
        val ptr = gtk_icon_theme_get_icon_sizes(gtkIconThemePtr, iconName)
        var finished = false
        var pos = 0
        while (!finished) {
            val item = ptr?.get(pos)
            if (item != null) tmpList += item else finished = true
            pos++
        }
        g_free(ptr)
        return tmpList.toTypedArray()
    }

    public actual fun fetchSearchPath(): Array<String> = memScoped {
        val tmpList = mutableListOf<String>()
        val totalElements = alloc<IntVar>()
        val path = alloc<CPointerVar<CPointerVar<ByteVar>>>()
        gtk_icon_theme_get_search_path(icon_theme = gtkIconThemePtr, path = path.ptr, n_elements = totalElements.ptr)
        @Suppress("ReplaceRangeToWithUntil")
        (0..totalElements.value - 1).forEach { pos ->
            tmpList += path.value?.get(pos)?.toKString() ?: ""
        }
        tmpList.toTypedArray()
    }

    public actual fun hasIcon(iconName: String): Boolean = gtk_icon_theme_has_icon(gtkIconThemePtr, iconName) == TRUE

    public actual fun listIcons(ctx: String): DoublyLinkedList =
        DoublyLinkedList.fromPointer(gtk_icon_theme_list_icons(gtkIconThemePtr, ctx))

    public actual fun changeCustomTheme(themeName: String) {
        gtk_icon_theme_set_custom_theme(gtkIconThemePtr, themeName)
    }

    public actual fun changeSearchPath(path: Array<String>): Unit = memScoped {
        gtk_icon_theme_set_search_path(icon_theme = gtkIconThemePtr, path = path.toCStringArray(this),
            n_elements = path.size)
    }

    public actual fun addResourcePath(path: String) {
        gtk_icon_theme_add_resource_path(gtkIconThemePtr, path)
    }

    public actual fun appendResourcePath(path: String) {
        gtk_icon_theme_append_search_path(gtkIconThemePtr, path)
    }

    public actual fun prependSearchPath(path: String) {
        gtk_icon_theme_prepend_search_path(gtkIconThemePtr, path)
    }

    public actual fun rescanIfNeeded(): Boolean = gtk_icon_theme_rescan_if_needed(gtkIconThemePtr) == TRUE

    public actual fun loadIcon(
        iconName: String,
        size: Int,
        flags: UInt
    ): Pair<ImageBuffer?, Error?> = memScoped {
        val tmpError = alloc<CPointerVar<GError>>()
        val tmpBuffer = gtk_icon_theme_load_icon(
            icon_name = iconName,
            icon_theme = gtkIconThemePtr,
            size = size,
            flags = flags,
            error = tmpError.ptr
        )
        val tmp = tmpError.value?.pointed
        val error = if (tmp != null) {
            Error.fromLiteral(domain = tmp.domain, code = tmp.code, message = tmp.message?.toKString() ?: "")
        } else {
            null
        }
        val imageBuffer = if (tmpBuffer != null) ImageBuffer.fromPointer(tmpBuffer) else null
        imageBuffer to error
    }

    public actual fun loadIconForScale(
        iconName: String,
        size: Int,
        scale: Int,
        flags: UInt
    ): Pair<ImageBuffer?, Error?> = memScoped {
        val tmpError = alloc<CPointerVar<GError>>()
        val tmpBuffer = gtk_icon_theme_load_icon_for_scale(
            icon_name = iconName,
            icon_theme = gtkIconThemePtr,
            size = size,
            flags = flags,
            error = tmpError.ptr,
            scale = scale
        )
        val tmp = tmpError.value?.pointed
        val error = if (tmp != null) {
            Error.fromLiteral(domain = tmp.domain, code = tmp.code, message = tmp.message?.toKString() ?: "")
        } else {
            null
        }
        val imageBuffer = if (tmpBuffer != null) ImageBuffer.fromPointer(tmpBuffer) else null
        imageBuffer to error
    }

    /**
     * Connects the *changed* event to a [handler] on a [IconTheme]. This event is fired when the current icon theme is
     * switched or GTK+ detects that a change has occurred in the contents of the current icon theme.
     * @param handler The event handler for the event.
     * @param userData User data to pass through to the [handler].
     */
    public fun connectChangedEvent(handler: CPointer<IconThemeChangedHandler>,
                                   userData: gpointer = fetchEmptyDataPointer()): ULong =
        connectGSignal(obj = gtkIconThemePtr, signal = IconThemeEvent.changed, slot = handler, data = userData)

    override fun disconnectEvent(handlerId: ULong) {
        disconnectGSignal(gtkIconThemePtr, handlerId)
    }
}

/**
 * The event handler for the *changed* event. Arguments:
 * 1. iconTheme: CPointer<GtkIconTheme>
 * 2. userData: gpointer
 */
public typealias IconThemeChangedHandler = CFunction<(iconTheme: CPointer<GtkIconTheme>, userData: gpointer) -> Unit>

public fun CPointer<GtkIconTheme>?.toIconTheme(): IconTheme = IconTheme.fromPointer(this)
