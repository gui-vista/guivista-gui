package io.gitlab.guiVista.gui

import io.gitlab.guiVista.core.Closable

/**
 * Is used to represent a (possibly translucent) color (comprised of red, green, blue, and alpha) in a way that is
 * compatible with Cairo’s notion of color.
 */
public expect class RgbaColor : Closable {
    /** The intensity of the red channel from *0.0* to *1.0* inclusive. */
    public var red: Double

    /** The intensity of the green channel from *0.0* to *1.0* inclusive. */
    public var green: Double

    /** The intensity of the blue channel from *0.0* to *1.0* inclusive. */
    public var blue: Double

    /** The opacity of the color from *0.0* for completely translucent to *1.0* for opaque. */
    public var alpha: Double

    public companion object {
        public fun create(red: Double = 0.0, green: Double = 0.0, blue: Double = 0.0, alpha: Double = 0.0): RgbaColor
    }
}
