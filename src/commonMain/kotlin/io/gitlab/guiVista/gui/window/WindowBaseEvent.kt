package io.gitlab.guiVista.gui.window

public object WindowBaseEvent {
    public const val activateDefault: String = "activate-default"
    public const val activateFocus: String = "activate-focus"
    public const val enableDebugging: String = "enable-debugging"
    public const val keysChanged: String = "keys-changed"
    public const val setFocus: String = "set-focus"
}
