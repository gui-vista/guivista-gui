package io.gitlab.guiVista.gui.window

import io.gitlab.guiVista.gui.GuiApplication

/** A window with [GuiApplication] support. */
public expect open class AppWindow : WindowBase
