package io.gitlab.guiVista.gui.window

import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.ImageBuffer
import io.gitlab.guiVista.gui.keyboard.AcceleratorGroup
import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for window objects. */
public expect interface WindowBase : BinBase {
    /** The custom title bar for the window. Will be *null* if there is no custom title bar. */
    public open var titleBar: WidgetBase?

    /**
     * The icon for this window.
     *
     * Data binding property name: **icon**
     */
    public open var icon: ImageBuffer?

    /**
     * A list of icons that are used for representing a window. From the list a icon is used when the window is
     * minimized (also known as iconified). Some window managers, or desktop environments may also place it in the
     * window frame, or display it in other contexts. The same icon can be passed in several hand-drawn sizes. The list
     * should contain the natural sizes your icon is available in; that is, don’t scale the image before passing it to
     * GTK+. Scaling is postponed until the last minute when the desired final size is known to allow best quality.
     *
     * By passing several sizes you may improve the final image quality of the icon, by reducing or eliminating
     * automatic image scaling. Recommended sizes to provide:
     * - 16x16
     * - 32x32
     * - 48x48
     * Also larger sizes (if you have them):
     * - 64x64
     * - 128x128
     *
     * Note that transient windows will inherit their icon from their transient parent. So there’s no need to
     * explicitly set the icon on transient windows.
     * @see defaultIconList
     */
    public open var iconList: DoublyLinkedList?

    /** The default widget is the widget that’s activated when the user presses *Enter* in a dialog (for example). */
    public open var defaultWidget: WidgetBase?

    /**
     * Whether the window frame should have a close button. Default value is *true*.
     *
     * Data binding property name: **deletable**
     */
    public open var deleteable: Boolean

    /**
     * If set to *true* then the window should receive the input focus. Default value is *true*.
     *
     * Data binding property name: **accept-focus**
     */
    public open var acceptFocus: Boolean

    /**
     * The widget to which this window is attached. Examples of places where specifying this relation is useful are
     * for instance a `GtkMenu` created by a `GtkComboBox`, a completion popup window created by `GtkEntry` or a
     * typeahead search entry created by `GtkTreeView`.
     *
     * Data binding property name: **attached-to**
     */
    public open var attachedTo: WidgetBase?

    /**
     * Name of the window. Default value is *""* (an empty String).
     *
     * Data binding property name: **title**
     */
    public open var title: String

    /**
     * If set to *true* then a user can resize the window. Default value is *true*.
     *
     * Data binding property name: **resizable**
     */
    public open var resizable: Boolean

    /**
     * If set to *true* then the window is maximized. Default value is *false*.
     *
     * Data binding property name: **is-maximized**
     */
    public open val isMaximized: Boolean

    /**
     * Whether the [Window] should be decorated by the window manager. Default value is *true*.
     *
     * Data binding property name: **decorated**
     */
    public open var decorated: Boolean

    /**
     * Whether the window frame should have a close button. Default value is *true*.
     *
     * Data binding property name: **deletable**
     */
    public open var deletable: Boolean

    /**
     * If this window should be destroyed when the parent is destroyed. Default value is *false*.
     *
     * Data binding property name: **destroy-with-parent**
     */
    public open var destroyWithParent: Boolean

    /**
     * Whether the window should receive the input focus when mapped. Default value is *true*.
     *
     * Data binding property name: **focus-on-map**
     */
    public open var focusOnMap: Boolean

    /**
     * Whether 'focus rectangles' are currently visible in this [Window]. Default value is *true*. This property is
     * maintained by GTK based on user input and should **NOT** be set by applications!
     *
     * Data binding property name: **focus-visible**
     */
    public open var focusVisible: Boolean

    /**
     * The window gravity of the window. Default value is *GdkGravity.GDK_GRAVITY_NORTH_WEST*. See [move], and
     * `GdkGravity` for more details about window gravity.
     *
     * Data binding property name: **gravity**
     */
    public open var gravity: ULong

    /**
     * Whether the title bar should be hidden during maximization. Default value is *false*.
     *
     * Data binding property name: **hide-titlebar-when-maximized**
     */
    public open var hideTitleBarWhenMaximized: Boolean

    /**
     * This property specifies the name of the themed icon to use as the window icon. Default value is *""*
     * (an empty String). See GtkIconTheme for more details.
     *
     * Data binding property name: **icon-name**
     */
    public open var iconName: String

    /**
     * Whether the top level is the current active window. Default value is *false*.
     *
     * Data binding property name: **is-active**
     */
    public open val isActive: Boolean

    /**
     * Whether mnemonics are currently visible in this window. Default value is *true*. This property is maintained by
     * GTK based on user input, and should **NOT** be set by applications!
     *
     * Data binding property name: **mnemonics-visible**
     */
    public open var mnemonicsVisible: Boolean

    /**
     * When *true* the window is modal (other windows are not usable while this one is up). Default value is *false*.
     *
     * Data binding property name: **modal**
     */
    public open var modal: Boolean

    /**
     * Unique identifier for the [Window] to be used when restoring a session. Default value is *""* (an empty String).
     *
     * Data binding property name: **role**
     */
    public open var role: String

    /**
     * When *true* the window should not be in the pager. Default value is *false*.
     *
     * Data binding property name: **skip-pager-hint**
     */
    public open var skipPagerHint: Boolean

    /**
     * When *true* the window should not be in the task bar. Default value is *false*.
     *
     * Data binding property name: **skip-taskbar-hint**
     */
    public open var skipTaskBarHint: Boolean

    /**
     * When *true* the window should be brought to the user's attention. Default value is *false*.
     *
     * Data binding property name: **urgency-hint**
     */
    public open var urgencyHint: Boolean

    /**
     * Whether the input focus is within this window. Default value is *false*.
     *
     * Data binding property name: **has-toplevel-focus**
     */
    public open val hasTopLevelFocus: Boolean

    /**
     * The focused widget in the window. To set the focus to a particular widget in the top level, it is usually more
     * convenient to use the [grabFocus][WidgetBase.grabFocus] function from [WidgetBase] instead of this function.
     */
    public open var focus: WidgetBase?

    public companion object {
        /**
         * An icon list to be used as fallback for windows that haven't used the [iconList] property to set up a window
         * specific icon list. This property allows you to set up the icon for all windows in your app at once.
         */
        public var defaultIconList: DoublyLinkedList?

        /**
         * An icon to be used as fallback for windows that haven't used the [iconList] property.
         * @see iconName
         */
        public var defaultIconName: String
    }

    /**
     * Associate [accelGroup] with the window, such that calling `AcceleratorGroup.update` on the window will activate
     * accelerators in [accelGroup].
     * @param accelGroup The acceleration group to add.
     */
    public open infix fun addAccelGroup(accelGroup: AcceleratorGroup)

    /**
     * Removes [accelGroup] from the window if it exists.
     * @param accelGroup The accelerator group to remove.
     * @see addAccelGroup
     */
    public open infix fun removeAccelGroup(accelGroup: AcceleratorGroup)

    /**
     * This function returns the position you need to pass to [move] to keep window in its current position. This means
     * that the meaning of the returned value varies with window gravity. The reliability of this function depends on
     * the windowing system currently in use. Some windowing systems such as Wayland do not support a global coordinate
     * system, and thus the position of the window will always be (0, 0). Others, like X11 do not have a reliable way
     * to obtain the geometry of the decorations of a window, if they are provided by the window manager. Additionally
     * on X11 the window manager has been known to mismanage window gravity, which result in windows moving even if you
     * use the coordinates of the current position as returned by this function.
     *
     * If you haven’t changed the window gravity, its gravity will be GDK_GRAVITY_NORTH_WEST. This means that this
     * function gets the position of the top left corner of the window manager frame for the window. The [move]
     * function sets the position of this same top left corner.
     *
     * If a window has gravity GDK_GRAVITY_STATIC the window manager frame is not relevant, and thus this function will
     * always produce accurate results. However you can’t use static gravity to do things like place a window in a
     * corner of the screen because static gravity ignores the window manager decorations.
     *
     * Ideally this function should return appropriate values if the window has client side decorations. Assuming that
     * the windowing system supports global coordinates. In practice saving the window position should not be left to
     * applications as they lack enough knowledge of the windowing system, and the window manager state to effectively
     * do so. The appropriate way to implement saving the window position is to use a platform specific protocol,
     * wherever that is available.
     * @return The current position as a Pair with the first element being the root x coordinate, and the last element
     * being the root y coordinate.
     * @see move
     */
    public open fun fetchPosition(): Pair<Int, Int>

    /**
     * Asks the window manager to move window to the given position. Window managers are free to ignore this. Most
     * window managers ignore requests for initial window positions (instead using a user defined placement algorithm),
     * and honor requests after the window has already been shown. **Note**: the position is the position of the
     * gravity determined reference point for the window. The gravity determines two things: first, the location of the
     * reference point in root window coordinates, and second which point on the window is positioned at the reference
     * point.
     *
     * By default the gravity is GDK_GRAVITY_NORTH_WEST, so the reference point is simply the x/y supplied to this
     * function. The top left corner of the window decorations (aka window frame or border) will be placed at x/y.
     * Therefore to position a window at the top left of the screen you want to use the default gravity
     * (which is GDK_GRAVITY_NORTH_WEST), and move the window to 0,0.
     *
     * To position a window at the bottom right corner of the screen you would set GDK_GRAVITY_SOUTH_EAST, which means
     * that the reference point is at x + the window width, and y + the window height, and the bottom-right corner of
     * the window border will be placed at that reference point. So to place a window in the bottom right corner you
     * would first set gravity to south east, then write:
     * `move(window, gdk_screen_width() - window_width, gdk_screen_height() - window_height)`. Do note that this example
     * doesn't take multi-head scenarios into account.
     * @param x The X coordinate to move the [Window] to.
     * @param y The Y coordinate to move the [Window] to.
     */
    public open fun move(x: Int, y: Int)

    /**
     * Changes the [window's][Window] startup notification identifier.
     * @param startupId The startup identifier to use.
     */
    public open infix fun changeStartupId(startupId: String)

    /**
     * Changes the default size of a window. If the window’s “natural” size (its size request) is larger than the
     * default, the default will be ignored. More generally, if the default size does not obey the geometry hints for
     * the window (`gtk_window_set_geometry_hints()` can be used to set these explicitly), the default size will be
     * clamped to the nearest permitted size. Unlike `gtk_widget_set_size_request()`, which sets a size request for a
     * widget and thus would keep users from shrinking the window. This function only sets the initial size just as if
     * the user had resized the window themselves. Users can still shrink the window again as they normally would.
     * Setting a default size of -1 means to use the “natural” default size (the size request of the window).
     *
     * For more control over a window’s initial size and how resizing works, investigate
     * `gtk_window_set_geometry_hints()`. For some uses, `gtk_window_resize()` is a more appropriate function. The
     * function `gtk_window_resize()` changes the current size of the window rather than the size to be used on initial
     * display, and always affects the window itself, not the geometry widget.
     *
     * The default size of a window only affects the first time a window is shown; if a window is hidden and re-shown,
     * it will remember the size it had prior to hiding, rather than using the default size. Windows can’t actually be
     * 0x0 in size, they must be at least 1x1, but passing 0 for width and height is OK, resulting in a 1x1 default
     * size. If you use this function to reestablish a previously saved window size, note that the appropriate size to
     * save is the one returned by `gtk_window_get_size()`. Using the window allocation directly will not work in all
     * circumstances and can lead to growing or shrinking windows.
     *
     * @param width Width in pixels, or -1 to unset the default width.
     * @param height Height in pixels, or -1 to unset the default height.
     */
    public open fun changeDefaultSize(width: Int, height: Int)

    /**
     * Asks to maximize window so that it becomes full-screen. Note that you shouldn’t assume the window is definitely
     * maximized afterward, because other entities (e.g. the user or window manager) could unmaximize it again, and not
     * all window managers support maximization. But normally the window will end up maximized. Just don’t write code
     * that crashes if not.
     *
     * It’s permitted to call this function before showing a window, in which case the window will be maximized when it
     * appears onscreen initially. You can track maximization via the **window-state-event** event on `GtkWidget`, or
     * by listening to notifications on the [isMaximized] property.
     */
    public open fun maximize()

    /**
     * Asks to unmaximize window . Note that you shouldn't assume the window is definitely unmaximized afterward
     * because other entities (e.g. the user or window manager) could maximize it again, and not all window managers
     * honor requests to unmaximize. But normally the window will end up unmaximized. Just don’t write code that
     * crashes if not. You can track maximization via the “window-state-event” event on GtkWidget.
     */
    public open fun unmaximize()

    /** Adds a new child to the window. */
    public open infix fun addChild(widget: WidgetBase)

    /** Removes a child from the window. */
    public open infix fun removeChild(widget: WidgetBase)

    /** Resets focus on a window. */
    public open fun resetFocus()

    /**
     * Allows the window to be setup without having to expose its internals. This function shouldn't be used for
     * creating the main layout (override [createMainLayout] instead) or the UI (use [createUi] instead) for the window.
     */
    public open fun setupWindow()

    /**
     * Creates the main layout for the window.
     * @return The main layout as a [ContainerBase], or *null* if there is no main layout.
     */
    public open fun createMainLayout(): ContainerBase?

    /**
     * Creates the user interface for the window.
     * @param init Initialization block for creating the user interface.
     */
    public open fun createUi(init: WindowBase.() -> Unit)

    /** Sets up window level events. */
    public open fun setupEvents()

    /**
     * Adds a mnemonic to this window.
     * @param keyVal The mnemonic.
     * @param target The widget that gets activated by the mnemonic.
     */
    public open fun addMnemonic(keyVal: UInt, target: WidgetBase)

    /**
     * Removes a mnemonic from this window.
     * @param keyVal The mnemonic.
     * @param target The widget that gets activated by the mnemonic.
     */
    public open fun removeMnemonic(keyVal: UInt, target: WidgetBase)

    /**
     * Presents a window to the user. This may mean raising the window in the stacking order, deiconifying it, moving
     * it to the current desktop, and/or giving it the keyboard focus, possibly dependent on the user’s platform,
     * window manager, and preferences. If window is hidden then this function calls `gtk_widget_show()` as well. This
     * function should be used when the user tries to open a window that’s already open. Say for example the
     * preferences dialog is currently open, and the user chooses Preferences from the menu a second time; use
     * [present] to move the already-open dialog where the user can see it.
     *
     * If you are calling this function in response to a user interaction, it is preferable to use
     * [presentWithTime].
     */
    public open fun present()

    /**
     * Presents a window to the user in response to a user interaction. If you need to present a window without a
     * timestamp, use [present].
     * @param timestamp The timestamp of the user interaction (typically a button or key press event) which triggered
     * this call.
     * @see present
     */
    public open fun presentWithTime(timestamp: UInt)

    /**
     * Asks to iconify (i.e. minimize) the specified window . Note that you shouldn't assume the window is definitely
     * iconified afterward, because other entities (e.g. the user or window manager) could deiconify it again, or
     * there may not be a window manager in which case iconification isn’t possible, etc. But normally the window will
     * end up iconified. Just don’t write code that crashes if not.
     *
     * It’s permitted to call this function before showing a window, in which case the window will be iconified before
     * it ever appears onscreen. You can track iconification via the **window-state-event** event on `GtkWidget`.
     */
    public open fun iconify()

    /**
     * Asks to deiconify (i.e. unminimize) the specified window . Note that you shouldn't assume the window is
     * definitely deiconified afterward, because other entities (e.g. the user or window manager)) could iconify it
     * again before your code which assumes deiconification gets to run. You can track iconification via the
     * **window-state-event** event on `GtkWidget`.
     */
    public open fun deiconify()

    /**
     * Asks to stick window, which means that it will appear on all user desktops. Note that you shouldn't assume the
     * window is definitely stuck afterward, because other entities (e.g. the user or window manager could unstick it
     * again, and some window managers do not support sticking windows. But normally the window will end up stuck.
     * Just don't write code that crashes if not.
     *
     * It’s permitted to call this function before showing a window. You can track stickiness via the
     * **window-state-event** event on `GtkWidget`.
     */
    public open fun stick()

    /**
     * Asks to unstick window, which means that it will appear on only one of the user’s desktops. Note that you
     * shouldn't assume the window is definitely unstuck afterward, because other entities (e.g. the user or window
     * manager) could stick it again. But normally the window will end up stuck. Just don’t write code that crashes if
     * not.
     *
     * You can track stickiness via the **window-state-event** event on `GtkWidget`.
     */
    public open fun unstick()

    /**
     * Asks to place window in the full screen state. Note that you shouldn't assume the window is definitely full
     * screen afterward, because other entities (e.g. the user or window manager) could unfullscreen it again, and not
     * all window managers honor requests to fullscreen windows. But normally the window will end up full screen. Just
     * don’t write code that crashes if not.
     *
     * You can track the full screen state via the **window-state-event** event on `GtkWidget`.
     */
    public open fun fullScreen()

    /**
     * Asks to toggle off the full screen state for window. Note that you shouldn't assume the window is definitely not
     * full screen afterward, because other entities (e.g. the user or window manager) could full screen it again, and
     * not all window managers honor requests to unfullscreen windows. But normally the window will end up restored to
     * its normal state. Just don’t write code that crashes if not.
     *
     * You can track the full screen state via the **window-state-event** event on `GtkWidget`.
     */
    public open fun unFullScreen()
}

/**
 * Returns a list of all existing top level windows. The widgets in the list are not individually referenced. If you
 * want to iterate through the list and perform actions involving callbacks that might destroy the widgets, you must
 * call `g_list_foreach (result, (GFunc)g_object_ref, NULL)` first, and then unreference **ALL** the widgets afterwards.
 * @return A list of the top level windows.
 */
public expect fun listTopLevelWindows(): DoublyLinkedList
