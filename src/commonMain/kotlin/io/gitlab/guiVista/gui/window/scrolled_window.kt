package io.gitlab.guiVista.gui.window

import io.gitlab.guiVista.gui.Adjustment

/**
 * Default implementation of [ScrolledWindowBase].
 */
public expect class ScrolledWindow : ScrolledWindowBase {
    public companion object {
        public fun create(newHAdjustment: Adjustment? = null, newVAdjustment: Adjustment? = null): ScrolledWindow
    }
}
