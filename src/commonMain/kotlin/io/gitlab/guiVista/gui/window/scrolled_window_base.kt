package io.gitlab.guiVista.gui.window

import io.gitlab.guiVista.gui.Adjustment
import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for a scrolled window (a window that adds scrollbars to its child widget). */
public expect interface ScrolledWindowBase : BinBase {
    /** Whether button presses are captured during kinetic scrolling. */
    public open var captureButtonPress: Boolean

    /**
     * The adjustment for the horizontal position.
     *
     * Data binding property name: **hadjustment**
     */
    public open var hAdjustment: Adjustment

    /**
     * Whether kinetic scrolling is enabled or not. Kinetic scrolling only applies to devices with source
     * `GDK_SOURCE_TOUCHSCREEN`. Default value is *true*.
     *
     * Data binding property name: **kinetic-scrolling**
     */
    public open var kineticScrolling: Boolean

    /**
     * The minimum content height of scrolled_window, or *-1* if not set. Default value is *-1*.
     *
     * Data binding property name: **min-content-height**
     */
    public open var minContentHeight: Int

    /**
     * The minimum content width of scrolled_window, or *-1* if not set. Default value is *-1*.
     *
     * Data binding property name: **min-content-width**
     */
    public open var minContentWidth: Int

    /**
     * Whether overlay scrolling is enabled or not. If it is then the scrollbars are only added as traditional widgets
     * when a mouse is present. Otherwise they are overlayed on top of the content, as narrow indicators. Default value
     * is *true*.
     *
     * Data binding property name: **overlay-scrolling**
     */
    public open var overlayScrolling: Boolean

    /**
     * The `GtkAdjustment` for the vertical position.
     *
     * Data binding property name: **vadjustment**
     */
    public open var vAdjustment: Adjustment

    /** The horizontal scrollbar of the scrolled window. */
    public open val hScrollBar: WidgetBase?

    /** The vertical scrollbar of the scrolled window. */
    public open val vScrollBar: WidgetBase?

    /**
     * Unsets the placement of the contents with respect to the scrollbars for the scrolled window. If no window
     * placement is set for a scrolled window, it defaults to GTK_CORNER_TOP_LEFT. See `placement` property.
     */
    public open fun unsetPlacement()
}
