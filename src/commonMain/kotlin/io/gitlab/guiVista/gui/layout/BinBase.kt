package io.gitlab.guiVista.gui.layout

import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for a container with just one child. */
public expect interface BinBase : ContainerBase {
    /**
     * Gets the child, or *null* if the bin contains no child widget. The returned widget does not have a reference
     * added so you do not need to unref it.
     */
    public open val child: WidgetBase?
}
