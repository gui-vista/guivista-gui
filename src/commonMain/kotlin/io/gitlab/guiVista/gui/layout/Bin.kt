package io.gitlab.guiVista.gui.layout

/** A container with just one child. */
public expect class Bin : BinBase
