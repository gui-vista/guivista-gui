package io.gitlab.guiVista.gui.layout

import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for a container for arranging buttons. */
public expect interface ButtonBoxBase : BoxBase, Orientable {
    /** The method being used to arrange the buttons in a button box */
    public open var layout: UInt

    /**
     * Sets whether [child] should appear in a secondary group of children. A typical use of a secondary child is the
     * help button in a dialog. This group appears after the other children if the style is `GTK_BUTTONBOX_START`,
     * `GTK_BUTTONBOX_SPREAD` or `GTK_BUTTONBOX_EDGE`, and before the other children if the style is
     * `GTK_BUTTONBOX_END`.
     *
     * For horizontal button boxes the definition of before/after depends on the direction of the
     * widget (see `gtk_widget_set_direction()`). If the style is `GTK_BUTTONBOX_START` or `GTK_BUTTONBOX_END` then the
     * secondary children are aligned at the other end of the button box from the main children. For the other styles
     * they appear immediately next to the main children.
     * @param child A child of the widget.
     * @param isSecondary If set to *true* then the child appears in a secondary group of the button box.
     */
    public open fun changeIsChildSecondary(child: WidgetBase, isSecondary: Boolean)

    /**
     * Returns whether child should appear in a secondary group of children.
     * @param child A child of the widget.
     * @return Whether child should appear in a secondary group of children.
     */
    public open fun fetchIsChildSecondary(child: WidgetBase): Boolean

    /**
     * Returns whether the child is exempted from homogenous sizing.
     * @param child A child of the widget.
     * @return A value of *true* if the child is not subject to homogenous sizing.
     */
    public open fun fetchIsChildNonHomogeneous(child: WidgetBase): Boolean

    /**
     * Sets whether the child is exempted from homogenous sizing.
     * @param child A child of the widget.
     * @param isNonHomogeneous The new value.
     */
    public open fun changeIsChildNonHomogeneous(child: WidgetBase, isNonHomogeneous: Boolean)
}