package io.gitlab.guiVista.gui.layout

import io.gitlab.guiVista.gui.widget.WidgetBase

/** A container which overlays widgets on top of each other. */
public expect class Overlay : BinBase {
    public companion object {
        public fun create(): Overlay
    }

    /**
     * Adds [widget] to overlay. The widget will be stacked on top of the main widget added with
     * [io.gitlab.guiVista.gui.layout.ContainerBase.add]. The position at which the widget is placed is determined from its
     * **halign**, and **valign** properties.
     * @param widget The widget to be added to the container.
     */
    public fun addOverlay(widget: WidgetBase)

    /**
     * Moves the [child] to a new index in the list of overlay children. The list contains overlays in the order that
     * these were added to the overlay. A widget’s index in the overlay children list determines which order the
     * children are drawn if they overlap. The first child is drawn at the bottom. It also affects the default focus
     * chain order.
     * @param child The overlaid widget to move.
     * @param pos The new index for the [child] in the list of overlay children of the overlay, starting from *0*. If
     * negative then this indicates the end of the list.
     */
    public fun reorderOverlay(child: WidgetBase, pos: Int)

    /**
     * Convenience function to get the value of the **pass-through** child property for the [widget].
     * @param widget An overlay child of the overlay.
     * @return Whether the [widget] is a pass through child.
     */
    public fun fetchOverlayPassThrough(widget: WidgetBase): Boolean

    /**
     * Convenience function to set the value of the **pass-through** child property for the widget.
     * @param widget An overlay child of the overlay.
     * @param passThrough Whether the child should pass the input through.
     */
    public fun changeOverlayPassThrough(widget: WidgetBase, passThrough: Boolean)
}
