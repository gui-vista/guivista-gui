package io.gitlab.guiVista.gui.layout

import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for box objects. */
public expect interface BoxBase : ContainerBase, Orientable {
    /**
     * A child widget that will be centered with respect to the full width of the box, even if the children at either
     * side take up different amounts of space.
     */
    public open var centerWidget: WidgetBase?

    /**
     * The amount of space between children. Default value is *0*.
     *
     * Data binding property name: **spacing**
     */
    public open var spacing: Int

    /**
     * If set to *true* then all the children are the same size. Default value is *false*.
     *
     * Data binding property name: **homogeneous**
     */
    public open var homogeneous: Boolean

    /**
     * Moves child to a new position in the list of box children. The list contains widgets packed `GTK_PACK_START` as
     * well as widgets packed `GTK_PACK_END`, in the order that these widgets were added to box. A widget’s position
     * in the box children list determines where the widget is packed into box. A child widget at some position in the
     * list will be packed just after all other widgets of the same packing type that appear earlier in the list.
     * @param child The child to move.
     * @param pos The new position for the child in the box starting from 0. If negative this indicates the end of the
     * list.
     */
    public open fun reorderChild(child: WidgetBase, pos: Int)

    /**
     * With a vertical box a [child] is added to the *top* of the GTK Box. If the box is horizontal then a [child] is
     * added to the *left* of the GTK Box.
     * @param child The widget to prepend.
     * @param fill If *true* then the added [child] will be sized to use all of the available space.
     * @param expand If *true* then the added [child] will be resized every time the box is resized.
     * @param padding The amount of padding to use for the [child] which is in pixels. By default no padding is used.
     */
    public open fun prependChild(
        child: WidgetBase,
        fill: Boolean = true,
        expand: Boolean = false,
        padding: UInt = 0u
    )

    /**
     * With a vertical box a [child] is added to the *bottom* of the GTK Box. If the box is horizontal then a [child]
     * is added to the *right* of the GTK Box.
     * @param child The widget to append.
     * @param fill If *true* then the added [child] will be sized to use all of the available space.
     * @param expand If *true* then the added [child] will be resized every time the box is resized.
     * @param padding The amount of padding to use for the [child] which is in pixels. By default no padding is used.
     */
    public open fun appendChild(
        child: WidgetBase,
        fill: Boolean = true,
        expand: Boolean = false,
        padding: UInt = 0u
    )
}
