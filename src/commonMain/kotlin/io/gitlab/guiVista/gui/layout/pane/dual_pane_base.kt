package io.gitlab.guiVista.gui.layout.pane

import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.layout.Orientable
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for a widget with two adjustable panes. */
public expect interface DualPaneBase : ContainerBase, Orientable {
    /**
     * Position of paned separator in pixels (0 means all the way to the left/top). Default value is *0*.
     *
     * Data binding property name: **position**
     */
    public open var position: Int

    /**
     * Setting this property to *true* indicates that the dual pane needs to provide stronger visual separation (e.g.
     * because it separates between two notebooks, whose tab rows would otherwise merge visually). Default value is
     * *false*.
     *
     * Data binding property name: **wide-handle**
     */
    public open var wideHandle: Boolean

    /** The first child of the paned widget. Will be *null* if this property isn't set. */
    public open val firstChild: WidgetBase?
    /** The second child of the paned widget. Will be *null* if this property isn't set. */
    public open val secondChild: WidgetBase?

    /**
     * Adds a [child] to the top or left pane with default parameters. This is equivalent to the following:
     * `pack1(paned, child, false, true)`
     * @param child The child to add.
     */
    public open fun add1(child: WidgetBase)

    /**
     * Adds a [child] to the bottom or right pane with default parameters. This is equivalent to the following:
     * `pack2(paned, child, true, true)`
     * @param child The child to add.
     */
    public open fun add2(child: WidgetBase)

    /**
     * Adds a [child] to the top or left pane.
     * @param child The child to add.
     * @param resize Should this child expand when the paned widget is resized.
     * @param shrink Can this child be made smaller than its requisition.
     */
    public open fun pack1(child: WidgetBase, resize: Boolean, shrink: Boolean)

    /**
     * Adds a [child] to the bottom or right pane.
     * @param child The child to add.
     * @param resize Should this child expand when the paned widget is resized.
     * @param shrink Can this child be made smaller than its requisition.
     */
    public open fun pack2(child: WidgetBase, resize: Boolean, shrink: Boolean)
}
