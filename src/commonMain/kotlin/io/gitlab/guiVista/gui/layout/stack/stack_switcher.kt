package io.gitlab.guiVista.gui.layout.stack

import io.gitlab.guiVista.gui.layout.BoxBase

/** A controller for Stack. */
public expect class StackSwitcher : BoxBase {
    /**
     * The stack to control. If set to *null* then no stack has been set explicitly.
     *
     * Data binding property name: **stack**
     */
    public var stack: Stack?

    public companion object {
        public fun create(): StackSwitcher
    }
}
