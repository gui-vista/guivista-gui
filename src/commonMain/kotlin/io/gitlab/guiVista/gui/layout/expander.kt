package io.gitlab.guiVista.gui.layout

import io.gitlab.guiVista.gui.widget.WidgetBase

/** A container which can hide its child. */
public expect class Expander : BinBase {
    /**
     * Whether the expander has been opened to reveal the child widget. Default value is *false*.
     *
     * Data binding property name: **expanded**
     */
    public var expanded: Boolean

    /**
     * Text of the expander's label. Default value is *""* (empty String).
     *
     * Data binding property name: **label**
     */
    public var label: String

    /**
     * Whether the label widget should fill all available horizontal space. Default value is *false*.
     *
     * Data binding property name: **label-fill**
     */
    public var labelFill: Boolean

    /**
     * A widget to display in place of the usual expander label.
     *
     * Data binding property name: **label-widget**
     */
    public var labelWidget: WidgetBase?

    /**
     * When this property is *true* the expander will resize the top level widget containing the expander upon
     * expanding, and collapsing. Default value is *false*.
     *
     * Data binding property name: **resize-toplevel**
     */
    public var resizeTopLevel: Boolean

    /**
     * The text of the label includes XML markup. See pango_parse_markup(). Default value is *false*.
     *
     * Data binding property name: **use-markup**
     */
    public var useMarkup: Boolean

    /**
     * If set then an underline in the text indicates the next character should be used for the mnemonic accelerator
     * key. Default value is *false*.
     *
     * Data binding property name: **use-underline**
     */
    public var useUnderline: Boolean

    public companion object {
        /**
         * Creates a new expander using [label] as the text of the label.
         * @param label The text of the label.
         * @return A new instance of [Expander].
         */
        public fun fromLabel(label: String): Expander

        /**
         * Creates a new expander using [mnemonic] as the text of the label. If characters in the label are preceded
         * by an underscore then they are underlined. If you need a literal underscore character in a label then use
         * **__** (two underscores). The first underlined character represents a keyboard accelerator called a
         * mnemonic. Pressing **Alt** and that key activates the button.
         * @param mnemonic The text of the label with an underscore in front of the mnemonic character.
         * @return A new instance of [Expander].
         */
        public fun fromMnemonic(mnemonic: String): Expander
    }
}
