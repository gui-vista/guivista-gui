package io.gitlab.guiVista.gui.layout.pane

public object DualPaneBaseEvent {
    public const val acceptPosition: String = "accept-position"
    public const val cancelPosition: String = "cancel-position"
    public const val cycleChildFocus: String = "cycle-child-focus"
    public const val cycleHandleFocus: String = "cycle-handle-focus"
    public const val moveHandle: String = "move-handle"
    public const val toggleHandleFocus: String = "toggle-handle-focus"
}