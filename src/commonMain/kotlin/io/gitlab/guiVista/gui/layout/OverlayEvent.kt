package io.gitlab.guiVista.gui.layout

public object OverlayEvent {
    public const val getChildPosition: String = "get-child-position"
}
