package io.gitlab.guiVista.gui.layout

/** For widgets that contain other widgets. */
public expect class Container : ContainerBase
