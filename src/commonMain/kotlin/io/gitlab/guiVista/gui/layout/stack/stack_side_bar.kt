package io.gitlab.guiVista.gui.layout.stack

import io.gitlab.guiVista.gui.layout.BinBase

/** An automatic sidebar widget. */
public expect class StackSideBar : BinBase {
    /**
     * The stack associated with this [StackSideBar]. The sidebar widget will automatically update according to the
     * order (packing), and items within the given [Stack]. If this property is set to *null* then nothing has been set
     * explicitly.
     *
     * Data binding property name: **stack**
     */
    public var stack: Stack?

    public companion object {
        public fun create(): StackSideBar
    }
}
