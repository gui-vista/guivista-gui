package io.gitlab.guiVista.gui.layout.pane

/** A widget with two adjustable panes. */
public expect class DualPane : DualPaneBase
