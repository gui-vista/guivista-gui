package io.gitlab.guiVista.gui.layout

import io.gitlab.guiVista.gui.widget.WidgetBase

/** A full width bar for presenting contextual actions. */
public expect class ActionBar : BinBase {
    /** The center widget for the [ActionBar]. */
    public var centerWidget: WidgetBase?

    public companion object {
        public fun create(): ActionBar
    }

    /**
     * Adds the child to the left of the [ActionBar].
     * @param child The child to add.
     */
    public fun prependChild(child: WidgetBase)

    /**
     * Adds the child to the right of the [ActionBar].
     * @param child The child to add.
     */
    public fun appendChild(child: WidgetBase)
}
