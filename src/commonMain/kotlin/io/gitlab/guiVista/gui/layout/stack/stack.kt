package io.gitlab.guiVista.gui.layout.stack

import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.widget.WidgetBase

/** A stacking container. */
public expect class Stack : ContainerBase {
    /**
     * The widget currently visible in the stack.
     *
     * Data binding property name: **visible-child**
     */
    public var visibleChild: WidgetBase?

    /**
     * The name of the widget currently visible in the stack. Default value is *""* (empty String).
     *
     * Data binding property name: **visible-child-name**
     */
    public var visibleChildName: String

    /**
     * If *true* then the [Stack] will request the same size for all its children. If it isn't then the
     * [Stack] may change size when a different child becomes visible.
     *
     * Homogeneity can be controlled separately for horizontal and vertical size, with the [hHomogeneous] and
     * [vHomogeneous].
     *
     * Data binding property name: **homogeneous**
     */
    public var homogeneous: Boolean

    /**
     * If *true* then the stack allocates the same width for all children. Default value is *true*.
     *
     * Data binding property name: **hhomogeneous**
     */
    public var hHomogeneous: Boolean

    /**
     * If *true* then the stack allocates the same height for all children. Default value is *true*.
     *
     * Data binding property name: **vhomogeneous**
     */
    public var vHomogeneous: Boolean

    /**
     * The animation duration in milliseconds. Default value is *200*.
     *
     * Data binding property name: **transition-duration**
     */
    public var transitionDuration: UInt

    /**
     * Whether or not the size should smoothly change when changing between differently sized children. Default value
     * is *false*.
     */
    public var interpolateSize: Boolean

    public companion object {
        public fun create(): Stack
    }

    /**
     * Adds a child to the stack. The child is identified by the [name].
     * @param child The widget to add.
     * @param name The name for the [child].
     */
    public fun addNamed(child: WidgetBase, name: String)

    /**
     * Adds a [child] to the stack. The [child] is identified by the [name]. The [title] will be used by
     * [StackSwitcher] to represent the [child] in a tab bar, so it should be short.
     * @param child The widget to add.
     * @param name The name for the [child].
     * @param title A human readable title for the [child].
     */
    public fun addTitled(child: WidgetBase, name: String, title: String)
}
