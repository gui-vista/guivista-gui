package io.gitlab.guiVista.gui.layout

import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.Adjustment
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for widgets which contain other widgets. Deals with basic layout. */
public expect interface ContainerBase : WidgetBase {
    /**
     * The width of the empty border outside the container's children. Default value is *0*.
     *
     * Data binding property name: **border-width**
     */
    public open var borderWidth: UInt

    /** Gets the [container’s][ContainerBase] non internal children. */
    public open val children: DoublyLinkedList

    /**
     * Fetches the current focus child widget inside [ContainerBase]. This is **not** the currently focused widget. That
     * can be obtained using the [focus][io.gitlab.guiVista.gui.window.WindowBase.focus] property from
     * [WindowBase][io.gitlab.guiVista.gui.window.WindowBase].
     */
    public open var focusChild: WidgetBase?

    /** The vertical focus adjustment for the [ContainerBase]. */
    public open var focusVAdjustment: Adjustment?

    /** The horizontal focus adjustment for the [ContainerBase]. */
    public open var focusHAdjustment: Adjustment?

    /**
     * Gets the type of the children supported by the container.
     * @return The data type (a GType), or *GType.G_TYPE_NONE* if no more children can be added.
     */
    public open fun childType(): ULong

    /**
     * Adds [widget] to [container][ContainerBase]. Typically used for simple containers such as
     * [window][io.gitlab.guiVista.gui.window.WindowBase], GtkFrame, or
     * [button][io.gitlab.guiVista.gui.widget.button.ButtonBase]. For more complicated layout containers such as [Box],
     * or [Grid], this function will pick default packing parameters that may not be correct. So consider functions
     * such as `gtk_box_pack_start()`, and `gtk_grid_attach()` as an alternative to [add] in those cases. A widget may
     * be added to only one container at a time. You can’t place the same widget inside two different containers.
     *
     * Note that some containers such as GtkScrolledWindow, or GtkListBox **may** add intermediate children between the
     * added widget, and the container.
     * @param widget A widget to be placed inside container.
     */
    public open infix fun add(widget: WidgetBase)

    /** Emits the *check-resize* signal on the [ContainerBase]. */
    public open fun checkResize()

    /**
     * Removes [widget] from [container][ContainerBase]. The [widget] must be inside the container. Note that container
     * will own a reference to [widget], and that this may be the last reference held. So removing a [widget] from its
     * container can destroy that [widget]. If you want to use the [widget] again you need to add a reference to it,
     * **before** removing it from a container using `g_object_ref()`. If you don’t want to use [widget] again it’s
     * usually more efficient to simply destroy it directly using `gtk_widget_destroy()` since this will remove it from
     * the container, and help break any circular reference count cycles.
     */
    public open infix fun remove(widget: WidgetBase)

    /**
     * Adds [widget] to [container][ContainerBase].
     * @see add
     */
    public open operator fun plusAssign(widget: WidgetBase)

    /**
     * Removes [widget] from [container][ContainerBase].
     * @see remove
     */
    public open operator fun minusAssign(widget: WidgetBase)
}
