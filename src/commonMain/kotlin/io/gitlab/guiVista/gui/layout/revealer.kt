package io.gitlab.guiVista.gui.layout

/** Hide and show with animation */
public expect class Revealer : BinBase {
    /**
     * Whether the container should reveal the child. Default value is *false*.
     *
     * Data binding property name: **reveal-child**
     */
    public var revealChild: Boolean

    /**
     * The animation duration in milliseconds. Default value is *250*.
     *
     * Data binding property name: **transition-duration**
     */
    public var transitionDuration: UInt

    /**
     * Whether the child is fully revealed, ie whether the transition to the revealed state is completed.
     *
     * Data binding property name: **child-revealed**
     */
    public val childRevealed: Boolean

    public companion object {
        public fun create(): Revealer
    }
}
