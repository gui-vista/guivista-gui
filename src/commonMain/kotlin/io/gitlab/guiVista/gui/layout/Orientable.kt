package io.gitlab.guiVista.gui.layout

/** An interface for flippable widgets. */
public expect interface Orientable
