package io.gitlab.guiVista.gui.printer

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase

/** Stores page setup information. */
public expect class PageSetup : ObjectBase {
    /** The paper size. */
    public var paperSize: PaperSize

    public companion object {
        public fun create(): PageSetup

        /**
         * Copies a [PageSetup].
         * @param original The [PageSetup] to copy.
         * @return A copy of [original].
         */
        public fun copy(original: PageSetup): PageSetup

        /**
         * Reads the page setup from the [file][fileName]. Returns a new [PageSetup] object with the restored page
         * setup, or *null* if an error occurred.
         * @param fileName The filename to read the page setup from.
         * @param error The error to use or *null*.
         * @return The restored page setup or *null*.
         * @see saveToFile
         */
        public fun fromFile(fileName: String, error: Error? = null): PageSetup?
    }

    /**
     * Reads the page setup from the [file][fileName].
     * @param fileName The filename to read the page setup from.
     * @param error The error to use or *null*.
     * @return A value of *true* on success.
     * @see saveToFile
     */
    public fun loadToFile(fileName: String, error: Error? = null): Boolean

    /**
     * This function saves the information from the page setup to [fileName].
     * @param fileName The file to save to.
     * @param error The error to use or *null*.
     * @return A value of *true* on success.
     */
    public fun saveToFile(fileName: String, error: Error? = null): Boolean

    /**
     * Sets the paper size of the [PageSetup] and modifies the margins according to the new [paper size][paperSize].
     * @param paperSize The paper size to use.
     */
    public fun changePaperSizeAndDefaultMargins(paperSize: PaperSize)
}
