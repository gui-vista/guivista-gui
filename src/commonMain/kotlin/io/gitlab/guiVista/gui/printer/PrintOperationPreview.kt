package io.gitlab.guiVista.gui.printer

public expect interface PrintOperationPreview {
    /**
     * Ends a preview. This function **MUST** be called to finish a custom print preview.
     */
    public open fun endPreview()

    /**
     * Returns whether the given page is included in the set of pages that have been selected for printing.
     * @param pageNum A page number.
     * @return A value of *true* if the page has been selected for printing.
     */
    public open fun pageSelectedInPreview(pageNum: Int): Boolean

    /**
     * Renders a page to the preview, using the print context that was passed to the **preview** handler together with
     * preview. A custom iprint preview should use this function in its **expose** handler to render the currently
     * selected page. Note that this function requires a suitable cairo context to be associated with the print context.
     * @param pageNum The page to render.
     */
    public open fun renderPage(pageNum: Int)
}