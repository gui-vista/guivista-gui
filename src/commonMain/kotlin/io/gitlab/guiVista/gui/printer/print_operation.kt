package io.gitlab.guiVista.gui.printer

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.gui.window.WindowBase

/** High level Printing API. */
public expect class PrintOperation : ObjectBase, PrintOperationPreview {
    /**
     * Determines whether there is a selection in your application. This can allow your application to print the
     * selection. This is typically used to make a **Selection** button sensitive. Default value is *false*.
     *
     * Data binding property name: **has-selection**
     */
    public var hasSelection: Boolean

    /**
     * The default page setup which is used by the **run** function, but it can be overridden on a per-page basis by
     * connecting to the **request-page-setup** event.
     *
     * Data binding property name: **default-page-setup**
     */
    public var defaultPageSetup: PageSetup?

    /**
     * The print settings for [PrintOperation]. This is typically used to re-establish print settings from a previous
     * print operation.
     *
     * Data binding property name: **print-settings**
     * @see run
     */
    public var printSettings: PrintSettings

    /**
     * The number of pages that will be printed. Note that this value is set during print preparation phase
     * (`GTK_PRINT_STATUS_PREPARING`), so this property should **NEVER** be called **before** the data generation phase
     * (`GTK_PRINT_STATUS_GENERATING_DATA`). You can connect to the **status-changed** event, and use the
     * [totalPagesToPrint] property when print status is `GTK_PRINT_STATUS_GENERATING_DATA`. This is typically used to
     * track the progress of the print operation.
     *
     * Default value is *-1*. Data binding property name: **n-pages-to-print**
     */
    public val totalPagesToPrint: Int

    /**
     * If *true* then the print operation will support print of selection. This allows the print dialog to show a
     * **Selection** button. Default value is *false*.
     *
     * Data binding property name: **support-selection**
     */
    public var supportSelection: Boolean

    /**
     * If *true* then the page size combo box, and orientation combo box are embedded into page setup page. Default
     * value is *false*.
     *
     * Data binding property name: **embed-page-setup**
     */
    public var embedPageSetup: Boolean

    /**
     * A String representation of the status of the print operation. The String is translated, and suitable for
     * displaying the print status e.g. in a [io.gitlab.guiVista.gui.widget.display.StatusBar]. Use
     * `gtk_print_operation_get_status()` to obtain a status value that is suitable for programmatic use.
     *
     * Data binding property name: **status-string**
     */
    public val statusString: String

    /**
     * A convenience property to find out if the print operation is finished, either successfully (
     * `GTK_PRINT_STATUS_FINISHED`), or unsuccessfully (`GTK_PRINT_STATUS_FINISHED_ABORTED`). Note, when you enable
     * print status tracking the print operation can be in a non finished state even after done has been called, as the
     * operation status then tracks the print job status on the printer.
     */
    public val isFinished: Boolean

    public companion object {
        public fun create(): PrintOperation
    }

    /**
     * Sets the number of pages in the document. This **MUST** be set to a positive number **before** the rendering
     * starts. It may be set in a **begin-print** event handler. Note that the page numbers passed to the
     * **request-page-setup**, and **draw-page** events are *0* based. For example if the user chooses to print all
     * pages then the last **draw-page** event will be for `page n_pages - 1`.
     * @param totalPages The number of pages.
     */
    public fun changeTotalPages(totalPages: Int)

    /**
     * Sets the name of the print job. The name is used to identify the job (e.g. in monitoring applications like
     * eggcups). If you don’t set a job name then a default one is picked by numbering successive print jobs.
     * @param jobName A [String] that identifies the print job.
     */
    public fun changeJobName(jobName: String)

    /**
     * Sets the current page. If this is called before the **run** function then the user will be able to select to
     * print only the current page. Note that this only makes sense for pre-paginated documents.
     * @param currentPage The current page, *0* based.
     */
    public fun changeCurrentPage(currentPage: Int)

    /**
     * If useFullPage is *true* then the transformation for the cairo context obtained from `GtkPrintContext` puts the
     * origin at the top left corner of the page (which may not be the top left corner of the sheet, depending on page
     * orientation and the number of pages per sheet). Otherwise, the origin is at the top left corner of the
     * imageable area (i.e. inside the margins).
     * @param useFullPage A value of *true* to set up the `GtkPrintContext` for the full page.
     */
    public fun changeUseFullPage(useFullPage: Boolean)

    /**
     * Sets up the [PrintOperation] to generate a file instead of showing the print dialog. The intended use of this
     * function is for implementing *Export to PDF* actions. Currently **PDF** is the only supported format. Note that
     * *Print to PDF* support is independent of this, and is done by letting the user pick the **Print to PDF** item
     * from the list of printers in the print dialog.
     * @param fileName The file name for the exported file.
     */
    public fun changeExportFileName(fileName: String)

    /**
     * If showProgress is *true* then the print operation will show a progress dialog during the print operation.
     * @param showProgress A value of *true* to show a progress dialog.
     */
    public fun changeShowProgress(showProgress: Boolean)

    /**
     * If trackStatus is *true* then the print operation will try to continue report on the status of the print job in
     * the printer queues and printer. This can allow your application to show things like *out of paper* issues, and
     * when the print job actually reaches the printer. This function is often implemented using some form of polling,
     * so it should **NOT** be enabled unless needed.
     * @param trackStatus A value of *true* to track status *after* printing.
     */
    public fun changeTrackPrintStatus(trackStatus: Boolean)

    /**
     * Sets the label for the tab holding custom widgets.
     * @param label The label to use, or *""* (an empty String) to use the default label.
     */
    public fun changeCustomTabLabel(label: String)

    /**
     * Cancels a running print operation. This function may be called from a **begin-print**, **paginate**, or
     * **draw-page** event handler to stop the currently running print operation.
     */
    public fun cancel()

    /**
     * This signalizes that drawing of a particular page is complete. It is called after completion of page drawing
     * (e.g. drawing in another thread). If [setupDeferDrawing] was called before then this function has to be called
     * by the application. In another case it is called by the library itself.
     */
    public fun drawPageFinish()

    /**
     * Sets up the [PrintOperation] to wait for calling of [drawPageFinish] from the application. It can be used for
     * drawing page in another thread. This function **MUST** be called in the callback of the **draw-page** event.
     */
    public fun setupDeferDrawing()

    /**
     * Sets whether the **run** function may return before the print operation is completed. Note that some platforms
     * may not allow asynchronous operation.
     * @param allowAsync A value of *true* to allow asynchronous operation.
     */
    public fun changeAllowAsync(allowAsync: Boolean)
}

/**
 * Runs a page setup dialog, letting the user modify the values from [pageSetup]. If the user cancels the dialog then
 * the returned [PageSetup] is identical to the passed in [pageSetup], otherwise it contains the modifications done in
 * the dialog. Note that this function may use a recursive main loop to show the page setup dialog. See
 * `gtk_print_run_page_setup_dialog_async()` if this is a problem.
 * @param parent The parent to use for the page setup dialog.
 * @param pageSetup An existing page setup.
 * @param printSettings The print settings to use.
 * @return A new page setup.
 */
public expect fun runPageSetupDialog(parent: WindowBase, pageSetup: PageSetup, printSettings: PrintSettings): PageSetup
