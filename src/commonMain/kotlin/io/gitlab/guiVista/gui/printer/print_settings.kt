package io.gitlab.guiVista.gui.printer

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase

/** Stores print settings. */
public expect class PrintSettings : ObjectBase {
    /** The paper size */
    public var paperSize: PaperSize

    /** The printer name. */
    public var printer: String

    /** Whether to use color. */
    public var useColor: Boolean

    /** Whether to collate the printed pages. */
    public var collate: Boolean

    /** Whether to reverse the order of the printed pages. */
    public var reverse: Boolean

    /** The number of copies to print. */
    public var totalCopies: Int

    /** The number of pages per sheet. */
    public var pagesPerSheet: Int

    /** The resolution in DPI. */
    public var resolution: Int

    /** The horizontal resolution in DPI. */
    public val hResolution: Int

    /** The vertical resolution in DPI. */
    public val vResolution: Int

    /** The resolution in LPI (lines per inch) */
    public var printLpi: Double

    /** The scale in percent. */
    public var scale: Double

    /** The default source. */
    public var defaultSource: String

    /** The media type as defined in PWG 5101.1-2002 PWG. */
    public var mediaType: String

    /** The dithering that is used. */
    public var dither: String

    /** The finishings. */
    public var finishings: String

    /** The output bin. */
    public var outputBin: String

    public companion object {
        public fun create(): PrintSettings

        /**
         * Reads the print settings from [fileName]. Returns a new [PrintSettings] object with the restored settings,
         * or *null* if an error occurred. If the file could not be loaded then [error] is set to either a
         * `GFileError` or `GKeyFileError`.
         * @param fileName The filename to read the settings from.
         * @param error The error to use or *null*.
         * @return The restored print settings or *null*.
         * @see saveToFile
         */
        public fun fromFile(fileName: String, error: Error? = null): PrintSettings?

        /**
         * Copies a [PrintSettings] object.
         * @param original The [PrintSettings] object to copy.
         * @return A new copy of the [original].
         */
        public fun copy(original: PrintSettings): PrintSettings
    }

    /**
     * Looks up the String value associated with [key].
     * @param key The key to use for the lookup.
     * @return The String value for [key].
     */
    public fun fetchStringValue(key: String): String

    /**
     * Associates [value] with [key].
     * @param key The key to use.
     * @param value A String value or *""* (an empty String).
     */
    public fun changeStringValue(key: String, value: String)

    /**
     * Removes any value associated with [key]. This has the same effect as setting the value to *""* (an empty String).
     * @param key The key to use.
     */
    public fun unsetValue(key: String)

    /**
     * Returns the Boolean represented by the value that is associated with [key]. The String *"true"* represents
     * *true*, any other string is *false*.
     * @param key The key to use.
     * @return A value of *true* if [key] maps to a true value.
     */
    public fun fetchBooleanValue(key: String): Boolean

    /**
     * Sets key to a boolean value.
     * @param key The key to use.
     * @param value The value to use.
     */
    public fun changeBooleanValue(key: String, value: Boolean)

    /**
     * Returns the double value associated with [key], or [default].
     * @param key The key to use.
     * @param default The default value to use if the value isn't a Double.
     * @return The Double value of [key] or [default].
     */
    public fun fetchDoubleValue(key: String, default: Double = 0.0): Double

    /**
     * Sets [key] to a Double [value].
     * @param key The key to use.
     * @param value The value to use.
     */
    public fun changeDoubleValue(key: String, value: Double)

    /**
     * Returns the integer value of [key], or [default].
     * @param key The key to use.
     * @param default The default value to use if the value isn't a Int.
     * @return The Int value of [key] or [default].
     */
    public fun fetchIntValue(key: String, default: Int = 0): Int

    /**
     * Sets [key] to a Int [value].
     * @param key The key to use.
     * @param value The value to use.
     */
    public fun changeIntValue(key: String, value: Int)

    /**
     * Sets the values of [resolution], [hResolution], and [vResolution].
     * @param hResolution The horizontal resolution in DPI.
     * @param vResolution The vertical resolution in DPI.
     */
    public fun changeHAndVResolution(hResolution: Int, vResolution: Int)

    /**
     * Reads the print settings from [fileName]. If the file could not be loaded then [error] is set to either a
     * `GFileError` or `GKeyFileError`.
     * @param fileName The filename to read the settings from.
     * @param error The error to use or *null*.
     * @return A value of *true* on success.
     * @see saveToFile
     */
    public fun loadFile(fileName: String, error: Error? = null): Boolean

    /**
     * This function saves the print settings from settings to [fileName]. If the file could not be loaded then [error]
     * is set to either a `GFileError` or `GKeyFileError`.
     * @param fileName The file to save to.
     * @param error The error to use or *null*.
     * @return A value of *true* on success.
     */
    public fun saveToFile(fileName: String, error: Error? = null): Boolean
}
