package io.gitlab.guiVista.gui.printer

import io.gitlab.guiVista.core.Closable
import io.gitlab.guiVista.core.dataType.DoublyLinkedList

/** Support for named paper sizes. */
public expect class PaperSize : Closable {
    /** The name of the [PaperSize]. */
    public val name: String

    /** The human readable name of the [PaperSize]. */
    public val displayName: String

    /** The PPD name of the [PaperSize], which may be *""* (an empty String). */
    public val ppdName: String

    /** Will be *true* if this [PaperSize] is an IPP standard paper size. */
    public val isIpp: Boolean

    /** Will be *true* if size isn't a standard paper size. */
    public val isCustom: Boolean

    /** The name of the default paper size, which depends on the current locale. */
    public val defaultSize: String

    public companion object {
        /**
         * Creates a new [PaperSize] object by parsing a PWG 5101.1-2002 paper name. If [name] is *""* (an empty
         * String) then the default paper size is returned.
         * @param name A paper size name or *""* (an empty String).
         * @return A new [PaperSize], use [close] when finished with the object.
         * @see defaultSize
         */
        public fun create(name: String = ""): PaperSize

        /**
         * Creates a new [PaperSize] object by using PPD information. If [ppdName] is not a recognized PPD paper name
         * then [ppdDisplayName], [width], and [height] are used to construct a custom [PaperSize] object.
         * @param ppdName A PPD paper name.
         * @param ppdDisplayName The corresponding human-readable name.
         * @param width The paper width in points.
         * @param height The paper height in points.
         * @return A new [PaperSize], use [close] when finished with the object.
         */
        public fun fromPpd(ppdName: String, ppdDisplayName: String, width: Double, height: Double): PaperSize

        /**
         * Creates a new [PaperSize] object by using IPP information. If [ippName] is not a recognized paper name then
         * width and height are used to construct a custom [PaperSize] object.
         * @param ippName An IPP paper name.
         * @return A new [PaperSize], use [close] when finished with the object.
         */
        public fun fromIpp(ippName: String): PaperSize

        /**
         * Copies an existing [PaperSize].
         * @param original The [PaperSize] to copy.
         * @return A copy of [original].
         */
        public fun copy(original: PaperSize): PaperSize
    }

    /**
     * Creates a list of known paper sizes.
     * @param includeCustom Whether to include custom paper sizes as defined in the page setup dialog.
     * @return A list of [PaperSize] objects.
     */
    public fun fetchPaperSizes(includeCustom: Boolean = false): DoublyLinkedList
}
