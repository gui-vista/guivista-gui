package io.gitlab.guiVista.gui.printer

public object PrintOperationEvent {
    public const val beginPrint: String = "begin-print"
    public const val drawPage: String = "draw-page"
    public const val endPrint: String = "end-print"
}
