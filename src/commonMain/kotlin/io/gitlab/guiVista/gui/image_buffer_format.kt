package io.gitlab.guiVista.gui

import io.gitlab.guiVista.core.Closable

/** Contains format information for a [ImageBuffer]. */
public expect class ImageBufferFormat : Closable {
    /** The name of the format. */
    public val name: String

    /** A description of the format. */
    public val desc: String

    /** Whether image buffers can be saved in the given format. */
    public val isWritable: Boolean

    /**
     * Whether this image format is scalable. If a file is in a scalable format then it is preferable to load it at the
     * desired size, rather than loading it at the default size, and scaling the resulting [ImageBuffer] to the desired
     * size.
     */
    public val isScalable: Boolean

    /**
     * Whether this image format is disabled. If a format is disabled then [ImageBuffer] won't use the image loader for
     * this format to load images. Applications can use this to avoid using image loaders with an inappropriate
     * license. Please look at [license].
     */
    public var disabled: Boolean

    /**
     * Returns information about the license of the image loader for the format. The returned [String] should be a
     * shorthand for a well known license, e.g. **LGPL**, **GPL**, **QPL**, **GPL/QPL**, or **other** to indicate some
     * other license.
     */
    public val license: String

    /**
     * Creates a copy of [ImageBufferFormat].
     * @return A new instance of [ImageBufferFormat]. Remember to call [close] when finished with the object.
     */
    public fun copy(): ImageBufferFormat

    /**
     * Get the mime types supported by the format.
     * @return An array of mime types.
     */
    public fun fetchMimeTypes(): Array<String>

    /**
     * The filename extensions typically used for files in the given format.
     * @return An array of filename extensions.
     */
    public fun fetchExtensions(): Array<String>
}
