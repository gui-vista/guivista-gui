package io.gitlab.guiVista.gui.text

import io.gitlab.guiVista.core.ObjectBase

/** A position in the buffer preserved across buffer modifications. */
public expect class TextMark : ObjectBase {
    /**
     * If the mark is visible. The text widget uses a visible mark to indicate where a drop will occur when dragging
     * and dropping text. Most other marks aren't visible.
     */
    public var visible: Boolean

    /**
     * Returns *true* if the mark has been removed from its buffer with `gtk_text_buffer_delete_mark()`. See
     * `gtk_text_buffer_add_mark()` for a way to add it to a buffer again.
     */
    public val deleted: Boolean

    /**
     * The mark name, or an empty String for anonymous marks.
     *
     * Data binding property name: **name**
     */
    public val name: String

    /** Gets the buffer this mark is located inside, or *null* if the mark is deleted. */
    public val buffer: TextBuffer?

    /**
     * Determines whether the mark has left gravity.
     *
     * Data binding property name: **left-gravity**
     */
    public val leftGravity: Boolean

    public companion object {
        public fun create(name: String = "", leftGravity: Boolean = false): TextMark
    }
}
