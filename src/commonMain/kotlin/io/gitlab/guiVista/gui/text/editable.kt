package io.gitlab.guiVista.gui.text

import io.gitlab.guiVista.core.ObjectBase

/** Interface for text-editing widgets. */
public expect interface Editable : ObjectBase {
    /** The current position of the cursor relative to the start of the content of the [Editable]. */
    public open var pos: Int

    /** Determines if the user can edit the text in the editable widget or not. */
    public open var isEditable: Boolean

    /**
     * Selects a region of text. The characters that are selected are those characters at positions from [startPos] up
     * to, but not including [endPos]. If [endPos] is negative then the characters selected are those characters from
     * [startPos] to the end of the text. Note that positions are specified in characters, not bytes.
     * @param startPos Start of the region.
     * @param endPos End of the region.
     */
    public open fun selectRegion(startPos: Int, endPos: Int)

    /**
     * Retrieves the selection bound of the editable. The start position will be filled with the start of the selection,
     * and end position with end. If no text was selected then both will be identical and *false* will be returned. Note
     * that positions are specified in characters, not bytes.
     * @return a Triple containing the following:
     * 1. The starting position or *null*.
     * 2. The end position or *null*
     * 3. A value of *true* if an area is selected, *false* otherwise.
     */
    public open fun fetchSelectionBounds(): Triple<Int?, Int?, Boolean>

    /**
     * Inserts [length] bytes of [newText] into the contents of the widget at the [position][pos]. Note that the
     * [position][pos] is in characters, not in bytes. The function updates the [position][pos] to point after the
     * newly inserted text.
     * @param newText The text to append.
     * @param length The length of the text in bytes or *-1*.
     * @param pos Location of the position text will be inserted at.
     */
    public open fun insertText(newText: String, length: Int, pos: Int)

    /**
     * Deletes a sequence of characters. The characters that are deleted are those characters at positions from
     * [startPos] up to, but not including [endPos]. If [endPos] is negative then the characters deleted are those from
     * [startPos] to the end of the text. Note that the positions are specified in characters, not bytes.
     * @param startPos Start position.
     * @param endPos End position.
     */
    public open fun deleteText(startPos: Int, endPos: Int)

    /**
     * Retrieves a sequence of characters. The characters that are retrieved are those characters at positions from
     * [startPos] up to, but not including [endPos]. If [endPos] is negative then the characters retrieved are those
     * characters from [startPos] to the end of the text. Note that positions are specified in characters, not bytes.
     * @param startPos Start of text.
     * @param endPos End of text.
     * @return The contents of the widget as a String.
     */
    public open fun fetchCharacters(startPos: Int, endPos: Int): String

    /** Removes the contents of the currently selected content in the [Editable] and puts it on the clipboard. */
    public open fun cutClipboard()

    /** Copies the contents of the currently selected content in the [Editable] and puts it on the clipboard. */
    public open fun copyClipboard()

    /** Pastes the content of the clipboard to the current position of the cursor in the [Editable]. */
    public open fun pasteClipboard()

    /**
     * Deletes the currently selected text of the [Editable]. This call doesn't do anything if there is no selected
     * text.
     */
    public open fun deleteSelection()
}