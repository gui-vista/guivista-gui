package io.gitlab.guiVista.gui.text

public object EntryBufferEvent {
    public const val deletedText: String = "deleted-text"
    public const val insertedText: String = "inserted-text"
}
