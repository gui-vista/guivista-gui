package io.gitlab.guiVista.gui.text

public object EditableEvent {
    public const val changed: String = "changed"
    public const val deleteText: String = "delete-text"
    public const val insertText: String = "insert-text"
}
