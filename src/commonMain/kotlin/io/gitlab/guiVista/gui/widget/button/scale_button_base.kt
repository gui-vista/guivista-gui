package io.gitlab.guiVista.gui.widget.button

import io.gitlab.guiVista.gui.Adjustment
import io.gitlab.guiVista.gui.layout.Orientable
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for a button which pops up a scale. */
public expect interface ScaleButtonBase : ButtonBase, Orientable {
    /**
     * The value of the scale. Default value is *0*.
     *
     * Data binding property name: **value**
     */
    public open var value: Double

    /** Retrieves the plus button of the scale button. */
    public open val plusButton: WidgetBase

    /** Retrieves the minus button of the scale button. */
    public open val minusButton: WidgetBase

    /**
     * The [Adjustment] that contains the current value of this scale button object.
     *
     * Data binding property name: **adjustment**
     */
    public open var adjustment: Adjustment

    /** The popup of the scale button. */
    public open val popup: WidgetBase

    /**
     * Sets the icons to be used by the scale button.
     * @param icons An Array of icon names.
     */
    public open fun changeIcons(icons: Array<String>)
}