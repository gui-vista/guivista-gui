package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.Adjustment
import io.gitlab.guiVista.gui.layout.Orientable

/** A base for widgets which visualize an adjustment. */
public expect interface Range : WidgetBase, Orientable {
    /**
     * Contains the current value of this range object.
     *
     * Data binding property name: **adjustment**
     */
    public open var adjustment: Adjustment?

    /**
     * The fill level (e.g. prebuffering of a network stream), which is best described by its most prominent use case.
     * It is an indicator for the amount of pre-buffering in a streaming media player. In that use case the value of
     * the range would indicate the current play position, and the fill level would be the position up to which the
     * file/stream has been downloaded.
     *
     * This amount of pre buffering can be displayed on the range’s trough and is themeable separately from the trough.
     * To enable fill level display use `gtk_range_set_show_fill_level()`. The range defaults to not showing the fill
     * level. Additionally it’s possible to restrict the range’s slider position to values which are smaller than the
     * fill level. This is controlled by `gtk_range_set_restrict_to_fill_level()`, and is by default enabled.
     *
     * Default value is *1.79769e+308*. Data binding property name: **fill-level**
     */
    public open var fillLevel: Double

    /**
     * Invert direction slider moves to increase range value. Default value is *false*.
     *
     * Data binding property name: **inverted**
     */
    public open var inverted: Boolean

    /**
     * Controls whether slider movement is restricted to an upper boundary set by the fill level. Default value is
     * *true*.
     *
     * Data binding property name: **restrict-to-fill-level**
     */
    public open var restrictToFillLevel: Boolean

    /**
     * The number of digits to round the value to when it changes, or -1. Default value is *-1*.
     *
     * Data binding property name: **round-digits**
     */
    public open var roundDigits: Int

    /**
     * Controls whether fill level indicator graphics are displayed on the trough. Default value is *false*.
     *
     * Data binding property name: **show-fill-level**
     */
    public open var showFillLevel: Boolean
}