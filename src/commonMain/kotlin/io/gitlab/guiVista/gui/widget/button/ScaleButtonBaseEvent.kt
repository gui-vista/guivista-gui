package io.gitlab.guiVista.gui.widget.button

public object ScaleButtonBaseEvent {
    public const val popdown: String = "popdown"
    public const val popup: String = "popup"
    public const val valueChanged: String = "value-changed"
}
