package io.gitlab.guiVista.gui.widget.menu.item

/** Base interface for check menu item objects. */
public expect interface CheckMenuItemBase : MenuItemBase {
    /**
     * Whether the menu item is checked. Default value is *false*.
     *
     * Data binding property name: **active**
     */
    public open var active: Boolean

    /**
     * Whether the menu item looks like a radio menu item. Default value is *false*.
     *
     * Data binding property name: **draw-as-radio**
     */
    public open var drawAsRadio: Boolean

    /**
     * Whether to display an "inconsistent" state. Default value is *false*.
     *
     * Data binding property name: **inconsistent**
     */
    public open var inconsistent: Boolean
}
