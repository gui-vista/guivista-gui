package io.gitlab.guiVista.gui.widget.menu

import io.gitlab.guiVista.gui.keyboard.AcceleratorGroup
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for menu objects. */
public expect interface MenuBase : MenuShell {
    /**
     * The monitor the menu will be popped up on. Default value is *-1*.
     *
     * Data binding property name: **monitor**
     */
    public open var monitor: Int

    /**
     * A Boolean that indicates whether the menu reserves space for toggles and icons, regardless of their actual
     * presence. This property should only be changed from its default value for special-purposes such as tabular
     * menus. Regular menus that are connected to a menu bar, or context menus should reserve toggle space for
     * consistency.
     *
     * Data binding property name: **reserve-toggle-size**
     */
    public open var reserveToggleSize: Boolean

    /**
     * An accel path used to conveniently construct accel paths of child items. Default value is *""* (an empty
     * String).
     *
     * Data binding property name: **accel-path**
     */
    public open var accelPath: String

    /**
     * The accelerator group that holds global accelerators for this menu.
     *
     * Data binding property name: **accel-group**
     */
    public open var accelGroup: AcceleratorGroup

    /**
     * Moves child to a new [position][pos] in the list of menu children.
     * @param child The menu item to move.
     * @param pos The new position to place the [child]. Positions are numbered from 0 to n - 1.
     */
    public open fun reorderChild(child: WidgetBase, pos: Int)

    /**
     * Adds a new menu item to a (table) menu. The number of **cells** that an item will occupy is specified by
     * [leftAttach], [rightAttach], [topAttach], and [bottomAttach]. Each of these represent the leftmost, rightmost,
     * uppermost, and lower column and row numbers of the table. (Columns and rows are indexed from zero).
     *
     * Note that this function isn't related to [detach].
     * @param child The menu item to attach.
     * @param leftAttach The column number to attach the left side of the item to.
     * @param rightAttach The column number to attach the right side of the item to.
     * @param topAttach The row number to attach the top of the item to.
     * @param bottomAttach The row number to attach the bottom of the item to.
     */
    public open fun attach(child: WidgetBase, leftAttach: UInt, rightAttach: UInt, topAttach: UInt, bottomAttach: UInt)

    /**
     * Gets the selected menu item from the menu. This is used by the `GtkComboBox`.
     * @return The menu item that was last selected in the menu. If a selection has not yet been made then the first
     * menu item is selected.
     */
    public open fun fetchActive(): WidgetBase?

    /**
     * Selects the specified menu item within the menu. This is used by the `GtkComboBox` and should not be used by
     * anyone else.
     * @param index The index of the menu item to select. Index values are from 0 to n-1.
     */
    public open fun changeActive(index: UInt)

    /** Gets the widget that the menu is attached to. */
    public open fun fetchAttachWidget(): WidgetBase?

    /** Removes the menu from the screen. */
    public open fun popDown()

    /** Repositions the menu according to its position function. */
    public open fun reposition()

    /**
     * Detaches the menu from the widget to which it had been attached. This function will call the callback function
     * *detacher*, which is provided when the `gtk_menu_attach_to_widget` function was called.
     */
    public open fun detach()
}
