package io.gitlab.guiVista.gui.widget

public object WidgetBaseEvent {
    public const val grabFocus: String = "grab-focus"
    public const val show: String = "show"
    public const val hide: String = "hide"
    public const val configure: String = "configure-event"
    public const val keyPress: String = "key-press-event"
    public const val keyRelease: String = "key-release-event"
    public const val activate: String = "activate"
    public const val realize: String = "realize"
    public const val sizeAllocate: String = "size-allocate"
    public const val draw: String = "draw"
}
