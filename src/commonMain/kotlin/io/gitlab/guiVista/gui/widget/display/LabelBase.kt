package io.gitlab.guiVista.gui.widget.display

import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for label objects. */
public expect interface LabelBase : WidgetBase {
    /** The label's string contents. Default value is *""* (an empty String). */
    public open var text: String

    /**
     * The desired maximum width of the label in characters. If this property is set to *-1* the width will be
     * calculated automatically. See the section on text layout for details of how “width-chars” and “max-width-chars”
     * determine the width of ellipsized and wrapped labels. Default value is *-1*.
     *
     * Data binding property name: **max-width-chars**
     */
    public open var maxWidthChars: Int

    /**
     * The desired width of the label in characters. If this property is set to *-1* the width will be calculated
     * automatically. See the section on text layout for details of how widthChars and [maxWidthChars] determine
     * the width of ellipsized and wrapped labels. Default value is *-1*.
     *
     * Data binding property name: **width-chars**
     */
    public open var widthChars: Int

    /**
     * The angle that the baseline of the label makes with the horizontal, in degrees, measured counterclockwise. An
     * angle of 90 reads from from bottom to top, an angle of 270, from top to bottom. Ignored if the label is
     * selectable. Default value is *0.0*.
     *
     * Data binding property name: **angle**
     */
    public open var angle: Double

    /**
     * The contents of the label. If the string contains Pango XML markup you will have to set the “use-markup”
     * property to *true* in order for the label to display the markup attributes. See also `gtk_label_set_markup()`
     * for a convenience function that sets both this property, and the “use-markup” property at the same time.
     *
     * If the string contains underlines acting as mnemonics you will have to set the “use-underline” property to
     * *true* in order for the label to display them. Default value is *""* (an empty String).
     *
     * Data binding property name: **label**
     */
    public open var label: String

    /**
     * The number of lines to which an "ellipsized", wrapping label should be limited. This property has no effect if
     * the label is not wrapping or "ellipsized". Set this property to *-1* if you don't want to limit the number of
     * lines. Default value is *-1*.
     *
     * Data binding property name: **lines**
     */
    public open var lines: Int

    /**
     * The mnemonic accelerator key for this label.
     *
     * Data binding property name: **mnemonic-keyval**
     */
    public open val mnemonicKeyVal: UInt

    /**
     * The widget to be activated when the label's mnemonic key is pressed.
     *
     * Data binding property name: **mnemonic-widget**
     */
    public open var mnemonicWidget: WidgetBase?

    /**
     * Whether the label text can be selected with the mouse. Default value is *false*.
     *
     * Data binding property name: **selectable**
     */
    public open var selectable: Boolean

    /**
     * Whether the label is in single line mode. In single line mode the height of the label does not depend on the
     * actual text, it is always set to ascent + descent of the font. This can be an advantage in situations where
     * resizing the label because of text changes would be distracting, e.g. in a statusbar.
     *
     * Default value is *false*. Data binding property name: **single-line-mode**
     */
    public open var singleLineMode: Boolean

    /**
     * Set this property to *true* to make the label track which links have been visited. It will then apply the
     * GTK_STATE_FLAG_VISITED when rendering this link in addition to GTK_STATE_FLAG_LINK. Default value is *true*.
     *
     * Data binding property name: **track-visited-links**
     */
    public open var trackVisitedLinks: Boolean

    /**
     * The text of the label includes XML markup. See pango_parse_markup(). Default value is *false*.
     *
     * Data binding property name: **use-markup**
     */
    public open var useMarkup: Boolean

    /**
     * If set an underline in the text indicates the next character should be used for the mnemonic accelerator key.
     * Default value is *false*.
     *
     * Data binding property name: **use-underline**
     */
    public open var useUnderline: Boolean

    /** If set wrap lines if the text becomes too wide. Default value is *false*. */
    public open var lineWrap: Boolean

    /**
     * This property determines the horizontal alignment of the label text inside the labels size allocation. Compare
     * this to “halign”, which determines how the labels size allocation is positioned in the space available for the
     * label. Default value is *0.5*.
     *
     * Data binding property name: **xalign**
     */
    public open var xAlign: Float

    /**
     * This property determines the horizontal alignment of the label text inside the labels size allocation. Compare
     * this to “valign”, which determines how the labels size allocation is positioned in the space available for the
     * label. Default value is *0.5*.
     *
     * Data binding property name: **yalign**
     */
    public open var yAlign: Float

    /**
     * Parses [str] which is marked up with the Pango text markup language, setting the label’s text and attribute list
     * based on the parse results. If characters in [str] are preceded by an underscore then they are underlined
     * indicating that they represent a keyboard accelerator called a mnemonic. The mnemonic key can be used to
     * activate another widget, chosen automatically, or explicitly using [mnemonicWidget].
     * @param str A markup string (see Pango markup format).
     */
    public open fun changeMarkupWithMnemonic(str: String)
}
