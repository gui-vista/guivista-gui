package io.gitlab.guiVista.gui.widget.tree

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.gui.cell.CellAreaBase
import io.gitlab.guiVista.gui.cell.CellLayout
import io.gitlab.guiVista.gui.cell.renderer.CellRendererBase
import io.gitlab.guiVista.gui.widget.WidgetBase

/** A visible column in a Tree View widget. */
public expect class TreeViewColumn : ObjectBase, CellLayout {
    /**
     * The title to appear in column header. Default value is *""* (an empty String).
     *
     * Data binding property name: **title**
     */
    public var title: String

    /**
     * Space which is inserted between cells. Default value is *0*.
     *
     * Data binding property name: **spacing**
     */
    public var spacing: Int

    /**
     * Whether to display the column. Default value is *true*.
     *
     * Data binding property name: **visible**
     */
    public var visible: Boolean

    /**
     * Whether the column is user-resizable. Default value is *false*.
     *
     * Data binding property name: **resizable**
     */
    public var resizable: Boolean

    /**
     * Current width of the column. Default value is *0*.
     *
     * Data binding property name: **width**
     */
    public val width: Int

    /**
     * Current fixed width of the column. Default value is *-1*.
     *
     * Data binding property name: **fixed-width**
     */
    public var fixedWidth: Int

    /**
     * Maximum allowed width of the column. Default value is *-1*.
     *
     * Data binding property name: **max-width**
     */
    public var maxWidth: Int

    /**
     * Minimum allowed width of the column. Default value is *-1*.
     *
     * Data binding property name: **min-width**
     */
    public var minWidth: Int

    /**
     * Whether column gets share of extra width allocated to the widget. Default value is *false*.
     *
     * Data binding property name: **expand**
     */
    public var expand: Boolean

    /**
     * Whether the header can be clicked. Default value is *false*.
     *
     * Data binding property name: **clickable**
     */
    public var clickable: Boolean

    /**
     * The widget to put in the column header button instead of column title. If *null* then column title is used
     * instead.
     *
     * Data binding property name: **widget**
     */
    public var widget: WidgetBase?

    /**
     * The X Alignment of the column header text, or widget.
     *
     * Data binding property name: **alignment**
     */
    public var alignment: Float

    /**
     * Whether the column can be reordered around the headers. Default value is *false*.
     *
     * Data binding property name: **reorderable**
     */
    public var reorderable: Boolean

    /**
     * Logical sort column ID this column sorts on when selected for sorting. Setting the sort column ID makes the
     * column header clickable. Set to *-1* to make the column unsortable. Default value is *-1*.
     *
     * Data binding property name: **sort-column-id**
     */
    public var sortColumnId: Int

    /**
     * Whether to show a sort indicator. Default value is *false*.
     *
     * Data binding property name: **sort-indicator**
     */
    public var sortIndicator: Boolean

    /**
     * Returns the [TreeView] where the tree column has been inserted. If column is currently not inserted in any tree
     * view then *null* is returned.
     */
    public val treeView: TreeView?

    /**
     * Returns the current X offset of the tree column in pixels.
     *
     * Data binding property name: **x-offset**
     */
    public val xOffset: Int

    public companion object {
        /**
         * Creates a new [TreeViewColumn].
         * @return A new [TreeViewColumn].
         */
        public fun create(): TreeViewColumn

        /**
         * Creates a new [TreeViewColumn] using [area] to render its cells.
         * @param area The cell area that the newly created column should use to layout cells.
         * @return A new [TreeViewColumn].
         */
        public fun createWithArea(area: CellAreaBase): TreeViewColumn
    }

    /**
     * Packs the cell into the beginning of the column. If [expand] is *false* then the cell is allocated no more space
     * than it needs. Any unused space is divided evenly between cells for which expand is *true*.
     * @param cell The cell renderer to use.
     * @param expand Is *true* if [cell] is to be given extra space allocated to the tree column.
     */
    public override fun prependCell(cell: CellRendererBase, expand: Boolean)

    /**
     * Packs the cell to the end of the column. If [expand] is *false* then the cell is allocated no more space
     * than it needs. Any unused space is divided evenly between cells for which expand is *true*.
     * @param cell The cell renderer to use.
     * @param expand Is *true* if [cell] is to be given extra space allocated to the tree column.
     */
    public override fun appendCell(cell: CellRendererBase, expand: Boolean)

    /** Unsets all the mappings on all renderers on the tree column. */
    public override fun clear()

    /**
     * Adds an attribute mapping to the list in the tree column. The [column] is the column of the model to get a value
     * from, and the attribute is the parameter on [cell] to be set from the value. So for example if column 2
     * of the model contains strings, then you could have the **text** attribute of a
     * [io.gitlab.guiVista.gui.cell.renderer.ToggleCellRenderer] get its values from column 2.
     * @param cell The cell renderer to set attributes on.
     * @param attr An attribute on the [renderer][cell].
     * @param column The column position on the model to get the [attribute][attr] from.
     */
    public override fun addAttribute(cell: CellRendererBase, attr: String, column: Int)

    /**
     * Adds multiple attributes to the list in the tree column.
     * @param cell The cell renderer to set attributes on.
     * @param attributes Each attribute is represented as a Pair containing the following:
     * 1. attrName - Attribute name.
     * 2. column - Column number.
     * @see addAttribute
     */
    public override fun addMultipleAttributes(cell: CellRendererBase, vararg attributes: Pair<String, Int>)

    /**
     * Clears all existing attributes that have been previously set.
     * @param cell The cell renderer to clear the attribute mapping on.
     */
    public override fun clearAttributes(cell: CellRendererBase)

    /**
     * Obtains the horizontal position, and size of a cell in a column. If the cell is not found in the column, then
     * the start position and width are not changed and *false* is returned.
     * @return A Triple containing the following:
     * 1. xOffset - The horizontal position of the cell within the tree column.
     * 2. width - The width of the cell.
     * 3. inTreeColumn - If *true* then the cell belongs to the tree column.
     */
    public fun fetchCellPosition(cellRenderer: CellRendererBase): Triple<Int, Int, Boolean>

    /**
     * Sets the current keyboard focus to be at the [cell], if the column contains 2 or more editable and activatable
     * cells.
     * @param cell The cell that is to have focus.
     */
    public fun focusCell(cell: CellRendererBase)

    /**
     * Flags the column, and the cell renderers added to this column to have their sizes renegotiated.
     */
    public fun resizeQueue()
}
