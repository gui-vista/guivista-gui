package io.gitlab.guiVista.gui.widget.dataEntry

import io.gitlab.guiVista.gui.event.EventBase
import io.gitlab.guiVista.gui.layout.BinBase

/** A toolbar that integrates a search entry. */
public expect class SearchBar : BinBase {
    /**
     * Whether to show the close button in the toolbar. Default value is *false*.
     *
     * Data binding property name: **show-close-button**
     */
    public var showCloseButton: Boolean

    /**
     * Whether the search mode is on or off.
     *
     * Data binding property name: **search-mode-enabled**
     */
    public var searchMode: Boolean

    public companion object {
        public fun create(): SearchBar
    }

    /**
     * Connects the Entry widget passed as the one to be used in this search bar. The entry should be a descendant of
     * the search bar. This is only required if the entry isn’t the direct child of the search bar (as in our main
     * example).
     * @param entry The Entry widget to connect to the [SearchBar].
     */
    public fun connectEntry(entry: EntryBase)

    /**
     * This function should be called when the top-level window which contains the search bar received a key event. If
     * the key event is handled by the search bar then the bar will be shown, the entry populated with the entered
     * text and `GDK_EVENT_STOP` will be returned. The caller should ensure that events are not propagated further.
     *
     * If no entry has been connected to the search bar, using [connectEntry] then this function will return
     * immediately with a warning.
     * @param event The event containing the key press events.
     * @return A value of `GDK_EVENT_STOP` if the key press event resulted in text being entered in the search entry
     * (and revealing the search bar if necessary), `GDK_EVENT_PROPAGATE` otherwise.
     */
    public fun handleEvent(event: EventBase): Boolean
}
