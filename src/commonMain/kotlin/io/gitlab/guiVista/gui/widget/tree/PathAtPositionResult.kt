package io.gitlab.guiVista.gui.widget.tree

import io.gitlab.guiVista.gui.tree.TreePath

/**
 * Contains results that are returned from the [TreeView.fetchPathAtPosition] function.
 * @param path The tree path.
 * @param column The tree view column.
 * @param cellXPos The X coordinate relative to the cell's placement.
 * @param cellYPos The Y coordinate relative to the cell's placement.
 * @param rowExists If *true* then the row exists at the coordinates.
 */
public data class PathAtPositionResult(
    val path: TreePath,
    val column: TreeViewColumn,
    val cellXPos: Int,
    val cellYPos: Int,
    val rowExists: Boolean
)
