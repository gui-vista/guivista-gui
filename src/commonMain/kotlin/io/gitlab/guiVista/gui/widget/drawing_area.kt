package io.gitlab.guiVista.gui.widget

/** A widget used for creating custom UI elements. You can draw on this widget. */
public expect class DrawingArea : WidgetBase {
    public companion object {
        /**
         * Creates a new [DrawingArea].
         * @return A new [DrawingArea] instance.
         */
        public fun create(): DrawingArea
    }
}
