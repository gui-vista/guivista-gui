package io.gitlab.guiVista.gui.widget.tool

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.gui.SizeGroup

/** An interface for containers containing tool item widgets. */
public expect interface ToolShell : ObjectBase {
    /**
     * Retrieves the current text alignment for the [ToolShell]. Tool items must not call this function directly, but
     * rely on [ToolItem's][io.gitlab.guiVista.gui.widget.tool.item.ToolItem]
     * [textAlignment][io.gitlab.guiVista.gui.widget.tool.item.ToolItem.textAlignment] property instead.
     */
    public open val textAlignment: Float

    /**
     * Retrieves the current text size group for the [ToolShell]. Tool items must not call this function directly, but
     * rely on [ToolItem's][io.gitlab.guiVista.gui.widget.tool.item.ToolItem]
     * [textSizeGroup][io.gitlab.guiVista.gui.widget.tool.item.ToolItem.textSizeGroup] property instead.
     */
    public open val textSizeGroup: SizeGroup

    /**
     * Calling this function signals the [ToolShell] that the overflow menu item for tool items have changed. If there
     * is an overflow menu, and if it is visible when this function is called then the menu **will** be rebuilt. Tool
     * items must not call this function directly, but rely on
     * [ToolItem's][io.gitlab.guiVista.gui.widget.tool.item.ToolItem]
     * [rebuildMenu][io.gitlab.guiVista.gui.widget.tool.item.ToolItem.rebuildMenu] function instead.
     */
    public open fun rebuildMenu()
}
