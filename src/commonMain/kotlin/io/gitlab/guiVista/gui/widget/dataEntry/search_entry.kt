package io.gitlab.guiVista.gui.widget.dataEntry

import io.gitlab.guiVista.gui.event.EventBase

/** An entry which shows a search icon. */
public expect class SearchEntry : EntryBase {
    public companion object {
        public fun create(): SearchEntry
    }

    /**
     * This function should be called when the top level window which contains the search entry received a key event.
     * If the entry is part of a GtkSearchBar it is preferable to call `gtk_search_bar_handle_event()` instead, which
     * will reveal the entry in addition to passing the event to this function. If the key event is handled by the
     * [SearchEntry] and starts or continues a search, *GDK_EVENT_STOP* will be returned. The caller should ensure that
     * the entry is shown in this case, and not propagate the event further.
     * @param event A key event.
     * @return *GDK_EVENT_STOP* if the key press event resulted in a search beginning or continuing. Otherwise return
     * *GDK_EVENT_PROPAGATE*.
     */
    public fun handleEvent(event: EventBase): Boolean
}
