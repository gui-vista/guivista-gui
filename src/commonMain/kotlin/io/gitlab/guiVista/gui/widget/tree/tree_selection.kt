package io.gitlab.guiVista.gui.widget.tree

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreeModelIterator
import io.gitlab.guiVista.gui.tree.TreePath

/** The selection object for [TreeView]. */
public expect class TreeSelection : ObjectBase {
    /** The tree view associated with this [TreeSelection]. */
    public val treeView: TreeView

    /** The number of rows that are selected. */
    public val totalSelectedRows: Int

    /**
     * Sets the iterator to the currently selected node if selection is set to `GTK_SELECTION_SINGLE`, or
     * `GTK_SELECTION_BROWSE`. Note that the iterator may be *null* if you just want to test if selection has any
     * selected nodes. The model is filled with the current model as a convenience. This function will not work if
     * selection is `GTK_SELECTION_MULTIPLE`.
     * @return A Triple containing the following:
     * 1. model - The tree model.
     * 2. iter - The tree model iterator.
     * 3. selectedNode - If *true* then the node is selected.
     */
    public fun fetchSelected(): Triple<TreeModelBase?, TreeModelIterator?, Boolean>

    /**
     * Creates a list of path of all selected rows.
     * @param model The tree model or *null*.
     * @return A list containing a [TreePath] for each selected row.
     */
    public fun fetchSelectedRows(model: TreeModelBase? = null): DoublyLinkedList

    /**
     * Select the row at [path].
     * @param path The tree path to be selected.
     */
    public fun selectPath(path: TreePath)

    /**
     * Unselects the row at [path].
     * @param path The tree path to be unselected.
     */
    public fun unselectPath(path: TreePath)

    /**
     * Returns *true* if the row pointed to by [path] is currently selected. If [path] does not point to a valid
     * location then FALSE is returned.
     * @param path A tree path to check selection on.
     * @return A value of *true* if the [path] is selected.
     */
    public fun pathIsSelected(path: TreePath): Boolean

    /**
     * Selects the specified [iterator][iter].
     * @param iter The iterator to be selected.
     */
    public fun selectIterator(iter: TreeModelIterator)

    /**
     * Unselects the specified [iterator][iter].
     * @param iter The iterator to be unselected.
     */
    public fun unselectIterator(iter: TreeModelIterator)

    /**
     * Returns *true* if the row at [iter] is currently selected.
     * @param iter A valid iterator.
     * @return A value of *true* if [iter] is selected.
     */
    public fun iteratorIsSelected(iter: TreeModelIterator): Boolean

    /**
     * Selects all the nodes. Note that selection must be set to `GTK_SELECTION_MULTIPLE` mode.
     */
    public fun selectAll()

    /** Unselects all the nodes. */
    public fun unselectAll()

    /**
     * Selects a range of nodes determined by [startPath], and [endPath] inclusive. Note that selection must be set to
     * `GTK_SELECTION_MULTIPLE` mode.
     * @param startPath The initial node of the range.
     * @param endPath The final node of the range.
     */
    public fun selectRange(startPath: TreePath, endPath: TreePath)

    /**
     * Unselects a range of nodes determined by [startPath] and [endPath] inclusive.
     * @param startPath The initial node of the range.
     * @param endPath The final node of the range.
     */
    public fun unselectRange(startPath: TreePath, endPath: TreePath)
}
