package io.gitlab.guiVista.gui.widget.display

import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.layout.Orientable
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Report important messages to the user. */
public expect class InfoBar : ContainerBase, Orientable {
    /** The action area of the [InfoBar]. */
    public val actionArea: WidgetBase

    /** The content area of the [InfoBar]. */
    public val contentArea: WidgetBase

    /**
     * Whether to include a standard close button. Default value is *false*.
     *
     * Data binding property name: **show-close-button**
     */
    public var showCloseButton: Boolean

    public companion object {
        public fun create(): InfoBar
    }

    /**
     * Adds more buttons which is the same as calling `addButton` repeatedly. Does the same thing as
     * `gtk_info_bar_add_buttons`.
     * @param buttons Pairs where each one consists of a Response ID, and button text.
     */
    public fun addMultipleButtons(vararg buttons: Pair<Int, String>)

    /** Emits the *response* signal with the given [Response ID][responseId] .*/
    public fun response(responseId: Int)
}
