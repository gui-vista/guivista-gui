package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.cell.CellAreaBase
import io.gitlab.guiVista.gui.tree.TreeModelBase

/** A widget used to choose from a list of items. */
public expect class ComboBox : ComboBoxBase {
    public companion object {
        /**
         * Creates a new [ComboBox]
         * @return The new [ComboBox].
         */
        public fun create(): ComboBox

        /**
         * Creates a new empty [ComboBox] with an entry.
         * @return The new [ComboBox].
         */
        public fun createWithEntry(): ComboBox

        /**
         * Creates a new [ComboBox] with the model initialized to [model].
         * @param model The model to use.
         * @return The new [ComboBox].
         */
        public fun createWithModel(model: TreeModelBase): ComboBox

        /**
         * Creates a new [ComboBox] with an entry, and the model initialized to [model].
         * @param model The model to use.
         * @return The new [ComboBox].
         */
        public fun createWithModelAndEntry(model: TreeModelBase): ComboBox

        /**
         * Creates a new empty [ComboBox] using [area] to layout cells.
         * @param area The cell area to use to layout cell renderers.
         * @return The new [ComboBox].
         */
        public fun createWithArea(area: CellAreaBase): ComboBox

        /**
         * Creates a new empty [ComboBox] with an entry. The new combo box will use [area] to layout cells.
         * @param area The cell area to use to layout cell renderers.
         * @return The new [ComboBox].
         */
        public fun createWithAreaAndEntry(area: CellAreaBase): ComboBox
    }
}
