package io.gitlab.guiVista.gui.widget

public object OpenGlAreaEvent {
    public const val createContext: String = "create-context"
    public const val render: String = "render"
    public const val resize: String = "resize"
}
