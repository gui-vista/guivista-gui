package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.layout.Orientable

/** Base interface for the separator widget. */
public expect interface SeparatorBase : WidgetBase, Orientable
