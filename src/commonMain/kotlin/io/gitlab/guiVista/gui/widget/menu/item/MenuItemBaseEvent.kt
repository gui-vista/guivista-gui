package io.gitlab.guiVista.gui.widget.menu.item

public object MenuItemBaseEvent {
    public const val activate: String = "activate"
    public const val activateItem: String = "activate-item"
    public const val deselect: String = "deselect"
    public const val select: String = "select"
}
