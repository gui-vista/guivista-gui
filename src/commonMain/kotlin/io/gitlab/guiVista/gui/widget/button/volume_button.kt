package io.gitlab.guiVista.gui.widget.button

/** A button which pops up a volume control. */
public expect class VolumeButton : ScaleButtonBase {
    public companion object {
        public fun create(): VolumeButton
    }
}
