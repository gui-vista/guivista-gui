package io.gitlab.guiVista.gui.widget

public object ComboBoxBaseEvent {
    public const val changed: String = "changed"
    public const val formatEntryText: String = "format-entry-text"
    public const val moveActive: String = "move-active"
    public const val popdown: String = "popdown"
    public const val popup: String = "popup"
}
