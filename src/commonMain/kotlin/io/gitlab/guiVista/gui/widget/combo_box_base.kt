package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.cell.CellEditableBase
import io.gitlab.guiVista.gui.cell.CellLayout
import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreeModelIterator

/** Base interface for a widget used to choose from a list of items. */
public expect interface ComboBoxBase : BinBase, CellEditableBase, CellLayout {
    /**
     * If this property is set to a positive value then the list will be displayed in multiple columns. The number of
     * columns is determined by this property. Default value is *0*.
     *
     * Data binding property name: **wrap-width**
     */
    public open var wrapWidth: Int

    /**
     * If this is set to a non negative value, it must be the index of a column of type `G_TYPE_INT` in the model. The
     * values of that column are used to determine how many rows a value in the list will span. Therefore the values in
     * the model column pointed to by this property must be greater than zero and not larger than [wrapWidth].
     * Default value is *-1*.
     *
     * Data binding property name: **row-span-column**
     */
    public open var rowSpanColumn: Int

    /**
     * If this is set to a non negative value, it must be the index of a column of type `G_TYPE_INT` in the model. The
     * values of that column are used to determine how many columns a value in the list will span. Default value is
     * *-1*.
     *
     * Data binding property name: **column-span-column**
     */
    public open var columnSpanColumn: Int

    /**
     * The item which is currently active. If the model is a non flat tree model, and the active item is not an
     * immediate child of the root of the tree then this property has the value `gtk_tree_path_get_indices (path)[0]`,
     * where path is the [io.gitlab.guiVista.gui.tree.TreePath] of the active item. Default value is *-1*.
     *
     * Data binding property name: **active**
     */
    public open var active: Int

    /**
     * The column in the combo box's model that provides String IDs for the values in the model, if != -1. Default
     * value is *-1*.
     *
     * Data binding property name: **id-column**
     */
    public open var idColumn: Int

    /**
     * The value of the ID column of the active row. Default value is *null*.
     *
     * Data binding property name: **active-id**
     */
    public open var activeId: String?

    /**
     * The model from which the combo box takes the values shown in the list.
     *
     * Data binding property name: **model**
     */
    public open var model: TreeModelBase

    /**
     * Whether the combo box has an entry.
     *
     * Data binding property name: **has-entry**
     */
    public open val hasEntry: Boolean

    /**
     * The column in the combo box's model to associate with Strings from the entry if the combo was created with
     * [hasEntry] set to *true*. Default value is *-1*.
     *
     * Data binding property name: **entry-text-column**
     */
    public open var entryTextColumn: Int

    /**
     * Whether the popup's width should be a fixed width matching the allocated width of the combo box. Default value
     * is *true*.
     *
     * Data binding property name: **popup-fixed-width**
     */
    public open var popupFixedWidth: Boolean

    /** The currently active iterator. When set to *null* the active iterator is unset. */
    public open var activeIterator: TreeModelIterator?

    open override fun disconnectEvent(handlerId: ULong)
}
