package io.gitlab.guiVista.gui.widget.button

import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.widget.PopoverBase
import io.gitlab.guiVista.gui.widget.menu.MenuBase
import io.gitlab.guiVista.io.application.menu.MenuModelBase

/** A widget that shows a popup when clicked on. */
public expect class MenuButton : ToggleButtonBase {
    /**
     * The widget to use to align the menu with.
     *
     * Data binding property name: **align-widget**
     */
    public var alignWidget: ContainerBase

    /**
     * The menu model from which the popup will be created. Depending on the [usePopover] property that may be a menu,
     * or a popover.
     *
     * Data binding property name: **menu-model**
     * @see menuModel
     */
    public var menuModel: MenuModelBase

    /**
     * The popover that will be popped up when the button is clicked.
     *
     * Data binding property name: **popover**
     */
    public var popover: PopoverBase

    /**
     * The menu that will be popped up when the button is clicked.
     *
     * Data binding property name: **popup**
     */
    public var popup: MenuBase

    /**
     * Whether to construct a popover from the menu model, or a [MenuBase]. Default value is *true*.
     *
     * Data binding property name: **use-popover**
     */
    public var usePopover: Boolean

    public companion object {
        public fun create(): MenuButton
    }
}
