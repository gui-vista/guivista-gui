package io.gitlab.guiVista.gui.widget

/** Default implementation of [SeparatorBase]. */
public expect class Separator : SeparatorBase
