package io.gitlab.guiVista.gui.widget.display

public object LevelBarEvent {
    public const val offsetChanged: String = "offset-changed"
}
