package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.core.dataType.variant.Variant

/** An interface for widgets that can be associated with actions */
public expect interface Actionable {
    /**
     * The name of the action with which this widget should be associated. Setting this property with an empty String
     * will unassociate this widget from any previous action.
     */
    public open var actionName: String

    /** The target value of an actionable widget. Setting this property with a *null* will unset the target value. */
    public open var actionTargetValue: Variant?

    /**
     * Sets the target of an actionable widget. This is a convenience function that creates a [Variant] for
     * [formatStr], and uses the result to set [actionTargetValue]. If you are setting a String valued target, and want
     * to set the action name at the same time, then you can use [changeDetailedActionName].
     * @param formatStr The format String.
     */
    public open infix fun changeActionTarget(formatStr: String)

    /**
     * Sets the action name, and associated String target value of an actionable widget. This allows for the effect of
     * both setting [actionName], and [actionTargetValue] in the common case that the target is String valued. Note
     * that [detailedActionName] is a String of the form **action::target** where action is the action name, and target
     * is the String to use as the target.
     */
    public open infix fun changeDetailedActionName(detailedActionName: String)
}
