package io.gitlab.guiVista.gui.widget.textEditor

import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.text.TextBuffer
import io.gitlab.guiVista.gui.text.TextBufferIterator
import io.gitlab.guiVista.gui.text.TextMark
import io.gitlab.guiVista.gui.widget.WidgetBase

/** A widget that displays a [io.gitlab.guiVista.gui.text.TextBuffer]. */
public expect class TextView : ContainerBase {
    /**
     * The buffer which is displayed.
     *
     * Data binding property name: **buffer**
     */
    public var buffer: TextBuffer

    /**
     * Whether the text can be modified by the user. Default value is *true*.
     *
     * Data binding property name: **editable**
     */
    public var editable: Boolean

    /**
     * If the insertion cursor is shown. Default value is *true*.
     *
     * Data binding property name: **cursor-visible**
     */
    public var cursorVisible: Boolean

    /**
     * Whether entered text overwrites existing contents. Default value is *false*.
     *
     * Data binding property name: **overwrite**
     */
    public var overwrite: Boolean

    /**
     * Pixels of blank space above paragraphs. Default value is *0*.
     *
     * Data binding property name: **pixels-above-lines**
     */
    public var pixelsAboveLines: Int

    /**
     * Pixels of blank space below paragraphs. Default value is *0*.
     *
     * Data binding property name: **pixels-below-lines**
     */
    public var pixelsBelowLines: Int

    /**
     * Pixels of blank space between wrapped lines in a paragraph. Default value is *0*.
     *
     * Data binding property name: **pixels-inside-wrap**
     */
    public var pixelsInsideWrap: Int

    /**
     * The left margin for text in the text view. Note that this property is confusingly named. In CSS terms the
     * value set here is padding, and it is applied in addition to the padding from the theme. Don't confuse this
     * property with **margin-left**.
     *
     * Data binding property name: **left-margine**
     */
    public var leftMargin: Int

    /**
     * The right margin for text in the text view. Note that this property is confusingly named. In CSS terms the
     * value set here is padding, and it is applied in addition to the padding from the theme. Don't confuse this
     * property with **margin-right**.
     *
     * Data binding property name: **right-margin**
     */
    public var rightMargin: Int

    /**
     * The top margin for text in the text view. Note that this property is confusingly named. In CSS terms the
     * value set here is padding, and it is applied in addition to the padding from the theme. Don't confuse this
     * property with **margin-top**.
     *
     * Data binding property name: **top-margin**
     */
    public var topMargin: Int

    /**
     * The bottom margin for text in the text view. Note that this property is confusingly named. In CSS terms the
     * value set here is padding, and it is applied in addition to the padding from the theme. Don't confuse this
     * property with **margin-bottom**.
     *
     * Data binding property name: **bottom-margin**
     */
    public var bottomMargin: Int

    /**
     * Amount to indent the paragraph, in pixels. Default value is *0*.
     *
     * Data binding property name: **indent**
     */
    public var indent: Int

    /**
     * Whether Tab will result in a tab character being entered. Default value is *true*.
     *
     * Data binding property name: **accepts-tab**
     */
    public var acceptsTab: Boolean

    /**
     * Whether to use a monospace font. Default value is *false*.
     *
     * Data binding property name: **monospace**
     */
    public var monospace: Boolean

    public companion object {
        public fun create(): TextView
    }

    /**
     * Moves the cursor to the currently visible region of the buffer, it it isn’t there already.
     * @return A value of *true* if the cursor had to be moved.
     */
    public fun placeCursorOnscreen(): Boolean

    /**
     * Updates the position of a child, as for gtk_text_view_add_child_in_window().
     * @param child The child widget already added to the text view.
     * @param xPos New X position in window coordinates.
     * @param yPos New X position in window coordinates.
     */
    public fun moveChild(child: WidgetBase, xPos: Int, yPos: Int)

    /**
     * Ensures that the cursor is shown (i.e. not in an **off** blink interval), and resets the time that it will stay
     * blinking (or visible in case blinking is disabled). This function should be called in response to user input
     * (e.g. from derived classes that override the text view's **key-press-event** handler).
     */
    public fun resetCursorBlink()

    /**
     * Scrolls the [TextView] so that mark is on the screen in the position indicated by [xAlign] and [yAlign]. An
     * alignment of *0.0* indicates left or top, *1.0* indicates right or bottom, *0.5* means center. If [useAlign] is
     * *false* then the text scrolls the minimal distance to get the mark onscreen, possibly not scrolling at all. The
     * effective screen for purposes of this function is reduced by a margin of size [withinMargin].
     * @param mark The text mark.
     * @param withinMargin Margin as a [0.0,0.5) fraction of screen size.
     * @param useAlign Whether to use alignment arguments (if *false* then just get the mark onscreen).
     * @param xAlign Horizontal alignment of mark within visible area.
     * @param yAlign Vertical alignment of mark within visible area.
     */
    public fun scrollToMark(mark: TextMark, withinMargin: Double, useAlign: Boolean, xAlign: Double, yAlign: Double)

    /**
     * Scrolls the [TextView] so that the [iterator][iter] is on the screen in the position indicated by [xAlign], and
     * [yAlign]. An alignment of *0.0* indicates left or top, *1.0* indicates right or bottom, *0.5* means center. If
     * [useAlign] is *false* then the text scrolls the minimal distance to get the mark onscreen, possibly not
     * scrolling at all. The effective screen for purposes of this function is reduced by a margin of size
     * [withinMargin].
     *
     * Note that this function uses the currently computed height of the lines in the text buffer. Line heights are
     * computed in an idle handler; so this function may not have the desired effect if it’s called before the height
     * computations. To avoid oddness consider using [scrollToMark] which saves a point to be scrolled to after line
     * validation.
     * @param iter The iterator to use.
     * @param withinMargin Margin as a [0.0,0.5) fraction of screen size.
     * @param useAlign Whether to use alignment arguments (if *false* then just get the mark onscreen).
     * @param xAlign Horizontal alignment of mark within visible area.
     * @param yAlign Vertical alignment of mark within visible area.
     * @return A value of *true* if scrolling occurred.
     */
    public fun scrollToIterator(
        iter: TextBufferIterator,
        withinMargin: Double,
        useAlign: Boolean,
        xAlign: Double,
        yAlign: Double
    ): Boolean

    /**
     * Scrolls the [TextView] the minimum distance such that [mark] is contained within the visible area of the widget.
     * @param mark A mark in the buffer for the [TextView].
     */
    public fun scrollMarkOnScreen(mark: TextMark)

    /**
     * Moves a mark within the buffer so that it's located within the currently visible text area.
     * @param mark The mark to use.
     * @return A value of *true* if the mark moved (wasn't already onscreen).
     */
    public fun moveMarkOnScreen(mark: TextMark): Boolean

    /**
     * Gets the iterator (returned as the second element) at the start of the line containing the coordinate [yPos].
     * The [yPos] is in buffer coordinates, convert from window coordinates with
     * `gtk_text_view_window_to_buffer_coords()`. The first returned element (lineTop) will be filled with the
     * coordinate of the top edge of the line.
     * @param yPos The Y coordinate.
     * @return A Pair containing the following:
     * 1. lineTop: Int
     * 2. iterator: TextBufferIterator
     */
    public fun fetchLineAtYPos(yPos: Int): Pair<Int, TextBufferIterator>

    /**
     * Gets the y coordinate (returned as the first element) of the top of the line containing the iterator (returned
     * as the third element), and the height of the line (returned as the second element). The coordinate is a buffer
     * coordinate; convert to window coordinates with `gtk_text_view_buffer_to_window_coords()`.
     * @return A Triple containing the following:
     * 1. yPos: Int
     * 2. height: Int
     * 3. iterator: TextBufferIterator
     */
    public fun fetchLineAtYPosRange(): Triple<Int, Int, TextBufferIterator>

    /**
     * Retrieves the iterator at buffer coordinates [xPos] and [yPos]. Buffer coordinates are coordinates for the
     * entire buffer, not just the currently displayed portion. If you have coordinates from an event you have to
     * convert those to buffer coordinates with `gtk_text_view_window_to_buffer_coords()`.
     * @param xPos X position in buffer coordinates.
     * @param yPos Y position in buffer coordinates.
     * @return A Pair containing the following:
     * 1. overTxt: Boolean
     * 2. iterator: TextBufferIterator
     */
    public fun fetchIteratorAtLocation(xPos: Int, yPos: Int): Pair<Boolean, TextBufferIterator>

    /**
     * Retrieves the iterator pointing to the character at buffer coordinates [xPos] and [yPos]. Buffer coordinates are
     * coordinates for the entire buffer, not just the currently displayed portion. If you have coordinates from an
     * event, you have to convert those to buffer coordinates with `gtk_text_view_window_to_buffer_coords()`.
     *
     * Note that this is different from [fetchIteratorAtLocation], which returns cursor locations, i.e. positions
     * between characters.
     * @param xPos X position in buffer coordinates.
     * @param yPos Y position in buffer coordinates.
     * @return A Triple containing the following:
     * 1. overTxt: Boolean
     * 2. trailing: Int
     * 3. iterator: TextBufferIterator
     */
    public fun fetchIteratorAtPosition(xPos: Int, yPos: Int): Triple<Boolean, Int, TextBufferIterator>

    /**
     * Moves the given [iterator][iter] forward by one display (wrapped) line. A display line is different from a
     * paragraph. Paragraphs are separated by new lines or other paragraph separator characters. Display lines are
     * created by line wrapping a paragraph. If wrapping is turned off, display lines and paragraphs will be the same.
     * Display lines are divided differently for each view since they depend on the view’s width; paragraphs are the
     * same in all views since they depend on the contents of the [TextBuffer].
     * @param iter The iterator.
     * @return A value of *true* if [iter] was moved, and is not on the end iterator.
     */
    public fun forwardDisplayLine(iter: TextBufferIterator): Boolean

    /**
     * Moves the given [iterator][iter] backward by one display (wrapped) line. A display line is different from a
     * paragraph. Paragraphs are separated by new lines or other paragraph separator characters. Display lines are
     * created by line wrapping a paragraph. If wrapping is turned off, display lines and paragraphs will be the same.
     * Display lines are divided differently for each view since they depend on the view’s width; paragraphs are the
     * same in all views since they depend on the contents of the [TextBuffer].
     * @param iter The iterator.
     * @return A value of *true* if [iter] was moved, and is not on the end iterator.
     */
    public fun backwardDisplayLine(iter: TextBufferIterator): Boolean

    /**
     * Moves the given [iterator][iter] forward to the next display line end. A display line is different from a
     * paragraph. Paragraphs are separated by new lines or other paragraph separator characters. Display lines are
     * created by line wrapping a paragraph. If wrapping is turned off, display lines and paragraphs will be the same.
     * Display lines are divided differently for each view since they depend on the view’s width; paragraphs are the
     * same in all views since they depend on the contents of the [TextBuffer].
     * @param iter The iterator.
     * @return A value of *true* if [iter] was moved and is not on the end iterator.
     */
    public fun forwardDisplayLineEnd(iter: TextBufferIterator): Boolean

    /**
     * Moves the given [iterator][iter] backward to the next display line start. A display line is different from a
     * paragraph. Paragraphs are separated by new lines or other paragraph separator characters. Display lines are
     * created by line wrapping a paragraph. If wrapping is turned off, display lines and paragraphs will be the same.
     * Display lines are divided differently for each view since they depend on the view’s width; paragraphs are the
     * same in all views since they depend on the contents of the [TextBuffer].
     * @param iter The iterator.
     * @return A value of *true* if [iter] was moved and is not on the end iterator.
     */
    public fun backwardDisplayLineStart(iter: TextBufferIterator): Boolean

    /**
     * Determines whether [iterator][iter] is at the start of a display line.
     * @param iter The iterator.
     * @return A value of *true* if [iter] begins a wrapped line
     * @see forwardDisplayLine
     */
    public fun startsDisplayLine(iter: TextBufferIterator): Boolean

    /**
     * Move the iterator a given number of characters visually, treating it as the strong cursor position. If [count]
     * is positive then the new strong cursor position will be count positions to the right of the old cursor position.
     * If [count] is negative then the new strong cursor position will be count positions to the left of the old cursor
     * position.
     *
     * In the presence of bi-directional text the correspondence between logical, and visual order will depend on the
     * direction of the current run, and there may be jumps when the cursor is moved off of the end of a run.
     * @param iter The iterator.
     * @param count Number of characters to move (negative moves left, positive moves right).
     * @return A value of *true* if [iter] moved and is not on the end iterator.
     */
    public fun moveVisually(iter: TextBufferIterator, count: Int): Boolean
}
