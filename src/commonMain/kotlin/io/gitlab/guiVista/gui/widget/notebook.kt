package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.layout.ContainerBase

/** A tabbed notebook container. */
public expect class Notebook : ContainerBase {
    /**
     * Whether tabs should be shown. Default value is *true*.
     *
     * Data binding property name: **show-tabs**
     */
    public var showTabs: Boolean

    /**
     * Whether the border should be shown. Default value is *true*.
     *
     * Data binding property name: **show-border**
     */
    public var showBorder: Boolean

    /**
     * If *true* then scroll arrows are added if there are too many tabs to fit. Default value is *false*.
     *
     * Data binding property name: **scrollable**
     */
    public var scrollable: Boolean

    /**
     * Group name for tab drag and drop. Default value is *""* (an empty String).
     *
     * Data binding property name: **group-name**
     */
    public var groupName: String

    /**
     * Returns the page number of the current page. This is an index (starting from 0) of the current page in the
     * notebook. If the notebook has no pages then *-1* will be returned.
     */
    public val currentPage: Int

    /** The number of pages in the notebook. */
    public val totalPages: Int

    public companion object {
        public fun create(): Notebook
    }

    /**
     * Appends a page to the notebook.
     * @param child The widget to use as the contents for the page.
     * @param tabLabel The widget to be used as the label for the page, or *null* to use the default label, “page N”.
     * @return The index (starting from *0*) of the appended page in the notebook, or *-1* if the function fails.
     */
    public fun appendPage(child: WidgetBase, tabLabel: WidgetBase? = null): Int

    /**
     * Appends a page to the notebook, specifying the widget to use as the label in the popup menu.
     * @param child The widget to use as the contents for the page.
     * @param tabLabel The widget to be used as the label for the page, or *null* to use the default label, “page N”.
     * @param menuLabel The widget to use as a label for the page-switch menu, if that is enabled. If *null*, and
     * [tabLabel] is a [io.gitlab.guiVista.gui.widget.display.LabelBase] or *null*, then the menu label will be a newly
     * created label with the same text as [tabLabel]; if tab_label is not a
     * [io.gitlab.guiVista.gui.widget.display.LabelBase] then [menuLabel] must be specified if the page-switch menu is
     * to be used.
     * @return The index (starting from *0*) of the appended page in the notebook, or *-1* if the function fails.
     */
    public fun appendPageMenu(child: WidgetBase, tabLabel: WidgetBase? = null, menuLabel: WidgetBase? = null): Int

    /**
     * Prepends a page to the notebook.
     * @param child The widget to use as the contents for the page.
     * @param tabLabel The widget to be used as the label for the page, or *null* to use the default label, “page N”.
     * @return The index (starting from *0*) of the prepended page in the notebook, or *-1* if the function fails.
     */
    public fun prependPage(child: WidgetBase, tabLabel: WidgetBase? = null): Int

    /**
     * Prepends a page to the notebook, specifying the widget to use as the label in the popup menu.
     * @param child The widget to use as the contents for the page.
     * @param tabLabel The widget to be used as the label for the page, or *null* to use the default label, “page N”.
     * @param menuLabel The widget to use as a label for the page-switch menu, if that is enabled. If *null*, and
     * [tabLabel] is a [io.gitlab.guiVista.gui.widget.display.LabelBase] or *null*, then the menu label will be a newly
     * created label with the same text as [tabLabel]; if tab_label is not a
     * [io.gitlab.guiVista.gui.widget.display.LabelBase] then [menuLabel] must be specified if the page-switch menu is
     * to be used.
     * @return The index (starting from *0*) of the prepended page in the notebook, or *-1* if the function fails.
     */
    public fun prependPageMenu(child: WidgetBase, tabLabel: WidgetBase? = null, menuLabel: WidgetBase? = null): Int

    /**
     * Insert a page into the notebook at the given position.
     * @param child The widget to use as the contents for the page.
     * @param tabLabel The widget to be used as the label for the page, or *null* to use the default label, “page N”.
     * @param pos The index (starting at *0*) at which to insert the page, or *-1* to append the page after all other
     * pages.
     * @return The index (starting from *0*) of the inserted page in the notebook, or *-1* if the function fails.
     */
    public fun insertPage(child: WidgetBase, tabLabel: WidgetBase? = null, pos: Int): Int

    /**
     * Insert a page into the notebook at the given position, specifying the widget to use as the label in the popup
     * menu.
     * @param child The widget to use as the contents for the page.
     * @param tabLabel The widget to be used as the label for the page, or *null* to use the default label, “page N”.
     * @param menuLabel The widget to use as a label for the page-switch menu, if that is enabled. If *null*, and
     * [tabLabel] is a [io.gitlab.guiVista.gui.widget.display.LabelBase] or *null*, then the menu label will be a newly
     * created label with the same text as [tabLabel]; if tab_label is not a
     * [io.gitlab.guiVista.gui.widget.display.LabelBase] then [menuLabel] must be specified if the page-switch menu is
     * to be used.
     * @param pos The index (starting at *0*) at which to insert the page, or *-1* to append the page after all other
     * pages.
     * @return The index (starting from *0*) of the inserted page in the notebook.
     */
    public fun insertPageMenu(
        child: WidgetBase,
        pos: Int,
        tabLabel: WidgetBase? = null,
        menuLabel: WidgetBase? = null
    ): Int

    /**
     * Removes a page from the notebook given its index in the notebook.
     * @param pageNum The index of a notebook page, starting from *0*. If *-1* then the last page will be removed.
     */
    public fun removePage(pageNum: Int = -1)

    /**
     * Removes the child from the notebook. This function is very similar to
     * [io.gitlab.guiVista.gui.layout.ContainerBase.remove], but additionally informs the notebook that the removal is
     * happening as part of a tab DND operation, which should **NOT** be cancelled!
     * @param child The child to remove.
     */
    public fun detachTab(child: WidgetBase)

    /**
     * Finds the index of the page which contains the given [child] widget.
     * @param child The widget to use.
     * @return The index of the page containing [child], or *-1* if [child] is not in the notebook.
     */
    public fun pageNumber(child: WidgetBase): Int

    /**
     * Switches to the next page. Nothing happens if the current page is the last page.
     */
    public fun nextPage()

    /**
     * Switches to the previous page. Nothing happens if the current page is the first page.
     */
    public fun previousPage()

    /**
     * Reorders the page containing child so that it appears in the [position][pos]. If the [position][pos] is greater
     * than or equal to the number of children in the list or negative then the [child] will be moved to the end of the
     * list.
     * @param child The child to move.
     * @param pos The new position, or *-1* to move to the end
     */
    public fun reorderChild(child: WidgetBase, pos: Int = -1)

    /**
     * Enables the popup menu: if the user clicks with the right mouse button on the tab labels, a menu with all the
     * pages will be popped up.
     */
    public fun enablePopup()

    /** Disables the popup menu. */
    public fun disablePopup()

    /**
     * Retrieves the menu label widget of the page containing child.
     * @param child The widget contained in the page of the notebook.
     * @return The menu label or *null* if the notebook page does not have a menu label other than the default
     * (the tab label).
     */
    public fun fetchMenuLabel(child: WidgetBase): WidgetBase?

    /**
     * Returns the child widget contained in the [page number][pageNum].
     * @param pageNum The index of a page in the notebook, or *-1* to get the last page.
     * @return The child widget or *null* if the [page number][pageNum] is out of bounds.
     */
    public fun fetchPageByNum(pageNum: Int): WidgetBase?

    /**
     * Returns the tab label widget for the [page][child]. A value of *null* is returned if [child] is not in notebook
     * or if no tab label has specifically been set for the [child].
     * @param child The widget to use.
     * @return The tab label, or *null*.
     */
    public fun fetchTabLabel(child: WidgetBase): WidgetBase?

    /**
     * Changes the menu label for the page containing the [child].
     * @param child The child widget.
     * @param menuLabel The menu label or *null* for default.
     */
    public fun changeMenuLabel(child: WidgetBase, menuLabel: WidgetBase? = null)

    /**
     * Creates a new label, and sets it as the menu label of child.
     * @param child The child widget.
     * @param menuText The label text.
     */
    public fun changeMenuLabelText(child: WidgetBase, menuText: String)

    /**
     * Changes the tab label for child. If *null* is specified for [tabLabel] then the page will have the label
     * “page N”.
     * @param child The page.
     * @param tabLabel The tab label widget to use or *null* for default tab label.
     */
    public fun changeTabLabel(child: WidgetBase, tabLabel: WidgetBase? = null)

    /**
     * Creates a new label and sets it as the tab label for the page containing the [child].
     * @param child The page.
     * @param tabText The label text.
     */
    public fun changeTabLabelText(child: WidgetBase, tabText: String)

    /**
     * Sets whether the notebook tab can be reordered via drag and drop or not.
     * @param child The child widget.
     * @param reorderable Whether the tab is reorderable or not.
     */
    public fun changeTabReorderable(child: WidgetBase, reorderable: Boolean)

    /**
     * Sets whether the tab can be detached from notebook to another notebook or widget. Note that 2 notebooks must
     * share a common group identifier to allow automatic tabs interchange between them. If you want a widget to
     * interact with a notebook through **"drag and drop"** (i.e.: accept dragged tabs from it) it must be set as a
     * drop destination, and accept the target **GTK_NOTEBOOK_TAB**. The notebook will fill the selection with a
     * [WidgetBase] pointing to the child widget that corresponds to the dropped tab.
     *
     * Note that you should use [detachTab] instead of [ContainerBase.remove] if you want to remove the tab from the source
     * notebook as part of accepting a drop. Otherwise the source notebook will think that the dragged tab was removed
     * from underneath the ongoing drag operation, and will initiate a drag cancel animation.
     * @param child The child widget.
     * @param detachable Whether the tab is detachable or not.
     * @see groupName
     */
    public fun changeTabDetachable(child: WidgetBase, detachable: Boolean)

    /**
     * Retrieves the text of the menu label for the page containing the [child].
     * @param child The child widget of a page in the notebook.
     * @return The text of the tab label, or *""* (an empty String) if the widget does not have a menu label other
     * than the default menu label, or the menu label widget is not a
     * [io.gitlab.guiVista.gui.widget.display.LabelBase]. The string is owned by the widget and must **NOT** be freed.
     */
    public fun fetchMenuLabelText(child: WidgetBase): String

    /**
     * Retrieves the text of the tab label for the page containing the [child].
     * @param child The child widget of a page in the notebook.
     * @return the text of the tab label, or *""* (an empty String) if the tab label widget is not a
     * [io.gitlab.guiVista.gui.widget.display.LabelBase]. The string is owned by the widget and must **NOT** be freed.
     */
    public fun fetchTabLabelText(child: WidgetBase): String

    /**
     * Gets whether the tab can be reordered via drag and drop or not.
     * @param child The child widget.
     * @return A value of *true* if the tab is reorderable.
     */
    public fun fetchTabReorderable(child: WidgetBase): Boolean

    /**
     * Returns whether the tab contents can be detached from notebook.
     * @param child The child widget.
     * @return A value of *true* if the tab is detachable.
     */
    public fun fetchTabDetachable(child: WidgetBase): Boolean
}
