package io.gitlab.guiVista.gui.widget.tool.item

/** Base interface for toggle tool button objects. */
public expect interface ToggleToolButtonBase : ToolButtonBase {
    /**
     * If the toggle tool button should be pressed in. Default value is *false*.
     *
     * Data binding property name: **active**
     */
    public open var active: Boolean
}
