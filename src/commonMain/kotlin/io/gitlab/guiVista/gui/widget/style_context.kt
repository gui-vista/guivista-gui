package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.RgbaColor

/** Stores styling information affecting a widget defined by `GtkWidgetPath`. */
public expect class StyleContext : ObjectBase {
    /**
     * The list of classes currently defined in this context. Convert each element to a Kotlin [String] with the
     * `toKString` function.
     */
    public val classList: DoublyLinkedList

    public companion object {
        /**
         * Creates a standalone [StyleContext]. This style context won’t be attached to any widget so you may want to
         * call `gtk_style_context_set_path()` yourself. This function is only useful when using the theming layer
         * separated from GTK+. If you are using [StyleContext] to theme widgets then use
         * [io.gitlab.guiVista.gui.widget.WidgetBase.styleContext] in order to get a style context ready to theme the
         * widget.
         * @return A newly created [StyleContext].
         */
        public fun create(): StyleContext
    }

    /**
     * Adds a style class to this context, so posterior calls to `gtk_style_context_get()`, or any of the
     * **gtk_render_*()** functions will make use of this new class for styling. In the CSS file format a
     * [io.gitlab.guiVista.gui.widget.dataEntry.Entry] defining a **search** class would be matched by:
    ```css
    entry.search { ... }
    ```
     * While any widget defining a **search** class would be matched by:
    ```css
    .search { ... }
    ```
     * @param className Class name to use in styling..
     */
    public infix fun addClass(className: String)

    /**
     * Returns *true* if this context currently has defined the given class name.
     * @param className A class name.
     * @return A value of *true* if this context has [className] defined.
     */
    public infix fun hasClass(className: String): Boolean

    /**
     * Looks up and resolves a color name in the context color map.
     * @param colorName The color name to lookup.
     * @return A Pair containing the following:
     * 1. colorFound - Will be *true* if [colorName] was found.
     * 2. color - The looked up color.
     */
    public infix fun lookupColor(colorName: String): Pair<Boolean, RgbaColor>

    /**
     * Removes [className] from this context.
     * @param className Class name to remove.
     */
    public infix fun removeClass(className: String)

    /**
     * Restores context state to a previous stage.
     * @see save
     */
    public fun restore()

    /**
     * Saves the context state so temporary modifications done through [addClass] , [removeClass],
     * `gtk_style_context_set_state()`, etc. can quickly be reverted in one go through [restore]. The matching call to
     * [restore] **MUST** be done **BEFORE** GTK returns to the main loop.
     */
    public fun save()

    /**
     * Sets the scale to use when getting image assets for the style.
     * @param scale The scale to use.
     */
    public fun changeScale(scale: Int)
}
