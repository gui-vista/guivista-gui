package io.gitlab.guiVista.gui.widget.menu

/** A menu widget. */
public expect class Menu : MenuBase {
    public companion object {
        public fun create(): Menu
    }
}
