package io.gitlab.guiVista.gui.widget.menu.item

import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.widget.Actionable
import io.gitlab.guiVista.gui.widget.menu.Menu

/** Base interface for menu item objects. */
public expect interface MenuItemBase : BinBase, Actionable {
    /** Whether the menu item reserves space for the submenu indicator regardless if it has a submenu or not. */
    public open var reserveIndicator: Boolean

    /**
     * The text for the child label. Default value is *""* (an empty String).
     *
     * Data binding property name: **label**
     */
    public open var label: String

    /**
     * When *true* the text is underlined to indicate mnemonics. Default value is *false*.
     *
     * Data binding property name: **use-underline**
     */
    public open var useUnderline: Boolean

    /**
     * Sets the accelerator path of the menu item, through which runtime changes of the menu item's accelerator caused
     * by the user can be identified and saved to persistent storage. Default value is *""* (an empty String).
     *
     * Data binding property name: **accel-path**
     */
    public open var accelPath: String

    /**
     * Sets whether the menu item appears justified at the right side of a menu bar. Default value is *false*.
     *
     * Data binding property name: **right-justified**
     */
    public open var rightJustified: Boolean

    /**
     * Sets or replaces the menu item’s submenu, or removes it when a *null* submenu is passed.
     *
     * Data binding property name: **submenu**
     */
    public open var subMenu: Menu?
}
