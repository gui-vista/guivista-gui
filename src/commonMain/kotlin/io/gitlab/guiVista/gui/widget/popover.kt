package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.io.application.menu.MenuModel

/** For Context dependent bubbles. */
public expect class Popover : PopoverBase {
    public companion object {
        public fun create(relativeTo: WidgetBase): Popover

        /**
         * Creates a [Popover] and populates it according to the [model]. The popover is pointed to the [relativeTo]
         * widget. The created buttons are connected to actions found in the [io.gitlab.guiVista.gui.window.AppWindow]
         * to which the popover belongs - typically by means of being attached to a widget that is contained within the
         * `GtkApplicationWindows` widget hierarchy. Actions can also be added using `gtk_widget_insert_action_group()`
         * on the menus attach widget or on any of its parent widgets.
         * @param relativeTo The widget that the popover is related to.
         * @param model The menu model to use.
         * @return A new instance of [Popover]
         */
        public fun fromModel(relativeTo: WidgetBase, model: MenuModel): Popover
    }
}
