package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.layout.BinBase

/** Base interface for a frame. */
public expect interface FrameBase : BinBase {
    /**
     * Text of the frame's label. Default value is *""*.
     *
     * Data binding property name: **label**
     */
    public open var label: String

    /**
     * A widget to display in place of the usual frame label.
     *
     * Data binding property name: **label-widget**
     */
    public open var labelWidget: WidgetBase?

    /**
     * Sets the alignment of the frame widget’s label. The default values for a newly created frame are *0.0*, and
     * *0.5*.
     * @param xAlign The position of the label along the top edge of the widget. A value of *0.0* represents left
     * alignment; *1.0* represents right alignment.
     * @param yAlign The y alignment of the label. A value of *0.0* aligns under the frame; *1.0* aligns above the
     * frame. If the values are exactly *0.0*, or *1.0* the gap in the frame won’t be painted because the label will be
     * completely above or below the frame.
     */
    public open fun changeLabelAlign(xAlign: Float, yAlign: Float)

    /**
     * Retrieves the X and Y alignment of the frame’s label.
     * @return A Pair that contains the following:
     * 1. xAlign
     * 2. yAlign
     * @see changeLabelAlign
     */
    public open fun fetchLabelAlign(): Pair<Float, Float>
}
