package io.gitlab.guiVista.gui.widget.menu.item

public object RadioMenuItemEvent {
    public const val groupChanged: String = "group-changed"
}
