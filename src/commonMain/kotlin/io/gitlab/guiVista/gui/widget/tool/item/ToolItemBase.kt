package io.gitlab.guiVista.gui.widget.tool.item

import io.gitlab.guiVista.gui.SizeGroup
import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.widget.WidgetBase

/** The base interface of widgets that can be added to a tool shell. */
public expect interface ToolItemBase : BinBase {
    /**
     * The size group used for labels in the tool item. Custom implementations of [ToolItemBase] should call this
     * function, and use the size group for labels.
     */
    public open val textSizeGroup: SizeGroup

    /**
     * Whether the toolbar item is considered important. When *true* the toolbar buttons show text in
     * `GTK_TOOLBAR_BOTH_HORIZ` mode. Default value is *false*.
     *
     * Data binding property name: **is-important**
     */
    public open var isImportant: Boolean

    /**
     * Whether the toolbar item is visible when the [ToolBar][io.gitlab.guiVista.gui.widget.tool.ToolBar] is in a
     * horizontal orientation. Default value is *true*.
     *
     * Data binding property name: **visible-horizontal**
     */
    public open var visibleHorizontal: Boolean

    /**
     * Whether the toolbar item is visible when the [ToolBar][io.gitlab.guiVista.gui.widget.tool.ToolBar] is in a
     * vertical orientation. Default value is *true*.
     *
     * Data binding property name: **visible-vertical**
     */
    public open var visibleVertical: Boolean

    /**
     * Whether tool item is allocated extra space when there is more room on the
     * [ToolBar][io.gitlab.guiVista.gui.widget.tool.ToolBar] then needed for the items. The effect is that the item
     * gets bigger when the [ToolBar][io.gitlab.guiVista.gui.widget.tool.ToolBar] gets bigger, and smaller when the
     * [ToolBar][io.gitlab.guiVista.gui.widget.tool.ToolBar] gets smaller.
     */
    public open var expand: Boolean

    /**
     * Whether tool item is to be allocated the same size as other homogeneous items. The effect is that all
     * homogeneous items will have the same width as the widest item.
     */
    public open var homogeneous: Boolean

    /**
     * Whether tool item has a drag window. When *true* the tool item can be used as a drag source through
     * `gtk_drag_source_set()`. When tool item has a drag window it will intercept all events, even those that would
     * otherwise be sent to a child of tool item.
     */
    public open var useDragWindow: Boolean

    /**
     * Fetches the text alignment used for tool item. Custom implementations of [ToolItemBase] should call this
     * function to find out how text should be aligned.
     */
    public open val textAlignment: Float

    /**
     * Calling this function signals to the toolbar that the overflow menu item for the tool item has changed. If the
     * overflow menu is visible when this function it called, then the menu will be rebuilt. The function must be
     * called when the tool item changes what it will do in response to the “create-menu-proxy” signal.
     */
    public open fun rebuildMenu()

    /**
     * Changes the text to be displayed as tooltip on the item. See `gtk_widget_set_tooltip_text()`.
     * @param text The text to be used as tooltip for the tool item.
     */
    public open infix fun changeTooltipText(text: String)

    /**
     * Sets the markup text to be displayed as a tooltip on the item.
     * @param markup The markup text to be used for the tooltip.
     */
    public open infix fun changeTooltipMarkup(markup: String)

    /**
     * Sets the menu item used in the toolbar overflow menu. The [id] is used to identify the caller of this function,
     * and should also be used with [fetchProxyMenuItemById].
     * @param id A string used to identify the menu item.
     * @param menuItem The menu item to use in the overflow menu.
     */
    public open fun changeProxyMenuItem(id: String, menuItem: WidgetBase)

    /**
     * If [id] matches the string passed to [changeProxyMenuItem] then return the corresponding menu item.
     * Custom implementations of [ToolItemBase] should use this function to update their menu item when the
     * tool item changes.
     * @param id A String used to identify the menu item.
     * @return The menu item passed to [changeProxyMenuItem] if the id matches.
     */
    public open fun fetchProxyMenuItemById(id: String): WidgetBase?

    /**
     * Returns the menu item that was last set by [changeProxyMenuItem], ie. the menu item that is going to appear in
     * the overflow menu.
     * @return The menu item that is going to appear in the overflow menu for tool item.
     */
    public open fun fetchProxyMenuItem(): WidgetBase?
}
