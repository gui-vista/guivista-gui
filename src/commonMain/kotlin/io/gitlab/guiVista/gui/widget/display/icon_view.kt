package io.gitlab.guiVista.gui.widget.display

import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.cell.CellAreaBase
import io.gitlab.guiVista.gui.cell.renderer.CellRendererBase
import io.gitlab.guiVista.gui.dragDrop.DragDropTargetEntry
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreePath

/**
 * Provides an alternative view on a [io.gitlab.guiVista.gui.tree.TreeModel]. It displays the model as a grid of icons
 * with labels. Like [io.gitlab.guiVista.gui.widget.tree.TreeView] it allows to selection between one or multiple
 * items. In addition to selection with the arrow keys, **rubberband** selection is supported, which is controlled by
 * dragging the pointer.
 *
 * Note that if the tree model is backed by an actual tree store (as opposed to a flat list where the mapping to icons
 * is obvious) then only the first level of the tree is displayed, and the tree’s branches are ignored.
 */
public expect class IconView : ContainerBase {
    /**
     * A list of paths of all selected items. If you are planning on modifying the model after calling this function,
     * you may want to convert the returned list into a list of `GtkTreeRowReferences`. To do this you can use
     * `gtk_tree_row_reference_new()`. After using the list remember to call the [DoublyLinkedList.close] function.
     */
    public val selectedItems: DoublyLinkedList

    /**
     * Specifies whether the **item-activated** event will be fired after a single click.
     *
     * Data binding property name: **activate-on-single-click**
     */
    public var activateOnSingleClick: Boolean

    /**
     * Specifies the space which is inserted between the columns of the icon view.
     *
     * Data binding property name: **column-spacing**
     */
    public var columnSpacing: Int

    /**
     * Contains the number of the columns in which the items should be displayed. If it is *-1* then the number of
     * columns will be chosen automatically to fill the available area.
     *
     * Data binding property name: **columns**
     */
    public var columns: Int

    /**
     * Specifies the padding around each of the icon view’s item.
     *
     * Data binding property name: **item-padding**
     */
    public var itemPadding: Int

    /**
     * Specifies the width to use for each item. If it is set to *-1* then the icon view will automatically determine
     * a suitable item size.
     *
     * Data binding property name: **item-width**
     */
    public var itemWidth: Int

    /**
     * Specifies the space which is inserted at the edges of the icon view.
     *
     * Data binding property name: **margin**
     */
    public var margin: Int

    /**
     * Contains the number of the model column containing markup information to be displayed. The markup column
     * **MUST** be of type `G_TYPE_STRING`. If this property and the [textColumn] property are both set to column
     * numbers then it overrides the text column. If both are set to *-1* then no text is displayed.
     *
     * Data binding property name: **markup-column**
     */
    public var markupColumn: Int

    /**
     * Contains the tree model used by icon view.
     *
     * Data binding property name: **model**
     */
    public var model: TreeModelBase

    /**
     * Contains the number of the model column containing the image buffers which are displayed. The image buffer
     * column **MUST** be of type `GDK_TYPE_PIXBUF`. Setting this property to *-1* turns off the display of image
     * buffers.
     *
     * Data binding property name: **pixbuf-column**
     */
    public var imageBufferColumn: Int

    /**
     * Specifies if the items can be reordered by **Drag N Drop**.
     *
     * Data binding property name: **reorderable**
     */
    public var reorderable: Boolean

    /**
     * Specifies the space which is inserted between the rows of the icon view.
     *
     * Data binding property name: **row-spacing**
     */
    public var rowSpacing: Int

    /**
     * Specifies the space which is inserted between the cells (i.e. the icon and the text) of an item.
     *
     * Data binding property name: **spacing**
     */
    public var spacing: Int

    /**
     * Contains the number of the model column containing the texts which are displayed. The text column **MUST** be
     * of type `G_TYPE_STRING`. If this property and the [markupColumn] property are both set to *-1* then no text is
     * displayed.
     *
     * Data binding property name: **text-column**
     */
    public var textColumn: Int

    /**
     * Contains the number of the column containing the tooltip.
     *
     * Data binding property name: **tooltip-column**
     */
    public var tooltipColumn: Int

    public companion object {
        public fun create(): IconView

        /**
         * Creates a new [IconView] widget using the specified [area] to layout cells inside the icons.
         * @param area The cell area to use for laying out the cells.
         * @return The newly created [IconView] widget.
         */
        public fun createWithArea(area: CellAreaBase): IconView

        /**
         * Creates a new [IconView] widget with the specified [model].
         * @param model The tree model to use.
         * @return The newly created [IconView] widget.
         */
        public fun createWithModel(model: TreeModelBase): IconView
    }

    /**
     * Converts widget coordinates to coordinates for the bin window.
     * @return A Pair containing the following:
     * 1. winXPos - The bin window X coordinate.
     * 2. winYPos - The bin window Y coordinate.
     */
    public fun convertWidgetToBinWindowCoordinates(widgetXPos: Int, widgetYPos: Int): Pair<Int, Int>

    /**
     * Turns [IconView] into a drop destination for automatic **Drag N Drop**. Calling this function sets
     * [reorderable] to *false*.
     * @param actions The bitmask of possible actions for a drop to this widget.
     * @param targets The table of targets that the drop will support.
     */
    public fun enableModelDropDestination(actions: UInt, vararg targets: DragDropTargetEntry)

    /**
     * Turns [IconView] into a drag source for automatic **Drag N Drop**. Calling this function sets [reorderable] to
     * *false*.
     * @param startBtnMask Mask of allowed buttons to start drag.
     * @param actions The bitmask of possible actions for a drag from this widget.
     * @param targets The table of targets that the drag will support.
     */
    public fun enableModelDragSource(startBtnMask: UInt, actions: UInt, vararg targets: DragDropTargetEntry)

    /**
     * Returns the current cursor path and cell. If the cursor isn’t currently set then path will be *null*. If no cell
     * currently has focus then cell will be *null*. Note that path **MUST** be closed with [TreePath.close] after it
     * is used.
     * @return A Triple containing the following:
     * 1. cursorSet - Will be *true* if the cursor is set.
     * 2. path - The current cursor path or *null*.
     * 2. cell - The current cursor cell or *null*.
     */
    public fun fetchCursor(): Triple<Boolean, TreePath?, CellRendererBase?>

    /**
     * Finds the path at the point (x, y), relative to bin window coordinates. In contrast to
     * `fetchPathAtPosition` this function also obtains the cell at the specified position. The returned path should
     * be closed with [TreePath.close] after it is used. See [convertWidgetToBinWindowCoordinates] for converting
     * widget coordinates to bin window coordinates.
     * @param xPos The X position to be identified.
     * @param yPos The Y position to be identified.
     * @return A Triple containing the following:
     * 1. itemExists - Will be *true* if an item exists at the specified position.
     * 2. path - The path or *null*.
     * 3. cell - The cell renderer responsible for the cell or *null*.
     */
    public fun fetchItemAtPosition(xPos: Int, yPos: Int): Triple<Boolean, TreePath?, CellRendererBase?>

    /**
     * Gets the column in which the item [path] is currently displayed. Column numbers start at *0*.
     * @param path The tree path of the item.
     * @return The column in which the item is displayed.
     */
    public fun fetchItemColumn(path: TreePath): Int

    /**
     * Gets the row in which the item path is currently displayed. Row numbers start at *0*.
     * @param path The tree path of the item.
     * @return The row in which the item is displayed.
     */
    public fun fetchItemRow(path: TreePath): Int

    /**
     * Finds the path at the point (x, y), relative to bin window coordinates. See [fetchItemAtPosition] if you are
     * also interested in the cell at the specified position. See [convertWidgetToBinWindowCoordinates] for converting
     * widget coordinates to bin window coordinates.
     * @param xPos The X position to be identified.
     * @param yPos The Y position to be identified.
     * @return The tree path corresponding to the icon or *null* if no icon exists at that position.
     */
    public fun fetchPathAtPosition(xPos: Int, yPos: Int): TreePath?

    /**
     * Sets startPath and endPath to be the first and last visible path. Note that there may be invisible paths in
     * between. Both paths should be closed with [TreePath.close] after use.
     * @return A Triple containing the following:
     * 1. validPaths - Will be *true* if valid paths were placed in startPath and endPath.
     * 2. startPath - The start of the region or *null*
     * 3. endPath - The end of the region or *null*
     */
    public fun fetchVisibleRange(): Triple<Boolean, TreePath?, TreePath?>

    /**
     * Activates the item determined by [path].
     * @param path The tree path to be activated.
     */
    public fun itemActivated(path: TreePath)

    /**
     * Returns *true* if the icon pointed to by [path] is currently selected. If [path] doesn't point to a valid
     * location then *false* is returned.
     * @param path The tree path to check selection on.
     * @return A value of *true* if [path] is selected.
     */
    public fun pathIsSelected(path: TreePath): Boolean

    /**
     * Moves the alignments of icon view to the position specified by [path]. Note that [rowAlign] determines where
     * the row is placed, and [colAlign] determines where column is placed. Both are expected to be between *0.0* and
     * *1.0*, where *0.0* means left/top alignment, *1.0* means right/bottom alignment, and *0.5* means center. If
     * [useAlign] is *false* then the alignment arguments are ignored, and the tree does the minimum amount of work to
     * scroll the item onto the screen. This means that the item will be scrolled to the edge closest to its current
     * position. However if the item is currently visible on the screen then nothing is done.
     *
     * This function only works if the model is set, and [path] is a valid row on the model. If the model changes
     * before the icon view is realized then the centered path will be modified to reflect this change.
     * @param path The path of the item to move to.
     * @param useAlign Whether to use alignment arguments.
     * @param rowAlign The vertical alignment of the item specified by [path].
     * @param colAlign The horizontal alignment of the item specified by [path].
     */
    public fun scrollToPath(path: TreePath, useAlign: Boolean, rowAlign: Float, colAlign: Float)

    /**
     * Selects all the icons. Note that the icon view **MUST** have its selection mode set to `GTK_SELECTION_MULTIPLE`.
     */
    public fun selectAll()

    /**
     * Selects the row at [path].
     * @param path The tree path to be selected.
     */
    public fun selectPath(path: TreePath)

    /**
     * Sets the current keyboard focus to be at [path] and selects it. This is useful when you want to focus the
     * user’s attention on a particular item. If [cell] isn't *null* then focus is given to the cell specified by it.
     * Additionally if [startEditing] is *true* then editing should be started in the specified cell.
     *
     * This function is often followed by [io.gitlab.guiVista.gui.widget.WidgetBase.grabFocus] (for the icon view) in
     * order to give keyboard focus to the widget. Please note that editing can only happen when the widget is realized.
     * @param path The tree path to use.
     * @param cell The cell renderer to use or *null*.
     * @param startEditing Will be *true* if the specified cell should start being edited.
     */
    public fun changeCursor(path: TreePath, cell: CellRendererBase?, startEditing: Boolean)

    /** Unselects all the icons. */
    public fun unselectAll()

    /**
     * Unselects the row at [path].
     * @param path The tree path to be unselected.
     */
    public fun unselectPath(path: TreePath)
}
