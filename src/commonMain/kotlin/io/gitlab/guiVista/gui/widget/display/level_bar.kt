package io.gitlab.guiVista.gui.widget.display

import io.gitlab.guiVista.gui.layout.Orientable
import io.gitlab.guiVista.gui.widget.WidgetBase

/** A bar that can used as a level indicator. */
public expect class LevelBar : WidgetBase, Orientable {
    /**
     * Level bars normally grow from top to bottom or left to right. Inverted level bars grow in the opposite
     * direction. Default value is *false*.
     *
     * Data binding property name: **inverted**
     */
    public var inverted: Boolean

    /**
     * Determines the maximum value of the interval that can be displayed by the bar. Default value is *1.0*.
     *
     * Data binding property name: **max-value**
     */
    public var maxValue: Double

    /**
     * Determines the minimum value of the interval that can be displayed by the bar. Default value is *0.0*.
     *
     * Data binding property name: **min-value**
     */
    public var minValue: Double

    /**
     * Determines the currently filled value of the level bar. Default value is *0.0*.
     *
     * Data binding property name: **value**
     */
    public var value: Double

    public companion object {
        public fun create(): LevelBar
    }
}