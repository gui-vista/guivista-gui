package io.gitlab.guiVista.gui.widget.menu

import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for menu objects. */
public expect interface MenuShell : ContainerBase {
    /**
     * Determines whether the menu, and its submenus grab the keyboard focus. Default value is *true*.
     *
     * Data binding property name: **take-focus**
     */
    public open var takeFocus: Boolean

    /** The currently selected item in the [MenuShell] */
    public open val selectedItem: WidgetBase?

    /** The parent [MenuShell]. A submenu parent is the GtkMenu or GtkMenuBar from which it was opened up. */
    public open val parentShell: WidgetBase?

    /** Cancels the selection within the menu shell. */
    public open fun cancel()

    /**
     * Activates the menu item within the menu shell.
     * @param menuItem The menu item to activate.
     * @param forceDeactivate If *true* force the deactivation of the menu shell after the menu item is activated.
     */
    public open fun activateItem(menuItem: WidgetBase, forceDeactivate: Boolean)

    /** Deselects the currently selected item from the menu shell. */
    public open fun deselect()

    /**
     * Select the first visible or selectable child of the menu shell. Don’t select tear off items unless the only item
     * is a tearoff item.
     * @param searchSensitive If *true* search for the first selectable menu item, otherwise select nothing if the
     * first item isn’t sensitive. This should be *false* if the menu is being popped up initially.
     */
    public open fun selectFirst(searchSensitive: Boolean = true)

    /**
     * Selects the menu item from the menu shell.
     * @param menuItem The menu item to select.
     */
    public open infix fun selectItem(menuItem: WidgetBase)

    /** Deactivates the menu shell. Typically this results in the menu shell being erased from the screen. */
    public open fun deactivate()

    /**
     * Adds a new menu item to the menu shell’s item list at the position indicated by position.
     * @param child The menu item to insert.
     * @param position The position in the item list where [child] is added. Positions are numbered from 0 to n-1.
     */
    public open fun insert(child: WidgetBase, position: Int)

    /**
     * Adds a new menu item to the beginning of the menu shell's item list.
     * @param child The menu item to prepend.
     */
    public open infix fun prepend(child: WidgetBase)

    /**
     * Adds a new GtkMenuItem to the end of the menu shell's item list.
     * @param child The menu item to append.
     */
    public open infix fun append(child: WidgetBase)
}
