package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.dialog.ColorChooser
import io.gitlab.guiVista.gui.layout.BoxBase

/** A widget for choosing colors. */
public expect class ColorChooserWidget : BoxBase, ColorChooser {
    public companion object {
        public fun create(): ColorChooserWidget
    }

    override fun close()

    override fun disconnectEvent(handlerId: ULong)
}
