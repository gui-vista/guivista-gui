package io.gitlab.guiVista.gui.widget.display

public object IconViewEvent {
    public const val itemActivated: String = "item-activated"
    public const val selectionChanged: String = "selection-changed"
}
