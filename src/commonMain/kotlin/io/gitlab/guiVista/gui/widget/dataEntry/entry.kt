package io.gitlab.guiVista.gui.widget.dataEntry

/** A single line text entry field. */
public expect class Entry : EntryBase {
    public companion object {
        public fun create(): Entry
    }
}
