package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.core.InitiallyUnowned
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.keyboard.AcceleratorGroup
import io.gitlab.guiVista.gui.widget.menu.item.MenuItemBase
import io.gitlab.guiVista.gui.window.WindowBase
import io.gitlab.guiVista.io.application.action.ActionGroup
import io.gitlab.guiVista.io.application.action.ActionGroupBase

/** Base interface for all widget's (controls). */
public expect interface WidgetBase : InitiallyUnowned {
    /**
     * The style context associated with the widget. Note that the context must **NOT** be closed since it is owned by
     * the widget itself.
     */
    public open val styleContext: StyleContext

    /**
     * If set to *true* then the widget can accept the input focus. Default value is *false*.
     *
     * Data binding property name: **can-focus**
     */
    public open var canFocus: Boolean

    /**
     * Enables or disables the emission of “query-tooltip” on widget. A value of *true* indicates that widget can have
     * a tooltip, in this case the widget will be queried using “query-tooltip” to determine whether it will provide a
     * tooltip or not. Note that setting this property to *true* for the first time will change the event masks of the
     * GdkWindows of this widget to include leave-notify and motion-notify events. This cannot and will not be undone
     * when the property is set to *false* again.
     *
     * Default value is *false*. Data binding property name: **has-tooltip**
     */
    public open var hasTooltip: Boolean

    /**
     * Sets the text of tooltip to be the given string. Property “has-tooltip” will automatically be set to *true* and
     * there will be taken care of “query-tooltip” in the default event handler. Note that if both [tooltipText] and
     * [tooltipMarkup] are set, the last one wins. Default value is *""* (an empty String).
     *
     * Data binding property name: **tooltip-text**
     */
    public open var tooltipText: String

    /**
     * If set to *true* then the widget is visible. Default value is *false*.
     *
     * Data binding property name: **visible**
     */
    public open var visible: Boolean

    /**
     * Margin on bottom side of widget. This property adds margin outside of the widget's normal size request, the
     * margin will be added in addition to the size from `gtk_widget_set_size_request()` for example. Default value is
     * *0*.
     *
     * Data binding property name: **margin-bottom**
     */
    public open var marginBottom: Int

    /**
     * Margin on end of widget, horizontally. This property supports left-to-right and right-to-left text directions.
     * This property adds margin outside of the widget's normal size request, the margin will be added in addition to
     * the size from `gtk_widget_set_size_request()` for example. Default value is *0*.
     *
     * Data binding property name: **margin-end**
     */
    public open var marginEnd: Int

    /**
     * Margin on start of widget, horizontally. This property supports left-to-right and right-to-left text directions.
     * This property adds margin outside of the widget's normal size request, the margin will be added in addition to
     * the size from `gtk_widget_set_size_request()` for example. Default value is *0*.
     *
     * Data binding property name: **margin-start**
     */
    public open var marginStart: Int

    /**
     * Margin on top side of widget. This property adds margin outside of the widget's normal size request, the margin
     * will be added in addition to the size from `gtk_widget_set_size_request()` for example. Default value is *0*.
     *
     * Data binding property name: **margin-top**
     */
    public open var marginTop: Int

    /**
     * Whether the application will paint directly on the widget. Default value is *false*.
     *
     * Data binding property name: **app-paintable**
     */
    public open var appPaintable: Boolean

    /**
     * Whether the widget can be the default widget. Default value is *false*.
     *
     * Data binding property name: **can-default**
     */
    public open var canDefault: Boolean

    /**
     * Whether the widget should grab focus when it is clicked with the mouse. This property is only relevant for
     * widgets that can take focus. Before 3.20 several widgets (GtkButton, GtkFileChooserButton, GtkComboBox)
     * implemented this property individually.
     *
     * Default value is *true*. Data binding property name: **focus-on-click**
     */
    public open var focusOnClick: Boolean

    /**
     * Whether the widget is the default widget. Default value is *false*.
     *
     * Data binding property name: **has-default**
     */
    public open val hasDefault: Boolean

    /**
     * Whether the widget has the input focus. Default value is *false*.
     *
     * Data binding property name: **has-focus**
     */
    public open val hasFocus: Boolean

    /**
     * Whether to expand horizontally. Default value is *false*.
     *
     * Data binding property name: **hexpand**
     */
    public open var hExpand: Boolean

    /**
     * Whether to use the [hExpand] property. Default value is *false*.
     *
     * Data binding property name: **hexpand-set**
     */
    public open var hExpandSet: Boolean

    /**
     * Whether the widget is the focus widget within the top level. Default value is *false*.
     *
     * Data binding property name: **is-focus**
     */
    public open val isFocus: Boolean

    /**
     * The name of the widget. Default value is *""* (an empty String).
     *
     * Data binding property name: **name**
     */
    public open var name: String

    /**
     * Whether the [showAll] function should not affect this widget. Default value is *false*.
     *
     * Data binding property name: **no-show-all**
     */
    public open var noShowAll: Boolean

    /**
     * The requested opacity of the widget. Before 3.8 this was only available in GtkWindow. Default value is *1.0*.
     *
     * Data binding property name: **opacity**
     */
    public open var opacity: Double

    /**
     * If *true* then the widget will receive the default action when it is focused. Default value is *false*.
     *
     * Data binding property name: **receives-default**
     */
    public open var receivesDefault: Boolean

    /**
     * The scale factor of the widget.
     *
     * Data binding property name: **scale-factor**
     */
    public open val scaleFactor: Int

    /**
     * Whether the widget responds to input.
     *
     * Data binding property name: **sensitive**
     */
    public open var sensitive: Boolean

    /**
     * Sets the text of tooltip to be the given string, which is marked up with the Pango text markup language. Also
     * see `gtk_tooltip_set_markup()`. This is a convenience property which will take care of getting the tooltip
     * shown if the given string isn't *null*, [hasTooltip] will automatically be set to *true*, and the
     * "query-tooltip" event will be handled in the default event handler. Note that if both [tooltipText], and
     * [tooltipMarkup] are set the last one wins.
     *
     * Default value is *""* (an empty String). Data binding property name: **tooltip-markup**
     */
    public open var tooltipMarkup: String

    /**
     * Whether to expand vertically. Default value is *false*.
     *
     * Data binding property name: **vexpand**
     */
    public open var vExpand: Boolean

    /**
     * Whether to use the [vExpand] property. Default value is *false*.
     *
     * Data binding property name: **vexpand-set**
     */
    public open var vExpandSet: Boolean

    /**
     * Installs an accelerator for this widget in [accelGroup] that causes [accelEvent] to be emitted if the
     * accelerator is activated. The [accelGroup] needs to be added to the widget’s top level via
     * [WindowBase.addAccelGroup], and the event must be of type `G_SIGNAL_ACTION`. Accelerators added through this
     * function are **NOT** user changeable during runtime. If you want to support accelerators that can be changed by
     * the user then use **addEntry** and [changeAccelPath] functions, or
     * [MenuItemBase.accelPath] instead.
     * @param accelEvent The widget event to emit on accelerator activation.
     * @param accelGroup The accelerator group for this widget, added to its top level.
     * @param accelKey GDK key val of the accelerator.
     * @param accelMods Modifier key combination of the accelerator.
     * @param accelFlags Flag accelerators, e.g. `GTK_ACCEL_VISIBLE`.
     */
    public open fun addAccelerator(
        accelEvent: String,
        accelGroup: AcceleratorGroup,
        accelKey: UInt,
        accelMods: UInt,
        accelFlags: UInt
    )

    /**
     * Removes an accelerator from the widget, previously installed with [addAccelerator].
     * @param accelGroup The accelerator group for this widget.
     * @param accelKey GDK key val of the accelerator.
     * @param accelMods Modifier key combination of the accelerator.
     * @return Whether an accelerator was installed, and could be removed.
     */
    public open fun removeAccelerator(accelGroup: AcceleratorGroup, accelKey: UInt, accelMods: UInt): Boolean

    /**
     * Given an [accelerator group][accelGroup], and an [accelerator path][accelPath], sets up an accelerator in
     * [accelGroup] so whenever the key binding that is defined for [accelPath] is pressed the widget will be
     * activated. This removes any accelerators (for any accelerator group) installed by previous calls to
     * [changeAccelPath]. Associating accelerators with paths allows them to be modified by the user, and the
     * modifications to be saved for future use. (See gtk_accel_map_save().)
     *
     * This function is a low level function that would most likely be used by a menu creation system like
     * `GtkUIManager`. If you use `GtkUIManager` then setting up accelerator paths will be done automatically. Even
     * when you you aren’t using `GtkUIManager`, if you only want to set up accelerators on menu items
     * then `gtk_menu_item_set_accel_path()` provides a somewhat more convenient interface.
     *
     * Note that [accelPath] will be stored in a `GQuark`. Therefore if you pass a static String, you can save some
     * memory by interning it first with `g_intern_static_string()`.
     * @param accelPath The path used to look up the accelerator.
     * @param accelGroup An accelerator group.
     */
    public open fun changeAccelPath(accelPath: String, accelGroup: AcceleratorGroup)

    /**
     * Lists the closures used by the widget for accelerator group connections with
     * `gtk_accel_group_connect_by_path()`, or `gtk_accel_group_connect()`. The closures can be used to monitor
     * accelerator changes on the widget, by connecting to the GtkAccelGroup ::accel-changed event of the
     * [AcceleratorGroup] of a closure which can be found out with `gtk_accel_group_from_accel_closure()`.
     */
    public open fun listAccelClosures(): DoublyLinkedList

    /**
     * Determines whether an accelerator that activates the event identified by [eventId] can currently be activated.
     * This is done by emitting the **can-activate-accel** event on the widget; if the event isn’t overridden by a
     * handler or in a derived widget then the default check is that the widget must be sensitive, and the widget and
     * all its ancestors mapped.
     * @param eventId The ID of a event installed on the widget.
     * @return A value of *true* if the accelerator can be activated.
     */
    public open infix fun canActivateAccel(eventId: UInt): Boolean

    /** Changes all margins for the widget. */
    public open fun changeMargins(bottom: Int, end: Int, start: Int, top: Int)

    /** Changes all margins for the widget using a single value. */
    public open fun changeAllMargins(value: Int)

    /** Clears all the widget's margins (bottom, end, start, and top). */
    public open fun clearMargins()

    /**
     * Causes widget to have the keyboard focus for the GtkWindow it's inside. Widget must be a focusable widget such
     * as a GtkEntry; something like GtkFrame won’t work. More precisely it must have the `GTK_CAN_FOCUS` flag set. Use
     * `gtk_widget_set_can_focus()` to modify that flag.
     *
     * The widget also needs to be realized and mapped. This is indicated by the related events. Grabbing the focus
     * immediately after creating the widget will likely fail and cause critical warnings.
     */
    public open fun grabFocus()

    /**
     * Recursively shows a widget, and any child widgets (if the [widget][WidgetBase] is a
     * [container][io.gitlab.guiVista.gui.layout.ContainerBase]).
     */
    public open fun showAll()

    /**
     * Inserts [group] into the widget. Children of the widget that implement `GtkActionable` can then be associated
     * with actions in group by setting their **action-name** to prefix **.action-name**. If [group] is *null* then a
     * previously inserted group for [name] is removed from the widget.
     * @param name The prefix for actions in the [group].
     * @param group The [ActionGroup] or *null*.
     */
    public open fun insertActionGroup(name: String, group: ActionGroup?)

    /**
     * Retrieves an array of strings containing the prefixes of GActionGroup's available to the widget.
     * @return An array of strings.
     */
    public open fun listActionPrefixes(): Array<String>

    /**
     * Retrieves the [ActionGroup] that was registered using [prefix]. The resulting [ActionGroup] may have been
     * registered to the widget or any [WidgetBase] in its ancestry. If no action group was found matching [prefix]
     * then *null* is returned.
     * @param prefix The prefix of the action group.
     * @return An [ActionGroup] or *null*.
     */
    public open fun fetchActionGroup(prefix: String): ActionGroupBase?
}
