package io.gitlab.guiVista.gui.widget.tool.item

import io.gitlab.guiVista.gui.widget.WidgetBase

/** A tool item that displays buttons. */
public expect class ToolButton : ToolButtonBase {
    public companion object {
        public fun create(iconWidget: WidgetBase, label: String): ToolButton
    }
}
