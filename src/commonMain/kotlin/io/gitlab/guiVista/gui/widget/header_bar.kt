package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.layout.ContainerBase

/** A box with a centered child. */
public expect class HeaderBar : ContainerBase {
    /**
     * The title to display. Default value is *""* (empty String).
     *
     * Data binding property name: **title**
     */
    public var title: String

    /**
     * The subtitle to display. Default value is *""* (empty String).
     *
     * Data binding property name: **subtitle**
     */
    public var subtitle: String

    /**
     * Custom title widget to display. Default value is *null*.
     *
     * Data binding property name: **custom-title**
     */
    public var customTitle: WidgetBase?

    /**
     * The decoration layout for buttons. If this property is not set, the “gtk-decoration-layout” setting is used.
     * Default value is *""* (empty String).
     *
     * There can be valid reasons for setting the declaration layout such as a header bar design that does not allow
     * for buttons to take room on the right, or only offers room for a single close button. Split header bars are
     * another example for overriding the setting. The format of the string is button names, separated by commas. A
     * colon separates the buttons that should appear on the left from those on the right. Recognized button names are
     * minimize, maximize, close, icon (the window icon) and menu (a menu button for the fallback app menu).
     *
     * For example, **menu:minimize,maximize,close** specifies a menu on the left, and minimize, maximize and close
     * buttons on the right. The layout can be unset by setting this property to a empty String (*""*).
     *
     * Data binding property name: **declaration-layout**
     */
    public var declarationLayout: String

    /**
     * If *true* then reserve space for a subtitle, even if none is currently set. Default value is *true*.
     *
     * Data binding property name: **has-subtitle**
     */
    public var hasSubtitle: Boolean

    /**
     * Whether to show window decorations. Which buttons are actually shown, and where is determined by the
     * [declarationLayout] property, and by the state of the window (e.g. a close button will not be shown if the
     * window can't be closed). Default value is *false*.
     *
     * Data binding property name: **show-close-button**
     */
    public var showCloseButton: Boolean

    public companion object {
        public fun create(): HeaderBar
    }

    /**
     * Adds the child to the left of the header bar.
     * @param child The Widget to add.
     */
    public fun prependChild(child: WidgetBase)

    /**
     * Adds the child to the right of the header bar.
     * @param child The Widget to add.
     */
    public fun appendChild(child: WidgetBase)
}
