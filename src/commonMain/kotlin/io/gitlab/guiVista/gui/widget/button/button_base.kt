package io.gitlab.guiVista.gui.widget.button

import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.gui.widget.Actionable
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for button objects. */
public expect interface ButtonBase : BinBase, Actionable {
    /**
     * Text of the label widget inside the button, if the button contains a label widget. Default value is *""* (an
     * empty String).
     *
     * Data binding property name: **label**
     */
    public open var label: String

    /**
     * If *true* the button will ignore the “gtk-button-images” setting, and always show the image if available. Use
     * this property if the button would be useless, or hard to use without the image. Default value is *false*.
     *
     * Data binding property name: **always-show-image**
     */
    public open var alwaysShowImage: Boolean

    /**
     * The child widget to appear next to the button text.
     *
     * Data binding property name: **image**
     */
    public open var image: WidgetBase?

    /**
     * If set an underline in the text indicates the next character should be used for the mnemonic accelerator key.
     * Default value is *false*.
     *
     * Data binding property name: **use-underline**
     */
    public open var useUnderline: Boolean
}
