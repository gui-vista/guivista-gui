package io.gitlab.guiVista.gui.widget

public object RangeEvent {
    public const val valueChanged: String = "value-changed"
    public const val moveSlider: String = "move-slider"
    public const val changeValue: String = "change-value"
    public const val adjustBounds: String = "adjust-bounds"
}
