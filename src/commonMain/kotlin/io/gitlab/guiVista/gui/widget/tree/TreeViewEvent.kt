package io.gitlab.guiVista.gui.widget.tree

public object TreeViewEvent {
    public const val columnsChanged: String = "columns-changed"
    public const val cursorChanged: String = "cursor-changed"
    public const val expandCollapseCursorRow: String = "expand-collapse-cursor-row"
    public const val moveCursor: String = "move-cursor"
    public const val rowActivated: String = "row-activated"
    public const val rowCollapsed: String = "row-collapsed"
    public const val rowExpanded: String = "row-expanded"
    public const val selectAll: String = "select-all"
    public const val selectCursorParent: String = "select-cursor-parent"
    public const val selectCursorRow: String = "select-cursor-row"
    public const val startInteractiveSearch: String = "start-interactive-search"
    public const val toggleCursorRow: String = "toggle-cursor-row"
    public const val unselectAll: String = "unselect-all"
}
