package io.gitlab.guiVista.gui.widget.button

/** Base interface for toggle button objects. */
public expect interface ToggleButtonBase : ButtonBase {
    /**
     * If the toggle button should be pressed in. Default value is *false*.
     *
     * Data binding property name: **active**
     */
    public open var active: Boolean

    /**
     * If the toggle button is in an "in between" state. Default value is *false*.
     *
     * Data binding property name: **inconsistent**
     */
    public open var inconsistent: Boolean

    /**
     * Whether the button is displayed as a separate indicator and label. You can call this function on a check button
     * or a radiobutton with `drawIndicator = false` to make the button look like a normal button. This can be used to
     * create linked strip of buttons that work like a GtkStackSwitcher.
     *
     * This function only affects instances of classes like GtkCheckButton and GtkRadioButton that derive from
     * GtkToggleButton, not instances of GtkToggleButton itself.
     */
    public open var mode: Boolean
}
