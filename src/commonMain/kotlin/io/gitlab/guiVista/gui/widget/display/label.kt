package io.gitlab.guiVista.gui.widget.display

/** A widget that displays a small to medium amount of text. */
public expect class Label : LabelBase {
    public companion object {
        public fun create(text: String = ""): Label
    }
}
