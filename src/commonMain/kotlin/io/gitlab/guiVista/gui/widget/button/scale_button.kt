package io.gitlab.guiVista.gui.widget.button

/** A button which pops up a scale. */
public expect class ScaleButton : ScaleButtonBase
