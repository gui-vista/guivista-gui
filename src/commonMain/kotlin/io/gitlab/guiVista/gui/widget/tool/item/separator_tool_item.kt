package io.gitlab.guiVista.gui.widget.tool.item

/** A toolbar item that separates groups of other toolbar items. */
public expect class SeparatorToolItem : ToolItemBase {
    /**
     * Whether the separator is drawn, or just blank.
     *
     * Data binding property name: **draw**
     */
    public var draw: Boolean

    public companion object {
        public fun create(): SeparatorToolItem
    }
}
