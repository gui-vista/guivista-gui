package io.gitlab.guiVista.gui.widget.button

/** Create buttons bound to a URL. */
public expect class LinkButton : ButtonBase {
    /**
     * The URI bound to this button. Default value is *""* (an empty String).
     *
     * Data binding property name: **uri**
     */
    public var uri: String

    /**
     * The *visited* state of this button. A visited link is drawn in a different color. Default value is *false*.
     *
     * Data binding property name: **visited**
     */
    public var visited: Boolean

    public companion object {
        public fun create(uri: String): LinkButton

        public fun createWithLabel(uri: String, label: String): LinkButton
    }
}
