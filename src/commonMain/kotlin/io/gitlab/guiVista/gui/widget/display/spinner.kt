package io.gitlab.guiVista.gui.widget.display

import io.gitlab.guiVista.gui.widget.WidgetBase

/** Show a spinner animation. */
public expect class Spinner : WidgetBase {
    /** Starts the animation of the spinner. */
    public fun start()

    /** Stops the animation of the spinner. */
    public fun stop()

    public companion object {
        public fun create(): Spinner
    }
}
