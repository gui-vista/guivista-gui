package io.gitlab.guiVista.gui.widget.dataEntry

import io.gitlab.guiVista.gui.cell.CellEditableBase
import io.gitlab.guiVista.gui.text.Editable
import io.gitlab.guiVista.gui.text.EntryBuffer
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for entry objects. */
public expect interface EntryBase : Editable, WidgetBase, CellEditableBase {
    /**
     * Whether to activate the default widget (such as the default button in a dialog) when Enter is pressed. Default
     * value is *false*.
     *
     * Data binding property name: **activates-default**
     */
    public open var activatesDefault: Boolean

    /**
     * The buffer that holds the text.
     *
     * Data binding property name: **buffer**
     */
    public open var buffer: EntryBuffer

    /**
     * If *false* then then the outside bevel is removed from the widget.
     *
     * Data binding property name: **has-frame**
     */
    public open var hasFrame: Boolean

    /**
     * Additional hints (beyond `inputPurpose`) that allow input methods to fine tune their behaviour.
     *
     * Data binding property name: **input-hints**
     */
    public open var inputHints: UInt

    /**
     * The invisible character is used when masking entry contents (in **password mode**). When it is not explicitly
     * set with the [invisibleChar] property, GTK determines the character to use from a list of possible candidates,
     * depending on availability in the current font. Default value is *.
     *
     * This style property allows the theme to prepend a character to the list of candidates.
     *
     * Data binding property name: **invisible-char**
     */
    public open var invisibleChar: Char

    /**
     * If text is overwritten when typing in the entry. Default value is *false*.
     *
     * Data binding property name: **overwrite-mode**
     */
    public open var overwriteMode: Boolean

    /**
     * The current fraction of the task that's been completed. Default value is *0*.
     *
     * Data binding property name: **progress-fraction**
     */
    public open var progressFraction: Double

    /**
     * The fraction of total entry width to move the progress bouncing block for each call to [progressPulse]. Default
     * value is *0.1*.
     *
     * Data binding property name: **progress-pulse-step**
     */
    public open var progressPulseStep: Double

    /**
     * The length of the text in the widget. Default value is *0*.
     *
     * Data binding property name: **text-length**
     */
    public open val textLength: UShort

    /**
     * When *false* the widget displays the **invisible char** instead of the actual text (password mode). Default
     * value is *true*.
     *
     * Data binding property name: **visibility**
     */
    public open var visibility: Boolean

    /**
     * The alignment for the contents of the entry. This controls the horizontal positioning of the contents when the
     * displayed text is shorter than the width of the entry.
     *
     * The horizontal alignment from *0* (left) to *1* (right). Reversed for RTL layouts.
     */
    public open var alignment: Float

    /**
     * String contents in the entry. Default value is *""* (an empty String).
     *
     * Data binding property name: **text**
     */
    public open var text: String

    /**
     * Placeholder text to use when [text] is empty. Default value is *""* (an empty String).
     *
     * Data binding property name: **placeholder-text**
     */
    public open var placeholderText: String

    /**
     * Maximum number of characters for this entry. Zero if no maximum. Default value is *0*.
     *
     * Data binding property name: **max-length**
     */
    public open var maxLength: Int

    /**
     * The desired maximum width of the [Entry] in characters. If this property is set to *-1* the width will be
     * calculated automatically. Default value is *-1*.
     *
     * Data binding property name: **max-width-chars**
     */
    public open var maxWidthChars: Int

    /**
     * Number of characters to leave space for in the [Entry]. Default value is *-1*.
     *
     * Data binding property name: **width-chars**
     */
    public open var widthChars: Int

    /**
     * Indicates that some progress is made but you don’t know how much. Causes the entry’s progress indicator to enter
     * **activity mode** where a block bounces back and forth. Each call to [progressPulse] causes the block to move by
     * a little bit (the amount of movement per pulse is determined by [progressPulseStep]).
     */
    public open fun progressPulse()

    /**
     * Unsets the invisible char previously set with [invisibleChar] so that the default invisible char is used again.
     */
    public open fun unsetInvisibleChar()

    /**
     * Causes entry to have keyboard focus. It behaves like [grabFocus], except that it doesn't select the contents of
     * the entry. You only want to call this on some special entries, which the user usually doesn't want to replace all
     * text in such as **search-as-you-type** entries.
     */
    public open fun grabFocusWithoutSelecting()

    open override fun disconnectEvent(handlerId: ULong)
}
