package io.gitlab.guiVista.gui.widget.button

import io.gitlab.guiVista.gui.widget.WidgetBase

/** A *light switch* style toggle. */
public expect class Switch : WidgetBase {
    /**
     * Whether the GtkSwitch widget is in its on or off state. Default value is *false*.
     *
     * Data binding property name: **active**
     */
    public var active: Boolean

    /**
     * The backend state that is controlled by the switch. Normally this is the same as “active”, unless the switch is
     * set up for delayed state changes. This function is typically called from a “state-set” signal handler. Default
     * value is *false*.
     *
     * Data binding property name: **state**
     */
    public var state: Boolean

    public companion object {
        public fun create(): Switch
    }
}
