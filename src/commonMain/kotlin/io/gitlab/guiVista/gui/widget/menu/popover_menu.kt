package io.gitlab.guiVista.gui.widget.menu

import io.gitlab.guiVista.gui.widget.PopoverBase

/** Popovers to use as menus. */
public expect class PopoverMenu : PopoverBase {
    public companion object {
        public fun create(): PopoverMenu
    }

    /**
     * Opens a sub menu of the popover. The [name] **MUST** be one of the names given to the sub menus of the popover
     * with **submenu**, or **main** to switch back to the main menu. Note that `GtkModelButton` will open sub menus
     * automatically when the **menu-name** property is set, so this function is only needed when you are using other
     * kinds of widgets to initiate menu changes.
     * @param name The name of the menu to switch to.
     */
    public fun openSubMenu(name: String)
}
