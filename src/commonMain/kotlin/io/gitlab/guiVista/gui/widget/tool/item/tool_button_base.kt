package io.gitlab.guiVista.gui.widget.tool.item

import io.gitlab.guiVista.gui.widget.WidgetBase

/** Base interface for tool button objects. */
public expect interface ToolButtonBase : ToolItemBase {
    /**
     * The name of the themed icon displayed on the item. This property only has an effect if not overridden by
     * [labelWidget], or [iconWidget] properties. Default value is *""* (an empty String).
     *
     * Data binding property name: **icon-name**
     */
    public open var iconName: String

    /**
     * Icon widget to display in the item.
     *
     * Data binding property name: **icon-widget**
     */
    public open var iconWidget: WidgetBase?

    /**
     * Text to show in the item. Default value is *""* (an empty String).
     *
     * Data binding property name: **label**
     */
    public open var label: String

    /**
     * Widget to use as the item label.
     *
     * Data binding property name: **label-widget**
     */
    public open var labelWidget: WidgetBase?

    /**
     * If set an underline in the [label] property indicates that the next character should be used for the mnemonic
     * accelerator key in the overflow menu. Default value is *false*.
     *
     * Data binding property name: **use-underline**
     */
    public open var useUnderline: Boolean
}
