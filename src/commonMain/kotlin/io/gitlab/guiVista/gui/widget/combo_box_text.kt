package io.gitlab.guiVista.gui.widget

/** A simple text only combo box. */
public expect class ComboBoxText : ComboBoxBase {
    /** The currently active string in the combo box or *null* if nothing is selected. */
    public val activeText: String?

    public companion object {
        public fun create(): ComboBoxText

        public fun createWithEntry(): ComboBoxText
    }

    /**
     * Appends [text] to the list of strings stored in the combo box. If [id] isn't *null* then it is used as the ID of
     * the row. This is the same as calling [insertText] with a position of *-1*.
     * @param id A String identifier for this value or *null*.
     * @param text The text to append.
     */
    public fun appendText(id: String? = null, text: String)

    /**
     * Prepends [text] to the list of strings stored in the combo box. If [id] isn't *null* then it is used as the ID
     * of the row. This is the same as calling [insertText] with a position of *0*.
     * @param id A String identifier for this value or *null*.
     * @param text The text to prepend.
     */
    public fun prependText(id: String?, text: String)

    /**
     * Inserts [text] at the [position][pos] in the list of strings stored in the combo box. If [id] isn't *null* then
     * it is used as the ID of the row. See “id-column”.

    If position is negative then text is appended.
     * @param pos An index to insert the [text].
     * @param id A String identifier for this value or *null*.
     * @param text The text to prepend.
     */
    public fun insertText(pos: Int, id: String?, text: String)

    /**
     * Removes the String at the [position][pos] from the combo box.
     * @param pos Index of the item to remove.
     */
    public fun removeText(pos: Int)

    /**
     * Removes all the text entries from the combo box.
     */
    public fun removeAllText()
}
