package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.core.dataType.SinglyLinkedList
import io.gitlab.guiVista.gui.window.ScrolledWindowBase
import io.gitlab.guiVista.io.file.FileBase

/** A sidebar that displays frequently used places in the file system. */
public expect class PlacesSideBar : ScrolledWindowBase {
    /**
     * Modes in which the calling application can open locations selected in the sidebar. Default value is
     * `GTK_PLACES_OPEN_NORMAL`.
     *
     * Data binding property name: **open-flags**
     */
    public var openFlags: UInt

    /**
     * The location to highlight in the sidebar.
     *
     * Data binding property name: **location**
     */
    public var location: FileBase

    /**
     * Whether the sidebar includes a builtin shortcut for recent files. Default value is *true*.
     *
     * Data binding property name: **show-recent**
     */
    public var showRecent: Boolean

    /**
     * Whether the sidebar includes a builtin shortcut to the Desktop folder. Default value is *true*.
     *
     * Data binding property name: **show-desktop**
     */
    public var showDesktop: Boolean

    /**
     * Whether the sidebar only includes local files. Default value is *false*.
     *
     * Data binding property name: **local-only**
     */
    public var localOnly: Boolean

    /**
     * Whether the sidebar includes a builtin shortcut to manually enter a location. Default value is *false*.
     *
     * Data binding property name: **show-enter-location**
     */
    public var showEnterLocation: Boolean

    /**
     * Whether the sidebar includes a builtin shortcut to the Trash location. Default value is *true*.
     *
     * Data binding property name: **show-trash**
     */
    public var showTrash: Boolean

    /**
     * Whether the sidebar includes an item to show external locations. Default value is *false*.
     *
     * Data binding property name: **show-other-locations**
     */
    public var showOtherLocations: Boolean

    public companion object {
        public fun create(): PlacesSideBar
    }

    /**
     * Applications may want to present some folders in the places sidebar if they could be immediately useful to
     * users. For example a drawing program could add a “/usr/share/clipart” location when the sidebar is being used
     * in an **Insert Clipart** dialog box. This function adds the specified location to a special place for immutable
     * shortcuts. The shortcuts are application-specific. They are not shared across applications, and they are not
     * persistent.
     *
     * If this function is called multiple times with different locations then the shortcuts are added to the
     * sidebar’s list in the same order as the function is called.
     * @param location The location to add as an application specific shortcut.
     */
    public infix fun addShortcut(location: FileBase)

    /**
     * Removes an application specific shortcut that has been previously been inserted with [addShortcut]. If the
     * [location] isn't a shortcut in the sidebar then nothing is done.
     * @param location The location to remove.
     */
    public infix fun removeShortcut(location: FileBase)

    /**
     * Gets the list of shortcuts.
     * @return A list of `GFile` of the locations that have been added as application specific shortcuts with
     * [addShortcut]. To free this list you can use the `g_slist_free_full` function.
     */
    public fun listShortcuts(): SinglyLinkedList

    /**
     * This function queries the bookmarks added by the user to the places sidebar, and returns one of them. This
     * function is used by [io.gitlab.guiVista.gui.dialog.FileChooser] to implement the **Alt-1**, **Alt-2**, etc.
     * shortcuts, which activate the corresponding bookmark.
     * @param index The index of the bookmark to query.
     * @return The bookmark specified by [index], or *null* if no such index exist. Note that the indices start at *0*,
     * even though the file chooser starts them with the keyboard shortcut **Alt-1**.
     */
    public fun fetchBookmarkByIndex(index: Int): FileBase?
}