package io.gitlab.guiVista.gui.widget.tool.item

/** A tool item containing a toggle button. */
public expect class ToggleToolButton : ToggleToolButtonBase {
    public companion object {
        public fun create(): ToggleToolButton
    }
}
