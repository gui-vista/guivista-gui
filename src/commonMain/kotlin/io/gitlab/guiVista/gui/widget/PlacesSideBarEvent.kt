package io.gitlab.guiVista.gui.widget

public object PlacesSideBarEvent {
    public const val mount: String = "mount"
    public const val openLocation: String = "open-location"
    public const val populatePopup: String = "populate-popup"
    public const val showEnterLocation: String = "show-enter-location"
    public const val showErrorMessage: String = "show-error-message"
    public const val showOtherLocationsWithFlags: String = "show-other-locations-with-flags"
    public const val unmount: String = "unmount"
}
