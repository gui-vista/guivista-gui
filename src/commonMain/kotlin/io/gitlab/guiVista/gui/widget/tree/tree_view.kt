package io.gitlab.guiVista.gui.widget.tree

import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.Tooltip
import io.gitlab.guiVista.gui.cell.renderer.CellRendererBase
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreePath
import io.gitlab.guiVista.gui.widget.dataEntry.EntryBase

/** A widget for displaying both trees, and lists. */
public expect class TreeView : ContainerBase {
    /** The [TreeSelection] associated with the tree view. */
    public val selection: TreeSelection

    /**
     * Whether the view is reorderable. Default value is *false*.
     *
     * Data binding property name: **reorderable**
     */
    public var reorderable: Boolean

    /** The number of columns in the given tree view. */
    public val totalColumns: UInt

    /** Extra indentation for each level. Default value is *0*. */
    public var levelIndentation: Int

    /**
     * Is *true* if the view has expanders. Default value is *true*.
     *
     * Data binding property name: **show-expanders**
     */
    public var showExpanders: Boolean

    /**
     * The model for the tree view.
     *
     * Data binding property name: **model**
     */
    public var model: TreeModelBase

    /**
     * Whether to show the column header buttons. Default value is *true*.
     *
     * Data binding property name: **headers-visible**
     */
    public var headersVisible: Boolean

    /**
     * Whether all header columns are clickable.
     *
     * Data binding property name: **headers-clickable**
     */
    public var headersClickable: Boolean

    /**
     * The activate-on-single-click property specifies whether the **row-activated** event will be fired after a single
     * click. Default value is *false*.
     *
     * Data binding property name: **activate-on-single-click**
     */
    public var activateOnSingleClick: Boolean

    /**
     * The column for the expander column.
     *
     * Data binding property name: **expander-column**
     */
    public var expanderColumn: TreeViewColumn

    /**
     * Model column to search through during interactive search. Default value is *-1*.
     *
     * Data binding property name: **search-column**
     */
    public var searchColumn: Int

    /** The Entry widget which is currently in use as an interactive search entry for the tree view. */
    public var searchEntry: EntryBase?

    /**
     * Enables or disables the hover selection mode of the tree view. Hover selection makes the selected row follow
     * the pointer. Currently this works only for the selection modes `GTK_SELECTION_SINGLE`, and
     * `GTK_SELECTION_BROWSE`. This mode is primarily intended for tree views in popups, e.g. in ComboBox or
     * EntryCompletion widget.
     *
     * Default value is *false*. Data binding property name: **hover-selection**
     */
    public var hoverSelection: Boolean

    /**
     * Enables or disables the hover expansion mode of the tree view. Hover expansion makes rows expand or collapse if
     * the pointer moves over them. This mode is primarily intended for tree views in popups, e.g. in ComboBox or
     * EntryCompletion widget.
     *
     * Default value is *false*. Data binding property name: **hover-expand**
     */
    public var hoverExpand: Boolean

    /**
     * Whether to enable selection of multiple items by dragging the mouse pointer. Default value is *false*.
     *
     * Data binding property name: **rubber-banding**
     */
    public var rubberBanding: Boolean

    /**
     * Whether tree lines should be drawn in the tree view. Default value is *false*.
     *
     * Data binding property name: **enable-tree-lines**
     */
    public var enableTreeLines: Boolean

    /**
     * The column in the model containing the tooltip texts for the rows. Default value is *-1*.
     *
     * Data binding property name: **tooltip-column**
     */
    public var tooltipColumn: Int

    public companion object {
        public fun create(): TreeView

        /**
         * Creates a new [TreeView] widget with the model initialized to [model].
         * @param model The model to use.
         * @return A new [TreeView].
         */
        public fun createWithModel(model: TreeModelBase): TreeView
    }

    /**
     * Appends column to the list of columns. If tree view has **fixed_height** mode enabled then [column] **MUST**
     * have its **sizing** property set to be `GTK_TREE_VIEW_COLUMN_FIXED`.
     * @param column The column to add.
     * @return The number of columns in the tree view after appending.
     */
    public fun appendColumn(column: TreeViewColumn): Int

    /**
     * Removes [column] from the tree view.
     * @param column The column to remove.
     * @return The number of columns in the tree view after removing.
     */
    public fun removeColumn(column: TreeViewColumn): Int

    /**
     * This inserts the [column] into the tree view at [pos]. If [pos] is *-1* then the column is inserted at the end.
     * If the tree view has **fixed_height** mode enabled then the [column] **MUST** have its **sizing** property set
     * to be `GTK_TREE_VIEW_COLUMN_FIXED`.
     * @param column The column to be inserted.
     * @return The number of columns in the tree view after insertion.
     */
    public fun insertColumn(column: TreeViewColumn, pos: Int = -1): Int

    /**
     * Gets the [TreeViewColumn] at the given [position][pos] in the tree view.
     * @param pos The position of the column, counting from *0*.
     * @return The [TreeViewColumn] or *null* if the [position][pos] is outside the range of columns.
     */
    public fun fetchColumn(pos: Int): TreeViewColumn?

    /**
     * Returns a list of all the tree view columns currently in the tree view. The returned list **MUST** be freed with
     * [DoublyLinkedList.close].
     * @return A list of tree view columns.
     */
    public fun fetchMultipleColumns(): DoublyLinkedList

    /**
     * Moves [column] to be after [baseColumn]. If [baseColumn] is *null* then [column] is placed in the first position.
     * @param column The column to be moved.
     * @param baseColumn The [TreeViewColumn] to be moved relative to or *null*.
     */
    public fun moveColumnAfter(column: TreeViewColumn, baseColumn: TreeViewColumn?)

    /**
     * Scrolls the tree view such that the top left corner of the visible area is [treeX] , [treeY], where [treeX], and
     * [treeY] are specified in tree coordinates. The tree view **MUST** be realized before this function is called. If
     * it isn't you probably want to be using [scrollToCell].
     *
     * If either [treeX] or [treeY] are *-1* then that direction isn’t scrolled.
     * @param treeX The X coordinate of new top-left pixel of visible area or *-1*.
     * @param treeY The Y coordinate of new top-left pixel of visible area or *-1*.
     */
    public fun scrollToPoint(treeX: Int, treeY: Int)

    /**
     * Moves the alignments of the tree view to the position specified by [column] and [path]. If [column] is *null*
     * then no horizontal scrolling occurs. Likewise if [path] is *null* then no vertical scrolling occurs. At a
     * minimum, one of column or path need to be non null. Note that [rowAlign] determines where the row is placed, and
     * [colAlign] determines where column is placed. Both are expected to be between *0.0* and *1.0*. A value of *0.0*
     * means left/top alignment, *1.0* means right/bottom alignment, and *0.5* means center.
     *
     * If use_align is *false* then the alignment arguments are ignored, and the tree does the minimum amount of work
     * to scroll the cell onto the screen. This means that the cell will be scrolled to the edge closest to its current
     * position. If the cell is currently visible on the screen then nothing is done.
     *
     * This function only works if the [model] is set, and [path] is a valid row on the [model]. If the [model] changes
     * before the tree view is realized then the centered path will be modified to reflect this change.
     * @param path The path of the row to move to or *null*.
     * @param column The [TreeViewColumn] to move horizontally to or *null*.
     * @param useAlign Whether to use alignment arguments or *false*.
     * @param rowAlign The vertical alignment of the row specified by [path].
     * @param colAlign The horizontal alignment of the column specified by [column].
     */
    public fun scrollToCell(path: TreePath?, column: TreeViewColumn?, useAlign: Boolean, rowAlign: Float,
                            colAlign: Float)

    /**
     * Sets the current keyboard focus to be at [path], and selects it. This is useful when you want to focus the
     * user’s attention on a particular row. If [focusColumn] isn't null then focus is given to the column specified
     * by it. Additionall, if [focusColumn] is specified, and [startEditing] is *true* then editing should be started
     * in the specified cell. This function is often followed by [io.gitlab.guiVista.gui.widget.WidgetBase.grabFocus]
     * (with the tree view) in order to give keyboard focus to the widget. Please note that editing can only happen
     * when the widget is realized.
     *
     * If [path] is invalid for [model] then the current cursor (if any) will be unset, and the function will return
     * without failing.
     * @param path The tree path to use.
     * @param focusColumn The column to have focus or *null*.
     * @param startEditing A value of *true* if the specified cell should start being edited.
     */
    public fun changeCursor(path: TreePath, focusColumn: TreeViewColumn?, startEditing: Boolean)

    /**
     * Sets the current keyboard focus to be at [path], and selects it. This is useful when you want to focus the
     * user’s attention on a particular row. If [focusColumn] isn't null then focus is given to the column specified
     * by it. If [focusColumn] and [focusCell] are not null, and [focusColumn] contains two or more editable or
     * activatable cells, then focus is given to the cell specified by [focusCell]. Additionally if [focusColumn] is
     * specified, and [startEditing] is *true* then editing should be started in the specified cell. This function is
     * often followed by [io.gitlab.guiVista.gui.widget.WidgetBase.grabFocus] (with the tree view) in order to give
     * keyboard focus to the widget. Please note that editing can only happen when the widget is realized.
     *
     * If path is invalid for [model] then the current cursor (if any) will be unset, and the function will return
     * without failing.
     * @param path The tree path to use.
     * @param focusColumn The column to have focus or *null*.
     * @param focusCell The cell renderer to use or *null*.
     * @param startEditing A value of *true* if the specified cell should start being edited.
     */
    public fun changeCursorOnCell(path: TreePath, focusColumn: TreeViewColumn?, focusCell: CellRendererBase?,
                                  startEditing: Boolean)

    /**
     * Returns path, and focusColumn with the current path and focus column. If the cursor isn’t currently set then
     * path will be *null*. If no column currently has focus then focusColumn will be *null*. The returned [TreePath]
     * (path) **MUST** be freed with [TreePath.close] when you are done with it.
     * @return A Pair containing the following:
     * 1. path - The current cursor path.
     * 2. focusColumn - The current focus column.
     */
    public fun fetchCursor(): Pair<TreePath?, TreeViewColumn?>

    /**
     * Activates the cell determined by [path] and [column].
     * @param path The tree path to be activated.
     * @param column The tree view column to be activated.
     */
    public fun activateRow(path: TreePath, column: TreeViewColumn)

    /** Recursively expands all nodes in the tree view. */
    public fun expandAll()

    /** Recursively collapses all visible, expanded nodes in the tree view. */
    public fun collapseAll()

    /**
     * Expands the row at [path]. This will also expand all parent rows of [path] as necessary.
     * @param path The path to a row.
     */
    public fun expandToPath(path: TreePath)

    /**
     * Opens the row so its children are visible.
     * @param path The path to a row.
     * @param openAll Whether to recursively expand, or just expand immediate children.
     * @return A value of *true* if the row existed and had children.
     */
    public fun expandRow(path: TreePath, openAll: Boolean): Boolean

    /**
     * Collapses a row (hides its child rows, if they exist).
     * @param path The path to a row in the tree view.
     * @return A value of *true* if the row was collapsed.
     */
    public fun collapseRow(path: TreePath): Boolean

    /**
     * Returns *true* if the node pointed to by [path] is expanded in the tree view.
     * @param path A [TreePath] to test expansion state.
     * @return A value of *true* if the [path] is expanded.
     */
    public fun rowExpanded(path: TreePath): Boolean

    /**
     * Finds the path at the point (xPos , yPos) relative to bin window coordinates (please see
     * `gtk_tree_view_get_bin_window()`). That is [xPos], and [yPos] are relative to an events coordinates. Note that
     * [xPos], and [yPos] **MUST** come from an event on the tree view only where
     * `event->window == gtk_tree_view_get_bin_window()`. It is primarily for things like popup menus. If path isn't
     * null then it will be filled with the [TreePath] at that point. This path should be freed with [TreePath.close].
     * If column isn't null then it will be filled with the column at that point.
     *
     * All cellXPos and cellYPos parameters return the coordinates relative to the cell background (i.e. the
     * background_area passed to `gtk_cell_renderer_render()`). This function is only meaningful if the tree view is
     * realized. Therefore this function will always return *false* if the tree view is not realized, or doesn't have a
     * [model].
     *
     * For converting widget coordinates (eg. the ones you get from `GtkWidget::query-tooltip`), please see
     * `gtk_tree_view_convert_widget_to_bin_window_coords()`.
     * @param xPos The X position to be identified (relative to bin window).
     * @param yPos The Y position to be identified (relative to bin window).
     * @return The result as [PathAtPositionResult].
     * @see PathAtPositionResult
     */
    public fun fetchPathAtPosition(xPos: Int, yPos: Int): PathAtPositionResult

    /**
     * Determine whether the point (xPos , yPos) in the tree view is blank. That is no cell content nor an expander
     * arrow is drawn at the location. If so then the location can be considered as the background. You might wish to
     * take special action on clicks on the background, such as clearing a current selection, having a custom context
     * menu or starting rubber banding. The xPos and yPos that are provided **MUST** be relative to bin window
     * coordinates. That is xPos, and yPos **MUST** come from an event on the tree view where
     * `event->window == gtk_tree_view_get_bin_window()`.
     *
     * For converting widget coordinates (eg. the ones you get from `GtkWidget::query-tooltip`), please see
     * `gtk_tree_view_convert_widget_to_bin_window_coords()`. The path, column, cellXPos, and cellYPos arguments will
     * be filled in likewise as for [fetchPathAtPosition].
     * @see fetchPathAtPosition
     * @see BlankAtPositionResult
     * @return The result as [BlankAtPositionResult].
     */
    public fun isBlankAtPosition(xPos: Int, yPos: Int): BlankAtPositionResult

    /**
     * Sets startPath and endPath to be the first and last visible path. Note that there may be invisible paths in
     * between. The paths should be freed with [TreePath.close] after use.
     * @return A Triple containing the following:
     * 1. startPath - The start of the region.
     * 2. endPath - The end of the region.
     * 3. validPaths - Is *true* if valid paths were placed in startPath, and endPath.
     */
    public fun fetchVisibleRange(): Triple<TreePath, TreePath, Boolean>

    /**
     * Converts tree coordinates (coordinates in full scrollable area of the tree) to widget coordinates.
     * @param treeXPos The X coordinate relative to the tree.
     * @param treeYPos The Y coordinate relative to the tree.
     * @return A Pair containing the following:
     * 1. widgetXPos - The widget X coordinate.
     * 1. widgetYPos - The widget Y coordinate.
     */
    public fun convertTreeToWidgetCoords(treeXPos: Int, treeYPos: Int): Pair<Int, Int>

    /**
     * Converts widget coordinates to coordinates for the tree (the full scrollable area of the tree).
     * @param widgetXPos The X coordinate relative to the widget.
     * @param widgetYPos The Y coordinate relative to the widget.
     * @return A Pair containing the following:
     * 1. treeXPos - The tree X coordinate.
     * 1. treeYPos - The tree Y coordinate.
     */
    public fun convertWidgetToTreeCoords(widgetXPos: Int, widgetYPos: Int): Pair<Int, Int>

    /**
     * Sets the tip area of [tooltip] to be the area covered by the row at [path]. See `gtk_tooltip_set_tip_area()`.
     * @see tooltipColumn
     * @param tooltip The tooltip to use.
     * @param path The path to use.
     */
    public fun changeTooltipRow(tooltip: Tooltip, path: TreePath)

    /**
     * Sets the tip area of [tooltip] to the area [path], [column], and [cell] have in common. For example if [path] is
     * *null* and [column] is set then the tip area will be set to the full area covered by [column]. See also
     * `gtk_tooltip_set_tip_area()`. Note that if [path] isn't specified and cell is set, and part of a column
     * containing the expander, then the [tooltip] might not show and hide at the correct position. In such cases
     * [path] **MUST** be set to the current node under the mouse cursor for this function to operate correctly.
     * @see tooltipColumn
     * @param tooltip The tooltip to use.
     * @param path A tree path or *null*.
     * @param column A tree view column or *null*.
     * @param cell A cell renderer or *null*.
     */
    public fun changeTooltipCell(tooltip: Tooltip, path: TreePath?, column: TreeViewColumn?, cell: CellRendererBase?)

    /**
     * This function is supposed to be used in a **query-tooltip** event handler for the tree view. The [xPos], [yPos],
     * and [keyboardTip] values which are received in the event handler, should be passed to this function without
     * modification. The return value indicates whether there is a tree view row at the given coordinates (*true*), or
     * not (*false*) for mouse tooltips. For keyboard tooltips the row returned will be the cursor row. When *true* any
     * of model, path, and iter which have been provided will be set to point to that row and the corresponding model.
     *
     * The xPos, and yPos parameters will always be converted to be relative to the tree view's bin window if
     * [keyboardTip] is *false*.
     * @param xPos The X coordinate (relative to widget coordinates).
     * @param yPos The Y coordinate (relative to widget coordinates).
     * @param keyboardTip Whether this is a keyboard tooltip or not.
     * @return The result as a [TooltipContextResult].
     */
    public fun fetchTooltipContext(xPos: Int, yPos: Int, keyboardTip: Boolean): TooltipContextResult
}
