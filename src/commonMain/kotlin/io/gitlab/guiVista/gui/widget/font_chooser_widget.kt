package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.dialog.FontChooser
import io.gitlab.guiVista.gui.layout.BoxBase

/** A widget for selecting fonts. */
public expect class FontChooserWidget : BoxBase, FontChooser {
    public companion object {
        public fun create(): FontChooserWidget
    }

    override fun disconnectEvent(handlerId: ULong)
}
