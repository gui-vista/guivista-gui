package io.gitlab.guiVista.gui.widget.tool.item

/** A widget that can be added to [ToolShell][io.gitlab.guiVista.gui.widget.tool.ToolShell]. */
public expect class ToolItem : ToolItemBase {
    public companion object {
        public fun create(): ToolItem
    }
}
