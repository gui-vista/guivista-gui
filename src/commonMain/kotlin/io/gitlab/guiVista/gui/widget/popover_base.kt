package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.layout.BinBase
import io.gitlab.guiVista.io.application.menu.MenuModel

/** Base interface for Context dependent bubbles. */
public expect interface PopoverBase : BinBase {
    /**
     * Sets whether the popover is modal (so other elements in the window do not receive input while the popover is
     * visible). Default value is *true*.
     *
     * Data binding property name: **modal**
     */
    public open var modal: Boolean

    /**
     * The attached widget.
     *
     * Data binding property name: **relative-to**
     */
    public open var relativeTo: WidgetBase

    /**
     * Whether show/hide transitions are enabled for this popover. Default value is *true*.
     *
     * Data binding property name: **transitions-enabled**
     */
    public open var transitionsEnabled: Boolean

    /**
     * Establishes a binding between a [PopoverBase] and a [MenuModel]. The contents of the popover are removed, and
     * then refilled with menu items according to the [model]. When the [model] changes the popover is updated. Calling
     * this function twice on popover with a different model will cause the first binding to be replaced with a binding
     * to the new model. If the [model] is *null* then any previous binding is undone and all children are removed.
     *
     * If [actionNamespace] isn't *null* then the effect is as if all actions mentioned in the [model] have their names
     * prefixed with the namespace, plus a dot. For example, if the action **quit** is mentioned and [actionNamespace]
     * is **app** then the effective action name is **app.quit**.
     *
     * This function uses `GtkActionable` to define the action name, and target values on the created menu items. If you
     * want to use an action group other than **app** and **win**, or if you want to use a menu shell outside of a
     * [io.gitlab.guiVista.gui.window.AppWindow], then you will need to attach your own action group to the widget
     * hierarchy using `gtk_widget_insert_action_group()`. As an example, if you created a group with a **quit**
     * action, and inserted it with the name **mygroup** then you would use the action name **mygroup.quit** in your
     * [MenuModel].
     * @param model The menu model to bind to or *null* to remove binding.
     * @param actionNamespace The namespace for the actions in the [model].
     */
    public open fun bindModel(model: MenuModel?, actionNamespace: String? = null)
}
