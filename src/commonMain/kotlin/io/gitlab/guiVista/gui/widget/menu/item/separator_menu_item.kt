package io.gitlab.guiVista.gui.widget.menu.item

/** A separator used in menus. */
public expect class SeparatorMenuItem : MenuItemBase {
    public companion object {
        public fun create(): SeparatorMenuItem
    }
}
