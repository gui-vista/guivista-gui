package io.gitlab.guiVista.gui.widget.tool.item

import io.gitlab.guiVista.core.dataType.SinglyLinkedList

/** A toolbar item that contains a radio button. */
public expect class RadioToolButton : ToggleToolButtonBase {
    /**
     * The radio button group button belongs to.
     *
     * Data binding property name: **group**
     */
    public var group: SinglyLinkedList?

    public companion object {
        public fun create(): RadioToolButton

        public fun fromWidget(group: RadioToolButton): RadioToolButton
    }
}
