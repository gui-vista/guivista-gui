package io.gitlab.guiVista.gui.widget.tree

import io.gitlab.guiVista.gui.tree.TreePath

/**
 * Contains results that are returned from the [TreeView.isBlankAtPosition] function.
 * @param blankArea If *true* then the area at the given coordinates is blank.
 * @param path The tree path.
 * @param column The tree view column.
 * @param cellXPos The X coordinate relative to where the cell is placed.
 * @param cellXPos The Y coordinate relative to where the cell is placed.
 */
public data class BlankAtPositionResult(
    val blankArea: Boolean,
    val path: TreePath,
    val column: TreeViewColumn,
    val cellXPos: Int,
    val cellYPos: Int
)
