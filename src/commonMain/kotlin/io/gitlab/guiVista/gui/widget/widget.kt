package io.gitlab.guiVista.gui.widget

/** Represents an existing widget (control). */
public expect class Widget : WidgetBase {
    public companion object {
        public fun fromWidgetBase(widgetBase: WidgetBase): Widget
    }
}
