package io.gitlab.guiVista.gui.widget.display

public object InfoBarEvent {
    public const val close: String = "close"
    public const val response: String = "response"
}
