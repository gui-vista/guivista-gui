package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.gui.layout.Orientable

/** A scroll bar that is used for providing scrolling support for a widget. */
public expect class ScrollBar : Range, Orientable
