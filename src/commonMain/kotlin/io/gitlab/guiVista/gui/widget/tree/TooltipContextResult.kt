package io.gitlab.guiVista.gui.widget.tree

import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreeModelIterator
import io.gitlab.guiVista.gui.tree.TreePath

/**
 * Contains results that are returned from the [TreeView.fetchTooltipContext] function.
 * @param model The tree model.
 * @param path The tree path.
 * @param iter The tree model iterator.
 * @param isRow If *true* then the given tooltip points to a row.
 */
public data class TooltipContextResult(
    val model: TreeModelBase,
    val path: TreePath,
    val iter: TreeModelIterator,
    val isRow: Boolean
)
