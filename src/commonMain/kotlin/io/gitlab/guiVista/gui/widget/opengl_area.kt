package io.gitlab.guiVista.gui.widget

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.InitiallyUnowned
import io.gitlab.guiVista.gui.OpenGlContext

/** A widget for custom drawing with OpenGL. */
public expect class OpenGlArea : InitiallyUnowned {
    /**
     * The [OpenGlContext] used by this area.
     *
     * Data binding property name: **context**
     */
    public val context: OpenGlContext?

    /**
     * An error on the [OpenGlArea] which will be shown instead of the GL rendering. This is useful in the
     * **create-context** event if GL context creation fails.
     */
    public var error: Error?

    /**
     * If *true* then the buffer allocated by the widget will have an alpha channel component, and when rendering to
     * the window the result will be composited over whatever is below the widget. On *false* there will be no alpha
     * channel, and the buffer will fully replace anything below the widget.
     *
     * Data binding property name: **has-alpha**
     */
    public var hasAlpha: Boolean

    /**
     * If *true* then the widget will allocate and enable a depth buffer for the target frame buffer. Otherwise there
     * will be none.
     *
     * Data binding property name: **has-depth-buffer**
     */
    public var hasDepthBuffer: Boolean

    /**
     * If *true* then the widget will allocate and enable a stencil buffer for the target frame buffer. Otherwise there
     * will be none.
     *
     * Data binding property name: **has-stencil-buffer**
     */
    public var hasStencilBuffer: Boolean

    /**
     * If *true* then the **render** event will be fired every time the widget draws. This is the default, and is
     * useful if drawing the widget is faster. On *false* the data from previous rendering is kept around, and will be
     * used for drawing the widget the next time, unless the window is resized. In order to force a rendering
     * [queueRender] **MUST** be called. This mode is useful when the scene changes seldomly, but takes a long time to
     * redraw.
     *
     * Data binding property name: **auto-render**
     */
    public var autoRender: Boolean

    public companion object {
        public fun create(): OpenGlArea
    }

    /**
     * Ensures that the `GdkGlContext` used by [OpenGlArea] is associated with the [OpenGlArea]. This function is
     * automatically called before firing the **render** event, and doesn't normally need to be called by application
     * code.
     */
    public fun makeCurrent()

    /**
     * Marks the currently rendered data (if any) as invalid, and queues a redraw of the widget, ensuring that the
     * **render** event is fired during the draw. This is only needed when the [autoRender] property has been
     * accessed with a *false* value. The default behaviour is to fire **render** on each draw.
     */
    public fun queueRender()

    /**
     * Ensures that the [OpenGlArea] frame buffer object is made the current draw and read target, and that all the
     * required buffers for the area are created and bound to the frame buffer. This function is automatically called
     * before firing the **render** signal, and doesn't normally need to be called by application code.
     */
    public fun attachBuffers()

    /**
     * Retrieves the required version of OpenGL set using [changeRequiredVersion].
     * @return A Pair containing the following:
     * 1. majorVer - Major version number
     * 2. minorVer - Minor version number
     */
    public fun fetchRequiredVersion(): Pair<Int, Int>

    /**
     * Sets the required version of OpenGL to be used when creating the context for the widget. This function **MUST**
     * be called **before** the area has been realized.
     * @param major The major version.
     * @param minor The minor version.
     */
    public fun changeRequiredVersion(major: Int, minor: Int)
}