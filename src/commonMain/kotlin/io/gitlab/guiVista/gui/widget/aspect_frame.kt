package io.gitlab.guiVista.gui.widget

/** A frame that constrains its child to a particular aspect ratio. */
public expect class AspectFrame : FrameBase {
    public companion object {
        public fun create(label: String, xAlign: Float, yAlign: Float, ratio: Float, obeyChild: Boolean): AspectFrame
    }

    /**
     * Sets parameters for an existing [AspectFrame].
     * @param xAlign Horizontal alignment of the child within the allocation of the [AspectFrame]. This ranges from
     * *0.0* (left aligned) to *1.0* (right aligned).
     * @param yAlign Vertical alignment of the child within the allocation of the [AspectFrame]. This ranges from *0.0*
     * (top aligned) to *1.0* (bottom aligned).
     * @param ratio The desired aspect ratio.
     * @param obeyChild If set to *true* then the ratio is ignored, and the aspect ratio is taken from the requisition
     * of the child.
     */
    public fun changeParameters(xAlign: Float, yAlign: Float, ratio: Float, obeyChild: Boolean)
}
