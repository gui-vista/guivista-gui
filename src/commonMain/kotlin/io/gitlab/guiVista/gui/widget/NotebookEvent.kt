package io.gitlab.guiVista.gui.widget

public object NotebookEvent {
    public const val changeCurrentPage: String = "change-current-page"
    public const val createWindow: String = "create-window"
    public const val focusTab: String = "focus-tab"
    public const val moveFocusOut: String = "move-focus-out"
    public const val pageAdded: String = "page-added"
    public const val pageRemoved: String = "page-removed"
    public const val pageReordered: String = "page-reordered"
    public const val reorderTab: String = "reorder-tab"
    public const val selectPage: String = "select-page"
    public const val switchPage: String = "switch-page"
}