package io.gitlab.guiVista.gui.widget.button

public object ButtonBaseEvent {
    public const val clicked: String = "clicked"
    public const val activate: String = "activate"
}
