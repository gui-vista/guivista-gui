package io.gitlab.guiVista.gui.widget.button

import io.gitlab.guiVista.gui.RgbaColor
import io.gitlab.guiVista.gui.dialog.ColorChooser

/** A button to launch a color selection dialog. */
public expect class ColorButton : ButtonBase, ColorChooser {
    /**
     * The title of the color selection dialog. Default value is *Pick a Color*.
     *
     * Data binding property name: **title**
     */
    public var title: String

    public companion object {
        /**
         * Creates a new [ColorButton]. This returns a widget in the form of a small button containing a swatch
         * representing the current selected color. When the button is clicked a color-selection dialog will open,
         * allowing the user to select a color. The swatch will be updated to reflect the new color when the user
         * finishes.
         * @return A new [ColorButton].
         */
        public fun create(): ColorButton

        /**
         * Creates a new color button that uses a [RGBA color][rgba].
         * @param rgba The color to use.
         * @return A new [ColorButton].
         */
        public fun createWithRgba(rgba: RgbaColor): ColorButton
    }

    override fun disconnectEvent(handlerId: ULong)

    override fun close()
}
