package io.gitlab.guiVista.gui.widget

/**
 * Default implementation of [FrameBase].
 */
public expect class Frame : FrameBase {
    public companion object {
        public fun create(label: String = ""): Frame
    }
}
