package io.gitlab.guiVista.gui.widget.menu

/** A subclass of GtkMenuShell which holds GtkMenuItem widgets */
public expect class MenuBar : MenuShell {
    public companion object {
        public fun create(): MenuBar
    }
}
