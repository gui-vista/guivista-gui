package io.gitlab.guiVista.gui.widget.button

import io.gitlab.guiVista.gui.dialog.FontChooser

/** A button to launch a font chooser dialog. */
public expect class FontButton : ButtonBase, FontChooser {
    /**
     * The name of the currently selected font. Default value is *Sans 12*.
     *
     * Data binding property name: **font-name**
     */
    public var fontName: String

    /**
     * If this property is set to *true* the name of the selected font style will be shown in the label. Default value
     * is *true*.
     *
     * Data binding property name: **show-style**
     * @see useFont
     */
    public var showStyle: Boolean

    /**
     * If this property is set to *true* then the selected font size will be shown in the label. Default value is
     * *true*.
     *
     * Data binding property name: **show-size**
     * @see useSize
     */
    public var showSize: Boolean

    /**
     * If this property is set to *true* then the label will be drawn in the selected font. Default value is *false*.
     *
     * Data binding property name: **use-font**
     */
    public var useFont: Boolean

    /**
     * If this property is set to TRUE, the label will be drawn with the selected font size. Default value is *false*.
     *
     * Data binding property name: **use-size**
     */
    public var useSize: Boolean

    public companion object {
        /**
         * Creates a new font picker widget.
         * @return The new [FontButton].
         */
        public fun create(): FontButton

        /**
         * Creates a new font picker widget with a selected [font][fontName].
         * @param fontName Name of font to display in font chooser dialog.
         * @return The new [FontButton].
         */
        public fun createWithFont(fontName: String): FontButton
    }

    override fun disconnectEvent(handlerId: ULong)
}
