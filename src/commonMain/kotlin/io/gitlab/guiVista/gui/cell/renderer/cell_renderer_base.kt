package io.gitlab.guiVista.gui.cell.renderer

import io.gitlab.guiVista.core.InitiallyUnowned
import io.gitlab.guiVista.gui.widget.WidgetBase

/** Renders a single cell. */
public expect interface CellRendererBase : InitiallyUnowned {
    /** Whether the cell renderer prefers a height-for-width layout, or a width-for-height layout. */
    public open val requestMode: UInt

    /**
     * The cell renderer’s visibility.
     *
     * Data binding property name: **visible**
     */
    public open var visible: Boolean

    /**
     * The cell renderer’s sensitivity.
     *
     * Data binding property name: **sensitive**
     */
    public open var sensitive: Boolean

    /** Whether the cell renderer can do something when activated. */
    public open val isActivatable: Boolean

    /**
     * Translates the cell renderer state to `GtkStateFlags`, based on the cell renderer and widget sensitivity, and
     * the given `GtkCellRendererState`.
     * @param widget The widget to use or *null*.
     * @param cellState The cell renderer state to translate.
     */
    public open fun fetchState(widget: WidgetBase?, cellState: UInt): UInt

    /**
     * Gets the width, and height with the appropriate size of the cell renderer.
     * @return A Pair with the following contents:
     * 1. width - The fixed width of the cell.
     * 2. height - The fixed height of the cell.
     */
    public open fun fetchFixedSize(): Pair<Int, Int>

    /**
     * Sets the renderer size to be explicit, independent of the properties set.
     * @param width The width of the cell renderer or *-1*.
     * @param height The height of the cell renderer or *-1*.
     */
    public open fun changeFixedSize(width: Int = -1, height: Int = -1)

    /**
     * Gets the xAlign and yAlign with the appropriate values of the cell renderer.
     * @return A Pair with the following contents:
     * 1. xAlign - The x alignment of the cell.
     * 2. yAlign - The y alignment of the cell.
     */
    public open fun fetchAlignment(): Pair<Float, Float>

    /**
     * Sets the renderer’s alignment within its available space.
     * @param xAlign The x alignment of the cell renderer.
     * @param yAlign The y alignment of the cell renderer.
     */
    public open fun changeAlignment(xAlign: Float, yAlign: Float)

    /**
     * Gets the xPad and yPad with the appropriate values of the cell renderer.
     * @return A Pair containing the following:
     * 1. xPad - The x padding of the cell.
     * 2. yPad - The y padding of the cell.
     */
    public open fun fetchPadding(): Pair<Int, Int>

    /**
     * Sets the renderer’s padding.
     * @param xPad The x padding of the cell renderer.
     * @param yPad The y padding of the cell renderer.
     */
    public open fun changePadding(xPad: Int, yPad: Int)

    /**
     * Retrieves a renderer’s natural size when rendered to the widget.
     * @param widget The widget this cell will be rendering to.
     * @return A Pair containing the following:
     * 1. minSize - The minimum size.
     * 2. naturalSize - The natural size.
     */
    public open fun fetchPreferredHeight(widget: WidgetBase): Pair<Int, Int>

    /**
     * Retrieves a cell renderers’s minimum, and natural height if it were rendered to the [widget] with the specified
     * [width].
     * @param widget The widget this cell will be rendering to.
     * @param width The size which is available for allocation.
     * @return A Pair containing the following:
     * 1. minHeight - The minimum size.
     * 2. naturalHeight - The preferred size.
     */
    public open fun fetchPreferredHeightForWidth(widget: WidgetBase, width: Int): Pair<Int, Int>

    /**
     * Retrieves a renderer’s natural size when rendered to the [widget].
     * @param widget The widget this cell will be rendering to.
     * @return A Pair containing the following:
     * 1. minSize - The minimum size.
     * 2. naturalSize - The natural size.
     */
    public open fun fetchPreferredWidth(widget: WidgetBase): Pair<Int, Int>

    /**
     * Retrieves a cell renderers’s minimum, and natural width if it were rendered to the [widget] with the specified
     * [height].
     * @param widget The widget this cell will be rendering to.
     * @param height The size which is available for allocation.
     * @return A Pair containing the following:
     * 1. minSize - The minimum size.
     * 2. naturalWidth - The preferred size.
     */
    public open fun fetchPreferredWidthForHeight(widget: WidgetBase, height: Int): Pair<Int, Int>
}
