package io.gitlab.guiVista.gui.cell

public object CellAreaBaseEvent {
    public const val addEditable: String = "add-editable"
    public const val applyAttributes: String = "apply-attributes"
    public const val focusChanged: String = "focus-changed"
    public const val removeEditable: String = "remove-editable"
}
