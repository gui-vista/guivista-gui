package io.gitlab.guiVista.gui.cell.renderer

/** Default implementation of [CellRendererBase]. */
public expect class CellRenderer : CellRendererBase
