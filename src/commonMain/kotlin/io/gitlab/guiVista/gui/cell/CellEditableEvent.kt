package io.gitlab.guiVista.gui.cell

public object CellEditableEvent {
    public const val editingDone: String = "editing-done"
    public const val removeWidget: String = "remove-widget"
}
