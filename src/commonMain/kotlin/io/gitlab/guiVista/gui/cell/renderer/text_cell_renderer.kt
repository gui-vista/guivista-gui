package io.gitlab.guiVista.gui.cell.renderer

/** Default implementation of [TextCellRendererBase]. */
public expect class TextCellRenderer : TextCellRendererBase {
    public companion object {
        public fun create(): TextCellRenderer
    }
}
