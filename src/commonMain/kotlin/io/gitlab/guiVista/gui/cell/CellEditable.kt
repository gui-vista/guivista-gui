package io.gitlab.guiVista.gui.cell

/** Default implementation of [CellEditableBase]. */
public expect class CellEditable : CellEditableBase
