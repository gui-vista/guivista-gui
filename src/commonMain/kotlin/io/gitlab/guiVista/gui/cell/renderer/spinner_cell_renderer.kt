package io.gitlab.guiVista.gui.cell.renderer

/** Renders a spinning animation in a cell. */
public expect class SpinnerCellRenderer : CellRendererBase {
    public companion object {
        public fun create(): SpinnerCellRenderer
    }
}
