package io.gitlab.guiVista.gui.cell

import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.cell.renderer.CellRendererBase

/** An interface for packing cells. */
public expect interface CellLayout {
    /** the cell renderers which have been added to [CellLayout]. */
    public open val cells: DoublyLinkedList

    /**
     * the underlying cell area which **might** be cell layout if called on a cell area or **might** be *null* if no
     * cell area is used by cell layout.
     */
    public open val area: CellAreaBase?

    /**
     * Packs the [cell] into the beginning of [CellLayout]. If [expand] is *false* then the [cell] is allocated no more
     * space than it needs. Any unused space is divided evenly between cells for which [expand] is *true*. Note that
     * reusing the same cell renderer isn't supported.
     * @param cell The cell to prepend.
     * @param expand Will be *true* if cell is to be given extra space allocated to [CellLayout].
     */
    public open fun prependCell(cell: CellRendererBase, expand: Boolean = true)

    /**
     * Adds the cell to the end of [CellLayout]. If [expand] is *false* then the [cell] is allocated no more space than
     * it needs. Any unused space is divided evenly between cells for which [expand] is *true*. Note that reusing the
     * same cell renderer isn't supported.
     * @param cell The cell to append.
     * @param expand Will be *true* if cell is to be given extra space allocated to [CellLayout].
     */
    public open fun appendCell(cell: CellRendererBase, expand: Boolean = true)

    /**
     * Re-inserts [cell] at the [position][pos]. Note that [cell] is already packed into [CellLayout] for this to
     * function properly.
     * @param cell The cell to reorder.
     * @param pos New position to insert the [cell] at.
     */
    public open fun reorder(cell: CellRendererBase, pos: Int)

    /**
     * Unsets all the mappings on all renderers on [CellLayout], and removes all renderers from [CellLayout].
     */
    public open fun clear()

    /**
     * Adds an attribute mapping to the list in [CellLayout]. The [column] is the column of the model to get a value
     * from, and the [attribute][attr] is the parameter on [cell] to be set from the value. So for example if column 2
     * of the model contains strings, you could have the **text** attribute of a `GtkCellRendererText` get its values
     * from column 2.
     * @param cell The cell to use.
     * @param attr An attribute on the renderer.
     * @param column The column position on the model to get the attribute from.
     */
    public open fun addAttribute(cell: CellRendererBase, attr: String, column: Int)

    /**
     * Adds multiple attributes to the list in [CellLayout].
     * @param cell The cell to use.
     * @param attributes Each attribute is represented as a Pair containing the following:
     * 1. attrName - Attribute name.
     * 2. column - Column number.
     * @see addAttribute
     */
    public open fun addMultipleAttributes(cell: CellRendererBase, vararg attributes: Pair<String, Int>)

    /**
     * Clears all existing attributes previously set with `gtk_cell_layout_set_attributes()`.
     * @param cell The cell to clear the attribute mapping on.
     */
    public open fun clearAttributes(cell: CellRendererBase)
}
