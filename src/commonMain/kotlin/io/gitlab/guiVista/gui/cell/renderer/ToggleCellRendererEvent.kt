package io.gitlab.guiVista.gui.cell.renderer

public object ToggleCellRendererEvent {
    public const val toggled: String = "toggled"
}
