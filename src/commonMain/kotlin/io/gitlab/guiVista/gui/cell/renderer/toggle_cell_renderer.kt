package io.gitlab.guiVista.gui.cell.renderer

/** Renders a toggle button in a cell. */
public expect class ToggleCellRenderer : CellRendererBase {
    /** Draw the toggle button as a radio button. Default value is *false*. */
    public var radio: Boolean

    /** The toggle state of the button. Default value is *false*. */
    public var active: Boolean

    /** Whether the toggle button can be activated. Default value is *true*. */
    public var activatable: Boolean

    public companion object {
        public fun create(): ToggleCellRenderer
    }
}
