package io.gitlab.guiVista.gui.cell.renderer

import io.gitlab.guiVista.gui.layout.Orientable

/** Renders numbers as progress bars. */
public expect class ProgressCellRenderer : CellRendererBase, Orientable {
    public companion object {
        public fun create(): ProgressCellRenderer
    }
}
