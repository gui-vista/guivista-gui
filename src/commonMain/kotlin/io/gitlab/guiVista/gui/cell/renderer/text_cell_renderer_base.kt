package io.gitlab.guiVista.gui.cell.renderer

/** Base interface for rendering text in a cell. */
public expect interface TextCellRendererBase : CellRendererBase {
    /**
     * Sets the height of the renderer to explicitly be determined by the **font**, and **yPad** property set on it.
     * Further changes in these properties do not affect the height, so they **MUST** be accompanied by a subsequent
     * call to this function. Using this function is unflexible, and should really only be used if calculating the
     * size of a cell is too slow (ie, a massive number of cells displayed). If [totalRows] is *-1* then the fixed
     * height is unset, and the height is determined by the properties again.
     * @param totalRows Number of rows of text each cell renderer is allocated, or *-1*.
     */
    public open fun changeFixedHeightFromFont(totalRows: Int = -1)
}
