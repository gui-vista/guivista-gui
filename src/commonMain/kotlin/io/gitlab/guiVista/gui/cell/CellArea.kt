package io.gitlab.guiVista.gui.cell

/** Default implementation of [CellAreaBase]. */
public expect class CellArea : CellAreaBase
