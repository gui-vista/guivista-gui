package io.gitlab.guiVista.gui.cell.renderer

public object CellRendererBaseEvent {
    public const val editingStarted: String = "editing-started"
    public const val editingCancelled: String = "editing-cancelled"
}
