package io.gitlab.guiVista.gui.cell

import io.gitlab.guiVista.core.InitiallyUnowned
import io.gitlab.guiVista.core.ValueBase
import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.gui.cell.renderer.CellRendererBase
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreeModelIterator
import io.gitlab.guiVista.gui.widget.WidgetBase

/** For laying out cell renderers. */
public expect interface CellAreaBase : CellLayout, InitiallyUnowned {
    /** Whether this area prefers a height-for-width layout, or a width-for-height layout. */
    public open val requestMode: UInt

    /**
     * The current [io.gitlab.guiVista.gui.tree.TreePath] string for the currently applied [TreeModelIterator].
     * This is implicitly updated when [applyAttributes] is called, and can be used to interact with renderers from
     * the [CellAreaBase] implementations.
     */
    public open val currentPathString: String

    /** Whether the area can do anything when activated after applying new attributes to this area. */
    public open val isActivatable: Boolean

    /**
     * The currently focused cell for this area.
     *
     * Data binding property name: **focus-cell**
     */
    public open var focusCell: CellRendererBase

    /**
     * The cell renderer in the area that is currently being edited.
     *
     * Data binding property name: **edited-cell**
     */
    public open val editedCell: CellRendererBase

    /**
     * The cell editable widget currently used to edit the currently edited cell.
     *
     * Data binding property name: **edit-widget**
     */
    public open val editWidget: CellEditableBase?

    /**
     * Adds [renderer] to area with the default child cell properties.
     * @param renderer The cell renderer to add to the area.
     */
    public open infix fun addCellRenderer(renderer: CellRendererBase)

    /**
     * Removes the [renderer] from the area.
     * @param renderer The cell renderer to remove from the area.
     */
    public open infix fun removeCellRenderer(renderer: CellRendererBase)

    /**
     * Checks if the area contains the [renderer].
     * @param renderer The renderer to check.
     * @return A value of *true* if the [renderer] is in the area.
     */
    public open infix fun hasRenderer(renderer: CellRendererBase): Boolean

    /**
     * Creates a [CellAreaContext] to be used with area for all purposes. [CellAreaContext] stores geometry information
     * for rows for which it was operated on. It is important to use the same context for the same row of data at all
     * times (i.e. one should render and handle events with the same [CellAreaContext] which was used to request the
     * size of those rows of data).
     * @return A newly created [CellAreaContext] which can be used with this area.
     */
    public open fun createContext(): CellAreaContext

    /**
     * This is sometimes needed for cases where rows need to share alignments in one orientation but may be separately
     * grouped in the opposing orientation. For instance `GtkIconView` creates all icons (rows) to have the same
     * width, and the cells need to have the same horizontal alignments. However each row of icons may have a separate
     * collective height. `GtkIconView` uses this to request the heights of each row based on a context which was
     * already used to request all the row widths that are to be displayed.
     * @param context The context top copy.
     * @return A newly created [CellAreaContext] copy of the context.
     */
    public open fun copyContext(context: CellAreaContext): CellAreaContext

    /**
     * Retrieves a cell area’s initial minimum and natural width. The area will store some geometrical information in
     * context along the way; when requesting sizes over an arbitrary number of rows, it’s not important to check the
     * minimum width and natural width of this call but rather to consult [CellAreaContext.fetchPreferredWidth] after
     * a series of requests.
     * @param context The context to perform this request with.
     * @param widget The widget where that this area will be rendering.
     * @return A Pair containing the following:
     * 1. minWidth - The minimum width.
     * 2. naturalWidth - The natural width.
     */
    public open fun fetchPreferredWidth(context: CellAreaContext, widget: WidgetBase): Pair<Int, Int>

    /**
     * Retrieves a cell area’s minimum and natural height if it would be given the specified width. The area stores
     * some geometrical information in context along the way while calling [fetchPreferredWidth]. It’s important to
     * perform a series of [fetchPreferredWidth] requests with context first, and then call [fetchPreferredHeight] on
     * each cell area individually to get the height for the width of each fully requested row.
     *
     * If at some point the width of a single row changes it should be requested with [fetchPreferredWidth] again, and
     * then the full width of the requested rows checked again with [CellAreaContext.fetchPreferredWidth].
     * @param context The context which has already been requested for widths.
     * @param widget The widget that this area will be rendering.
     * @param width The width for which to check the height of this area.
     * @return A Pair containing the following:
     * 1. minHeight - The minimum height.
     * 2. naturalHeight - The natural height.
     */
    public open fun fetchPreferredHeightForWidth(
        context: CellAreaContext,
        widget: WidgetBase,
        width: Int
    ): Pair<Int, Int>

    /**
     * Retrieves a cell area’s initial minimum and natural height. The area will store some geometrical information in
     * context along the way; when requesting sizes over an arbitrary number of rows, it’s not important to check the
     * minimum height, and natural height of this call but rather to consult [CellAreaContext.fetchPreferredHeight]
     * after a series of requests.
     * @param context The context to perform this request with.
     * @param widget The widget that this area will be rendering.
     * @return A Pair containing the following:
     * 1. minHeight - The minimum height.
     * 2. naturalHeight - The natural height.
     */
    public open fun fetchPreferredHeight(context: CellAreaContext, widget: WidgetBase): Pair<Int, Int>

    /**
     * Retrieves a cell area’s minimum and natural width if it would be given the specified height. The area stores
     * some geometrical information in context along the way while calling [fetchPreferredHeight]. It’s important to
     * perform a series of [fetchPreferredHeight] requests with context first, and then call
     * [fetchPreferredWidthForHeight] on each cell area individually to get the height for the width of each fully
     * requested row.
     *
     * If at some point the height of a single row changes then it should be requested with [fetchPreferredHeight]
     * again and then the full height of the requested rows checked again with [CellAreaContext.fetchPreferredHeight].
     * @param context The context to perform this request with.
     * @param widget The widget that this area will be rendering.
     * @param height The height for which to check the width of this area.
     * @return A Pair containing the following:
     * 1. minWidth - The minimum width.
     * 2. naturalWidth - The natural width.
     */
    public open fun fetchPreferredWidthForHeight(
        context: CellAreaContext,
        widget: WidgetBase,
        height: Int
    ): Pair<Int, Int>

    /**
     * Applies any connected attributes to the renderers in this area by pulling the values from the
     * [tree model][treeModel].
     * @param treeModel The tree model to pull values from.
     * @param iter The iterator in the [tree model][treeModel] to apply values for.
     * @param isExpander Whether the [iterator][iter] has children..
     * @param isExpanded Whether the [iterator][iter] is expanded in the view, and children are visible.
     */
    public open fun applyAttributes(
        treeModel: TreeModelBase,
        iter: TreeModelIterator,
        isExpander: Boolean,
        isExpanded: Boolean
    )

    /**
     * Connects an [attribute][attr] to apply values from column for the tree model in use.
     * @param renderer The cell renderer to connect an [attribute][attr] for.
     * @param attr The attribute name.
     * @param column The tree model column to fetch the attribute values from.
     */
    public open fun connectAttribute(renderer: CellRendererBase, attr: String, column: Int)

    /**
     * Disconnects [attribute][attr] for the [renderer] in this area so that the [attribute][attr] will no longer be
     * updated with values from the model.
     * @param renderer The cell renderer to disconnect an [attribute][attr] for.
     * @param attr The attribute name.
     */
    public open fun disconnectAttribute(renderer: CellRendererBase, attr: String)

    /**
     * Returns the model column that an [attribute][attr] has been mapped to, or *-1* if the [attribute][attr] isn't
     * mapped.
     * @param renderer The cell renderer to use.
     * @param attr An attribute on the [renderer].
     * @return The model column or *-1*.
     */
    public open fun fetchColumnForAttribute(renderer: CellRendererBase, attr: String)

    /**
     * Sets a cell property for renderer in this area.
     * @param renderer The cell renderer in this area.
     * @param propName The name of the cell property to set.
     * @param value The value to set the cell property to.
     */
    public open fun changeProperty(renderer: CellRendererBase, propName: String, value: ValueBase)

    /**
     * Gets the value of a cell property for the [renderer] in this area.
     * @param renderer The cell renderer in this area.
     * @param propName The name of the property to get.
     * @param propType The type of property to get.
     * @return The value of the property.
     */
    public open fun fetchProperty(renderer: CellRendererBase, propName: String, propType: ULong): ValueBase

    /**
     * Adds [sibling] to the [renderer’s][renderer] focusable area. The focus will be drawn around the [renderer] and
     * all of its siblings if the [renderer] can focus for a given row. Events handled by focus siblings can also
     * activate the given focusable renderer.
     * @param renderer The cell renderer expected to have focus.
     * @param sibling The cell renderer to add to renderer’s focus area.
     */
    public open fun addFocusSibling(renderer: CellRendererBase, sibling: CellRendererBase)

    /**
     * Removes [sibling] from [renderer’s][renderer] focus sibling list.
     * @param renderer The cell renderer expected to have focus.
     * @param sibling The cell renderer to remove from [renderer’s][renderer] focus area.
     * @see addFocusSibling
     */
    public open fun removeFocusSibling(renderer: CellRendererBase, sibling: CellRendererBase)

    /**
     * Returns whether [sibling] is one of [renderer’s][renderer] focus siblings.
     * @param renderer The cell renderer expected to have focus.
     * @param sibling The cell renderer to check against [renderer’s][renderer] sibling list.
     * @return A value of *true* if [sibling] is a focus sibling of the [renderer].
     * @see addFocusSibling
     */
    public open fun isFocusSibling(renderer: CellRendererBase, sibling: CellRendererBase): Boolean

    /**
     * Gets the focus sibling cell renderers for the renderer.
     * @param renderer The cell renderer expected to have focus.
     * @return A doubly linked list of cell renderers. The returned list is internal and should **NOT** be freed!
     */
    public open fun fetchFocusSiblings(renderer: CellRendererBase): DoublyLinkedList

    /**
     * Gets the cell renderer which is expected to be focusable for which [renderer] is, or may be a sibling. This is
     * handy for [CellAreaBase] implementations when handling events, after determining the renderer at the event
     * location it can then chose to activate the focus cell for which the event cell may have been a sibling.
     * @param renderer The cell renderer to use.
     * @return The cell renderer for which [renderer] is a sibling or *null*.
     */
    public open fun fetchFocusFromSibling(renderer: CellRendererBase): CellRendererBase?

    /**
     * Explicitly stops the editing of the currently edited cell. If [cancelled] is *true* then the currently edited
     * cell renderer will fire the `::editing-canceled` event, otherwise the `::editing-done` event will be fired on
     * the current edit widget.
     * @param cancelled Whether editing was cancelled.
     * @see editedCell
     */
    public open fun stopEditing(cancelled: Boolean)
}
