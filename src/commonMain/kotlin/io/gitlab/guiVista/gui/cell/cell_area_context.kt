package io.gitlab.guiVista.gui.cell

import io.gitlab.guiVista.core.ObjectBase

/** Stores geometrical information for a series of rows in a [CellArea]. */
public expect class CellAreaContext : ObjectBase {
    /** The cell area this context was created by. */
    public val area: CellAreaBase

    /**
     * Allocates a [width] and/or a [height] for all rows which are to be rendered with this context. Usually
     * allocation is performed only horizontally or sometimes vertically since a group of rows are usually rendered
     * side by side vertically or horizontally, and share either the same width or the same height. Sometimes they are
     * allocated in both horizontal, and vertical orientations producing a homogeneous effect of the rows. This is
     * generally the case for the TreeView widget when **fixed-height-mode** is enabled.
     * @param width The allocated width for all tree model rows rendered with the context, or *-1*.
     * @param height The allocated height for all tree model rows rendered with the context, or *-1*.
     */
    public fun allocate(width: Int, height: Int)

    /**
     * Resets any previously cached request and allocation data. When the underlying tree model data changes its
     * important to reset the context if the content size is allowed to shrink. If the content size is only allowed to
     * grow (this is usually an option for views rendering large data stores as a measure of optimization), then only
     * the row that changed or was inserted needs to be (re)requested with [CellAreaBase.fetchPreferredWidth].
     *
     * When the new overall size of the context requires that the allocated size changes (or whenever this allocation
     * changes at all), the variable row sizes need to be re-requested for every row. For instance if the rows are
     * displayed all with the same width from top to bottom, then a change in the allocated width necessitates a
     * recalculation of all the displayed row heights using [CellAreaBase.fetchPreferredHeightForWidth].
     */
    public fun reset()

    /**
     * Gets the accumulative preferred width for all rows which have been requested with this context. After
     * [reset] is called and/or before ever requesting the size of a cell area the returned values are *0*.
     * @return A Pair containing the following:
     * 1. minWidth - The minimum width.
     * 2. naturalWidth - The natural width.
     */
    public fun fetchPreferredWidth(): Pair<Int, Int>

    /**
     * Gets the accumulative preferred height for all rows which have been requested with this context. After
     * [reset] is called and/or before ever requesting the size of a cell area the returned values are *0*.
     * @return A Pair containing the following:
     * 1. minHeight - The minimum height
     * 2. naturalHeight - The natural height.
     */
    public fun fetchPreferredHeight(): Pair<Int, Int>

    /**
     * Gets the accumulative preferred height for width for all rows which have been requested for the same said width
     * with this context. After [reset] is called and/or before ever requesting the size of a cell area the returned
     * values are *-1*.
     * @param width A proposed width for allocation.
     * @return A Pair containing the following:
     * 1. minHeight - The minimum height
     * 2. naturalHeight - The natural height.
     */
    public fun fetchPreferredHeightForWidth(width: Int): Pair<Int, Int>

    /**
     * Gets the accumulative preferred width for height for all rows which have been requested for the same said
     * height with this context. After [reset] is called and/or before ever requesting the size of a cell area the
     * returned values are *-1*.
     * @param height A proposed width for allocation.
     * @return A Pair containing the following:
     * 1. minWidth - The minimum width.
     * 2. naturalWidth - The natural width.
     */
    public fun fetchPreferredWidthForHeight(height: Int): Pair<Int, Int>

    /**
     * Fetches the current allocation size for the context. If the context was not allocated in width or height, or if
     * the context was recently reset with [reset] then the returned value will be *-1*.
     * @return A Pair containing the following:
     * 1. width - The allocated width.
     * 2. height - The allocated height.
     */
    public fun fetchAllocation(): Pair<Int, Int>

    /**
     * Causes the minimum and/or natural width to grow if the new proposed sizes exceed the current minimum ,and
     * natural width. This is used by [CellAreaContext] implementations during the request process over a series of
     * tree model rows to progressively push the requested width over a series of [CellAreaBase.fetchPreferredWidth]
     * requests.
     * @param minWidth The proposed new minimum width for the context.
     * @param naturalWidth The proposed new natural width for the context.
     */
    public fun pushPreferredWidth(minWidth: Int, naturalWidth: Int)

    /**
     * Causes the minimum and/or natural height to grow if the new proposed sizes exceed the current minimum, and
     * natural height. This is used by [CellAreaContext] implementations during the request process over a series of
     * tree model rows to progressively push the requested height over a series of [CellAreaBase.fetchPreferredHeight]
     * requests.
     * @param minHeight The proposed new minimum height for the context.
     * @param naturalHeight The proposed new natural height for the context.
     */
    public fun pushPreferredHeight(minHeight: Int, naturalHeight: Int)
}