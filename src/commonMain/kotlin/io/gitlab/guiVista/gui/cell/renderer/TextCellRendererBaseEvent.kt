package io.gitlab.guiVista.gui.cell.renderer

public object TextCellRendererBaseEvent {
    public const val edited: String = "edited"
}
