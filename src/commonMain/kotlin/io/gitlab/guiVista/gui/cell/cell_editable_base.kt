package io.gitlab.guiVista.gui.cell

import io.gitlab.guiVista.core.ObjectBase

/** Interface for widgets which are used for editing cells. */
public expect interface CellEditableBase : ObjectBase
