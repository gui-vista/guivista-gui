package io.gitlab.guiVista.gui.event

/**
 * Used for button press and button release events. The type field will be one of the following:
 * - GDK_BUTTON_PRESS
 * - GDK_2BUTTON_PRESS
 * - GDK_3BUTTON_PRESS
 * - GDK_BUTTON_RELEASE
 *
 * Double and triple clicks result in a sequence of events being received. For double clicks the order of events will
 * be:
 * 1. GDK_BUTTON_PRESS
 * 2. GDK_BUTTON_RELEASE
 * 3. GDK_BUTTON_PRESS
 * 4. GDK_2BUTTON_PRESS
 * 5. GDK_BUTTON_RELEASE
 *
 * Note that the first click is received just like a normal button press, while the second click results in a
 * `GDK_2BUTTON_PRESS` being received just after the `GDK_BUTTON_PRESS`. Triple clicks are very similar to double
 * clicks, except that `GDK_3BUTTON_PRESS` is inserted after the third click. The order of the events is:
 * 1. GDK_BUTTON_PRESS
 * 2. GDK_BUTTON_RELEASE
 * 3. GDK_BUTTON_PRESS
 * 4. GDK_2BUTTON_PRESS
 * 5. GDK_BUTTON_RELEASE
 * 6. GDK_BUTTON_PRESS
 * 7. GDK_3BUTTON_PRESS
 * 8. GDK_BUTTON_RELEASE
 *
 * For a double click to occur the second button press must occur within 1/4 of a second of the first. For a triple
 * click to occur the third button press must also occur within 1/2 second of the first button press.
 */
public expect class ButtonEvent : EventBase {
    /**
     * The type of the event (`GDK_BUTTON_PRESS`, `GDK_2BUTTON_PRESS`, `GDK_3BUTTON_PRESS` or `GDK_BUTTON_RELEASE`).
     */
    override val type: Int

    /** A value of *true* if the event was sent explicitly. */
    public val sendEvent: Boolean

    /** The time of the event in milliseconds. */
    public val time: UInt

    /** The X coordinate of the pointer relative to the window. */
    public val xPos: Double

    /** The Y coordinate of the pointer relative to the window. */
    public val yPos: Double

    /** The xPos, yPos translated to the axes of device, or *null* if device is the mouse. */
    public val axes: Double?

    /**
     * A bit-mask representing the state of the modifier keys (e.g. **Control**, **Shift**, and **Alt**), and the
     * pointer buttons. Refer to `GdkModifierType`.
     */
    public val state: UInt

    /**
     * The button which was pressed or released, numbered from *1* to *5*. Normally button 1 is the left mouse button,
     * 2 is the middle button, and 3 is the right button. On two button mice the middle button can often be simulated
     * by pressing both mouse buttons together.
     */
    public val button: UInt

    /** The X coordinate of the pointer relative to the root of the screen. */
    public val xPosRoot: Double

    /** The Y coordinate of the pointer relative to the root of the screen. */
    public val yPosRoot: Double

    public companion object {
        public fun create(type: Int): ButtonEvent
    }
}
