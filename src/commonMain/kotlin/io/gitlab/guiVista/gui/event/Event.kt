package io.gitlab.guiVista.gui.event

/** Default implementation of [EventBase]. */
public expect class Event : EventBase {
    public companion object {
        public fun create(type: Int): Event
    }
}
