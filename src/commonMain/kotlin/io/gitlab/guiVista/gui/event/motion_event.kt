package io.gitlab.guiVista.gui.event

/** Generated when the pointer moves. */
public expect class MotionEvent : EventBase {
    /** A value of *true* if the event was sent explicitly. */
    public val sendEvent: Boolean

    /** The time of the event in milliseconds. */
    public val time: UInt

    /** The X coordinate of the pointer relative to the window. */
    public val xPos: Double

    /** The Y coordinate of the pointer relative to the window. */
    public val yPos: Double

    /** The xPos, yPos translated to the axes of device, or *null* if device is the mouse. */
    public val axes: Double?

    /**
     * A bit-mask representing the state of the modifier keys (e.g. **Control**, **Shift**, and **Alt**), and the
     * pointer buttons. Refer to `GdkModifierType`.
     */
    public val state: UInt

    /** Set to *true* if this event is just a hint, see the `GDK_POINTER_MOTION_HINT_MASK` value of `GdkEventMask`. */
    public val isHint: Boolean

    /** The X coordinate of the pointer relative to the root of the screen. */
    public val xPosRoot: Double

    /** The Y coordinate of the pointer relative to the root of the screen. */
    public val yPosRoot: Double

    public companion object {
        public fun create(type: Int): MotionEvent
    }
}
