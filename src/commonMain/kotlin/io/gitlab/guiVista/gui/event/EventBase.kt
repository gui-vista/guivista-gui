package io.gitlab.guiVista.gui.event

import io.gitlab.guiVista.core.Closable

/** Provides meta data for a event. */
public expect interface EventBase : Closable {
    /** Indicates the type of event being represented. */
    public open val type: Int
}
