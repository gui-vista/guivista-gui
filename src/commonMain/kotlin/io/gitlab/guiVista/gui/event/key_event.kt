package io.gitlab.guiVista.gui.event

/** Describes a key press or key release event. */
public expect class KeyEvent : EventBase {
    /** The type of the event (`GDK_KEY_PRESS` or `GDK_KEY_RELEASE`). */
    override val type: Int

    /** A value of *true* if the event was sent explicitly. */
    public val sendEvent: Boolean

    /** The time of the event in milliseconds. */
    public val time: UInt

    /**
     * A bit-mask representing the state of the modifier keys (e.g. **Control**, **Shift**, and **Alt**), and the
     * pointer buttons. Refer to `GdkModifierType`.
     */
    public val state: UInt

    /**
     * The key that was pressed or released. Refer to the **gdk/gdkkeysyms.h** header file for a complete list of GDK
     * key codes.
     */
    public val keyValue: UInt

    /** The raw code of the key that was pressed or released. */
    public val hardwareKeyCode: UShort

    /** The keyboard group */
    public val group: UByte

    /** A flag that indicates if [hardwareKeyCode] is mapped to a modifier. */
    public val isModifier: Boolean

    public companion object {
        public fun create(type: Int): KeyEvent
    }
}
