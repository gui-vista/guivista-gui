package io.gitlab.guiVista.gui.event

import io.gitlab.guiVista.gui.dragDrop.DragDropContext

/** Generated during **Drag N Drop** operations. */
public expect class DragDropEvent : EventBase {
    /**
     * The type of the event (`GDK_DRAG_ENTER`, `GDK_DRAG_LEAVE`, `GDK_DRAG_MOTION`, `GDK_DRAG_STATUS`,
     * `GDK_DROP_START`, or `GDK_DROP_FINISHED`).
     */
    override val type: Int

    /** A value of *true* if the event was sent explicitly. */
    public val sendEvent: Boolean

    /** The time of the event in milliseconds. */
    public val time: UInt

    /**
     * The X coordinate of the pointer relative to the root of the screen. Only set for `GDK_DRAG_MOTION` and
     * `GDK_DROP_START`.
     */
    public val rootXPos: Short

    /**
     * The Y coordinate of the pointer relative to the root of the screen. Only set for `GDK_DRAG_MOTION` and
     * `GDK_DROP_START`.
     */
    public val rootYPos: Short

    /** The [DragDropContext] for the current **Drag N Drop** operation. */
    public val context: DragDropContext

    public companion object {
        public fun create(type: Int): DragDropEvent
    }
}
