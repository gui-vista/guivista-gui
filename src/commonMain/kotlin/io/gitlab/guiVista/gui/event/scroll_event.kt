package io.gitlab.guiVista.gui.event

/**
 * Generated from button presses for the buttons 4 to 7. Wheel mice are usually configured to generate button press
 * events for buttons 4 and 5 when the wheel is turned. Some GDK backends can also generate **smooth** scroll events,
 * which can be recognized by the `GDK_SCROLL_SMOOTH` scroll direction. For these the scroll deltas can be obtained
 * via the [scrollDeltas] property.
 */
public expect class ScrollEvent : EventBase {
    /** The type of the event (`GDK_SCROLL`). */
    override val type: Int

    /** The scroll deltas (xPos, yPos) from the scroll event. */
    public val scrollDeltas: Pair<Double, Double>

    /** A value of *true* if the event was sent explicitly. */
    public val sendEvent: Boolean

    /** The time of the event in milliseconds. */
    public val time: UInt

    /** The X coordinate of the pointer relative to the window. */
    public val xPos: Double

    /** The Y coordinate of the pointer relative to the window. */
    public val yPos: Double

    /**
     * A bit-mask representing the state of the modifier keys (e.g. **Control**, **Shift**, and **Alt**), and the
     * pointer buttons. Refer to `GdkModifierType`.
     */
    public val state: UInt

    /** The X coordinate of the pointer relative to the root of the screen. */
    public val xPosRoot: Double

    /** The Y coordinate of the pointer relative to the root of the screen. */
    public val yPosRoot: Double

    /** The X coordinate of the scroll delta. */
    public val deltaXPos: Double

    /** The Y coordinate of the scroll delta. */
    public val deltaYPos: Double

    public companion object {
        public fun create(type: Int): ScrollEvent
    }
}
