package io.gitlab.guiVista.gui.keyboard

import io.gitlab.guiVista.core.ObjectBase

/** Groups of global keyboard accelerators for an entire window. */
public expect class AcceleratorGroup : ObjectBase {
    /**
     * The modifier mask for this accel group.
     *
     * Data binding property name: **modifier-mask**
     */
    public val modifierMask: UInt

    /**
     * Is the accel group locked. Default value is *false*.
     *
     * Data binding property name: **is-locked**
     */
    public val isLocked: Boolean

    /**
     * Locks the given accelerator group. Locking an accelerator group prevents the accelerators contained within it to
     * be changed during runtime. Refer to `gtk_accel_map_change_entry()` about runtime accelerator changes. If called
     * more than once then [AcceleratorGroup] remains locked until [unlock] has been called an
     * equivalent number of times.
     */
    public fun lock()

    /** Undoes the last call to [lock] on this accel_group/ */
    public fun unlock()

    /**
     * Removes a keyboard shortcut (accelerator).
     * @param accelMod The accelerator modifier in UInt form (eg Ctrl)
     * @param accelKey The accelerator key (eg *s*).
     */
    public fun removeShortcut(accelMod: UInt, accelKey: Char)

    public companion object {
        public fun create(): AcceleratorGroup
    }
}