package io.gitlab.guiVista.gui.keyboard

import io.gitlab.guiVista.core.ObjectBase

/** Loadable keyboard accelerator specifications. */
public expect object AcceleratorMap : ObjectBase {
    /**
     * Registers a new accelerator with the global accelerator map. This function should only be called once per
     * [accelPath] with the canonical [accelKey] and [accelMods] for this path. To change the accelerator during
     * runtime programatically, use [changeEntry]. Set [accelKey], and [accelMods] to *0* to request a removal of the
     * accelerator.
     *
     * Note that [accelPath] will be stored in a `GQuark`. Therefore if you pass a static string, you can save some
     * memory by interning it first with `g_intern_static_string()`.
     * @param accelPath A valid accelerator path.
     * @param accelKey The accelerator key.
     * @param accelMods The accelerator modifiers.
     */
    public fun addEntry(accelPath: String, accelKey: UInt, accelMods: UInt)

    /**
     * Changes the [accelKey] and [accelMods] currently associated with [accelPath]. Due to conflicts with other
     * accelerators a change may not always be possible. The [replace] parameter indicates whether other accelerators
     * may be deleted to resolve such conflicts. A change will only occur if all conflicts could be resolved (which
     * might not be the case if conflicting accelerators are locked). Successful changes are indicated by a *true*
     * return value.
     *
     * Note that [accelPath] will be stored in a `GQuark`. Therefore if you pass a static string you can save some
     * memory by interning it first with `g_intern_static_string()`.
     * @param accelPath A valid accelerator path.
     * @param accelKey The new accelerator key.
     * @param accelMods The new accelerator modifiers.
     * @param replace If *true* then other accelerators *may* be deleted upon conflicts.
     * @return A value of *true* if the accelerator could be changed.
     */
    public fun changeEntry(accelPath: String, accelKey: UInt, accelMods: UInt, replace: Boolean): Boolean

    /**
     * Parses a file previously saved with [save] for accelerator specifications, and propagates them accordingly.
     * @param fileName A file containing accelerator specifications in the GLib file name encoding.
     */
    public fun load(fileName: String)

    /**
     * Saves current accelerator specifications (accelerator path, key and modifiers) to [fileName]. The file is
     * written in a format suitable to be read back in by [load].
     * @param fileName The name of the file to contain accelerator specifications in the GLib file name encoding.
     */
    public fun save(fileName: String)

    /**
     * Locks the given accelerator path. If the accelerator map doesn’t yet contain an entry for [accelPath] then a new
     * one is created. Locking an accelerator path prevents its accelerator from being changed during runtime. A locked
     * accelerator path can be unlocked by [unlockPath]. Refer to `gtk_accel_map_change_entry()` for information about
     * runtime accelerator changes.
     *
     * If called more than once then [accelPath] remains locked until `gtk_accel_map_unlock_path()` has been called an
     * equivalent number of times. Note that locking of individual accelerator paths is independent from locking the
     * [AcceleratorGroup] containing them. For runtime accelerator changes to be possible, both the accelerator path
     * and its [AcceleratorGroup] have to be unlocked.
     * @param accelPath A valid accelerator path.
     */
    public fun lockPath(accelPath: String)

    /**
     * Undoes the last call to [lockPath] on this [accelPath]. Refer to [lockPath] for information about accelerator
     * path locking.
     * @param accelPath A valid accelerator path.
     * @see lockPath
     */
    public fun unlockPath(accelPath: String)

    /**
     * Looks up the accelerator entry for [accelPath].
     * @param accelPath A valid accelerator path.
     * @return A value of *true* if the [accelPath] is known.
     */
    public fun lookupEntry(accelPath: String): Boolean

    /**
     * File descriptor variant of [load]. Note that the file descriptor will **NOT** be closed by this function.
     * @param fd A valid readable file descriptor.
     */
    public fun loadFileDescriptor(fd: Int)

    /**
     * File descriptor variant of [save]. Note that the file descriptor will **NOT** be closed by this function.
     * @param fd A valid readable file descriptor.
     */
    public fun saveFileDescriptor(fd: Int)
}
