package io.gitlab.guiVista.gui.tree

import io.gitlab.guiVista.core.Closable

/** Iterates over a tree model. */
public expect class TreeModelIterator : Closable {
    public companion object {
        /**
         * Creates a new instance of [TreeModelIterator] which isn't initialized.
         * @return The new iterator.
         */
        public fun create(): TreeModelIterator
    }
}
