package io.gitlab.guiVista.gui.tree

import io.gitlab.guiVista.core.ObjectBase

/** The interface for sortable models used by GtkTreeView. */
public expect interface TreeSortableBase : ObjectBase {
    /**
     * Returns *true* if the model has a default sort function. This is used primarily by `GtkTreeViewColumns` in order
     * to determine if a model can go back to the default state, or not.
     */
    public open val hasDefaultSortFunc: Boolean
}
