package io.gitlab.guiVista.gui.tree

import io.gitlab.guiVista.core.ValueBase

public expect class TreeStore : TreeModelBase {
    public companion object {
        /**
         * Creates a new instance of [TreeStore]. Note that only standard GObject types are supported. Below is an
         * example:
         * ```kotlin
         * // Create a new TreeStore comprised of 3 columns.
         * TreeStore.create(G_TYPE_INT, G_TYPE_STRING, GDK_TYPE_PIXBUF)
         * ```
         * @param types The GObject types to use as the columns.
         * @return A new [TreeStore] instance.
         */
        public fun create(vararg types: ULong): TreeStore
    }

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [String].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [String] value for the cell.
     */
    public fun changeStringValue(iter: TreeModelIterator, column: Int, value: String)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [UInt].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [UInt] value for the cell.
     */
    public fun changeUIntValue(iter: TreeModelIterator, column: Int, value: UInt)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [Int].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [Int] value for the cell.
     */
    public fun changeIntValue(iter: TreeModelIterator, column: Int, value: Int)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [ULong].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [ULong] value for the cell.
     */
    public fun changeULongValue(iter: TreeModelIterator, column: Int, value: ULong)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [Long].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [Long] value for the cell.
     */
    public fun changeLongValue(iter: TreeModelIterator, column: Int, value: Long)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [Float].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [Float] value for the cell.
     */
    public fun changeFloatValue(iter: TreeModelIterator, column: Int, value: Float)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [Double].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [Double] value for the cell.
     */
    public fun changeDoubleValue(iter: TreeModelIterator, column: Int, value: Double)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [UShort].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [UShort] value for the cell.
     */
    public fun changeUShortValue(iter: TreeModelIterator, column: Int, value: UShort)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [Short].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [Short] value for the cell.
     */
    public fun changeShortValue(iter: TreeModelIterator, column: Int, value: Short)

    /**
     * Sets the data in the cell specified by [iter], and [column]. The type of value **MUST** be convertible to the
     * type of the column.
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New value for the cell.
     */
    public fun changeValue(iter: TreeModelIterator, column: Int, value: ValueBase)

    /**
     * Changes multiple values via columns and values as two arrays.
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param columns An Array of column numbers.
     * @param values An Array of values.
     */
    public fun changeMultipleValues(iter: TreeModelIterator, columns: IntArray, values: Array<ValueBase>)

    /**
     * Removes the [iterator][iter] from the tree store. After being removed the [iterator][iter] is set to the next
     * valid row at that level, or invalidated if it previously pointed to the last one.
     * @param iter A valid iterator.
     * @return A value of *true* if [iter] is valid.
     */
    public fun remove(iter: TreeModelIterator): Boolean

    /**
     * Creates a new row at [pos]. If parent isn't null then the row will be made a child of [parent]. Otherwise the
     * row will be created at the top level. If [pos] is *-1* or is larger than the number of rows at that level then
     * the new row will be inserted to the end of the list. The [iterator][iter] will be changed to point to this new
     * row. The row will be empty after this function is called.
     *
     * To fill in values you need to call [changeValue].
     * @param iter An unset [TreeModelIterator] to set to the new row.
     * @param parent A valid [TreeModelIterator] or *null*.
     * @param pos The position to insert the new row, or *-1* for last.
     */
    public fun insert(iter: TreeModelIterator, parent: TreeModelIterator? = null, pos: Int = -1)

    /**
     * Inserts a new row before [sibling]. If [sibling] is *null* then the row will be appended to parent’s children.
     * If [parent] and [sibling] are *null* then the row will be appended to the top level. If both [sibling] and
     * [parent] are set then [parent] must be the parent of [sibling]. When [sibling] is set then the [parent] is
     * optional.
     *
     * The [iterator][iter] will be changed to point to this new row. The row will be empty after this function is
     * called. To fill in values you need to call [changeValue].
     */
    public fun insertBefore(iter: TreeModelIterator, parent: TreeModelIterator? = null,
                            sibling: TreeModelIterator? = null)

    /**
     * Inserts a new row after [sibling]. If sibling is *null* then the row will be prepended to parent’s children. If
     * [parent] and [sibling] are *null* then the row will be prepended to the top level. If both [sibling] and
     * [parent] are set then [parent] must be the parent of [sibling]. When [sibling] is set then the [parent] is
     * optional.
     *
     * The [iterator][iter] will be changed to point to this new row. The row will be empty after this function is
     * called. To fill in values you need to call [changeValue].
     * @param iter An unset [TreeModelIterator] to set to the new row.
     * @param parent A valid [TreeModelIterator] or *null*.
     * @param sibling A valid [TreeModelIterator] or *null*.
     */
    public fun insertAfter(iter: TreeModelIterator, parent: TreeModelIterator? = null,
                           sibling: TreeModelIterator? = null)

    /**
     * Creates a new row at [pos]. The [iterator][iter] will be changed to point to this new row. If [pos] is *-1*, or
     * larger than the number of rows on the list, then the new row will be appended to the list. The row will be
     * filled with the values given to this function.
     * @param iter An unset [TreeModelIterator] to set to the new row or *null*.
     * @param parent A valid [TreeModelIterator] or *null*.
     * @param pos The position to insert the new row, or *-1* for last.
     * @param columns An Array of column numbers.
     * @param values An Array of values.
     */
    public fun insertWithValues(
        iter: TreeModelIterator,
        parent: TreeModelIterator? = null,
        pos: Int,
        columns: IntArray,
        values: Array<ValueBase>
    )

    /**
     * Prepends a new row to the tree store. If [parent] isn't null then it will prepend the new row before the first
     * child of [parent], otherwise it will prepend a row to the top level. The [iterator][iter] will be changed to
     * point to this new row. The row will be empty after this function is called. To fill in values you need to call
     * [changeValue].
     * @param iter An unset [TreeModelIterator] to set to the prepended row.
     * @param parent A valid [TreeModelIterator] or *null*.
     */
    public fun prepend(iter: TreeModelIterator, parent: TreeModelIterator? = null)

    /**
     * Appends a new row to the tree store. If [parent] isn't null then it will append the new row after the last
     * child of [parent], otherwise it will append a row to the top level. The [iterator][iter] will be changed to
     * point to this new row. The row will be empty after this function is called. To fill in values you need to call
     * [changeValue].
     * @param iter An unset [TreeModelIterator] to set to the appended row.
     * @param parent A valid [TreeModelIterator] or *null*.
     */
    public fun append(iter: TreeModelIterator, parent: TreeModelIterator? = null)

    /** Removes all rows from the tree store. */
    public fun clear()

    /**
     * Returns a value of *true* if the [iterator][iter] is an ancestor of [descendant]. That is the [iterator][iter]
     * is the parent (or grandparent or great-grandparent) of [descendant].
     * @param iter A valid iterator.
     * @param descendant A valid iterator.
     * @return A value of *true* if the [iterator][iter] is an ancestor of [descendant].
     */
    public fun isAncestor(iter: TreeModelIterator, descendant: TreeModelIterator): Boolean

    /**
     * Returns the depth of the [iterator][iter]. This will be *0* for anything on the root level, *1* for anything
     * down a level, etc.
     * @param iter A valid iterator.
     * @return The depth of the [iterator][iter].
     */
    public fun iteratorDepth(iter: TreeModelIterator): Int

    /**
     * Reorders the children of parent in the tree store to follow the order indicated by [newOrder]. Note that this
     * function only works with unsorted stores.
     * @param parent A [TreeModelIterator] or *null*.
     * @param newOrder An array of integers mapping the new position of each child to its old position before the
     * re-ordering, i.e. newOrder: newpos = oldpos.
     */
    public fun reorder(parent: TreeModelIterator? = null, newOrder: IntArray)

    /**
     * Swaps [iterA] and [iterB] in this tree store. Note that this function only works with unsorted stores.
     * @param iterA First iterator.
     * @param iterB Second iterator.
     */
    public fun swap(iterA: TreeModelIterator, iterB: TreeModelIterator)

    /**
     * Moves [iter] in the tree store to the position before [pos]. The [iterator][iter] and [pos] should be in the
     * same level. Note that this function only works with unsorted stores. If [pos] is *null* then [iter] will be
     * moved to the end of the level.
     * @param iter The iterator.
     * @param pos The position as a iterator, or *null*.
     */
    public fun moveBefore(iter: TreeModelIterator, pos: TreeModelIterator?)

    /**
     * Moves iter in the tree store to the position after [pos]. The [iterator][iter] and [pos] should be in the same
     * level. Note that this function only works with unsorted stores. If [pos] is *null* then [iter] will be moved to
     * the start of the level.
     * @param iter The iterator.
     * @param pos The position as a iterator, or *null*.
     */
    public fun moveAfter(iter: TreeModelIterator, pos: TreeModelIterator?)
}
