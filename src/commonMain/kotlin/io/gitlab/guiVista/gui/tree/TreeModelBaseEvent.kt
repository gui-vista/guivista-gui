package io.gitlab.guiVista.gui.tree

public object TreeModelBaseEvent {
    public const val rowChanged: String = "row-changed"
    public const val rowDeleted: String = "row-deleted"
    public const val rowHasChildToggled: String = "row-has-child-toggled"
    public const val rowInserted: String = "row-inserted"
    public const val rowsReordered: String = "rows-reordered"
}
