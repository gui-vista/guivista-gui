package io.gitlab.guiVista.gui.tree

/** Default implementation of [TreeSortableBase]. */
public expect class TreeSortable : TreeSortableBase
