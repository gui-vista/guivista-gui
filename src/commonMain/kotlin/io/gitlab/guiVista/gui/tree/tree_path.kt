package io.gitlab.guiVista.gui.tree

import io.gitlab.guiVista.core.Closable

public expect class TreePath : Closable {
    /** The current depth of this tree path. */
    public val depth: Int

    /** The current indices of this tree path. This is an array of integers, each representing a node in a tree. */
    public val indices: Array<Int>

    public companion object {
        /**
         * Creates a new [TreePath]. This refers to a row.
         * @return A newly created [TreePath].
         */
        public fun create(): TreePath

        /**
         * Creates a new [TreePath] initialized to [path]. It is expected that [path] will be a colon separated list of
         * numbers. For example the string **10:4:0** would create a path of depth *3* pointing to the 11th child of
         * the root node, the 5th child of that 11th child, and the 1st child of that 5th child. If an invalid path
         * String is passed in then *null* is returned.
         * @param path The string representation of a path.
         * @return A newly created [TreePath] or *null*.
         */
        public fun fromString(path: String): TreePath?
    }

    /**
     * Appends a new index to this tree path. As a result the [depth] is increased.
     * @param index The index to use.
     */
    public infix fun appendIndex(index: Int)

    /**
     * Prepends a new index to this tree path. As a result the [depth] is increased.
     * @param index The index to use.
     */
    public infix fun prependIndex(index: Int)

    /**
     * Creates a new [TreePath] as a copy of this tree path.
     * @return A new instance of [TreePath].
     */
    public fun copy(): TreePath

    /**
     * Compares this tree path with the [other path][otherPath]. If this tree path appears before [otherPath] in a
     * tree then *-1* is returned. If [otherPath] appears before this tree path then *1* is returned. If the two nodes
     * are equal then *0* is returned.
     * @param otherPath The other [TreePath] to compare to.
     * @return The relative positions of this tree path, and the [other path][otherPath].
     */
    public fun compareTo(otherPath: TreePath): Int

    /** Moves this tree path to point to the next node at the current depth. */
    public fun next()

    /**
     * Moves this tree path to point to the previous node at the current depth, if it exists.
     * @return A value of *true* if this tree path has a previous node, and the move was made.
     */
    public fun previous(): Boolean

    /**
     * Moves this tree path to point to its parent node, if it has a parent.
     * @return A value of *true* if this tree path has a parent, and the move was made.
     */
    public fun up(): Boolean

    /** Moves this tree path to point to the first child of the current path. */
    public fun down()

    /**
     * Returns *true* if descendant is a descendant of this tree path.
     * @param descendant Another [TreePath].
     * @return A value of *true* if [descendant] is contained inside this tree path.
     */
    public fun isAncestor(descendant: TreePath): Boolean

    /**
     * Returns *true* if this tree path is a descendant of [ancestor].
     * @param ancestor Another [TreePath].
     * @return A value of *true* if [ancestor] contains this tree path somewhere below it.
     */
    public fun isDescendant(ancestor: TreePath): Boolean
}
