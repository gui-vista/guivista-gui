package io.gitlab.guiVista.gui.tree

public object TreeSortableBaseEvent {
    public const val sortColumnChanged: String = "sort-column-changed"
}
