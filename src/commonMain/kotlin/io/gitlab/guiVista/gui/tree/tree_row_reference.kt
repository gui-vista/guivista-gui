package io.gitlab.guiVista.gui.tree

import io.gitlab.guiVista.core.Closable

/** Provides a reference to a tree row. */
public expect class TreeRowReference : Closable {
    /** The model that the row reference is monitoring. */
    public val model: TreeModelBase

    /** The path that the row reference currently points to, or *null* if the path pointed to is no longer valid. */
    public val path: TreePath?

    public companion object {
        /**
         * Creates a row reference based on the [path]. This reference will keep pointing to the node pointed to by
         * the [path] so long as it exists. Any changes that occur on [model] are propagated, and the [path] is updated
         * appropriately. If [path] isn’t a valid path in [model] then *null* is returned.
         * @param model The tree model.
         * @param path A valid tree path to monitor.
         * @return The new [TreeRowReference] instance or *null*.
         */
        public fun create(model: TreeModelBase, path: TreePath): TreeRowReference?
    }

    /**
     * Returns *true* if the reference isn't null, and refers to a current valid path.
     * @return A value of *true* if the reference points to a valid path.
     */
    public fun valid(): Boolean

    /**
     * Copies this [TreeRowReference].
     * @return A copy of this [TreeRowReference].
     */
    public fun copy(): TreeRowReference
}