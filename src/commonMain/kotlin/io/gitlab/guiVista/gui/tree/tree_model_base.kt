package io.gitlab.guiVista.gui.tree

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.ValueBase

/** The tree interface used by the tree view widget. */
public expect interface TreeModelBase : ObjectBase {
    /** The number of columns supported by this tree model. */
    public open val totalColumns: Int

    /**
     * Returns the type of the column.
     * @param index The column index.
     * @return The column type.
     */
    public open fun fetchColumnType(index: Int): ULong

    /**
     * Gets an iterator pointing to [path]. If [path] doesn't exist then the iterator is set to an invalid iterator,
     * and *false* is returned.
     * @param path The tree path.
     * @return A Pair containing the following:
     * 1. isSet - A value of *true* if the iterator was set.
     * 2. iter - The iterator pointing to [path], or an invalid one.
     */
    public open fun fetchIterator(path: TreePath): Pair<Boolean, TreeModelIterator>

    /**
     * Get an iterator pointing to the [path String][pathStr], if it exists. Otherwise the iterator is left invalid,
     * and *false* is returned.
     * @param pathStr A String representation of the tree path.
     * @return A Pair containing hte following:
     * 1. isSet - A value of *true* if the iterator was set.
     * 2. iter - The iterator pointing to the [path String][pathStr] or an invalid one.
     */
    public open fun fetchIteratorFromString(pathStr: String): Pair<Boolean, TreeModelIterator>

    /**
     * Gets the first iterator in the tree (the one at the path *0*) and returns
     * *true*. Returns *false* if the tree is empty.
     * @return A Pair containing the following:
     * 1. isSet - A value of *true* if the iterator was set.
     * 2. iter - The first iterator, or an invalid one.
     */
    public open fun fetchFirstIterator(): Pair<Boolean, TreeModelIterator>

    /**
     * Returns a newly created [TreePath] referenced by the [iterator][iter]. This path should be freed with
     * [TreePath.close].
     * @param iter The iterator to use.
     * @return The newly created [TreePath].
     */
    public open fun fetchPath(iter: TreeModelIterator): TreePath

    /**
     * Initializes and sets a value to that at the [column][col]. When done with the value the [ValueBase.close] needs
     * to be called to free any allocated memory.
     * @param iter The iterator to use.
     * @param col The column to lookup the value at.
     * @param valueType The value's type.
     * @return The set value.
     */
    public open fun fetchValue(iter: TreeModelIterator, col: Int, valueType: ULong): ValueBase

    /**
     * Gets the [String] value of a [column][col] in the row referenced by [iter].
     * @param iter A row in the tree model.
     * @param col The column number.
     * @return The [String] value.
     */
    public open fun fetchStringValue(iter: TreeModelIterator, col: Int): String

    /**
     * Gets the [UInt] value of a [column][col] in the row referenced by [iter].
     * @param iter A row in the tree model.
     * @param col The column number.
     * @return The [UInt] value.
     */
    public open fun fetchUIntValue(iter: TreeModelIterator, col: Int): UInt

    /**
     * Gets the [Int] value of a [column][col] in the row referenced by [iter].
     * @param iter A row in the tree model.
     * @param col The column number.
     * @return The [Int] value.
     */
    public open fun fetchIntValue(iter: TreeModelIterator, col: Int): Int

    /**
     * Gets the [ULong] value of a [column][col] in the row referenced by [iter].
     * @param iter A row in the tree model.
     * @param col The column number.
     * @return The [ULong] value.
     */
    public open fun fetchULongValue(iter: TreeModelIterator, col: Int): ULong

    /**
     * Gets the [Long] value of a [column][col] in the row referenced by [iter].
     * @param iter A row in the tree model.
     * @param col The column number.
     * @return The [Long] value.
     */
    public open fun fetchLongValue(iter: TreeModelIterator, col: Int): Long

    /**
     * Gets the [Float] value of a [column][col] in the row referenced by [iter].
     * @param iter A row in the tree model.
     * @param col The column number.
     * @return The [Float] value.
     */
    public open fun fetchFloatValue(iter: TreeModelIterator, col: Int): Float

    /**
     * Gets the [Double] value of a [column][col] in the row referenced by [iter].
     * @param iter A row in the tree model.
     * @param col The column number.
     * @return The [Double] value.
     */
    public open fun fetchDoubleValue(iter: TreeModelIterator, col: Int): Double

    /**
     * Gets the [UShort] value of a [column][col] in the row referenced by [iter].
     * @param iter A row in the tree model.
     * @param col The column number.
     * @return The [UShort] value.
     */
    public open fun fetchUShortValue(iter: TreeModelIterator, col: Int): UShort

    /**
     * Gets the [Short] value of a [column][col] in the row referenced by [iter].
     * @param iter A row in the tree model.
     * @param col The column number.
     * @return The [Short] value.
     */
    public open fun fetchShortValue(iter: TreeModelIterator, col: Int): Short

    /**
     * Generates a String representation of the [iterator][iter]. This String is a **:** separated list of numbers. For
     * example **4:10:0:3** would be an acceptable return value for this string.
     * @param iter The iterator to use.
     * @return A new String.
     */
    public open fun fetchStringFromIterator(iter: TreeModelIterator): String

    /**
     * Lets the tree reference the node. This is an optional function for models to implement. To be more specific
     * models may ignore this call as it exists primarily for performance reasons. This function is primarily meant as
     * a way for views to let caching models know when nodes are being displayed (and hence, whether or not to cache
     * that node). Being displayed means a node is in an expanded branch, regardless of whether the node is currently
     * visible in the viewport. For example a file-system based model would not want to keep the entire file-hierarchy
     * in memory, just the sections that are currently being displayed by every current view.
     *
     * A model should be expected to be able to get an [iterator][iter] independent of its reffed state.
     * @param iter The iterator to use.
     */
    public open infix fun referenceNode(iter: TreeModelIterator)

    /**
     * Lets the tree unreference the node. This is an optional function for models to implement. To be more specific
     * models may ignore this call as it exists primarily for performance reasons. Please note that nodes that are
     * deleted are not unreferrenced.
     * @param iter The iterator to use.
     * @see referenceNode
     */
    public open infix fun unreferenceNode(iter: TreeModelIterator)
}
