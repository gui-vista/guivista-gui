package io.gitlab.guiVista.gui

import io.gitlab.guiVista.core.ObjectBase

/** Displays an icon in the system tray. */
public expect class StatusIcon : ObjectBase {
    /**
     * Whether the status icon is embedded in a notification area. Default value is *false*.
     *
     * Data binding property name: **embedded**
     */
    public val embedded: Boolean

    /**
     * Enables or disables the emission of **query-tooltip** on the status icon. A value of *true* indicates that the
     * status icon can have a tooltip. In this case the status icon will be queried using **query-tooltip** to
     * determine whether it will provide a tooltip or not. Note that setting this property to *true* for the first
     * time will change the event masks of the windows of this status icon to include **leave-notify**, and
     * **motion-notify** events. This will not be undone when the property is set to *false* again.
     *
     * Whether this property is respected is platform dependent. For plain text tooltips use [tooltipText] in
     * preference.
     *
     * Default value is *false*. Data binding property name: **has-tooltip**
     */
    public var hasTooltip: Boolean

    /**
     * The name of the icon from the icon theme. Default value is *""* (an empty [String]).
     *
     * Data binding property name: **icon-name**
     */
    public var iconName: String

    /**
     * The image buffer to display.
     *
     * Data binding property name: **pixbuf**
     */
    public var imageBuffer: ImageBuffer

    /**
     * The size of the icon. Default value is *0*.
     *
     * Data binding property name: **size**
     */
    public val size: Int

    /**
     * The title of this tray icon. This should be a short, human readable, localized string describing the tray icon.
     * It may be used by tools like screen readers to render the tray icon. Default value is *""* (an empty [String]).
     *
     * Data binding property name: **title**
     */
    public var title: String

    /**
     * Sets the text of tooltip to be the given string, which is marked up with the Pango text markup language. This is
     * a convenience property which will take care of getting the tooltip shown if the given string isn't empty. The
     * [hasTooltip] property will automatically be set to *true*, and the default handler for the **query-tooltip**
     * event will take care of displaying the tooltip. On some platforms the embedded markup will be ignored.
     *
     * Default value is *""* (an empty [String]). Data binding property name: **tooltip-markup**
     * @see Tooltip.changeMarkup
     */
    public var tooltipMarkup: String

    /**
     * Sets the text of tooltip to be the given string. This is a convenience property which will take care of getting
     * the tooltip shown if the given string isn't empty. The [hasTooltip] property will automatically be set to
     * *true*, and the default handler for the **query-tooltip** event will take care of displaying the tooltip.
     *
     * Note that some platforms have limitations on the length of tooltips that they allow on status icons, e.g.
     * Windows only shows the first 64 characters. Default value is *""* (an empty [String]).
     *
     * Data binding property name: **tooltip-text**
     * @see Tooltip.changeText
     */
    public var tooltipText: String

    /**
     * Whether the status icon is visible. Default value is *true*.
     *
     * Data binding property name: **visible**
     */
    public var visible: Boolean

    public companion object {
        /**
         * Creates an empty status icon object.
         * @return A new [StatusIcon] object.
         */
        public fun create(): StatusIcon

        /**
         * Creates a status icon displaying the [imageBuffer]. The image will be scaled down to fit in the available
         * space in the notification area, if necessary.
         * @param imageBuffer The image buffer to use.
         * @return A new [StatusIcon] object.
         */
        public fun fromImageBuffer(imageBuffer: ImageBuffer): StatusIcon

        /**
         * Creates a status icon displaying the [file][fileName]. The image will be scaled down to fit in the
         * available space in the notification area, if necessary.
         * @param fileName The path to the file.
         * @return A new [StatusIcon] object.
         */
        public fun fromFile(fileName: String): StatusIcon

        /**
         * Creates a status icon displaying an icon from the current icon theme. If the current icon theme is changed
         * then the icon will be updated appropriately.
         * @param iconName The name of the icon to use.
         * @return A new [StatusIcon] object.
         */
        public fun fromIconName(iconName: String): StatusIcon
    }
}
