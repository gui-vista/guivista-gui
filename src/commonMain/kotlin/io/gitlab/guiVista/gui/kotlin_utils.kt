package io.gitlab.guiVista.gui

internal fun Set<String>.toByteArray(): ByteArray {
    val result = ByteArray(size)
    forEachIndexed { pos, item -> result[pos] = item.toByte() }
    return result
}

internal fun Collection<String>.toByteArray(): ByteArray {
    val result = ByteArray(size)
    forEachIndexed { pos, item -> result[pos] = item.toByte() }
    return result
}

/**
 * Creates byte arrays for keys and values from a Map.
 * @return A Pair containing the following:
 * 1. keys
 * 2. values
 */
internal fun Map<String, String>.createByteArrays() = keys.toByteArray() to values.toByteArray()
