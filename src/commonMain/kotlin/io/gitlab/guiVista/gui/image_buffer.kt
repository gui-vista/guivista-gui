package io.gitlab.guiVista.gui

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.dataType.SinglyLinkedList

/** Information that describes an image. */
public expect class ImageBuffer : ObjectBase {
    /** The number of channels in a [ImageBuffer]. */
    public val totalChannels: Int

    /** Whether a [ImageBuffer] has an alpha channel (opacity information). */
    public val hasAlpha: Boolean

    /** The number of bits per color sample in a [ImageBuffer]. */
    public val bitsPerSample: Int

    /** The width of a [ImageBuffer] in pixels. */
    public val width: Int

    /** The height of a [ImageBuffer] in pixels. */
    public val height: Int

    /**
     * The rowStride of a [ImageBuffer], which is the number of bytes between the start of a row and the start of the
     * next row.
     */
    public val rowStride: Int

    /** The length of the pixel data in bytes. */
    public val byteLength: ULong

    /**
     * Gets the pixel data.
     * @return A Pair containing the following:
     * 1. length - The size of the data.
     * 2. data - The pixel data.
     */
    public fun fetchPixels(): Pair<UInt, UByte>

    /**
     * Looks up key in the list of options that may have been attached to the [ImageBuffer] when it was loaded, or that
     * may have been attached by another function using [changeOption]. For instance the ANI loader provides **Title**
     * and **Artist** options. The ICO, XBM, and XPM loaders provide **x_hot** and **y_hot** hot-spot options for
     * cursor definitions. The PNG loader provides the tEXt ancillary chunk key/value pairs as options.
     *
     * The TIFF and JPEG loaders return an **orientation** option string that corresponds to the embedded TIFF/Exif
     * orientation tag (if present). Note that the TIFF loader sets the **multipage** option string to **yes** when a
     * multi-page TIFF is loaded. The JPEG and PNG loaders set **x-dpi** and **y-dpi** if the file contains image
     * density information in dots per inch.
     * @return The value associated with [key]. Will be *""* (an empty String) if the key doesn't exist.
     */
    public fun fetchOption(key: String): String

    /**
     * Attaches a key/value pair as an option to a [ImageBuffer]. If key already exists in the list of options attached
     * to [ImageBuffer] then the new value is ignored and *false* is returned.
     * @param key The String to use for the key.
     * @param value The String to use for the value.
     * @return A value of *true* on success.
     */
    public fun changeOption(key: String, value: String): Boolean

    /**
     * Gets the raw pixel data, which must **NOT** be modified. This function allows skipping the implicit copy that
     * must be made if [fetchPixels] is called on a read only [ImageBuffer].
     * @return Raw pixel data.
     */
    public fun readPixels(): UByte

    /**
     * Saves [ImageBuffer] to a file in format type . By default **jpeg**, **png**, **ico**, and **bmp** are possible
     * file formats to save in, but more formats may be installed.
     * @param fileName Name of the file to save.
     * @param type Name of the file format.
     * @param options The options to set.
     * @param error The error to use or *null*.
     * @return A value of *true* if an error has been set.
     */
    public fun saveToFile(fileName: String, type: String, options: Map<String, String>, error: Error? = null): Boolean

    /**
     * Saves [ImageBuffer] to a new buffer in format [type], which is currently **jpeg**, **png**, **tiff**, **ico**,
     * or **bmp**. This is a convenience function that uses the **saveToCallback** function to do the real work. Note
     * that the buffer isn't null terminated and may contain embedded nulls. If [error] is set then *false* will be
     * returned and buffer will be set to *null*. Possible errors include those in the `GDK_PIXBUF_ERROR` domain.
     * @param type Name of file format.
     * @param options The options to set.
     * @param bufferSize The size of the buffer.
     * @param error The error to use or *null*.
     * @return A Pair containing the following:
     * 1. buffer - Contains image data that is stored in memory.
     * 2. errorSet - If *true* then an error has been set.
     * @see saveToFile
     */
    public fun saveToBuffer(
        type: String,
        options: Map<String, String>,
        bufferSize: Int,
        error: Error? = null
    ): Pair<ByteArray, Boolean>

    public companion object {
        /**
         * Creates a new [ImageBuffer] by loading an image from a file. The file format is detected automatically. If
         * *null* is returned then error will be set. Possible errors are in the `GDK_PIXBUF_ERROR`, and
         * `G_FILE_ERROR` domains.
         * @param fileName Name of file to load in the GLib file name encoding.
         * @param error The error to use or *null*.
         * @return A newly-created [ImageBuffer] with a reference count of *1*, or *null* if any of several error
         * conditions occurred:
         * - File could not be opened
         * - No loader for the file's format
         * - Not enough memory to allocate the image buffer
         * - Image file contained invalid data
         */
        public fun fromFile(fileName: String, error: Error? = null): ImageBuffer?

        /**
         * Creates a new [ImageBuffer] by loading an image from a file. The file format is detected automatically. If
         * *null* is returned then error will be set. Possible errors are in the `GDK_PIXBUF_ERROR`, and
         * `G_FILE_ERROR domains`. The image will be scaled to fit in the requested size, preserving the image's aspect
         * ratio. Note that the returned [ImageBuffer] may be smaller than [width] x [height], if the aspect ratio
         * requires it. To load and image at the requested size, regardless of aspect ratio use [fromFileAtScale].
         * @param fileName Name of file to load in the GLib file name encoding.
         * @param width The width the image should have or *-1* to not constrain the width.
         * @param height The height the image should have or *-1* to not constrain the height.
         * @param error The error to use or *null*.
         * @return A newly created [ImageBuffer] with a reference count of *1*, or *null* if any of several error
         * conditions occurred:
         * - File could not be opened
         * - No loader for the file's format
         * - Not enough memory to allocate the image buffer
         * - Image file contained invalid data
         */
        public fun fromFileAtSize(fileName: String, width: Int, height: Int, error: Error? = null): ImageBuffer?

        /**
         * Creates a new [ImageBuffer] by loading an image from a file. The file format is detected automatically. If
         * *null* is returned then error will be set. Possible errors are in the `GDK_PIXBUF_ERROR`, and
         * `G_FILE_ERROR` domains. The image will be scaled to fit in the requested size, optionally preserving the
         * image's aspect ratio. When preserving the aspect ratio a width of *-1* will cause the image to be scaled to
         * the exact given height, and a height of *-1* will cause the image to be scaled to the exact given width.
         * When not preserving aspect ratio a width or height of *-1* means to not scale the image at all in that
         * dimension.
         * @param fileName Name of file to load in the GLib file name encoding.
         * @param width The width the image should have or *-1* to not constrain the width.
         * @param height The height the image should have or *-1* to not constrain the height.
         * @param preserveAspectRatio When *true* the aspect ratio will be preserved.
         * @param error The error to use or *null*.
         * @return A newly created [ImageBuffer] with a reference count of *1*, or *null* if any of several error
         * conditions occurred:
         * - File could not be opened
         * - No loader for the file's format
         * - Not enough memory to allocate the image buffer
         * - Image file contained invalid data.
         */
        public fun fromFileAtScale(
            fileName: String,
            width: Int,
            height: Int,
            preserveAspectRatio: Boolean,
            error: Error? = null
        ): ImageBuffer?

        /**
         * Creates a new [ImageBuffer] by loading an image from an resource. The file format is detected
         * automatically. If *null* is returned then error will be set.
         * @param resPath The path of the resource file.
         * @param error The error to use or *null*.
         * @return A newly created [ImageBuffer], or *null* if any of several error conditions occurred:
         * - File could not be opened
         * - Image format is not supported
         * - Not enough memory to allocate the image buffer
         * - The stream contained invalid data
         * - Operation was cancelled.
         */
        public fun fromResource(resPath: String, error: Error? = null): ImageBuffer?

        /**
         * Creates a new [ImageBuffer] by loading an image from an resource. The file format is detected automatically.
         * If *null* is returned then error will be set. The image will be scaled to fit in the requested size,
         * optionally preserving the image's aspect ratio. When preserving the aspect ratio a width of *-1* will cause
         * the image to be scaled to the exact given height, and a height of *-1* will cause the image to be scaled to
         * the exact given width. When not preserving aspect ratio a width or height of *-1* means to not scale the
         * image at all in that dimension. The stream isn't closed.
         * @param resPath The path of the resource file.
         * @param width The width the image should have or *-1* to not constrain the width.
         * @param height The height the image should have or *-1* to not constrain the height.
         * @param preserveAspectRatio When *true* the aspect ratio will be preserved.
         * @param error The error to use or *null*.
         * @return A newly created [ImageBuffer], or *null* if any of several error conditions occurred:
         * - File could not be opened
         * - Image format is not supported
         * - Not enough memory to allocate the image buffer
         * - The stream contained invalid data
         * - Operation was cancelled.
         */
        public fun fromResourceAtScale(
            resPath: String,
            width: Int,
            height: Int,
            preserveAspectRatio: Boolean,
            error: Error? = null
        ): ImageBuffer?

        /**
         * Creates a new [ImageBuffer] by parsing XPM data in memory.
         * @param data The XPM data.
         * @return A new [ImageBuffer] with a reference count of *1*.
         */
        public fun fromXpmData(data: ByteArray): ImageBuffer
    }

    /**
     * Parses an image file far enough to determine its format and size.
     * @param fileName The name of the file to identify.
     * @return A Triple containing the following:
     * 1. width - The width of the image.
     * 2. height - The height of the image.
     * 3. format - The image format information. Will be *null* if the image format isn't recognised.
     */
    public fun fetchFileInfo(fileName: String): Triple<Int, Int, ImageBufferFormat?>
}

/**
 * Obtains the available information about the image formats supported by [ImageBuffer] as a list. Each element in
 * the list is a `CPointer<GdkPixbufFormat>`. Use the [ImageBufferFormat] class for each element in the list. The
 * list **SHOULD** be closed when it is no longer used. All list elements shouldn't be closed since they are
 * already owned by [ImageBuffer].
 */
public expect val imageBufferFormats: SinglyLinkedList
