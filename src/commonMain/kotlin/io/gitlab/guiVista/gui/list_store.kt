package io.gitlab.guiVista.gui

import io.gitlab.guiVista.core.ValueBase
import io.gitlab.guiVista.gui.tree.TreeModelBase
import io.gitlab.guiVista.gui.tree.TreeModelIterator

/** A list like data structure that can be used with the Tree View widget. */
public expect class ListStore : TreeModelBase {
    public companion object {
        /**
         * Creates a new instance of [ListStore]. Note that only standard GObject types are supported. Below is an
         * example:
         * ```kotlin
         * // Create a new ListStore comprised of 3 columns.
         * ListStore.create(G_TYPE_INT, G_TYPE_STRING, GDK_TYPE_PIXBUF)
         * ```
         * @param types The GObject types to use as the columns.
         * @return A new [ListStore] instance.
         */
        public fun create(vararg types: ULong): ListStore
    }

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [String].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [String] value for the cell.
     */
    public fun changeStringValue(iter: TreeModelIterator, column: Int, value: String)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [UInt].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [UInt] value for the cell.
     */
    public fun changeUIntValue(iter: TreeModelIterator, column: Int, value: UInt)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [Int].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [Int] value for the cell.
     */
    public fun changeIntValue(iter: TreeModelIterator, column: Int, value: Int)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [ULong].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [ULong] value for the cell.
     */
    public fun changeULongValue(iter: TreeModelIterator, column: Int, value: ULong)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [Long].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [Long] value for the cell.
     */
    public fun changeLongValue(iter: TreeModelIterator, column: Int, value: Long)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [Float].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [Float] value for the cell.
     */
    public fun changeFloatValue(iter: TreeModelIterator, column: Int, value: Float)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [Double].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [Double] value for the cell.
     */
    public fun changeDoubleValue(iter: TreeModelIterator, column: Int, value: Double)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [UShort].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [UShort] value for the cell.
     */
    public fun changeUShortValue(iter: TreeModelIterator, column: Int, value: UShort)

    /**
     * Sets the data in the cell specified by [iter], and [column] to a [Short].
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New [Short] value for the cell.
     */
    public fun changeShortValue(iter: TreeModelIterator, column: Int, value: Short)

    /**
     * Sets the data in the cell specified by [iter], and [column]. The type of value **MUST** be convertible to the
     * type of the column.
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param column The column number to modify.
     * @param value New value for the cell.
     */
    public fun changeValue(iter: TreeModelIterator, column: Int, value: ValueBase)

    /**
     * Changes multiple values via columns and values as two arrays.
     * @param iter A valid [TreeModelIterator] for the row being modified.
     * @param columns An Array of column numbers.
     * @param values An Array of values.
     */
    public fun changeMultipleValues(iter: TreeModelIterator, columns: IntArray, values: Array<ValueBase>)

    /**
     * Removes the given row from the list store. After being removed the [iterator][iter] is set to be the next valid
     * row, or invalidated if it pointed to the last row in the list store.
     * @param iter A valid iterator.
     * @return A value of *true* if [iter] is valid.
     */
    public fun remove(iter: TreeModelIterator): Boolean

    /**
     * Creates a new row at [pos]. The [iterator][iter] will be changed to point to this new row. If [pos] is *-1*, or
     * is larger than the number of rows on the list then the new row will be appended to the list. The row will be
     * empty after this function is called. To fill in values you need to call [changeValue].
     * @param iter An unset [TreeModelIterator] to set to the new row.
     * @param pos The position to insert the new row, or *-1* for last.
     */
    public fun insert(iter: TreeModelIterator, pos: Int)

    /**
     * Inserts a new row before [sibling]. If [sibling] is *null* then the row will be appended to the end of the list.
     * The [iterator][iter] will be changed to point to this new row.
     *
     * The row will be empty after this function is called. To fill in values you need to call [changeValue].
     * @param iter An unset [TreeModelIterator] to set to the new row.
     * @param sibling A valid [TreeModelIterator] or *null*.
     */
    public fun insertBefore(iter: TreeModelIterator, sibling: TreeModelIterator? = null)

    /**
     * Inserts a new row after [sibling]. If [sibling] is *null* then the row will be prepended to the beginning of the
     * list. The [iterator][iter] will be changed to point to this new row.
     *
     * The row will be empty after this function is called. To fill in values you need to call [changeValue].
     * @param iter An unset [TreeModelIterator] to set to the new row.
     * @param sibling A valid [TreeModelIterator] or *null*.
     */
    public fun insertAfter(iter: TreeModelIterator, sibling: TreeModelIterator? = null)

    /**
     * Creates a new row at [pos]. The [iterator][iter] will be changed to point to this new row. If [pos] is *-1*, or
     * larger than the number of rows in the list then the new row will be appended to the list. The row will be filled
     * with the values given to this function.
     * @param iter An unset [TreeModelIterator] to set to the new row or *null*.
     * @param pos The position to insert the new row, or *-1* for last.
     * @param columns An Array of column numbers.
     * @param values An Array of values.
     */
    public fun insertWithValues(iter: TreeModelIterator, pos: Int, columns: IntArray, values: Array<ValueBase>)

    /**
     * Prepends a new row to the list store. The [iterator][iter] will be changed to point to this new row.
     *
     * The row will be empty after this function is called. To fill in values you need to call [changeValue].
     * @param iter An unset [TreeModelIterator] to set to the prepended row.
     */
    public fun prepend(iter: TreeModelIterator)

    /**
     * Appends a new row to the list store. The [iterator][iter] will be changed to point to this new row.
     *
     * The row will be empty after this function is called. To fill in values you need to call [changeValue].
     * @param iter An unset [TreeModelIterator] to set to the appended row.
     */
    public fun append(iter: TreeModelIterator)

    /** Removes all rows from the list store. */
    public fun clear()

    /**
     * Reorders store to follow the order indicated by [newOrder]. Note that this function only works with unsorted
     * stores.
     * @param newOrder An Array of integers mapping the new position of each child to its old position before the
     * re-ordering, i.e. newOrder: newPos = oldPos. It must have exactly as many items as the list store’s length.
     */
    public fun reorder(newOrder: IntArray)

    /**
     * Swaps [iterA] and [iterB] in this store. Note that this function only works with unsorted stores.
     * @param iterA First iterator.
     * @param iterB Second iterator.
     */
    public fun swap(iterA: TreeModelIterator, iterB: TreeModelIterator)

    /**
     * Moves [iter] in this store to the position before [pos]. Note that this function only works with unsorted
     * stores. If [pos] is *null* then [iter] will be moved to the end of the list.
     * @param iter The iterator.
     * @param pos The position as a iterator, or *null*.
     */
    public fun moveBefore(iter: TreeModelIterator, pos: TreeModelIterator?)

    /**
     * Moves [iter] in this store to the position after [pos]. Note that this function only works with unsorted
     * stores. If [pos] is *null* then [iter] will be moved to the start of the list.
     * @param iter The iterator.
     * @param pos The position as a iterator, or *null*.
     */
    public fun moveAfter(iter: TreeModelIterator, pos: TreeModelIterator?)
}
