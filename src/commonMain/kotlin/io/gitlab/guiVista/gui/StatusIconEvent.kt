package io.gitlab.guiVista.gui

public object StatusIconEvent {
    public const val activate: String = "activate"
    public const val buttonPressEvent: String = "button-press-event"
    public const val buttonReleaseEvent: String = "button-release-event"
    public const val queryTooltip: String = "query-tooltip"
    public const val scrollEvent: String = "scroll-event"
    public const val sizeChanged: String = "size-changed"
    public const val popupMenu: String = "popup-menu"
}
