package io.gitlab.guiVista.gui

/** An opaque type representing a string as an index into a table of strings on the X server. */
public expect class Atom {
    /** The string corresponding to an atom. */
    public val name: String

    public companion object {
        /**
         * Finds or creates an atom corresponding to a given string.
         * @param name A string.
         * @return The [Atom] corresponding to [name].
         */
        public fun internString(name: String): Atom

        /**
         * Finds or creates an atom corresponding to a given string. Note that this function is identical to
         * [internString] except that if a new [Atom] is created the string itself is used rather than a copy. This
         * saves memory, but can only be used if the string will always exist. It can be used with statically
         * allocated strings in the main program, but not with statically allocated memory in dynamically loaded
         * modules, if you expect to ever unload the module again (e.g. do not use this function in GTK+ theme engines).
         * @param name A static string.
         * @return The [Atom] corresponding to [name].
         */
        public fun internStaticString(name: String): Atom
    }
}
