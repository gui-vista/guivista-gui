package io.gitlab.guiVista.gui.dragDrop

public object DragDropContextEvent {
    public const val actionChanged: String = "action-changed"
    public const val cancel: String = "cancel"
    public const val dragDropFinished: String = "dnd-finished"
    public const val dropPerformed: String = "drop-performed"
}
