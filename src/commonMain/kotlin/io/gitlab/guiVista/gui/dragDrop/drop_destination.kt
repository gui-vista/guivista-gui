package io.gitlab.guiVista.gui.dragDrop

import io.gitlab.guiVista.gui.Atom
import io.gitlab.guiVista.gui.widget.WidgetBase

// Provides all functionality for the drag destination (a widget) as part of the **Drag N Drop** support.

/**
 * Sets a widget as a potential drop destination, and adds default behaviors. The default behaviors listed in
 * [defaultFlags] have an effect similar to installing default handlers for the widget’s **Drag N Drop** events
 * (`GtkWidget::drag-motion`, `GtkWidget::drag-drop` etc). They all exist for convenience. When passing
 * `GTK_DEST_DEFAULT_ALL` for instance it is sufficient to connect to the widget’s `GtkWidget::drag-data-received`
 * event signal to get primitive, but consistent **Drag N Drop** support.
 *
 * Things become more complicated when you try to preview the dragged data, as described in the documentation for
 * `GtkWidget::drag-motion`. The default behaviors described by flags make some assumptions that can conflict with
 * your own event handlers. For instance `GTK_DEST_DEFAULT_DROP` causes invocations of `gdk_drag_status()` in the
 * context of `GtkWidget::drag-motion`, and invocations of `gtk_drag_finish()` in `GtkWidget::drag-data-received`.
 * Especially the later is dramatic when your own `GtkWidget::drag-motion` handler calls `gtk_drag_get_data()` to
 * inspect the dragged data.
 * @param defaultFlags Which types of drag behavior to use.
 * @param dragActions A bitmask of possible actions for a drop onto this widget.
 * @param targets Zero or more target entries indicating the drop types that this widget will accept. Later you can
 * access the list with [targets], and [findDropTarget].
 */
public expect fun WidgetBase.setDropDestination(
    defaultFlags: UInt,
    dragActions: UInt,
    vararg targets: DragDropTargetEntry
)

/**
 * Clears information about a drop destination set with [setDropDestination]. The widget will no longer receive
 * notification of drags.
 */
public expect fun WidgetBase.unsetDropDestination()

/**
 * Add the image targets supported by GtkSelectionData to the target list of the drop destination. The targets are
 * added with `info = 0`. If you need another value then use [DragDropTargetList.addImageTargets], and set the
 * [dropTargets] property.
 */
public expect fun WidgetBase.addDropImageTargets()

/**
 * Add the text targets supported by `GtkSelectionData` to the target list of the drop destination. The targets are
 * added with `info = 0`. If you need another value then use [DragDropTargetList.addTextTargets], and set the
 * [dropTargets] property.
 */
public expect fun WidgetBase.addDropTextTargets()

/**
 * Add the URI targets supported by `GtkSelectionData` to the target list of the drop destination. The targets are
 * added with `info = 0`. If you need another value then use [DragDropTargetList.addUriTargets], and set the
 * [dropTargets] property.
 */
public expect fun WidgetBase.addDropUriTargets()

/**
 * Looks for a match between the supported targets of [ctx] and [targetList], returning the first matching target,
 * otherwise returning `GDK_NONE`. Note that [targetList] should usually be the return value from
 * [dropTargets], but some widgets may have different valid targets for different parts of the widget; in that
 * case they will have to implement a drag motion handler that passes the correct target list to this function.
 * @return First target that the source offers and the destination can accept, or `GDK_NONE`.
 */
public expect fun WidgetBase.findDropTarget(ctx: DragDropContext, targetList: DragDropTargetList): Atom

/** The list of drop targets the widget can accept from **Drag N Drop**. Will be *null* if empty. */
public expect var WidgetBase.dropTargets: DragDropTargetList?

/** Whether the widget has been configured to always fire **drag-motion** events. */
public expect var WidgetBase.trackDragMotion: Boolean
