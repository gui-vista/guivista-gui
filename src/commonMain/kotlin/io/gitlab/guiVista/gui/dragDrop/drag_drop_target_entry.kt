package io.gitlab.guiVista.gui.dragDrop

import io.gitlab.guiVista.core.Closable

/**
 * Represents a single type of data than can be supplied for by a widget for a selection, or for supplied or received
 * during **Drag N Drop**.
 */
public expect class DragDropTargetEntry : Closable {
    /** The target type. **/
    public var target: String

    /** A set of flags for **Drag N Drop**. */
    public var flags: UInt

    /**
     * An application assigned [Int] ID which will get passed as a parameter to e.g the widget **selection-get**
     * event. It allows the application to identify the target type without extensive [String] compares.
     */
    public var info: UInt

    public companion object {
        /**
         * Makes a new [DragDropTargetEntry].
         * @param target The target type.
         * @param flags A set of flags for **Drag N Drop**.
         * @param info An identifier that will be passed back to the application.
         * @return An instance of [DragDropTargetEntry]. Remember to [close] the instance when finished with it.
         */
        public fun create(target: String, flags: UInt, info: UInt): DragDropTargetEntry
    }
}
