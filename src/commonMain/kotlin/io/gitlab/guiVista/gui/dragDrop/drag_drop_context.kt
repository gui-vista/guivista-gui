package io.gitlab.guiVista.gui.dragDrop

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.dataType.DoublyLinkedList

/** Acts as a context for **Drag N Drop**. */
public expect class DragDropContext : ObjectBase {
    /** The bitmask of actions proposed by the source if [suggestedAction] returns `GDK_ACTION_ASK`. */
    public val actions: UInt

    /** The action chosen by the drag destination. */
    public val selectedAction: UInt

    /** The suggested drag action of the context. */
    public val suggestedAction: UInt

    /** The list of targets in this context. */
    public val targets: DoublyLinkedList

    /**
     * Sets the position of the drag window that will be kept under the cursor hotspot. Initially the hotspot is at the
     * top left corner of the drag window.
     * @param xPos X coordinate of the drag window hotspot.
     * @param yPos Y coordinate of the drag window hotspot.
     */
    public fun changeHotspot(xPos: Int, yPos: Int)
}
