package io.gitlab.guiVista.gui.dragDrop

import io.gitlab.guiVista.core.Closable
import io.gitlab.guiVista.gui.Atom
import io.gitlab.guiVista.gui.ImageBuffer
import io.gitlab.guiVista.gui.text.TextBuffer

/** Provides **Drag N Drop** selection data. */
public expect class DragDropSelectionData : Closable {
    /** The data type of the selection. */
    public val dataType: Atom

    /** The format of the selection. */
    public val format: Int

    /**
     * Retrieves the target of the selection.
     * @return The target of the selection.
     */
    public fun fetchTarget(): Atom

    /**
     * Gets the raw data of the selection.
     * @return The raw data.
     */
    public fun fetchData(): Array<UByte>

    /**
     * Gets the contents of the selection data as a [Atom].
     * @return The selection data.
     */
    public fun fetchDataAsAtom(): Atom

    /**
     * Gets the length of the raw data of the selection.
     * @return The length of the raw data.
     */
    public fun fetchDataLength(): Int

    /**
     * Gets the contents of [DragDropSelectionData] as an array of targets. This can be used to interpret the results
     * of getting the standard TARGETS target that is always supplied for any selection.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the selection data contains a valid array of targets.
     * 2. targets - Contains zero or more targets.
     */
    public fun fetchMultipleTargets(): Pair<Boolean, Array<Atom>>

    /**
     * Gets the contents of the selection data as a [ImageBuffer].
     * @return If the selection data contained a recognized image type and it could be converted to a GdkPixbuf then a
     * [ImageBuffer] object is returned, otherwise *null*. If the result isn't *null* then [close] **MUST** be called
     * after using the object.
     */
    public fun fetchDataAsImageBuffer(): ImageBuffer?

    /**
     * Gets the contents of the selection data as a UTF-8 string.
     * @return If the selection data contained a recognized text type, and it could be converted to UTF-8 then a non
     * empty string is returned, otherwise *""* (an empty [String]) is returned.
     */
    public fun fetchDataAsText(): String

    /**
     * Gets the contents of the selection data as array of URIs.
     * @return If the selection data contains a list of URIs then a non empty [String] array is returned, otherwise an
     * empty [String] array is returned.
     */
    public fun fetchDataAsUris(): Array<String>

    /**
     * Sets the contents of the selection from a [ImageBuffer]. The [imageBuffer] is converted to the form determined
     * by `selection_data->target`.
     * @param imageBuffer The image buffer to use.
     * @return A value of *true* if the selection was set successfully.
     */
    public fun changeDataAsImageBuffer(imageBuffer: ImageBuffer): Boolean

    /**
     * Sets the contents of the selection from a UTF-8 encoded string. The [string][str] is converted to the form
     * determined by `selection_data->target`.
     * @param str A UTF-8 [String].
     * @return A value of *true* if the selection was set successfully.
     */
    public fun changeDataAsString(str: String): Boolean

    /**
     * Sets the contents of the selection from a list of URIs. The [string][uris] is converted to the form determined
     * by `selection_data->target`.
     * @param uris One or more URIs to use for the selection.
     * @return A value of *true* if the selection was set successfully.
     */
    public fun changeDataAsUris(uris: Array<String>): Boolean

    /**
     * Given a [DragDropSelectionData] object holding a list of targets, determines if any of the targets in targets
     * can be used to provide a [ImageBuffer].
     * @param writable Whether to accept only targets for which can be converted in a [ImageBuffer].
     * @return A value of *true* if selection data holds a list of targets, and a suitable target for images is
     * included.
     */
    public fun targetsIncludeImage(writable: Boolean): Boolean

    /**
     * Given a [DragDropSelectionData] object holding a list of targets, determines if any of the targets in targets
     * can be used to provide rich text.
     * @param buffer The buffer to use.
     * @return A value of *true* if selection data holds a list of targets, and a suitable target for rich text is
     * included.
     */
    public fun targetsIncludeRichText(buffer: TextBuffer): Boolean

    /**
     * Given a [DragDropSelectionData] object holding a list of targets, determines if any of the targets in targets
     * can be used to provide text.
     * @return A value of *true* if selection data holds a list of targets, and a suitable target for text is included.
     */
    public fun targetsIncludeText(): Boolean

    /**
     * Given a [DragDropSelectionData] object holding a list of targets, determines if any of the targets in targets
     * can be used to provide a list or URIs.
     * @return A value of *true* if selection_data holds a list of targets, and a suitable target for URI lists is
     * included.
     */
    public fun targetsIncludeUri(): Boolean
}
