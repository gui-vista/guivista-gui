package io.gitlab.guiVista.gui.dragDrop

import io.gitlab.guiVista.core.Closable
import io.gitlab.guiVista.gui.Atom
import io.gitlab.guiVista.gui.text.TextBuffer

/** Is a list of `GtkTargetPair` and should be treated as opaque. */
public expect class DragDropTargetList : Closable {
    public companion object {
        /**
         * Creates a new [DragDropTargetList] from an array of [DragDropTargetEntry].
         * @param targets Zero or more targets.
         * @return An instance of [DragDropTargetList].
         */
        public fun create(vararg targets: DragDropTargetEntry): DragDropTargetList
    }

    /**
     * Appends another target to this [DragDropTargetList].
     * @param target The interned atom representing the target.
     * @param flags The flags for this target.
     * @param info An identifier that will be passed back to the application.
     */
    public fun add(target: Atom, flags: UInt, info: UInt)

    /**
     * Appends the image targets supported by [DragDropSelectionData] to the target list. All targets are added with
     * the same info.
     * @param info An identifier that will be passed back to the application.
     * @param writable Whether to add only targets for which GTK+ knows how to convert a
     * [io.gitlab.guiVista.gui.ImageBuffer] into the format.
     */
    public fun addImageTargets(info: UInt, writable: Boolean)

    /**
     * Appends the rich text targets registered with `gtk_text_buffer_register_serialize_format()` or
     * `gtk_text_buffer_register_deserialize_format()` to the target list. All targets are added with the same info.
     * @param info An identifier that will be passed back to the application.
     * @param deserializable If *true* then deserializable rich text formats will be added, serializable formats
     * otherwise.
     * @param buffer The text buffer to use.
     */
    public fun addRichTextTargets(info: UInt, deserializable: Boolean, buffer: TextBuffer)

    /**
     * Prepends a table of [DragDropTargetEntry] to a target list.
     * @param targets An array of targets.
     */
    public fun addTable(vararg targets: DragDropTargetEntry)

    /**
     * Appends the text targets supported by [DragDropSelectionData] to the target list. All targets are added with the
     * same info.
     * @param info An identifier that will be passed back to the application.
     */
    public fun addTextTargets(info: UInt)

    /**
     * Appends the URI targets supported by [DragDropSelectionData] to the target list. All targets are added with the
     * same info.
     * @param info An identifier that will be passed back to the application.
     */
    public fun addUriTargets(info: UInt)

    /**
     * Looks up a given target in this [DragDropTargetList].
     * @param target An interned atom representing the target to search for.
     * @return A Pair containing the following:
     * 1. found - A value of *true* if the [target] was found.
     * 2. info - The application info for the [target].
     */
    public fun find(target: Atom): Pair<Boolean, Int>

    /**
     * Removes a target from a target list.
     * @param target The interned atom representing the target.
     */
    public fun remove(target: Atom)
}
