package io.gitlab.guiVista.gui.dragDrop

import io.gitlab.guiVista.gui.ImageBuffer
import io.gitlab.guiVista.gui.widget.WidgetBase
import io.gitlab.guiVista.io.icon.IconBase

/**
 * Add the writable image targets supported by `GtkSelectionData` to the target list of the drag source. The targets
 * are added with info = 0. If you need another value then use [DragDropTargetList.addImageTargets], and set the
 * [dragTargets] property.
 */
public expect fun WidgetBase.addDragImageTargets()

/**
 * Add the text targets supported by `GtkSelectionData` to the target list of the drag source. The targets
 * are added with info = 0. If you need another value then use [DragDropTargetList.addTextTargets], and set the
 * [dragTargets] property.
 */
public expect fun WidgetBase.addDragTextTargets()

/**
 * Add the URI targets supported by `GtkSelectionData` to the target list of the drag source. The targets
 * are added with info = 0. If you need another value then use [DragDropTargetList.addUriTargets], and set the
 * [dragTargets] property.
 */
public expect fun WidgetBase.addDragUriTargets()

/**
 * The list of drag targets this widget can provide for **Drag N Drop**. Will be *null* if empty.
 */
public expect var WidgetBase.dragTargets: DragDropTargetList?

/**
 * Sets up a widget as a drag source so that GTK+ will start a drag operation when the user clicks and drags on the
 * widget. The widget **MUST** have a window.
 * @param startBtnMask The bitmask of buttons that can start the drag.
 * @param dragActions The bitmask of possible actions for a drag from this widget.
 * @param targets The table of targets that the drag will support, may be empty.
 */
public expect fun WidgetBase.setDragSource(startBtnMask: UInt, dragActions: UInt, vararg targets: DragDropTargetEntry)

/** Undoes the effects of [setDragSource]. */
public expect fun WidgetBase.unsetDragSource()

/**
 * Sets the icon that will be used for drags from a particular source to icon.
 * @param icon The icon to use.
 */
public expect fun WidgetBase.changeDragIcon(icon: IconBase)

/**
 * Sets the icon that will be used for drags from a particular source to a themed icon.
 * @param iconName The name of the icon to use.
 */
public expect fun WidgetBase.changeDragIconName(iconName: String)

/**
 * Sets the icon that will be used for drags from a particular widget from a [ImageBuffer]. A reference is retained for
 * [ImageBuffer], and will be released when it is no longer needed.
 * @param iconBuffer The [ImageBuffer] for the drag icon.
 */
public expect fun WidgetBase.changeDragIconBuffer(iconBuffer: ImageBuffer)
