package io.gitlab.guiVista.gui.dragDrop

public object DragDropEvent {
    public const val dragBegin: String = "drag-begin"
    public const val dragDataGet: String = "drag-data-get"
    public const val dragDataDelete: String = "drag-data-delete"
    public const val dragEnd: String = "drag-end"
    public const val dragMotion: String = "drag-motion"
    public const val dragDrop: String = "drag-drop"
    public const val dragDataReceived: String = "drag-data-received"
}
