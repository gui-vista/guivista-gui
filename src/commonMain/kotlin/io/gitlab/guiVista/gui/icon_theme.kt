package io.gitlab.guiVista.gui

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.core.dataType.DoublyLinkedList

/**
 * Provides a facility for looking up icons by name and size. The main reason for using a name rather than simply
 * providing a filename is to allow different icons to be used depending on what icon theme is selected by the user.
 */
public expect class IconTheme : ObjectBase {
    /**
     * The name of an icon that is representative of the current theme (eg, to use when presenting a list of
     * themes to the user). Close the object afterwards.
     */
    public val exampleIconName: String

    /**
     * The list of contexts available within the current hierarchy of icon themes.
     * @see listIcons
     */
    public val contextList: DoublyLinkedList

    public companion object {
        /**
         * Creates a new icon theme object. Icon theme objects are used to lookup up an icon by name in a particular
         * icon theme.
         * @return The new [IconTheme] object.
         */
        public fun create(): IconTheme

        /**
         * Gets the icon theme for the default screen.
         * @return A unique [IconTheme] associated with the default screen. This icon theme is associated with the
         * screen, and can be used as long as the screen is open. **DO NOT** attempt to close the object!
         */
        public fun fetchDefault(): IconTheme
    }

    /**
     * Returns an array of integers describing the sizes at which the icon is available without scaling. A size of
     * *-1* means that the icon is available in a scalable format.
     * @param iconName The name of the icon.
     * @return An array of available icon sizes.
     */
    public fun fetchIconSizes(iconName: String): Array<Int>

    /**
     * Gets the current search path.
     * @return The search path.
     * @see changeSearchPath
     */
    public fun fetchSearchPath(): Array<String>

    /**
     * Checks whether an icon theme includes an icon for a particular name.
     * @param iconName The name of an icon.
     * @return A value of *true* if the icon theme has an icon matching [iconName].
     */
    public fun hasIcon(iconName: String): Boolean

    /**
     * Lists the icons in the current icon theme. Only a subset of the icons can be listed by providing a context
     * string. The set of values for the context string is system dependent, but will typically include such values as
     * **Applications** and **MimeTypes**.
     * @param ctx A string identifying a particular type of icon, or *""* (an empty [String]) to list all icons.
     * @return A list of icons.
     */
    public fun listIcons(ctx: String = ""): DoublyLinkedList

    /**
     * Sets the name of the icon theme that the [IconTheme] object uses overriding system configuration.
     * @param themeName Name of icon theme to use instead of configured theme, or *""* (an empty [String]) to unset a
     * previously set custom theme.
     */
    public fun changeCustomTheme(themeName: String = "")

    /**
     * Sets the search path for the icon theme object. When looking for an icon theme GTK+ will search for a
     * subdirectory of one or more of the directories in path with the same name as the icon theme containing an
     * **index.theme** file. Themes from multiple of the path elements are combined to allow themes to be extended by
     * adding icons in the user’s home directory.
     *
     * In addition if an icon found isn’t found either in the current icon theme or the default icon theme, and an
     * image file with the right name is found directly in one of the elements of path, then that image will be used
     * for the icon name. This is legacy feature, and new icons should be put into the fallback icon theme, which is
     * called **hicolor** rather than directly on the icon path.
     * @param path One or more directories that are searched for icon themes.
     */
    public fun changeSearchPath(path: Array<String>)

    /**
     * Adds a resource path that will be looked at when looking for icons, similar to search paths. This function
     * should be used to make application specific icons available as part of the icon theme. The resources are
     * considered as part of the **hicolor** icon theme and **MUST** be located in subdirectories that are defined in
     * the **hicolor** icon theme, such as `path/16x16/actions/run.png`. Icons that are directly placed in the resource
     * path instead of a subdirectory are also considered as ultimate fallback.
     * @param path A resource path.
     */
    public fun addResourcePath(path: String)

    /**
     * Appends a directory to the search path.
     * @see changeSearchPath
     */
    public fun appendResourcePath(path: String)

    /**
     * Prepends a directory to the search path.
     * @param path Directory name to prepend to the icon path.
     */
    public fun prependSearchPath(path: String)

    /**
     * Checks to see if the icon theme has changed; if it has then any currently cached information is discarded and
     * will be reloaded next time the icon theme is accessed.
     * @return A value of *true* if the icon theme has changed, and needs to be reloaded.
     */
    public fun rescanIfNeeded(): Boolean

    /**
     * Looks up an icon in an icon theme, scales it to the given size and renders it into an [ImageBuffer]. This is a
     * convenience function. If more details about the icon are needed then use `lookupIcon` followed by
     * `gtk_icon_info_load_icon()`.
     *
     * Note that you probably want to listen for icon theme changes and update the icon. This is usually done by
     * connecting to the `GtkWidget::style-set` event. If for some reason you do not want to update the icon when the
     * icon theme changes, you should consider using `gdk_pixbuf_copy()` to make a private copy of the [ImageBuffer]
     * returned by this function. Otherwise GTK+ may need to keep the old icon theme loaded, which would be a waste of
     * memory.
     * @param iconName The name of the icon to lookup.
     * @param size The desired icon size. The resulting icon may not be exactly this size. See
     * `gtk_icon_info_load_icon()`.
     * @param flags Flags modifying the behavior of the icon lookup.
     * @return A Pair containing the following:
     * 1. imageBuffer - The rendered icon; this may be a newly created icon or a new reference to an internal icon, so
     * you must not modify the icon. Remember to close the object when finished with it. Will be *null* if the icon
     * isn't found.
     * 2. error - Contains error information, or is *null* if no error occurred.
     */
    public fun loadIcon(
        iconName: String,
        size: Int,
        flags: UInt
    ): Pair<ImageBuffer?, Error?>

    /**
     * Looks up an icon in an icon theme for a particular window scale, scales it to the given size and renders it
     * into an [ImageBuffer]. This is a convenience function. If more details about the icon are needed then use
     * `lookupIcon` followed by `gtk_icon_info_load_icon()`.
     *
     * Note that you probably want to listen for icon theme changes and update the icon. This is usually done by
     * connecting to the `GtkWidget::style-set` event. If for some reason you do not want to update the icon when the
     * icon theme changes, you should consider using `gdk_pixbuf_copy()` to make a private copy of the [ImageBuffer]
     * returned by this function. Otherwise GTK+ may need to keep the old icon theme loaded, which would be a waste of
     * memory.
     * @param iconName The name of the icon to lookup.
     * @param size The desired icon size. The resulting icon may not be exactly this size. See
     * `gtk_icon_info_load_icon()`.
     * @param scale Desired scale.
     * @param flags Flags modifying the behavior of the icon lookup.
     * @return A Pair containing the following:
     * 1. imageBuffer - The rendered icon; this may be a newly created icon or a new reference to an internal icon, so
     * you must not modify the icon. Remember to close the object when finished with it. Will be *null* if the icon
     * isn't found.
     * 2. error - Contains error information, or is *null* if no error occurred.
     */
    public fun loadIconForScale(
        iconName: String,
        size: Int,
        scale: Int,
        flags: UInt
    ): Pair<ImageBuffer?, Error?>
}
