package io.gitlab.guiVista.gui.dialog

import io.gitlab.guiVista.gui.window.WindowBase

/** A dialog for selecting fonts. */
public expect class FontChooserDialog : DialogBase, FontChooser {
    public companion object {
        public fun create(title: String = "", parent: WindowBase? = null): FontChooserDialog
    }

    override fun disconnectEvent(handlerId: ULong)
}
