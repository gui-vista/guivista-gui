package io.gitlab.guiVista.gui.dialog

import io.gitlab.guiVista.gui.window.WindowBase

/** A dialog for choosing colors. */
public expect class ColorChooserDialog : DialogBase, ColorChooser {
    public companion object {
        public fun create(title: String = "", parent: WindowBase? = null): ColorChooserDialog
    }

    override fun disconnectEvent(handlerId: ULong)

    override fun close()
}
