package io.gitlab.guiVista.gui.dialog

/** Default implementation of [DialogBase]. */
public expect class Dialog : DialogBase
