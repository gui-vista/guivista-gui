package io.gitlab.guiVista.gui.dialog

import io.gitlab.guiVista.core.Error
import io.gitlab.guiVista.core.dataType.SinglyLinkedList
import io.gitlab.guiVista.gui.FileFilter
import io.gitlab.guiVista.gui.widget.WidgetBase
import io.gitlab.guiVista.io.file.File
import io.gitlab.guiVista.io.file.FileBase

/** File chooser interface used by `GtkFileChooserWidget`, and `GtkFileChooserDialog`. */
public expect interface FileChooser {
    /** The current folder of chooser as a [File]. */
    public open val currentFolderFile: FileBase

    /**
     * The [File] that should be previewed in a custom preview Internal function. See [previewUri] property. Returns
     * *null* if no file is selected, or the [File] for the file to preview. Remember to call the **close** function
     * when finished with the [File] object.
     */
    public open val previewFile: FileBase?

    /** The current name in the file selector. */
    public open var currentName: String

    /** The filename to preview. Will return *""* if no file was selected. */
    public open val previewFileName: String

    /** The URI that should be previewed in a custom preview widget. See [previewWidget].  */
    public open val previewUri: String

    /**
     * Whether a file chooser that isn't in `GTK_FILE_CHOOSER_ACTION_OPEN` mode will offer the user the option to
     * create new folders. Default value is *true*.
     */
    public open var createFolders: Boolean

    /**
     * Whether a file chooser in `GTK_FILE_CHOOSER_ACTION_SAVE` mode will present an overwrite confirmation dialog if
     * the user selects a file name that already exists. Default value is *false*.
     *
     * Data binding property name: **do-overwrite-confirmation**
     */
    public open var doOverwriteConfirmation: Boolean

    /**
     * Application supplied widget for extra options.
     *
     * Data binding property name: **extra-widget**
     */
    public open var extraWidget: WidgetBase?

    /**
     * The current filter for selecting which files are displayed.
     *
     * Data binding property name: **filter**
     */
    public open var filter: FileFilter?

    /**
     * Whether the selected file(s) should be limited to local file: URLs. Default value is *true*.
     *
     * Data binding property name: **local-only**
     */
    public open var localOnly: Boolean

    /**
     * Application supplied widget for custom previews.
     *
     * Data binding property name: **preview-widget**
     */
    public open var previewWidget: WidgetBase?

    /** Whether the application supplied widget for custom previews should be shown. Default value is *true*. */
    public open var previewWidgetActive: Boolean

    /**
     * Whether to allow multiple files to be selected. Default value is *false*.
     *
     * Data binding property name: **select-multiple**
     */
    public open var selectMultiple: Boolean

    /**
     * Whether the hidden files and folders should be displayed. Default value is *false*.
     *
     * Data binding property name: **show-hidden**
     */
    public open var showHidden: Boolean

    /**
     * Whether to display a stock label with the name of the previewed file. Default value is *true*.
     *
     * Data binding property name: **use-preview-label**
     */
    public open var usePreviewLabel: Boolean

    /**
     * Gets the filename for the currently selected file in the file selector. The filename is returned as an absolute
     * path. If multiple files are selected, one of the filenames will be returned at random. If the file chooser is in
     * folder mode then this function returns the selected folder.
     * @return The currently selected filename, or *""* if no file is selected, or the selected file can't be
     * represented with a local filename.
     */
    public open fun fetchFileName(): String

    /**
     * Sets filename as the current filename for the file chooser, by changing to the file’s parent folder and actually
     * selecting the file in list; all other files will be unselected. If the chooser is in
     * `GTK_FILE_CHOOSER_ACTION_SAVE` mode then the file’s base name will also appear in the dialog’s file name entry.
     *
     * Note that the file **MUST** exist or nothing will be done except for the directory change. You should use this
     * function only when implementing a save dialog for which you already have a file name to which the user may save.
     * For example when the user opens an existing file, and then does Save As... to save a copy, or a modified version.
     * @param fileName The filename to set as current.
     * @return A value of *true* if the filename has been changed.
     */
    public open fun changeFileName(fileName: String): Boolean

    /**
     * Selects a filename. If the file name isn’t in the current folder of chooser, then the current folder of chooser
     * will be changed to the folder containing the filename.
     * @param fileName The filename to set.
     * @return A value of *true* if the filename has been selected.
     */
    public open fun selectFileName(fileName: String): Boolean

    /**
     * Unselects a currently selected filename. If the filename is not in the current directory, does not exist, or is
     * otherwise not currently selected, does nothing.
     * @param fileName The filename to unselect.
     */
    public open fun unselectFileName(fileName: String)

    /** Selects all the files in the current folder of a file chooser. */
    public open fun selectAll()

    /** Unselects all the files in the current folder of a file chooser. */
    public open fun unselectAll()

    /**
     * Lists all the selected files and sub folders in the current folder of the [FileChooser]. The returned names are
     * full absolute paths. If files in the current folder cannot be represented as local filenames they will be
     * ignored. (See gtk_file_chooser_get_uris())
     */
    public open fun fetchFileNames(): SinglyLinkedList

    /**
     * Sets the current folder for chooser from a local filename. The user will be shown the full contents of the
     * current folder, plus user interface elements for navigating to other folders.
     * @param fileName The full path of the new current folder.
     * @return A value of *true* if the current folder has been set.
     */
    public open fun changeCurrentFolder(fileName: String): Boolean

    /**
     * Gets the current folder of chooser as a local filename. Note that this is the folder that the file chooser is
     * currently displaying (e.g. "/home/username/Documents"), which is not the same as the currently selected folder
     * if the chooser is in `GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER` mode (e.g.
     * /home/username/Documents/selected-folder/". To get the currently selected folder in that mode, use
     * `gtk_file_chooser_get_uri()` as the usual way to get the selection.
     * @return The full path of the current folder, or *""* if the current path cannot be represented as a local
     * filename. This function will also return *""* if the file chooser was unable to load the last folder that was
     * requested from it; for example, as would be for calling [changeCurrentFolder] on a nonexistent folder.
     * @see changeCurrentFolder
     */
    public open fun fetchCurrentFolder(): String

    /**
     * Gets the URI for the currently selected file in the file selector. If multiple files are selected, one of the
     * filenames will be returned at random. If the file chooser is in folder mode then this function returns the
     * selected folder.
     * @return The currently selected URI, or *""* if no file is selected. If `gtk_file_chooser_set_local_only()` is
     * set to TRUE (the default) a local URI will be returned for any FUSE locations.
     */
    public open fun fetchUri(): String

    /**
     * Sets the file referred to by uri as the current file for the file chooser by changing to the URI’s parent
     * folder, and actually selecting the URI in the list. If the chooser is `GTK_FILE_CHOOSER_ACTION_SAVE` mode then
     * the URI’s base name will also appear in the dialog’s file name entry. Note that the URI **MUST** exist, or
     * nothing will be done except for the directory change.
     *
     * You should use this function only when implementing a save dialog for which you already have a file name to
     * which the user may save. For example when the user opens an existing file, and then does Save As... to save a
     * copy or a modified version.
     * @param uri The uri to set as current.
     * @return A value of *true* if the uri has been set.
     */
    public open fun changeUri(uri: String): Boolean

    /**
     * Selects the file to by uri. If the URI doesn’t refer to a file in the current folder of chooser, then the
     * current folder of chooser will be changed to the folder containing filename.
     * @param uri The uri to select.
     * @return A value of *true* if the uri has been selected.
     */
    public open fun selectUri(uri: String): Boolean

    /**
     * Unselects the file referred to by uri. If the file is not in the current directory, does not exist, or is
     * otherwise not currently selected, does nothing.
     * @param uri The uri to unselect.
     */
    public open fun unselectUri(uri: String)

    /**
     * Lists all the selected files and sub folders in the current folder of chooser. The returned names are full
     * absolute URIs.
     * @return A list containing the URIs of all selected files, and sub folders in the current folder. Remember to
     * [close the list][SinglyLinkedList.close] afterwards.
     */
    public open fun fetchMultipleUris(): SinglyLinkedList

    /**
     * Sets the current folder for chooser from an URI. The user will be shown the full contents of the current folder,
     * plus user interface elements for navigating to other folders.
     * @param uri The URI for the new current folder.
     * @return A value of *true* if the folder could be changed successfully.
     */
    public open fun changeCurrentFolderUri(uri: String): Boolean

    /**
     * Gets the current folder of chooser as an URI. Note that this is the folder that the file chooser is currently
     * displaying (e.g. "file:///home/username/Documents"), which is not the same as the currently-selected folder if
     * the chooser is in `GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER` mode (e.g.
     * "file:///home/username/Documents/selected-folder/". To get the currently selected folder in that mode, use
     * [fetchUri] as the usual way to get the selection.
     * @see changeCurrentFolderUri
     */
    public open fun fetchCurrentFolderUri(): String

    /**
     * Adds [filter] to the list of filters that the user can select between. When a filter is selected, only files that
     * are passed by that filter are displayed. Note that the chooser takes ownership of the filter, so you have to
     * ref and sink it if you want to keep a reference.
     * @param filter A file filter.
     */
    public open infix fun addFilter(filter: FileFilter)

    /**
     * Removes [filter] from the list of filters that the user can select between.
     * @param filter A file filter.
     */
    public open infix fun removeFilter(filter: FileFilter)

    /**
     * Lists the current set of user selectable filters.
     * @see addFilter
     * @see removeFilter
     * @return A list containing the current set of user selectable filters. The contents of the list are owned by
     * GTK+, but you must close the list itself with [SinglyLinkedList.close] when you are done with it.
     */
    public open fun listFilters(): SinglyLinkedList

    /**
     * Adds a folder to be displayed with the shortcut folders in a file chooser. Note that shortcut folders do not get
     * saved, as they are provided by the application. For example you can use this to add a
     * “/usr/share/mydrawprogram/Clipart” folder to the volume list.
     * @param folder The filename of the folder to add.
     * @param error The location to store error, or *null*.
     * @return A value of *true* if the folder could be added successfully, *false* otherwise. In the latter case the
     * error will be set as appropriate.
     */
    public open fun addShortcutFolder(folder: String, error: Error? = null): Boolean

    /**
     * Removes a folder from a file chooser’s list of shortcut folders.
     * @param folder The filename of the folder to remove.
     * @param error The location to store error, or *null*.
     * @return A value of *true* if the operation succeeds, *false* otherwise. In the latter case, the error will be
     * set as appropriate.
     * @see addShortcutFolder
     */
    public open fun removeShortcutFolder(folder: String, error: Error? = null): Boolean

    /**
     * Queries the list of shortcut folders in the file chooser as set by [addShortcutFolder].
     * @return A list of folder filenames, or *null* if there are no shortcut folders. Close the returned list with
     * [SinglyLinkedList.close].
     */
    public open fun listShortcutFolders(): SinglyLinkedList?

    /**
     * Adds a folder URI to be displayed with the shortcut folders in a file chooser. Note that shortcut folders do not
     * get saved, as they are provided by the application. For example you can use this to add a
     * “file:///usr/share/mydrawprogram/Clipart” folder to the volume list.
     * @param uri URI of the folder to add.
     * @param error The location to store error, or *null*.
     * @return A value of *true* if the folder could be added successfully, *false* otherwise. In the latter case the
     * error will be set as appropriate.
     */
    public open fun addShortcutFolderUri(uri: String, error: Error? = null): Boolean

    /**
     * Removes a folder URI from a file chooser’s list of shortcut folders.
     * @param uri URI of the folder to remove.
     * @param error The location to store error, or *null*.
     * @return A value of *true* if the operation succeeds, *false* otherwise. In the latter case the error will be set
     * as appropriate.
     * @see addShortcutFolderUri
     */
    public open fun removeShortcutFolderUri(uri: String, error: Error? = null): Boolean

    /**
     * Queries the list of shortcut folders in the file chooser, as set by [addShortcutFolderUri].
     * @return A list of folder URIs, or *null* if there are no shortcut folders. Close the returned list with
     * [SinglyLinkedList.close].
     */
    public open fun listAllShortcutFolderUris(): SinglyLinkedList?

    /**
     * Gets the [File] for the currently selected file in the file selector. If multiple files are selected, one of the
     * files will be returned at random. If the file chooser is in folder mode then this function returns the selected
     * folder.
     * @return A selected [File]. You own the returned file. Use the **close** function to close it.
     */
    public open fun fetchFile(): FileBase

    /**
     * Lists all the selected files and sub folders in the current folder of chooser as [File]. An internal function,
     * see `gtk_file_chooser_get_uris()`.
     */
    public open fun fetchAllFiles(): SinglyLinkedList

    /**
     * Selects the file referred to by file. An internal function.
     * @param file The file to select.
     * @param error The location to store error, or *null*.
     * @return A value of *true* if the [file] is selected.
     * @see selectUri
     */
    public open fun selectFile(file: FileBase, error: Error? = null): Boolean

    /**
     * Sets the current folder for chooser from a [file]. Internal function.
     * @param file The file for the new folder
     * @param error The location to store error, or *null*.
     * @return A value of *true* if the folder could be changed successfully, *false* otherwise.
     * @see changeCurrentFolderUri
     */
    public open fun changeCurrentFolderFile(file: FileBase, error: Error? = null): Boolean

    /**
     * Sets file as the current filename for the file chooser, by changing to the file’s parent folder and actually
     * selecting the file in the list. If the chooser is in `GTK_FILE_CHOOSER_ACTION_SAVE` mode then the file’s base
     * name will also appear in the dialog’s file name entry. If the file name isn’t in the current folder of chooser,
     * then the current folder of chooser will be changed to the folder containing filename. This is equivalent to a
     * sequence of [unselectAll] followed by [selectFileName].
     *
     * Note that the file **MUST** exist, or nothing will be done except for the directory change. If you are
     * implementing a save dialog, you should use this function if you already have a file name to which the user may
     * save; for example, when the user opens an existing file and then does *Save As...*.
     * @param file The file to set as current.
     * @param error The location to store the error, or *null* to ignore errors.
     * @return A value of *true* if the file has been set.
     */
    public open fun changeFile(file: FileBase, error: Error? = null): Boolean

    /**
     * Unselects the file referred to by file. If the file is not in the current directory, does not exist, or is
     * otherwise not currently selected, does nothing.
     */
    public open fun unselectFile(file: FileBase)
}
