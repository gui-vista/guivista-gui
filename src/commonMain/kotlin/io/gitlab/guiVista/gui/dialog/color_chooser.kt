package io.gitlab.guiVista.gui.dialog

import io.gitlab.guiVista.core.ObjectBase
import io.gitlab.guiVista.gui.RgbaColor

/** An interface implemented by widgets for choosing colors. */
public expect interface ColorChooser : ObjectBase {
    /**
     * This property contains the currently selected color. The property can be set to change the current selection
     * programmatically.
     *
     * Data binding property name: **rgba**
     */
    public open var rgba: RgbaColor

    /**
     * When this property is *true* the colors may have alpha (translucency) information. When it is *false* the
     * `GdkRGBA` struct obtained via the **rgba** property will be forced to have alpha == 1. Implementations are
     * expected to show alpha by rendering the color over a non-uniform background (like a checkerboard pattern).
     *
     * Data binding property name: **use-alpha**
     */
    public open var useAlpha: Boolean
}
