package io.gitlab.guiVista.gui.dialog

public object FontChooserEvent {
    public const val fontActivated: String = "font-activated"
}
