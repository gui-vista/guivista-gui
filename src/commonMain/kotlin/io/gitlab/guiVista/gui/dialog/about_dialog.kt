package io.gitlab.guiVista.gui.dialog

import io.gitlab.guiVista.gui.ImageBuffer
import io.gitlab.guiVista.gui.window.WindowBase

/** Display information about an application. */
public expect class AboutDialog : DialogBase {
    /**
     * The name of the program. If this is not set then it defaults to `g_get_application_name()`. Default value is
     * *""* (an empty String).
     *
     * Data binding property name: **program-name**
     */
    public var programName: String

    /**
     * The version of the program. Default value is *""* (an empty String).
     *
     * Data binding property name: **version**
     */
    public var version: String

    /**
     * Copyright information for the program. Default value is *""* (an empty String).
     *
     * Data binding property name: **copyright**
     */
    public var copyright: String

    /**
     * Comments about the program. This [String] is displayed in a label in the main dialog, thus it should be a short
     * explanation of the main purpose of the program, **NOT** a detailed list of features. Default value is *""* (an
     * empty String).
     *
     * Data binding property name: **comments**
     */
    public var comments: String

    /**
     * The license of the program. This [String] is displayed in a text view in a secondary dialog, therefore it is
     * fine to use a long multi-paragraph text. Note that the text is only wrapped in the text view if the
     * [wrapLicense] property is set to *true*, otherwise the text itself **MUST** contain the intended line breaks.
     * When setting this property to a non null value, the **licenseType** property is set to `GTK_LICENSE_CUSTOM` as a
     * side effect.
     *
     * Data binding property name: **license**
     */
    public var license: String

    /**
     * The URL for the link to the website of the program. This should be a [String] starting with "http://". Default
     * value is *""* (an empty String).
     *
     * Data binding property name: **website**
     */
    public var website: String

    /**
     * The label for the link to the website of the program. Default value is *""* (an empty String).
     *
     * Data binding property name: **website-label**
     */
    public var websiteLabel: String

    /**
     * The people who contributed artwork to the program, as an array of strings. Each [String] may contain email
     * addresses and URLs, which will be displayed as links, see the introduction for more details.
     *
     * Data binding property name: **artists**
     */
    public var artists: Array<String>

    /**
     * The authors of the program, as an array of strings. Each [String] may contain email addresses and URLs, which
     * will be displayed as links, see the introduction for more details.
     *
     * Data binding property name: **authors**
     */
    public var authors: Array<String>

    /**
     * Credits to the translators. This [String] should be marked as translatable. The string may contain email
     * addresses and URLs, which will be displayed as links, see the introduction for more details. Default value is
     * *""* (an empty String).
     *
     * Data binding property name: **translator-credits**
     */
    public var translatorCredits: String

    /**
     * A logo for the about box. If it is *null* then the default window icon set with `gtk_window_set_default_icon()`
     * will be used.
     *
     * Data binding property name: **logo**
     */
    public var logo: ImageBuffer?

    /**
     * A named icon to use as the logo for the about box. This property overrides the [logo] property. Default value is
     * *image-missing*.
     *
     * Data binding property name: **logo-icon-name**
     */
    public var logoIconName: String

    /**
     * Whether to wrap the text in the license dialog. Default value is *false*.
     *
     * Data binding property name: **wrap-license**
     */
    public var wrapLicense: Boolean

    /**
     * The people documenting the program, as an array of strings. Each [String] may contain email addresses and URLs,
     * which will be displayed as links, see the introduction for more details.
     *
     * Data binding property name: **documenters**
     */
    public var documenters: Array<String>

    public companion object {
        public fun create(): AboutDialog
    }

    /**
     * Creates a new section in the Credits page.
     * @param sectionName The name of the section.
     * @param people The people who belong to that section.
     */
    public fun addCreditSection(sectionName: String, vararg people: String)

    /**
     * This is a convenience function for showing an application’s about box. The constructed dialog is associated with
     * the parent window, and reused for future invocations of this function.
     * @param programName The version of the program.
     * @param parent Transient parent or *null* for none.
     */
    public fun show(programName: String = "", parent: WindowBase? = null)
}