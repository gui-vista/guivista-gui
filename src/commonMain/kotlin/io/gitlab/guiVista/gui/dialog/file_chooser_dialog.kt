package io.gitlab.guiVista.gui.dialog

/** A file chooser dialog that is suitable for **File/Open** or **File/Save** commands. */
public expect class FileChooserDialog : FileChooser, DialogBase
