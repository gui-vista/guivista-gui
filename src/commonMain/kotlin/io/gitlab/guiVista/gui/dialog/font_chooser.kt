package io.gitlab.guiVista.gui.dialog

import io.gitlab.guiVista.core.ObjectBase

/** A interface implemented by widgets displaying fonts. */
public expect interface FontChooser : ObjectBase {
    /**
     * The font description, e.g. **Sans Italic 12**. Default value is *Sans 10*.
     *
     * Data binding property name: **font**
     */
    public open var font: String

    /** The selected font size, or *-1* if no font size is selected. */
    public open val fontSize: Int

    /**
     * The String with which to preview the font. Default value is *The quick brown fox jumps over the lazy dog.*.
     *
     * Data binding property name: **preview-text**
     */
    public open var previewText: String

    /**
     * Whether to show an entry to change the preview text. Default value is *true*.
     *
     * Data binding property name: **show-preview-entry**
     */
    public open var showPreviewEntry: Boolean
}
