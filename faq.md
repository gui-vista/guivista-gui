# GUI Vista GUI Frequently Asked Questions

## Which Targets Are Supported?

Two Linux targets (**linuxX64**, and **linuxArm32Hfp**). There are plans to support the **linuxArm64** target in the
future. Other Unix based platforms (eg BSD) are being considered, however that is heavily dependent on Kotlin Native
support. Kotlin Native has severe portability issues that hinder its ability to support additional targets, and the
Kotlin team's focus with Kotlin Native is mainly focused on Apple platforms (the team has a **VERY** unhealthy
obsession), which is causing enormous support issues to the detriment of existing Kotlin Native targets that aren't
based on an Apple platform.

## Is GUI Vista GUI A GTK Language Binding?

No. The library was created from the ground up to be as **Kotlinic** (idiomatic Kotlin) as possible, and provide APIs in
a style that **Kotliners** are accustomed to. Think of the library as a custom API, which uses an existing library as a
base to provide essential functionality.

## Why Was GUI Vista GUI Created?

GUI Vista GUI was originally created to solve a problem where there was no easy way to create a **native** Linux desktop
application, using a general purpose statically typed programming language (not tied into a single platform
like [Vala](https://wiki.gnome.org/Projects/Vala)) that is balanced between developer productivity and performance.
Originally the library was just called GUI Vista Core however that made the library too difficult to maintain since
everything was in a single module.

## What Are The Library Dependencies?

The library depends on the following libraries:

- [GUI Vista IO](https://gitlab.com/gui-vista/guivista-io) - Provides Input/Output functionality (including file IO)
- [GUI Vista Core](https://gitlab.com/gui-vista/guivista-core) - Provides core functionality
- [GLib](https://en.wikipedia.org/wiki/GLib) - Core GObject functionality
- [GIO](https://en.wikipedia.org/wiki/GIO_(software)) - General Input/Output functionality (including file IO) that is
  GObject based
- [GTK](https://gtk.org/) - Cross platform GUI library that is GObject based
- [Kotlin Standard library](https://kotlinlang.org/api/latest/jvm/stdlib/)
- [KotlinX C Interop](https://kotlinlang.org/api/latest/jvm/stdlib/kotlinx.cinterop/) - Provides interoperability with C
  libraries for Kotlin Native programs/libraries

## Are There Any Similar Libraries

Yes, there is a single library called [gtk-kt](https://github.com/Doomsdayrs/gtk-kt) which covers similar functionality,
but is a third party GTK language binding (not officially supported by the
[Gnome Foundation](https://foundation.gnome.org/)) instead of being a library that provides custom APIs built from the
ground up.

## Which GUI Toolkit Is The Library Based On?

[GTK](https://gtk.org/) 3. In the future there are plans to use GTK 4 (most major Linux desktop distributions don't
support it yet) although that would require using custom versioning to avoid conflicts. It is important to note that GTK
3 will still be supported until the first stable version of GTK 5 is released (in approximately 10 years time). Most
major GTK based applications are currently using GTK 3 (at the time GTK 4 was released).

## Which Kotlin Development Platform Is Used?

[Kotlin Native](https://kotlinlang.org/docs/native-overview.html).

## Does The Library Provide Backwards Compatibility?

No. The library depends heavily on Kotlin Native which is currently under development, and doesn't provide a stable
platform/API surface yet. If you would like to see Kotlin Native provide backwards compatibility please raise the issue
with the Kotlin team.

## What Is The Recommended Deployment Method For Desktop Applications?

[FlatPak](https://flatpak.org/) which is supported by most of the major Linux desktop distributions (including
[Linux Mint](https://www.linuxmint.com/)). Note that FlatPak only works on Linux.
