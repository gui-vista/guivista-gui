# GUI Vista GUI (guivista-gui)

A Kotlin Native library for developing a GTK application in a Kotlin Native project. This library uses GTK 3
(requires GTK 3.20 or later), and has some APIs that depend on GDK (requires GDK 2.32 or later). GTK concepts (Slot,
Signal, Widget etc) are used in this library. Everything in this library uses GTK 3.20, and GDK 2.32 as a base for
backwards compatibility. **Warning** - This library depends on Kotlin Native (requires Kotlin 1.4.31) which is currently
in beta, and doesn't provide any backwards compatibility guarantees!


## Setup Gradle Build File

In order to use the library with Gradle (version 6.7.1 or higher) do the following:

1. Open/create a Kotlin Native project which targets **linuxX64** or **linuxArm32Hfp**
2. Open the project's **build.gradle.kts** file
3. Insert the following into the **repositories** block:
```kotlin
mavenCentral()
```
4. Create a library definition file called **glib2.def** which contains the following:
```
linkerOpts = -lglib-2.0 -lgobject-2.0
linkerOpts.linux_x64 = -L/usr/lib/x86_64-linux-gnu
linkerOpts.linux_arm32_hfp = -L/mnt/pi_image/usr/lib/arm-linux-gnueabihf
```
5. Create a library definition file called **gio2.def** which contains the following:
```
linkerOpts = -lgio-2.0
linkerOpts.linux_x64 = -L/usr/lib/x86_64-linux-gnu
linkerOpts.linux_arm32_hfp = -L/mnt/pi_image/usr/lib/arm-linux-gnueabihf
```
6. Create a library definition file called **gtk3.def** which contains the following:
```
linkerOpts = -lgtk-3 -lgdk-3 -latk-1.0 -lpangocairo-1.0 -lgdk_pixbuf-2.0 -lcairo-gobject -lpango-1.0 -lcairo
linkerOpts.linux_x64 = -L/usr/lib/x86_64-linux-gnu
linkerOpts.linux_arm32_hfp = -L/mnt/pi_image/usr/lib/arm-linux-gnueabihf
```
7. Add the following C library dependencies:
```kotlin
cinterops.create("glib2")
cinterops.create("gio2")
cinterops.create("gtk3")
```

8. Add the GUI Vista GUI library dependency: `implementation("io.gitlab.gui-vista:guivista-gui:$guiVistaVer")`

The build file should look similar to the following:
```kotlin
// ...
repositories {
    mavenCentral()
}

kotlin {
   // ...
   linuxX64 {
      // ...
      compilations.getByName("main") {
         cinterops.create("glib2")
         cinterops.create("gio2")
         cinterops.create("gtk3")
         dependencies {
            val guiVistaVer = "0.5.0"
            implementation("io.gitlab.gui-vista:guivista-gui:$guiVistaVer")
         }
      }
   }
}
```

## Basic Usage

A basic GTK program using this library requires using an Application object that has the Application ID, setting the
custom **activate** event handler for the `GuiApplication` class, assigning the default **activate** event handler
(calls the custom event handler) for the `GuiApplication` class, and creating the AppWindow UI in the event handler for
the **activate** event.

1. Create the **main.kt** file with a **main** function containing the following:

```kotlin
guiApplicationActivateHandler = ::activateApplication
```

2. Define the top level **mainWin** variable:

```kotlin
private lateinit var mainWin: AppWindow
```

3. Define the top level **activateApplication** function (handles creating the AppWindow UI):

```kotlin
private fun activateApplication(app: Application) {}
```

4. Print the Application ID, and create the AppWindow UI in the **activateApplication** function:

```kotlin
private fun activateApplication(app: Application) {
   println("Application ID: ${app.appId}")
   mainWin.createUi {
      title = "GUI App"
        visible = true
    }
}
```

5. Create an instance of GuiApplication with `GuiApplication.create` function and pass through the
   **org.example.basicgui** Application ID, and call the `use` function with a lambda (as the second line in the
   **main** function):
```kotlin
GuiApplication.create(id = "org.example.basicgui").use {}
```

6. In the lambda initialise **mainWin**, call the `assignDefaultActivateHandler` function, and run the application:

```kotlin
GuiApplication.create("org.example.basicgui").use {
   mainWin = AppWindow(this)
   assignDefaultActivateHandler()
   println("Application Status: ${run()}")
}
```
7. Insert the following imports:

- import io.gitlab.guiVista.gui.window.AppWindow
- import io.gitlab.guiVista.gui.GuiApplication
- import io.gitlab.guiVista.gui.guiApplicationActivateHandler
- import io.gitlab.guiVista.io.application.Application


After completing the steps above the **main.kt** file should look like the following:

```kotlin
import io.gitlab.guiVista.gui.window.AppWindow
import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.guiApplicationActivateHandler
import io.gitlab.guiVista.io.application.Application

private lateinit var mainWin: AppWindow

fun main() {
   guiApplicationActivateHandler = ::activateApplication
   GuiApplication.create(id = "org.example.basicgui").use {
      mainWin = AppWindow(this)
      assignDefaultActivateHandler()
      println("Application Status: ${run()}")
   }
}

private fun activateApplication(app: Application) {
   mainWin.createUi {
      title = "Basic GUI"
      visible = true
   }
}
```

## Custom Window

With GUI Vista GUI it is possible to develop a custom window. In order to do this a class needs to be defined that
either extends `io.gitlab.guiVista.gui.window.Window`, or implements `io.gitlab.guiVista.gui.window.AppWindow`. Both
Window, and AppWindow get their properties/functions from `io.gitlab.guiVista.gui.window.WindowBase`.

If there is a main layout used then the `createMainLayout` function should be overridden to create the layout. The UI
for the window should be created in the `createUi` function (override it). Default focus for the window is handled in
the `resetFocus` function (override it), where a widget in the window **"grabs"** focus. Below is an example of a custom
window:

```kotlin
// ...

internal class MainWindow(app: GuiApplication) : AppWindow(app) {
   private val nameEntry by lazy { createNameEntry() }
   private val greetingLbl by lazy { createGreetingLbl() }

   private fun createGreetingLbl() = labelWidget(text = "")

   override fun createMainLayout(): Container? = boxLayout(orientation = GtkOrientation.GTK_ORIENTATION_VERTICAL) {
      spacing = 20
      changeAllMargins(5)
      this += createInputLayout()
      this += greetingLbl
   }

    private fun createInputLayout() = boxLayout {
        spacing = 5
        prependChild(nameEntry)
        prependChild(createGreetingBtn())
    }

    private fun createNameEntry() = entryWidget {
        text = ""
        placeholderText = "Enter name"
    }

    private fun createGreetingBtn() = buttonWidget(label = "Display Greeting") {
        // Events are connected here...
    }

    fun updateGreeting() {
        greetingLbl.text = "Hello ${nameEntry.text}! :)"
        nameEntry.text = ""
    }
}

// Custom event handlers are defined here...
```

## Handling Events

With GTK signals (events) are handled by connecting slots (event handlers) to them. When a signal no longer needs to be
handled by a slot then the slot is disconnected (via the `disconnectEvent` function). Each slot needs to be defined as a
top level function, and cannot capture Kotlin class instances. Below is an example of a defined slot:

```kotlin
private fun greetingBtnClicked(@Suppress("UNUSED_PARAMETER") btn: CPointer<GtkButton>?, userData: gpointer) {
    val mainWin = userData.asStableRef<MainWindow>().get()
    mainWin.updateGreeting()
}
```

Note that a stable reference is used, which comes from the **userData** parameter. A stable reference allows a Kotlin
class instance to be used without capturing it in the slot. Below is an example of connecting a slot to a signal:

```kotlin
connectClickedEvent(staticCFunction(::greetingBtnClicked), stableRef.asCPointer())
```

A slot has to be converted to a `CFunction` first (via the `staticCFunction` function), **before** it can be used as a 
function reference. If a Kotlin class instance is to be accessible in the slot then it needs to be passed as a 
`CPointer` (eg `stableRef.asCPointer()`). A stable reference is defined in a Kotlin class definition, eg:

```kotlin
class MainWindow(app: Application) : AppWindow(app) {
    val stableRef = StableRef.create(this)
    // ...
}
```

Alternatively if the Kotlin class instance has state/functionality that is accessible via a `CPointer` then the Kotlin
class creation technique can be used in a slot, eg:

```kotlin
private fun activateApplication(app: CPointer<GApplication>, @Suppress("UNUSED_PARAMETER") userData: gpointer) {
   println("Activating application...")
   println("Application ID: ${Application(appPtr = app).appId}")
   // ...
}
```

## Displaying A List

A list is displayed by using a TreeView combined with a ListStore (is a model that maintains a list of items). Each
TreeView contains one or more columns which are represented by TreeViewColumn. There are various top level functions
available for quickly creating columns with a preset cell renderer (determines how a column is displayed). Below are the
functions that are available for creating columns:

- treeViewTextColumn
- treeViewToggleColumn
- treeViewSpinnerColumn
- treeViewProgressColumn
- treeViewColumn (used for creating a custom column)

Just before a TreeView can be created a ListStore **MUST** be created. Below is an example of creating a ListStore that
has two columns of type String (G_TYPE_STRING), and Int (G_TYPE_BOOLEAN - is an Int that represents a Boolean):

```kotlin
// ...
private val listStore by lazy { ListStore.create(G_TYPE_STRING, G_TYPE_BOOLEAN) }
```

Below is an example of creating a column:

```kotlin
// ...
private val taskCol by lazy {
   treeViewTextColumn(columnId = 0, enableSorting = true) {
      title = "Task"
      sortIndicator = true
   }
}
```

After the columns are created a TreeView is then created which contains the store, and the columns, eg:

```kotlin
// ...
private val listView by lazy {
   treeViewWidget(model = listStore) {
      selection.mode = GtkSelectionMode.GTK_SELECTION_SINGLE
      appendColumn(taskCol)
      appendColumn(completedCol)
   }
}
```

## Concurrency

GUI Vista follows the concurrency model that GTK uses where the main thread is the UI thread, which means all GUI
related tasks are performed on that thread. Any background tasks that need to be performed **MUST** run on a separate
thread otherwise the UI thread could be blocked, which is likely to **freeze** the application and prevent the user from
doing other things with the application resulting in a bad user experience. With event handlers it is always a good idea
to offload background work to a separate thread, and do as little work as possible in the handler itself since the
handler is run on the UI thread.

To run a background task on a separate thread use the `runOnBackgroundThread` function, eg:

```kotlin
fun showOpenDialog(parent: WindowBase) {
   // ...
   runOnBackgroundThread(staticCFunction(::runOpenFileTask))
   // ...
}

private fun runOpenFileTask(@Suppress("UNUSED_PARAMETER") userData: COpaquePointer?): COpaquePointer? {
   initRuntimeIfNeeded()
   // Do background task here.
   return null
}
```

On a background thread GUI tasks can be run on the UI thread with the `runOnUiThread` function, eg:

```kotlin
private fun runOpenFileTask(@Suppress("UNUSED_PARAMETER") userData: COpaquePointer?): COpaquePointer? {
   initRuntimeIfNeeded()
   // ...
   runOnUiThread(staticCFunction { _: COpaquePointer? ->
      // Do GUI tasks here.
      FALSE
   })
   return null
}
```
